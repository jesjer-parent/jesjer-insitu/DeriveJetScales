have 3 directories corresponding to 3 different JMS results :

```
jms_EtLogMoEtBins_ni_mcW/
jms_EtLogMoEtBins_ni_xsecWnewSmooth/
jms_EtLogMoEtBins_ni_xsecWoldSmooth/
```

prepare a `plots.py` script containing :

```
from DeriveJetScales.JMSComparator import Comparator, comp, newObjs, updateStyle

def newObjs_EtlogEtBins_ni( suffix="" , LineColor=ROOT.kRed , variant=""):
    o= updateStyle(newObjs( "jms_EtLogMoEtBins_ni"+suffix+"/AntiKt10LCTopoTrimmedPtFrac5SmallR20Jets_JMS.root",binSystem="EtLogMoEtBins",
                             useNoMNumInv = False,),
                   LineColor = LineColor, Title = "(Et,log(m/Et)) "+variant )
    o.tag = "(Et,log(m/Et)) "+variant
    return o

comp.setObjsList( [
    newObjs_EtlogEtBins_ni("_mcW", ROOT.kBlack, "MC weights"),
    newObjs_EtlogEtBins_ni("_xsecWoldSmooth", ROOT.kRed, "Xsec weights"),
    newObjs_EtlogEtBins_ni("_xsecWnewSmooth", ROOT.kBlue, "Xsec weights, fixed smoothing"),    
    ])
```


start an interactive debug session, for example (to be adapted) :
`pythonh deriveJES.py AntiKt10LCTopoTrimmedPtFrac5SmallR20 --inputPattern  'JZ*root' -c AntiKt10LCTopoTrimmedPtFrac5SmallR20_etlogmet.py -t AntiKt10LCTopoTrimmedPtFrac5SmallR20Jets -s Debug`

in the session run :

```
# exec the script : this prepares a comparator and load 3 JMS result to be compared
execfile('plots.py')

# plots comparing mass Response in mass windows :
comp.drawLargeMassBins("mR",suffix='pdf',) 

# plots comparing calibrated response distribs in each mass window :
comp.drawAllResp(suffix="allRespMassWindComp.pdf",respContainer="closure_mR_in_inclPtEtatrue_py", normalize=True, drawUncalib=False)


```

To do plot comparing only the uncalibrated responses of the 2 first results (in this case :
using mc weights only vs xsection weights), comment the last "newObjs_EtlogEtBins_ni" above and re-run the session:

```
execfile('plots.py')
comp.drawAllResp(suffix="allRawRespComp.pdf",respContainer="mR_in_PtEtaMtrue_py", normalize=True, drawUncalib=False)

```

