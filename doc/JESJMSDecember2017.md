This page serves as a reference for the JES and JMS derivation
performed in December 2017.


AntiKt10LCTopoTrimmedPtFrac5SmallR20Jets
========================================

The tag/branch used for this derivation is :

The config file used is script/AntiKt10LCTopoTrimmedPtFrac5SmallR20Jets.py

Links to presentation
-------------------


JES derivation
--------------
The JES derivation is a re-implementation of the previous code in the
`DeriveJES` package. 
Parameters are identical. In particular :
```python
# gaussian fit :
balanceFitter.SetFitOpt("REQS")

# response vs E fit :
eRespFitter.m_JES_Function = "polBestChi2+groom"
```


JMS Derivation
---------------


Main parameters :
```python
binSystem = "ELogMoEBins"

eBins = tripleBins(seq(200,1000,200)+seq(1200,4000,400)+[6000])
# that is :
eBins = [200, 266.6, 333.2, 400, 466.6, 533.2, 600,
666.6, 733.2, 800, 866.6, 933.2, 1000, 1066.6, 1133.2, 1200, 1333.2,
1466.4, 1600, 1733.2, 1866.4, 2000, 2133.2, 2266.4, 2400, 2533.2,
2666.4, 2800, 2933.2, 3066.4, 3200, 3333.2, 3466.4, 3600, 3733.2,
3866.4, 4000, 4666.0, 5332.0, 6000]

logMoEBins = tripleBins([-6,-5, -4.5, -4.25, -4, -3.75, -3.5, -3.25,
-3,-2.75, -2.5, -2.25, -2,-1.75,-1.5, -1.25,0])
# that is :
logMoEBins = [-6, -5.667, -5.334, -5, -4.8335, -4.667, -4.5, -4.41675, -4.3335,
-4.25, -4.16675, -4.0835, -4, -3.91675, -3.8335, -3.75, -3.66675,
-3.5835, -3.5, -3.41675, -3.3335, -3.25, -3.16675, -3.0835, -3,
-2.91675, -2.8335, -2.75, -2.66675, -2.5835, -2.5, -2.41675, -2.3335,
-2.25, -2.16675, -2.0835, -2, -1.91675, -1.8335, -1.75, -1.66675,
-1.5835, -1.5, -1.41675, -1.3335, -1.25, -0.83375, -0.4175, 0]

etaBins = [0,0.2,0.4,0.8, 1.0 , 1.2, 1.3, 1.4,1.5, 1.6 ,1.7 , 2., 2.25, 2.5, 2.75, 3 ]

# Gaussian fit option :
balanceFitter_jms.SetFitOpt("RQE0S")

```
