Controling which calibration factors is applied during closure tests
===========================================

By default the package will do the closure tests with the calibration
    factors existing in the JMS output file (`der.fOut_jms`). 
It is possible to re-run the closure tests using a different set of
    calibration factors, thus allowing to compare the closure of
    different calibration on the same samples.
    

First we assume we have run all the `EtaJES` and `JMS` derivation steps as
well as the `Plots.jms`. The run directory thus contains an output JMS
directory, typically `jms_ELogMoEBins/` .

Let's say we now want to test the closure with a set of calib
constants labelled `ALT` .
Copy the existing output directory :
``` 
cp -R jms_ELogMoEBins/ jms_ELogMoEBins_ALT/
``` 

then create a alternate derivation script containing :

```python

# execute the regular script 
execfile("AntiKt10LCTopoTrimmedPtFrac5SmallR20_finebins.py")

# move the configure function
configure_old = configure

# we can then redefine a new one which will call the regular one
# and change a few parameters including the calibration factors set.
def configure():
    # call the regular configure to obtain the CalibrationObjects :
    objs = configure_old()
    
    # below we'll read the TH3F containing alternative calib constant 
    
    # name of file containing the TH3F (typically from a
    # "*_calibFactors.root" file which is produced by running the -s Plots.jms option)
    alternName = "../somewhere_ALT/jms_ELogMoEBins/AntiKt10LCTopoTrimmedPtFrac5SmallR20_JMSvfbmCalo_calibFactors.root"
    alternTag = "ALT" 

    objs.alternFile = ROOT.TFile(alternName)
    objs.alternTH3 = objs.alternFile.Get("mRsmooth_3D")
    # assign the TH3 to our ResponseBuilder objects (thus the system will not pick up the default one)
    objs.respBuilder.m_jmsRecoBin.m_correctionFactors3d = objs.alternTH3
    
    # change the output dir to match the one we pre-copied
    objs.jmsWorkDir = objs.jmsWorkDir[:-1]+"_"+alternTag+"/"
    
    return objs

``` 

We then use the alternate scripts instead of the regular one and
execute the closure step :

``` 
python deriveJES.py AntiKt10LCTopoTrimmedPtFrac5SmallR20 --inputPattern  'MC16*_JZ0_12W.root' -c ALTernatescript.py -t AntiKt10LCTopoTrimmedPtFrac5SmallR20Jets -s JMS.closureresp+JMS.closurefits
``` 

The closure results will then be available from the
`jms_ELogMoEBins_ALT/`. We can then run the `-s Plots.jms` option with
the same alternate script to obtain the jms plots.


Comparing closure results on the same plots
======================================
The above procedure allows to create several JMS output files
containing each their set of closure  histos/graphs.
It is then possible to compare them on a same plot.

We write a helper script using the utilities in
`python/JMSCompator.py` :

```python
# import the JMS comparison utilities.
from DeriveJetScales.JMSComparator import Comparator, comp , newObjs, updateStyle


# ***************
# comp is the main comparator object.
# tell it what to compare :
comp.setObjsList(
    [
     # first the default CalibrationObjects instance (updateStyle()
     #   just returns its 1st argument)
        updateStyle(objs,LineColor=ROOT.kRed,Title="nominal"),
     # then we add 2 other instances, reading data from other JMS
     #   output files
        updateStyle(newObjs("jms_ELogMoEBins_ALT1/AntiKt10LCTopoTrimmedPtFrac5SmallR20_JMSvfbmCalo.root",binSystem="ELogMoEBins", tag='ALT1' ), LineColor=ROOT.kBlue, Title="option 1"),
        updateStyle(newObjs("jms_ELogMoEBins_ALT2/AntiKt10LCTopoTrimmedPtFrac5SmallR20_JMSvfbmCalo.root",binSystem="ELogMoEBins", tag='ALT2' ), LineColor=ROOT.kGreen, Title="option 2"),

    ]  )
objs.tag=Nominal'

```

Then we start an interactive session with option `-s Debug`. At the
interactive prompt :
```
# execute the above script
execfile('plots.py')
# compare the JMS factors vs E at different (M,eta) values : 
r=comp.multiCompareJMSCorr('E', ('M','Eta'), [ (50,0.2), (80,0.2) , (150,0.2) , (50,1.), (80,1.) , (150,1.)  ] )

# compare the closure in mass windows 
comp.drawLargeMassBins("mR",suffix='pdf', refHistoStyle=dict(Title="uncalibrated", desc_y="m_{calo} response") )
comp.drawLargeMassBins("mIQR",suffix='pdf' )

``` 
