This files lists the history of released recommendations.

The goal is to keep a list of links to calibration constants,
validation plots, CVMFS or git tag relevant to each released
calibration.



October/November 2018, MC16d
=========================

JES and JMS recommandations released for 
  * AntiKt10LCTopoTrimmedPtFrac5SmallR20Jets for mCalo and mTA
  * AntiKt10LCTopoCSSKSoftDropBeta100Zcut10 for mCalo and mTA
  * AntiKt10TrackCaloClusterTrimmedPtFrac5SmallR20 (no mTA for TCC
    jets)

Calibrations were release using only MC16d samples

git tag  recomm2018_11 
-------

Links
-----
EOS space 
```
/eos/atlas/atlascerngroupdisk/perf-jets/JSS/Rel21/MC16d-CalibClosure-JESJMS/JMS-calo
/eos/atlas/atlascerngroupdisk/perf-jets/JSS/Rel21/MC16d-CalibClosure-JESJMS/JMS-TA
/eos/atlas/atlascerngroupdisk/perf-jets/JSS/Rel21/MC16d-CalibClosure-JESJMS/JMS-TCC
```

Scripts and options
--------------
The very fine bin options was used to derive the JMS calibration
using the script : `scripts/AntiKt10LCTopoTrimmedPtFrac5SmallR20_finebins.py`. 
To reproduce, adapt this script to the input files and making sure
it's options are similar than those found in the `*ConfigDump.txt`
from the above eos directories.

