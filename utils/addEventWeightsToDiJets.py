import ROOT
import os.path

## ########################################
## This script allows to update a ntuple, adding a new variable per tree contained in the ntuple.
## the new variable is  the "full" mc event weight to be applied to Pythia Dijet events.
## The weight depends on the mc generator weights (which is expected to exist in the ntuple as a float)
## and the xsection and filter efficiency per sample (see table below).
##  
##
## Usage :
## edit the templateFileName variable below so it matches your dijet ntuple name
## run an interactive python session
## call reweightAllSamples() or reweightSample("JZ3") ...

########################################

# template name for input ntuples. {sample} will be replaced by  "JZX"
templateFileName = "{sample}.root"

# the xsection and efficiencies for each samples
xsectionMap= {
    # sample : (xsection, gen_eff) 
    "JZ0" : (7.8420E+07 , 9.7193E-01), 
    "JZ1" : (7.8420E+07 , 6.7198E-04), 
    "JZ2" : (2.4334E+06 , 3.3264E-04), 
    "JZ3" : (2.6454E+04 , 3.1953E-04), 
    "JZ4" : (2.5464E+02 , 5.3009E-04), 
    "JZ5" : (4.5536E+00 , 9.2325E-04), 
    "JZ6" : (2.5752E-01 , 9.4016E-04), 
    "JZ7" : (1.6214E-02 , 3.9282E-04), 
    "JZ8" : (6.2505E-04 , 1.0162E-02), 
    "JZ9" : (1.9640E-05 , 1.2054E-02), 
    "JZ10" : (1.1961E-06 , 5.8935E-03), 
    "JZ11" : (4.2260E-08 , 2.7015E-03), 
    "JZ12" : (1.0370E-09 , 4.2502E-04), 
    }



# define and compile a C++ function so the reweighting is fast.
ROOT.gInterpreter.LoadText("""
void addNewWeight(TTree* t, double reweight){

  //double EvweightLC;   double new_weightLC;
  float EvweightLC;   float new_weightLC;


  //  TBranch* newBranchLC = t->Branch("new_EventWeight",&new_weightLC, "new_weightLC/D");
    TBranch* newBranchLC = t->Branch("new_EventWeight",&new_weightLC, "new_weightLC/F");

  t->SetBranchAddress("EventWeight",&EvweightLC);

  int entriesLC = t->GetEntries();
  //entriesLC = 30;
  for (int i=0; i<entriesLC; i++) {
    t->GetEntry(i);
    new_weightLC = EvweightLC*reweight;
    //std::cout << EvweightLC << "  " << reweight<< "  "<< new_weightLC << std::endl;
    newBranchLC->Fill();
    if(i%1000000==0) std::cout << i << std::endl;
  }
  t->Write("",TObject::kOverwrite);
}
""")




def reweightSample( sample ):
    """Add event weight in all TTrees found in ntuples corresponding to sample """
    
    inputName = templateFileName.format( sample = sample )

    if not os.path.exists( inputName ):        
        print "\nWARNING : input file ", inputName, " does not exist\n"
        return
    
    print "Adding weight to ", inputName

    f1 = ROOT.TFile(inputName,"update")    
    f1.cd()
    
    h_events = f1.Get("h_events");
    evn = h_events.GetEntries();

    xs , eff = xsectionMap[sample]

    print "xsection = ",xs 
    print "eff = ",eff 
  
    reweight = xs*eff/evn;

    trees = ["AntiKt4LCTopoJets", "AntiKt4EMTopoJets", "AntiKt10LCTopoTrimmedPtFrac5SmallR20Jets", "AntiKt10TrackCaloClusterTrimmedPtFrac5SmallR20Jets", "AntiKt10LCTopoCSSKSoftDropBeta100Zcut10Jets"]
    for tname in trees:
        t = f1.Get(tname)
        if(t) :
            # call the C++ function defined above
            ROOT.addNewWeight(t, reweight)
            print "added weight to ", tname, " in ", inputName
        
    
    
def reweightAllSamples():
    for sample in xsectionMap.keys():
        reweightSample( sample )
