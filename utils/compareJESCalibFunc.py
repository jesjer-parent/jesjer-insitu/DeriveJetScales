## Utility to compare 2 sets of EtaJES calibrations.
##
## Usage : start in 'Debug' mode then execute :
## execfile("path/to/DeriveJetScales/utils/compareJESCalibFunc.py")
## compareEtaJESFactors( [ ("ConfigFile1.config", "title1") , ("ConfigFile2.config", "title2" ) ] )
## See below for the possible options


def ratioTF1(f1,f2):
    funcDef='TF1 {f}("{f}","[](double *x, double *p){{ return {f1}->Eval(x[0]) / {f2}->Eval(x[0]); }}",{min},{max},{np})';
    name = 'Ratio'+f1.GetName()+f2.GetName()
    ROOT.gInterpreter.ProcessLine( funcDef.format(
        f = name,
        f1 = f1.GetName(),
        f2 = f2.GetName(),
        min=f1.GetXmin(),
        max=f1.GetXmax(),
        np = f1.GetNpar()+f2.GetNpar()
    ) )
    return getattr(ROOT, name)


def compareEtaJESFactors( fileSpecs, yrange=None, erange=(150, 5000), legPos=(0.6,0.2,0.8,0.4), ptLine=150, outName="compareTF1.pdf", drawRatio=False):
    """Draw multiple JES and Eta calibration factors on same plot, bin by bin.
The factors to be drawn are read from calibration config file as produced by the EtaJES derivation, or from the global 'objs' object.
fileSpecs is in the form [ ("filename1.config", "title1"), ... ] 
    """

    jesTF1s = [ [] for i in fileSpecs ]
    etaTF1s = [ [] for i in fileSpecs ]
    colors = [1, 2, 3, 4, 5, 6, 7, 8, 9, 11, 12, 28, 32, 38, 41, 42, 45, 46, 49]

    # read inputs into jesTF1s and etaTF1s -----------------------
    for fi, (fname, title) in enumerate(fileSpecs):
        if isinstance(fname,str):
            jes1 , eta1 = JESPlots.configFileToTF1(fname, 'f'+str(fi), erange)
        else:
            objs.readEtaJESFromFile(der.fOut_jes, [  "DEta_vs_Etrue_Func" , "R_vs_Enuminv_Func"])
            jes1 , eta1 = objs.R_vs_Enuminv_Func , objs.DEta_vs_Etrue_Func
        for i, fjes in enumerate(jes1):
            feta = eta1[i]
            fjes.SetName("JES"+str(i)+str(fi))
            feta.SetName("EtaCorr"+str(i)+str(fi))
            fjes.SetTitle(title+' JES')
            feta.SetTitle(title+' EtaCorr')
            fjes.SetLineColor(colors[fi])
            feta.SetLineColor(colors[fi])
            fjes.SetRange(*erange)
            feta.SetRange(*erange)
            jesTF1s[fi].append(fjes)
            etaTF1s[fi].append(feta)

    # prepare output 
    from DeriveJetScales.DrawUtils import buildPage, PdfSaver
    page = buildPage(PdfSaver, 2, outName)
    p = page.currentPad()

    objs.etaBins.m_isGeV  = False

    if yrange is None:
        yrange = (0.0,1.05)
        
    # a vertical line placed at energy corresponding to ptLine
    line = ROOT.TLine()
    line.SetLineStyle(2)
    line.SetLineColor(ROOT.kBlue)

    # define helper functions
    def drawVertLine(yrange, p):
        # draw the vertical line 
        eLine = ptLine*ROOT.TMath.CosH(objs.etaBins.binLowEdge(i))        
        mmaxNDC = (ROOT.TMath.Log10(eLine)-p.GetX1())/(p.GetX2()-p.GetX1()) +0.01
        #print p,"   ",mmaxNDC, "__", eLine, p.GetX2()
        line.DrawLine( eLine, yrange[0], eLine, yrange[1] )
        ROOT.myText(mmaxNDC, 0.85, ROOT.kBlue, "p_{T}<%dGeV"%(ptLine,))

    def drawGraph(funcs, p, yrange):
        funcs[0].Draw()
        funcs[0].SetMinimum(yrange[0])
        funcs[0].SetMaximum(yrange[1])
        for f in funcs[1:]:
            f.Draw("same")
        p.SetGridy()
        p.SetLogx(True)
        p.BuildLegend(0.6,0.6,0.8,0.8, "Eta Bin "+str(i)+" "+objs.etaBins.describeBin(i))
        p.Update()
        drawVertLine(yrange,p)

    def dividePad(p):
        #print p, p.GetListOfPrimitives().GetSize()
        if p.GetListOfPrimitives().GetSize()>0:
            return p.GetPad(1), p.GetPad(2)
        p.Divide(1,2)
        p1 = p.GetPad(1)
        p1.SetPad(0.,0.3,1,1)
        p2 = p.GetPad(2)
        p2.SetPad(0.,0.0,1,0.3)
        p1.SetBottomMargin(0)
        p2.SetTopMargin(0)
        #print 'divided pad'
        return p1, p2

    ratiolines = [ROOT.TLine(150,1.01,5000,1.01), ROOT.TLine(150,1.0,5000,1.0), ROOT.TLine(150,0.99,5000,0.99) ]
    for l in (ratiolines[0],ratiolines[2]):              
        l.SetLineStyle(2)
        l.SetLineColor(ROOT.kBlue)
    
    def drawWithRatio(funcs,p,yrange):
        p1,p2 = dividePad(p)
        p1=p.cd(1)
        drawGraph(funcs,p1,yrange)
        p2=p.cd(2)
        p2.SetLogx(True)
        fr = ratioTF1(funcs[0],funcs[1])        
        fr.Draw()
        #fr.GetYaxis().SetRangeUser(0.95, 1.05)
        fr.GetYaxis().SetLabelSize(0.1)
        fr.GetXaxis().SetLabelSize(0.12)
        fr.GetXaxis().SetTitle("E [GeV]")
        fr.SetMaximum(1.05)
        fr.SetMinimum(0.95)
        for l in ratiolines: l.Draw("same")
        #p.Update()
        return p1,p2,fr

    
    # JES calib factor plots 
    drawFunc = drawWithRatio if drawRatio else drawGraph

    for i, funcs in enumerate(zip(*jesTF1s) ):
        drawFunc(funcs,p , yrange)
        p=page.nextPad()    

    page.forceSave()

    # Eta calib factor plots 
    for i, funcs in enumerate(zip(*etaTF1s)):
        drawGraph(funcs, p , (-0.05, 0.05))
        p=page.nextPad()    

    page.forceSave()    
    page.close()
        
        
