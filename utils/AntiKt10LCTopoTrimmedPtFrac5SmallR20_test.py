# This file demonstrates how to hack the usual JES/JMS derivation configuration
# in order to produce additionnal new histograms for testing/validation/debugging purposes.
# This method has the advantage of leaving untouched the standard derivation code, just producing
# additionnal info.

# first we define a new C++ class inheriting DJS::ResponseBuilder.
# This class only redefine the validation(int) function.
# We directly load it and JIT compile it in  ROOT :
ROOT.gInterpreter.LoadText("""
class MyRespBuilder : public DJS::ResponseBuilder {
public:
    virtual bool validation(int i) {

    double eta_reco = (*m_eta_reco)[i];

    int ieta_jes = m_etaBins->findBin( eta_reco );
    if(!m_etaBins->validBin(ieta_jes)) return true; // out of range

    double e_true = (*m_e_true)[i] * m_energyUnit;
    double e_reco = (*m_e_reco)[i] * m_energyUnit;
    double m_true = (*m_m_true)[i] * m_energyUnit;
    double m_reco = (*m_m_reco)[i] * m_energyUnit;
    if(m_reco==0.) {return true;}

    double jesFactor = getCorrFactor( e_reco, eta_reco, ieta_jes);
    double e_recoCorr = e_reco * jesFactor;

    double eta_Corr = eta_reco - getEtaCorr(ieta_jes, e_recoCorr);
    double m_recoCorr = m_reco * jesFactor;

    double m_var = jms_mVar_reco(m_recoCorr, e_recoCorr);
    double pt_var = jms_ptVar_reco(e_recoCorr, 0);

    double m_var_true = jms_mVar_true(m_true, e_true);
    //if(std::isnan(m_var_true)) std::cout<< " !! nan m_var_true "<< m_true << " / "<< pt_true << std::endl;
    double pt_var_true = jms_ptVar_true(e_true, 0);
    
    int bin_true=m_jmsBPS->findBin(pt_var_true, m_var_true, std::fabs(eta_Corr) );

    int bin =m_jmsBPS->findBin(pt_var, m_var, std::fabs(eta_Corr) );
    if(bin == -1 ) return true;

    //double eta_true = (*m_eta_true)[i];
    //double pt_true = TMath::Sqrt(e_true*e_true - m_true*m_true)/TMath::CosH(eta_true);
    //double pt_reco = TMath::Sqrt(e_reco*e_reco - m_reco*m_reco)/TMath::CosH(eta_reco);


    double jmsCorr = jmsCorrection(e_recoCorr, m_recoCorr, eta_Corr);
    double mResp = m_recoCorr*jmsCorr/m_true;

    closure_mR_in_PtEtaMnuminv[bin]->Fill( mResp, m_evntWeight);
    return true;
    }

    BPS::BinnedTH1 closure_mR_in_PtEtaMnuminv;

    };

""")

print 
print "!!!!!!!!!!!!! Modified derivation !!!!!!!!!!!!!!!!!!!!!!!! "
print 

#  redefine the ResponseBuilder class with our new class :
ResponseBuilder = ROOT.MyRespBuilder
# then we load the usual configuration 
execfile("AntiKt10LCTopoTrimmedPtFrac5SmallR20.py")

# Move the standard configure() function to a new name
configure_standard = configure

# Redefine a configure() ...
def configure():
    # ... which calls the standard configure
    objs = configure_standard()

    # and adds a few more definition on the new C++ class we're using
    # (we're using __assign__ to make sure the assignement is done on the C++ side and we're not just redefining a python member)
    objs.respBuilder.closure_mR_in_PtEtaMnuminv.__assign__(BinnedTH1("closure_mR_in_PtEtaMnuminv", objs.jmsBPS, h1(200,0,5), Title="Mass Response closure", var_x='m response',LineColor=2))

    objs.respBuilder.MvsE_not_in_true.__assign__(BinnedTH2("MvsE_not_in_true", objs.jmsBPS, (100,0,6000,100,-6,0), Title="m/E vs e ", var_x='m response',LineColor=2))

    return objs

# now we just have to invoke this config file instead of the standard AntiKt10LCTopoTrimmedPtFrac5SmallR20.py one
