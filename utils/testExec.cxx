#include "TFile.h"
#include "TTree.h"

#include "DeriveJetScales/InputReader.h"
#include "DeriveJetScales/ResponseBuilder.h"


using namespace DJS;

int main(){

  BPS::BinnedPhaseSpace b;
  ResponseBuilder respBuilder;
  InputReaderVector inputReader;
  inputReader.m_evntWeight_bname = "new_EventWeight";
  inputReader.m_e_true_bname = "E_true";
  inputReader.m_e_reco_bname = "E_corr1";
  inputReader.m_eta_true_bname = "eta_true";
  inputReader.m_eta_reco_bname = "eta_corr1";
  inputReader.m_eta_det_bname = "eta_det_corr1";
  inputReader.m_m_true_bname = "m_true";
  inputReader.m_m_reco_bname = "m_corr1"    ;
  inputReader.m_m_ta_bname = "m_TA_corr1";
    // objs.inputReader = inputReader;
  respBuilder.m_inputReader = &inputReader;

  respBuilder.m_jmsBPS = &b;
  respBuilder.m_jesCorr.m_minPtExtrapolated = 10 ;
  respBuilder.m_jesCorr.m_minPtETACorr = 8;
  respBuilder.m_jesCorr.m_maxEETACorr = 2500 ;
  
  respBuilder.m_jmsTrueBin =  new ELogMovEBinning();
  respBuilder.m_jmsRecoBin =  new ELogMovEBinning();

  respBuilder.m_maxEvents = 2000;
  respBuilder.m_firstEvent = 0;

  respBuilder.m_objects = new CalibrationObjects();
  respBuilder.m_jmsCombination = nullptr;


  TFile f("JZ8W.root");

  TTree* tree = (TTree*) f.Get("AntiKt10TrackCaloClusterTrimmedPtFrac5SmallR20Jets");

  f.ls();
  std::cout << tree << std::endl;
  
  respBuilder.m_mode = (ResponseBuilder::Mode) 2000;
  respBuilder.processTree(tree);
  
  
}
