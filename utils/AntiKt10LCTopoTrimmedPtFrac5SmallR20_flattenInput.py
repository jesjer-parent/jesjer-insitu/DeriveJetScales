# This file demonstrates how to hack the usual JES/JMS derivation configuration
# in order to produce additionnal new histograms for testing/validation/debugging purposes.
# This method has the advantage of leaving untouched the standard derivation code, just producing
# additionnal info.

# here we apply the idea to produce a flat tree in a single file
# out of a (collection of) TTree with vector<double> branches
# the flat tree variable will have JES calib pre-applied
#
# Advantages of flat ntuples 
# - speed up the JMS derivation (faster to read, JES already applied)
# - required by some MVA (BDT, NN, ...) frameworks.

# First we define a new C++ class inheriting DJS::ResponseBuilder.
# This class only redefine the validation(int) and processEvent() functions.

# We directly load it and JIT compile it in  ROOT :

ROOT.gInterpreter.LoadText("""

  class ResponseBuilderFlat : public DJS::ResponseBuilder {
  public:

    bool processEvent(){
     if(m_initNPV) {
      m_tree->SetBranchStatus("NPV", true);
      m_tree->SetBranchAddress("NPV", &m_npv);
      //m_tree->SetBranchStatus("phi_true", true);
      //m_tree->SetBranchAddress("phi_true", &m_phi_true);
      //m_tree->SetBranchStatus("phi_corr1", true);
     //m_tree->SetBranchAddress("phi_corr1", &m_phi_reco);
       m_initNPV=false;


       m_outFile = new TFile(m_outFileName.c_str(),"recreate");
       m_outFile->cd();
       m_outTree = new TTree(m_tree->GetName(), "lll");
       m_outTree->Branch("m_reco", &m_reco);
       m_outTree->Branch("e_reco", &e_reco);
       m_outTree->Branch("eta_reco", &eta_reco);
       m_outTree->Branch("m_ta", &m_ta);
       m_outTree->Branch("NPV", &m_npv);
       m_outTree->Branch("m_true", &m_true);
       m_outTree->Branch("e_true", &e_true);
       m_outTree->Branch("eta_true", &eta_true);
       m_outTree->Branch("eventWeight", &eventWeight);
       m_outTree->Branch("jetindex", &m_jetind);
       
     }

     return ResponseBuilder::processEvent();
    }

    // JMS steps --------------
    bool jmsRvsEtrue(int i) {return true;}
    bool jmsRvsNuminv(int i){return true;}
    bool jmsClosure(int i){return true;}

    //----------------------------------
    // The only step we redefine here
    // 
    virtual bool validation(int i) {

      DJS::JetData j = m_inputReader->readCalibJetData(i);
      eventWeight = m_inputReader->m_evntWeight;
      
      if(! j.jms_valid ) return true;

      m_reco = j.m_reco;
      e_reco = j.e_reco;
      eta_reco = j.eta_det; // j.eta_reco; !!! JMS uses eta det !
      m_ta = j.m_ta;
      m_true = j.m_true;
      e_true = j.e_true;
      eta_true = j.eta_true;
      m_jetind = i;
      m_outTree->Fill();
      return true;
    }


    bool m_initNPV=true;
    float m_npv;

    TTree * m_tree = NULL;

    TTree * m_outTree = NULL;
    TFile * m_outFile = nullptr;
    std::string m_outFileName="flatTree.root";

    // outvars
    int m_jetind;
    float m_reco, e_reco, eta_reco, m_ta;
    float m_true, e_true, eta_true;
    float eventWeight;
  };

""")

print 
print "!!!!!!!!!!!!! Modified derivation !!!!!!!!!!!!!!!!!!!!!!!! "
print 

#  redefine the ResponseBuilder class with our new class :
ResponseBuilder = ROOT.ResponseBuilderFlat
# then we load the usual configuration 
execfile("AntiKt10LCTopoTrimmedPtFrac5SmallR20.py")

# Move the standard configure() function to a new name
configure_standard = configure

# Redefine a configure() ...
def configure():
    # ... which calls the standard configure
    objs = configure_standard()

    
    inputReader = objs.inputReader
    inputReader.m_evntWeight_bname = "new_EventWeight"
    inputReader.m_e_true_bname = "E_true"
    inputReader.m_e_reco_bname = "E_corr1"
    inputReader.m_eta_true_bname = "eta_true"
    inputReader.m_eta_reco_bname = "eta_corr1"
    inputReader.m_eta_det_bname = "eta_det_corr1"
    inputReader.m_m_true_bname = "m_true"
    # set mass branch name according to type 
    inputReader.m_m_reco_bname = "m_corr1"    
    inputReader.m_m_ta_bname = "m_TA_corr1"


    from BinnedPhaseSpace.BinnedPhaseSpaceConfig import BinnedTH1, h1
    #objs.respBuilder.mR_vs_DeltaR_in_PtEtaMtrue = BinnedTH2("mR_vs_DeltaR_in_PtEtaMtrue", objs.jmsBPS, h2(80,0,0.4, 200,0.01,5), var_x="DeltaR", var_y="mR" )
    #objs.respBuilder.mR_vs_eta_in_PtEtaMtrue = BinnedTH2("mR_vs_eta_in_PtEtaMtrue", objs.jmsBPS, h2(80,0,1.2, 200,0.01,5), var_x="DeltaR", var_y="mR" )

    return objs

def runValidation(self):
    objs = self.objs 
    # make sure we have the EtaJES constants loaded :
    objs.readEtaJESFromFile(self.fOut_jes, ["R_vs_Enuminv_Func", "R_vs_Etrue_Func", "DEta_vs_Etrue_Func"] )


    RB = ROOT.DJS.ResponseBuilder

    respBuilder = objs.respBuilder
    respBuilder.m_tree = self.tree
    respBuilder.m_mode = RB.JMS_Closure


    respBuilder.processTree(self.tree)

    respBuilder.m_outFile.Write()
    respBuilder.m_outFile.Close()
    
CalibrationDerivation.runValidation = runValidation

# now we run the derivation interactively calling in this script.
# the command line looks like :
#python -i deriveJES.py AntiKt10LCTopoTrimmedPtFrac5SmallR20 --inputPattern  'JZ*.root' -c AntiKt10LCTopoTrimmedPtFrac5SmallR20_flattenInput.py -t AntiKt10LCTopoTrimmedPtFrac5SmallR20Jets -s NoRun

# then we can just run
# der.runValidation()



