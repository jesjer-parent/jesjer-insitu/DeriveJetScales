# this file is -*- python -*-
#
# Convenience script to obtain a minimal interactive session with history
#  invoke like 'python -i ipython.py  [somescript.py] [some arguments...]'
#
# history is stored in local directory under this name :
HISTORYFILE = ".pyroot_hist"

import readline
import rlcompleter
import os, sys
import atexit

sys.path.append('.')
## History management : 
histfile = os.path.join(os.environ["PWD"], HISTORYFILE)
try:
    readline.read_history_file(histfile)
except IOError:
    pass
readline.set_history_length(600)
atexit.register(readline.write_history_file, histfile)
del os, histfile
readline.parse_and_bind("tab: complete")
readline.parse_and_bind("set show-all-if-ambiguous off")

# if python files are passed as args, execute them
if len(sys.argv)>1:
    #print 'CCC' , sys.argv
    # remove 1st arg so that 1st executed script sees arguments as if it was directly invoked.
    del(sys.argv[0])            
    for arg in sys.argv[:] :
        if arg.startswith('--'): break
        if arg[-3:]=='.py':
            execfile(arg)
        else:
            break
