# This is standlone a pyROOT script to test the numerical inversion formalism
# 
#  1) Assume we know the response as function of true mass R(m_true) 
#  2) The num inv procedure defines a calibration function C such as
#             C( m*R(m) ) = 1./R(m)
# that is : the correction at numerically inversed position (==m_true*R(m_true)) is the
# 1 over the response at the non num inv position (==m_tue)
#
#  3) if we define the function Ir as the inverse of m*R(m), that is Ir(m*R(m))=m, then
#        C(m) = 1./R(Ir(m))
#
#  4) With C(m) we can then evaluate the calibrated distribution from the uncalibrated distribution.
#

# This script defines a set of R(m) functions. Each are associated together with a distribution function into
# a NumInvTest() object
#
# commands :
# h2=niList[10].drawTH2F('calib',opt='colz')
# niList[-1].resp.Draw()
# niList[9].drawCalibResp(200)
# # draw all tested funcs in a pdf file :
# r = drawMultipleResp(niList, (100,200,300) )


import ROOT
import numpy as np

from DeriveJetScales.DrawUtils import buildLegend


step=1.
x_mass_0 = np.arange(5,400,step)
x_mass_ext = np.concatenate( (np.arange(1e-10,5,.05), x_mass_0, np.array([400,420, 450, 500])))
x_mass = x_mass_ext

class GraphFunc:
    """ An object representing a function from a TGraph"""
    def __init__(self,g, over_g):
        self.g = g
        self.over_g = over_g
    def __call__(self,x):
        return self.g.Eval(x)

    @staticmethod
    def buildIr(resp):
        y = np.vectorize(resp)(x_mass)*x_mass
        g = ROOT.TGraph(len(x_mass),y,x_mass)
        #over_g = ROOT.TGraph(len(x_mass),x_mass,1/np.vectorize(resp)(y))
        over_g = None
        return GraphFunc(g, over_g)


resolF = ROOT.TF1("resol", "0.15+0.1*60/x")
resolF2 = ROOT.TF1("resol", "0.21+0.1*60/x")
resolF3 = ROOT.TF1("resol", "0.11+0.1*100*100/x^2",10,300)
def gaussianRespPDF(resp_pdf,r, m):
    resp_pdf.SetParameter(0,1)
    resp_pdf.SetParameter(1,r)
    resp_pdf.SetParameter(2,0.15+0.1*60/m)


def gaussianRespPDF2(resp_pdf,r, m):
    resp_pdf.SetParameter(0,1)
    resp_pdf.SetParameter(1,r)
    resp_pdf.SetParameter(2,0.21+0.1*60/m)
    

def ratioTF1(f1,f2):
    funcDef='TF1 {f}("{f}","[](double *x, double *p){{ return {f1}->Eval(x[0]) / {f2}->Eval(x[0]); }}",{min},{max},{np})';
    name = 'Ratio'+f1.GetName()+f2.GetName()
    ROOT.gInterpreter.ProcessLine( funcDef.format(
        f = name,
        f1 = f1.GetName(),
        f2 = f2.GetName(),
        min=f1.GetXmin(),
        max=f1.GetXmax(),
        np = f1.GetNpar()+f2.GetNpar()
    ) )
    return getattr(ROOT, name)

def generateTF1Factory(exp,prefix,np=0):
    funcDef='TF1 {f}("{f}","[](double *x, double *p){{ '+exp+'}}",{min},{max},{np})';
    def generator(f1,f2,**args):
        name = prefix+f1.GetName()+f2.GetName()
        fullexp = funcDef.format(
            f = name,
            f1 = f1.GetName(),
            f2 = f2.GetName(),
            min=f1.GetXmin(),
            max=f1.GetXmax(),
            np = np, **args
        ) 
        ROOT.gInterpreter.ProcessLine( fullexp )
        return getattr(ROOT, name)
    return generator

def generateTF2Factory(exp,prefix,np=0):
    funcDef='TF2 {f}("{f}","[](double *x, double *p){{ '+exp+'}}",{min1},{max1},{min2},{max2},{np})';
    def generator(f1,f2, min2=0, max2=1, **args):
        name = prefix+f1.GetName()+f2.GetName()+"2D"
        fullexp = funcDef.format(
            f = name,
            f1 = f1.GetName(),
            f2 = f2.GetName(),
            min1=f1.GetXmin(),
            max1=f1.GetXmax(),
            min2=min2,
            max2=max2,
            np = np, **args
        ) 
        ROOT.gInterpreter.ProcessLine( fullexp )
        return getattr(ROOT, name)
    return generator

ROOT.gInterpreter.ProcessLine("double Mt=4;")

# create a function which returns a function.
calibDistrib = generateTF1Factory('return gaus_pdf->Eval(x[0]*{f1}->Eval(x[0]*p[0]))*( {f1}->Eval(p[0]*x[0]) + p[0]*x[0]*{f2}->Eval(p[0]*x[0]) ) ;','calibDist', np=1)
#  ---> g=calibDistrib( f, fprime), then g is the pdf of Xf(X) where X has a gaussian pdf defined by 'gaus_pdf'.
#  !!!! WARNING !!!  this is mathematically correct only if xf(x) is strictly increasing.


#calibDistrib2D = generateTF2Factory('return TMath::Gaus(x[0]*{f1}->Eval(x[0]*x[1]), {f1}->Eval(x[1]), (0.21+0.1*60/x[1])  )*( {f1}->Eval(x[1]*x[0]) + x[1]*x[0]*{f2}->Eval(x[1]*x[0]) ) ;','calibDist', np=1)
#calibDistrib2D = generateTF2Factory('double sig=13. ; return TMath::Gaus( x[0]*{f1}->Eval(x[0]*x[1]) , {f1}->Eval(x[1]), sig  )*( {f1}->Eval(x[1]*x[0]) + x[1]*x[0]*{f2}->Eval(x[1]*x[0]) ) ;','calibDist', np=1)
oneoverx = ROOT.TF1("overx","1/x")
calibDistrib2D = generateTF2Factory('double sig= 0.15+0.1*60*overx->Eval(x[1]); return TMath::Gaus( x[0]*{f1}->Eval(x[0]*x[1]) , {f1}->Eval(x[1]), sig  )*( {f1}->Eval(x[1]*x[0]) + x[1]*x[0]*{f2}->Eval(x[1]*x[0]) ) ;','calibDist', np=1)
#calibDistrib2D = generateTF2Factory('return 2/x[1] ;','calibDist', np=0)
#calibDistrib = generateTF1Factory('return gaus_pdf->Eval(x[0]) ;','calibDist')


    
class NumInvTest:
    def __init__(self, resp, resol=None):
        self.resp = resp
        self.resp_pdf = gaus_pdf
        self.resp_pdf.SetRange(0,5)
        if resol is None:
            resol = resolF
        self.resol = resol
        #self.adaptRespPDF = resp_pdfFunc
        self.inverseInited = False
        self.calib_dist = None
        
    def adaptRespPDF(self,r,m):
        self.resp_pdf.SetParameter(0,1)
        self.resp_pdf.SetParameter(1,r)
        self.resp_pdf.SetParameter(2,self.resol(m))
        
    def initInverse(self):
        if self.inverseInited:return        
        resp = self.resp

        if not np.isfinite(resp(x_mass[0])):
            self.x_mass = x_mass[1:]
        else:
            self.x_mass = x_mass
        Ir = GraphFunc.buildIr(resp)
        self.calib = lambda x:1/resp(Ir(x))
        self.calibV = np.vectorize(self.calib )(self.x_mass)
        self.calibV=np.nan_to_num(self.calibV)
        self.calibG = ROOT.TGraph( len(self.x_mass), self.x_mass, self.calibV)
        self.calibG.SetTitle("Calib factor vs M_{reco}")
        self.calibG.SetFillStyle(0)
        self.calibG.SetMarkerStyle(0)
        self.Ir = Ir
        self.inverseInited = True
        print '  inited   '
        
    def drawTH2F(self,calibF=None, opt='lego0', vsMreco=False):
        self.initInverse()
        if calibF is None: calibF = lambda x:1
        elif calibF=='calib': calibF = self.calib
        h2 = ROOT.TH2F("c","cc", len(self.x_mass), self.x_mass[0],self.x_mass[-1]+step, 100,0, 3)
        rrange = np.arange(0,3., 3./100.)
        resp_pdf = self.resp_pdf
        resp = self.resp
        h2Fill = h2.Fill
        if vsMreco:
            filler = lambda m,r : h2Fill(m*r,r)
            masses = x_mass_ext
        else: # normal
            filler = lambda m,r : h2Fill(m,r*calibF(r*m))
            masses = self.x_mass[1:]
        for m in masses:
            r = resp(m)
            self.adaptRespPDF(r, m)            
            for i in xrange(20000):
                r = resp_pdf.GetRandom()
                filler(m,r)
        h2.Draw(opt)
        return h2

    def drawRespVsMreco(self, opt='colz'):
        self.initInverse()

    def analyticalResp2D(self, opt='col'):
        f =calibDistrib2D(self.resp , self.resp.deriv, 1, 400)
        f.SetRange(0,1,3,400)
        f.SetNpx(150)
        f.SetNpy(150)
        f.SetMaximum(2)
        f.Draw('colz')
        return f
    
    def analyticalResp(self, m=50, opt='L', calibrated=True):
        r = self.resp(m)
        self.adaptRespPDF( r, m)
        if calibrated :
            if self.calib_dist is None:
                f =calibDistrib(self.resp , self.resp.deriv)                
                f.SetRange(0,3)
                f.SetLineColor(ROOT.kRed)
                f.SetTitle("calibrated")
                self.calib_dist = f            
            f = self.calib_dist
            f.SetParameter(0,m)
            if opt is not None:f.GetHistogram().DrawCopy(opt)
        else:
            f = self.resp_pdf
            f.SetRange(0,3)
            f.SetLineColor(ROOT.kBlack)
            if opt is not None:f.DrawCopy(opt)
        return f
    
    def analyticalResponses(self,m):
        hu=self.analyticalResp(m, calibrated=False, opt='L')
        hc=self.analyticalResp(m, calibrated=True, opt='L same')
        # hu=self.analyticalResp(m, calibrated=False, opt=None)
        # hc=self.analyticalResp(m, calibrated=True, opt=None)
        # ymax = 1.1*max(hu.GetMaximum(), hc.GetMaximum())
        # hc.SetMaximum(ymax)
        # hc.GetHistogram().DrawCopy('L')
        # hu.DrawCopy('L same')
        return (hc,hu)

        
    def drawCalibVsRecoRatio(self,m=50,opt='AL', np=200):
        self.initInverse()
        r = self.resp(m)
        self.adaptRespPDF( r, m)
        g = ROOT.TGraph(np)
        step = 3./np
        calibF = self.calib
        for i in xrange(np):
            #g.SetPoint(i,i*step, i*step*calibF(m*i*step)) # calib factor as func of reco ratio
            g.SetPoint(i,i*step, i*step*calibF(m*i*step))  # calibrate ratio factor as func of reco ratio (the)
            g.Draw(opt)
        return g
    
    def drawCalibResp(self, m=50, opt='', np=30000):
        self.initInverse()
        r = self.resp(m)
        self.adaptRespPDF( r, m)
        h1 = ROOT.TH1F("cresp%d"%(m,), "calibrated", 100,0,3)
        resp_pdf = self.resp_pdf
        calibF = self.calib
        for i in xrange(np):
            r = resp_pdf.GetRandom()
            h1.Fill(r*calibF(r*m))
        h1.SetLineColor(2)
        h1.Draw(opt)
        return h1


    def drawUncalibResp(self,m=50, opt=''):
        self.initInverse()
        r = self.resp(m)
        self.adaptRespPDF(r, m)
        h1 = ROOT.TH1F("ucresp%d"%(m,), "uncalibrated", 100,0,3)
        h1.FillRandom(self.resp_pdf.GetName(), 30000)
        h1.SetLineColor(1)
        h1.Draw(opt)
        return h1

    def responsesatM(self,m):
        hu=self.drawUncalibResp(m)
        h=self.drawCalibResp(m, opt='same')

        return (h,hu)

    def fullDrawAtM(self, mL, page=None, suffix=''):
        self.initInverse()
        if isinstance(mL, (int, long, float,)):
            mL = (mL,) # convert to a tuple

        if page is None:
            from DeriveJetScales.DrawUtils import buildPage
            page = buildPage(suffix, 1+len(mL), "testNI" )

        self.resp.Draw()
        self.resp.GetXaxis().SetTitle("mass")
        self.resp.SetMaximum(2.5)
        self.resp.SetMinimum(0.)
        #self.Ir.over_g.Draw("L")
        self.calibG.Draw("L")
        #gslope = self.responseVariation("L")
        #gslope = self.responseAt1widdth("L")
        #gslope = self.calibVariation("L")
        gslope = self.calibVariationPerWidth("L")
        gslope.SetLineColor(ROOT.kBlue)
        gslope.SetFillStyle(0)
        # gslope2 = self.responseAt1widdth("L", low=True)
        # #gslope = self.calibVariation("L")
        # gslope2.SetLineColor(ROOT.kGreen)
        # gslope2.SetFillStyle(0)
        
        
        leg = buildLegend([self.resp, self.calibG, gslope ], legPos=(0.65,0.55,0.95,0.8), style='l')
        li = ROOT.TLine(10,1.,400,1.)
        li.SetLineColor(ROOT.kBlack)
        li.SetLineWidth(1)
        li.SetLineStyle(2)
        li.Draw()
        page.graphObjects += [li, gslope, leg]
        ROOT.myText(0.4, 0.85, ROOT.kBlack, "Response (gaussian peak) vs mass")

        for m in mL:
            page.nextPad()
            #r=self.responsesatM(m)
            res=self.analyticalResponses(m)
            li = ROOT.TLine(1.0,0.,1.,1.)
            li.SetLineStyle(2)
            li.SetLineColor(ROOT.kBlue)
            li.Draw()
            l=buildLegend(res, legPos=(0.5,0.6,0.7,0.8), style='l')
            w = self.resol(m)
            r = self.resp(m)
            #tt = (r+0.5*w)*self.calib(m*(1+0.5*w)) - (r-0.5*w)*self.calib(m*(1-0.5*w))
            #self.adaptRespPDF(r-0.5*w, m*(r-0.5*w))            
            tt =(r-0.5*w)*self.calib(m*(r-0.5*w))
            #self.adaptRespPDF(r+0.5*w, m*(r+0.5*w))            
            tt2 =(r+0.5*w)*self.calib(m*(r+0.5*w))
            #print r+0.5*w, m*(r+0.5*w), self.calib(m*(r+0.5*w))
            l.AddEntry(None, "cc =%1.2f , %1.2f"%( tt2-tt , (tt2-tt)/w) )
            ROOT.myText(0.5, 0.85, ROOT.kBlack, "mass responses at m_{true}=%dGeV"%(m,))
            page.graphObjects += res +(l,li)

        #page.nextPad()
        #f=self.analyticalResp2D()
        #page.graphObjects += [f]
        return page

    def responseAt1widdth(self, opt=None, low=False):
        if low:
            rpw = np.vectorize(self.resp)(self.x_mass) - 0.5*np.vectorize(self.resol)(self.x_mass)
            rpw[ rpw<= 0 ] = 0.01
            suff="-#sigma"
        else:
            rpw = np.vectorize(self.resp)(self.x_mass) + 0.5*np.vectorize(self.resol)(self.x_mass)
            suff="+#sigma"
        y = rpw* np.vectorize(self.calib)(rpw*self.x_mass)
        y = np.nan_to_num(y)
        g = ROOT.TGraph(len(self.x_mass), self.x_mass, y)
        #g.SetTitle("Response variation w.r.t width")
        g.SetTitle("response at "+suff)
        if opt is not None:
            g.Draw(opt)
            g.SetMaximum(2.)
        return g

        
    def responseVariation(self, opt=None):
        """ R(M) variation relative to the width of gaussian distrib when M varies by width of gaussian at point M """
        y = np.abs( np.vectorize(self.resp.deriv)(self.x_mass) )
        y *= self.x_mass #np.vectorize(self.resol)(self.x_mass)*self.x_mass
        g = ROOT.TGraph(len(self.x_mass), self.x_mass, y)
        #g.SetTitle("Response variation w.r.t width")
        g.SetTitle("( #frac{dR}{dM}(M)#Delta M ) / #sigma")
        if opt is not None:
            g.Draw(opt)
            g.SetMaximum(2.)
        return g

    def resolOverresp(self,opt=None):
        self.initInverse()
        y = np.vectorize(self.resol)(self.x_mass) / np.vectorize(self.resp)(self.x_mass)
        g = ROOT.TGraph(len(self.x_mass), self.x_mass, y)
        g.SetTitle(" resol/resp")
        if opt is not None:
            g.Draw(opt)
            g.SetMaximum(2.)
        return g


    def calibVariationPerWidth(self,opt=None):
        w = np.vectorize(self.resol)(self.x_mass)
        r = np.vectorize(self.resp)(self.x_mass)
        
        y = self.calibV
        dy = (y[:-1] - y[1:])/(self.x_mass[:-1] - self.x_mass[1:])
        x_mid =0.5*(self.x_mass[:-1] + self.x_mass[1:])
        dcalib = np.interp( self.x_mass*r ,x_mid, dy)
        g = ROOT.TGraph(len(w), self.x_mass, dcalib*w*self.x_mass*r)
        #g = ROOT.TGraph(len(w), x_mid, 100*dy)

        g.SetTitle("#delta calib * resol")
        if opt is not None:
            g.Draw(opt)
            g.SetMaximum(2.)
        return g
        
        
    def calibVariation(self,opt=None):
        self.initInverse()
        y = self.calibV
        dy = np.abs(y[:-1] - y[1:])/step 
        x_mid =0.5*(self.x_mass[:-1] + self.x_mass[1:])
        g = ROOT.TGraph(len(dy), x_mid, dy*x_mid)
        g.SetTitle("calib variation vs M_{reco}")
        g.SetFillStyle(0)
        g.SetMarkerStyle(0)
        if opt is not None:
            g.Draw(opt)
            g.SetMaximum(2.)
        return g

    def numInvMass(self):
        nimass = ROOT.TF1("nimass",'x*('+str(self.resp.GetExpFormula())+')',self.resp.GetXmin(), self.resp.GetXmax())
        for i in range(self.resp.GetNpar()):
            nimass.SetParameter(i,self.resp.GetParameter(i))
        return nimass


import numpy as np
def histoAsNPArray(h2):
    ndim, dtype = {
        ROOT.TH2F : (2 , np.float32),
        ROOT.TH2D : (2 , np.float64),
    }[h2.__class__]
    shape = [
        lambda x:(), lambda h : (h.GetNbinsX()+2,), lambda h : (h.GetNbinsX()+2,h.GetNbinsY()+2),
    ][ndim](h2)
    
    a = np.ndarray( shape, dtype, h2.GetArray() , order='F')
    return a
    
class NumInvTestJMS(NumInvTest):
    massHistos = None
    etaAxis = None
    fOut_jms = None

    def __init__(self, E, ieta=0, calibSpec=("NI",None)):
        hResp  = fOut_jms.Get("JMSbins_sub01_mRsmooth_vs_PtMtrue_in_Eta_"+str(ieta))
        hIQR = fOut_jms.Get("JMSbins_sub01_mIQRsmooth_vs_PtMtrue_in_Eta_"+str(ieta))

        self.resp_pdf = gaus_pdf
        self.resp_pdf.SetRange(0,5)

        eta = self.etaAxis.binCenter(ieta)
        ie = hResp.GetXaxis().FindBin(E)
        self.eta=eta
        self.ie = ie
        self.ieta =ieta
        self.E = E
        respF = histoAsNPArray(hResp)
        respA = respF[ie,:][1:-1] #  '[1:-1]' is to exclude overflow bins

        resolA = histoAsNPArray(hIQR)[ie,:][1:-1]
        
        mPos = hResp.GetYaxis().GetBinCenter
        ePos = hResp.GetXaxis().GetBinCenter
        nM = hResp.GetNbinsY()

        from math import exp
        self.masses = np.array( [ exp(mPos(i))*self.E for i in xrange(nM) ], dtype=np.float32)
        self.logmoe = np.array( [ mPos(i) for i in xrange(nM) ], dtype=np.float32)
        self.respA = respA
        self.resolA = resolA

        if calibSpec[0].startswith("NI"):
            print "ASSS"
            self.buildNICalib()
            if calibSpec[0].startswith("NILimit"):
                self.limitCorrecWidth( self.calibA)
        elif calibSpec[0]=="preComputed":
            jmsCorr = lambda m : calibSpec[1].jmsCorrection_noEtaInt(E,m,eta)
            self.jmsBin = calibSpec[1]
            self.calibA = np.vectorize(jmsCorr)(self.masses)
            
        self.gcalib = ROOT.TGraph(len(self.calibA), self.masses.astype(np.float64), self.calibA.astype(np.float64))
        masses = self.masses
        def resp(m):
            return np.interp(m,masses,respA)
        self.resp = resp
        def calib(m):
            return np.interp(m,masses,calibA)
        self.calib = calib
        def resol(m):
            return np.interp(m,masses,self.resolA)
        self.resol = resol
        

    def buildNICalib(self):
        from math import exp, log
        masses = self.masses
        respA = self.respA
        logmoe = self.logmoe
        if True: # enforce nonDecreasing:
            fbin=0
            stop = 30
            delta = respA[fbin:stop-1+fbin] - respA[fbin+1:stop+fbin]
            for j,d in reversed(list(enumerate(delta))):
                if j== (stop-2): continue
                if d < delta[j+1]:
                    delta[j] = delta[j+1]
            respA[fbin:stop-1+fbin] = np.flipud(np.cumsum(np.flipud(delta)))+respA[stop-1+fbin]
                
        # numerical inversion
        nilogmoe = logmoe + np.vectorize(log)(respA)
        niRespA = np.interp(  logmoe , nilogmoe, respA  )
        # smooth ---> ???
        calibA = 1./niRespA
        #self.limitCorrecWidth(self.calibA)
        
        self.calibA = calibA


    def limitCorrecWidth(self, calib, nonClosureMitigation=False ):
        print " limitCorrecWidth ", calib
        resp, resol,  masses = self.respA, self.resolA, self.masses
        #calib = 1./calib_
        #calib = calib_

        deltamasses = (masses[:-1] - masses[1:])
        dcalib = (calib[:-1] - calib[1:])/ deltamasses # this is (dcalib/dm)(m)
        mass_mid = 0.5*(masses[:-1] + masses[1:])
        deltamasses = np.concatenate( [deltamasses, np.array([deltamasses[-1]])])

        #print mass_mid.dtype, dcalib.dtype


        dcalib_m = np.interp( masses , mass_mid, dcalib  ) # this is still (dcalib/dm)(m)
        # (we just shifted the mass back to the original mass points)

        dcalib_mr = np.interp( masses*resp , masses, dcalib_m)# this is (dcalib/dm)(m*R(m))
        testedValues = dcalib_mr*masses*resol

        dcalib_m2 = dcalib_m.copy()
        #return masses.astype(np.float64), calib.astype(np.float64), dcalib_m.astype(np.float64) ,testedValues.astype( np.float64) #dcalib_mr.astype(np.float64)
        #return masses.astype(np.float64), calib.astype(np.float64), dcalib_m.astype(np.float64) ,dcalib_m2.astype(np.float64)
        #dcalib_mrc = dcalib_mr.copy()
        T = 0.1+0.01*self.eta
        pos = (testedValues > T)
        dcalib_mr[ pos ] = T / (masses*resol)[pos]
        #return masses.astype(np.float64), calib.astype(np.float64), dcalib_mr.astype(np.float64) ,dcalib_m.astype(np.float64)            
        dcalib_m = np.interp(masses, masses, dcalib_mr)
        #print  T, pos
        #return masses.astype(np.float64), dcalib_m.astype(np.float64), dcalib_m2.astype(np.float64) ,dcalib_mr.astype(np.float64)


        # start from last bin (high mass) since it's the most reliable
        calib_c = calib[-20] + np.flipud(np.cumsum(np.flipud((dcalib_m*deltamasses)[:-20])))

        #print len(calib_c)
        #print E, pos 
        #return masses.astype(np.float64), calib.astype(np.float64), dcalib_m.astype(np.float64) ,calib_c.astype(np.float64)

        calib2_ = calib.copy()
        #calib_[:-20] = 1./calib_c
        calib[:-20] = calib_c
        if nonClosureMitigation:
            closureW = 1+abs(1-closureA[self.ie,:][1:-1]) #  '[1:-1]' is to exclude overflow bins
            calib[:] = (calib+closureW*calib2_)/(1+closureW)
            #calib3_ = (calib_+closureW*calib2_)/(1+closureW)
            #print closureW

    def drawUncalibResp(self, m=(40,80), opt='', np=30000):
        h1 = ROOT.TH1F("resp%d_%d"%m, "uncalibrated", 100,0,3)
        print "AAA"
        ie = self.massHistos[self.ieta].GetXaxis().FindBin(self.E)
        hM = self.massHistos[self.ieta].ProjectionY("massSpect"+str(self.ie), ie, ie)
        print "AAA"
        b0 = hM.FindBin(m[0])
        b1 = hM.FindBin(m[1])
        norm = hM.Integral(b0,b1)
        print "AAA"
        print b0,b1
        for i in range(b0,b1+1):
            c = hM.GetBinContent(i)/norm
            m = hM.GetBinCenter(i)
            r = self.resp(m)
            self.adaptRespPDF( r, m)
            h1.FillRandom(self.resp_pdf.GetName(), int(np*c))
        h1.SetLineColor(1)
        h1.Draw(opt)
        print "AAA"
        return h1
        
    def drawCalibResp(self, m=(40,80), opt='', np=30000):
        h1 = ROOT.TH1F("cresp%d_%d"%m, "calibrated", 100,0,3)
        print "UUU"
        if not hasattr(ROOT,"CalibFiller") : ROOT.gInterpreter.LoadText(calibFiller)
        filler = ROOT.CalibFiller(self.gcalib, self.resp_pdf)
        print "UUU"

        ie = self.massHistos[self.ieta].GetXaxis().FindBin(self.E)
        hM = self.massHistos[self.ieta].ProjectionY("massSpect"+str(self.ie), ie, ie)
        b0 = hM.FindBin(m[0])
        b1 = hM.FindBin(m[1])
        norm = hM.Integral(b0,b1)
        for i in range(b0,b1+1):
            c = hM.GetBinContent(i)/norm
            m = hM.GetBinCenter(i)
            r = self.resp(m)
            self.adaptRespPDF( r, m)
            print m, r, int(np*c )
            filler.fill(m, h1, int(np*c))
        h1.SetLineColor(2)
        h1.Draw(opt)
        return h1

calibFiller="""
struct CalibFiller {
  CalibFiller(TGraph *c, TF1* p): calib(c), pdf(p){} 
  TGraph *calib;
  TF1* pdf;
  void fill(double m, TH1F* h, int n){
   for(int i=0;i<n;i++){
     double r = pdf->GetRandom();
     h->Fill(r*calib->Eval(r*m)); 
   }
 }
};
"""


    
def drawMultipleResp(niL, mL, suffix='pdf'):
    from DeriveJetScales.DrawUtils import buildPage
    page = buildPage(suffix, 4, "multitestNI" )

    for ni in niL:
        ni.fullDrawAtM( mL, page=page)
        page.nextPad()
        #page.forceSave()
    page.close()
    return page
    
    
    
gaus_pdf = ROOT.TF1("gaus_pdf","gaus")
gaus_pdf.SetTitle("uncalibrated")
countF=0
def resetF(func, *args):
    global countF
    #f = func.Clone(func.GetName()+str(countF))
    f = func.Clone(func.GetName()+str(countF))
    f.deriv = func.deriv.Clone(func.deriv.GetName()+str(countF))
    for i, p in enumerate(args):
        f.SetParameter(i,p)
        f.deriv.SetParameter(i,p)
    f.SetTitle("Response vs M_{true}")
    f.SetFillStyle(0)
    countF+=1

    ROOT
    return f

xmin = x_mass[0]
xmax=400

resp_polynom = ROOT.TF1("resp_poly","[0] + [1]*x^[2] + [3]*x^[4]", xmin, xmax)
resp_polynom.deriv = ROOT.TF1("resp_polyd"," [2]*[1]*x^([2]-1) + [4]*[3]*x^([4]-1)", xmin, xmax)

resp_exp = ROOT.TF1("resp_exp","[0] + [1]*(x+[3])*exp(-(x)/[2])", xmin, xmax)
resp_exp.deriv = ROOT.TF1("resp_expd","[1]*(1- (x+[3])/[2])*exp(-(x)/[2])", xmin, xmax)

resp_expox = ROOT.TF1("resp_expox","[0] + [1]*x^[3]*exp(-x/[2])", xmin, xmax)
resp_expox.deriv = ROOT.TF1("resp_expoxd","[1]*([3]*x^([3]-1)-x^[3]/[2])*exp(-x/[2])", xmin, xmax)

resp_tanh = ROOT.TF1("resp_tanh","[0] + [1]*0.5*(tanh(([2]-x)*[3])+1)", xmin, xmax)
resp_tanh.deriv = ROOT.TF1("resp_tanhd"," -[1]*0.5*( -tanh( ([2]-x)*[3])^2+1)*[3]", xmin, xmax)

#resp_tanhX = ROOT.TF1("resp","[0] + [1]*0.5*(tanh(([2]-x)*[3])+1)*abs([2]-x)*[3]", xmin, xmax)
resp_tanh2 = ROOT.TF1("resp_tanh2","[0] + [1]*0.5*(tanh(([2]-x)*[3]*(1 + exp(([2]-x)*[3])) )+1) ", xmin, xmax)
resp_tanh2.deriv = ROOT.TF1("resp_tanh2d","0.5*[1]*(-[3]**2*([2] - x)*exp([3]*([2] - x)) - [3]*(exp([3]*([2] - x)) + 1))*(-tanh([3]*([2] - x)*(exp([3]*([2] - x)) + 1))**2 + 1)", xmin, xmax)
if 1:

    niList = [

        # exponentially decreasing, without plateau
        NumInvTest(resetF(resp_expox, 1.1, 40, 60, -0.8 ) ), # response infinite at 0
        NumInvTest(resetF(resp_expox, 1.1, 40, 60, -0.8 ), resol=resolF3 ), # response infinite at 0
        NumInvTest(resetF(resp_exp, 1., 0.1, 10, 70)  ), # response finite at 0 (== [1]*[3])

        #resp_polynom = "[0] + [1]*x^[2] + [3]*x^[4]"
        NumInvTest(resetF(resp_polynom, 1, -9.9, -1)  ), # increasing toward 1       
        NumInvTest(resetF(resp_polynom, 1, -20, -1)  ), # increasing toward 1       
        NumInvTest(resetF(resp_polynom, 0.9, -9.9, -1)  ), # increasing toward 1       

        NumInvTest(resetF(resp_exp, 1., -0.4, 15)  ), # bump around par [2], increasing toward 1       


        NumInvTest(resetF(resp_polynom, 1.1, 30, -1)  ), # decreasing toward 1.1       
        NumInvTest(resetF(resp_polynom, 0.9, 60, -1)  ), # decreasing toward 1.1       
        NumInvTest(resetF(resp_polynom, 0.9, ROOT.TMath.Sqrt(50), -0.5)  ), # much slower descent

        #resp_exp = "[0] + [1]*x*exp(-x/[3])"
        NumInvTest(resetF(resp_exp, 1., 0.1, 50)  ), # bump around par [2]
        NumInvTest(resetF(resp_exp, 1.1, 0.1, 50)  ), # bump around par [2]
        NumInvTest(resetF(resp_exp, 1.1, 0.05, 50)  ), # bump around par [2]
        NumInvTest(resetF(resp_exp, 1.1, 0.03, 90)  ), # bump around par [2]
        NumInvTest(resetF(resp_exp, 1.1, 0.03, 90)  , resol = resolF3 ), # bump around par [2]



        # "[0] + [1]*tanh(([2]-x)*[3])"
        #step around par [2]
        NumInvTest(resetF(resp_tanh, 1., 0.6, 100, 1./30)  ), #
        NumInvTest(resetF(resp_tanh, 1., 0.6, 100, 1./70)  ), #
        #NumInvTest(resetF(resp_polynom, 1.1, 50**3, -3)  ), # m*R(M) NOT invertible !
        #NumInvTest(resetF(resp_tanh, 1.1, 0.6, 100, 1./30)  ), # 
        NumInvTest(resetF(resp_tanh, 1, 0.6, 100, 1.)  ), # 
        NumInvTest(resetF(resp_tanh, 1, 0.2, 100, 1./30)  ), # 
        NumInvTest(resetF(resp_tanh, 1.0, 0.1, 100, 1./30)  ), #
        NumInvTest(resetF(resp_tanh, 1.0, 0.1, 150, 1./30)  ), #

        NumInvTest(resetF(resp_tanh2, 1., 1.4, 50, 1./40)  ), # 

    ]

elif 1:
    resolFx = ROOT.TF1("resol", "0.11+0.1*[0]*[0]/x^2",10,300)
    def cloneR(c):
        f = resolFx.Clone()
        f.SetParameter(0,c)
        return f
    niList =[
        #NumInvTest(resetF(resp_expox, 1.1, 40, 60, -0.8 ) ), # response infinite at 0
        NumInvTest(resetF(resp_expox, 1.1, 40, 60, -0.8 ), resol=cloneR(50) ), # response infinite at 0
        NumInvTest(resetF(resp_expox, 1.1, 40, 60, -0.8 ), resol=cloneR(60) ), # response infinite at 0
        NumInvTest(resetF(resp_expox, 1.1, 40, 60, -0.8 ), resol=cloneR(70) ), # response infinite at 0
        NumInvTest(resetF(resp_expox, 1.1, 40, 60, -0.8 ), resol=cloneR(80) ), # response infinite at 0
        NumInvTest(resetF(resp_expox, 1.1, 40, 60, -0.8 ), resol=cloneR(90) ), # response infinite at 0
        NumInvTest(resetF(resp_expox, 1.1, 40, 60, -0.8 ), resol=cloneR(100) ), # response infinite at 0
        NumInvTest(resetF(resp_expox, 1.1, 40, 60, -0.8 ), resol=cloneR(120) ), # response infinite at 0
        
    ]
else:
    def libraryName(package): return "lib"+package+".so"

    ROOT.gSystem.Load(libraryName("JES_ResponseFitter"))
    ROOT.gSystem.Load(libraryName("BinnedPhaseSpace"))
    ROOT.gSystem.Load(libraryName("DeriveJetScales"))

    jetType = "AntiKt10LCTopoCSSKSoftDropBeta100Zcut10"
    fmassH = ROOT.TFile("../vartests/massSpectrumSoftDrop.root")
    NumInvTestJMS.massHistos = fmassH.massHistos


    fname="../vartests/jms_ELogMoEBins_nidirectNILimitNCM/"+jetType+"_JMSvfbmCalo.root"
    fOut_jms = ROOT.TFile(fname)
    NumInvTestJMS.fOut_jms

    from BinnedPhaseSpace.BinnedPhaseSpaceConfig import AxisSpec , BinnedPhaseSpace
    from DeriveJetScales.PyHelpers import seq
    jmsEtaBins = [0,0.2,0.6, 1.0, 1.1, 1.2, 1.3,  1.4, 2., 3  ]
    jmsBPS = BinnedPhaseSpace( "JMSbins",
                               # bins from BinHadler/BinHandler.cxx
                               AxisSpec("E", "E", seq(200,1000,200)+seq(1200,2000,400)+[3000,4000,6000], isGeV=True ),
                               AxisSpec("m", "m_{true}",seq(0,100,10)+[120,140,200, 250, 300], isGeV=True),
                               AxisSpec("abseta", "|eta|",jmsEtaBins, isGeV=False)
    )
    NumInvTestJMS.etaAxis = jmsBPS.m_a3

    def buildJMSBin(tag, hname=None):
        fname="../vartests/jms_ELogMoEBins_ni"+tag+"/"+jetType+"_JMSvfbmCalo.root"
        corrFile = ROOT.TFile(fname)
        jmsBin = ROOT.DJS.ELogMovEBinning()
        jmsBin.setBinnedPhaseSpace( jmsBPS)
        if hname is None : hname=tag
        hcorr = corrFile.Get("mRsmooth_"+hname)
        jmsBin.initCorrections(hcorr)
        jmsBin.corrFile = corrFile
        return jmsBin
    
    niList = [
        #NumInvTestJMS( 2500,ieta=0, calibSpec=("NI",None)),
        #NumInvTestJMS( 1500,ieta=0, calibSpec=("preComputed",buildJMSBin("directNInoLimit"))),
        NumInvTestJMS( 2500,ieta=0, calibSpec=("preComputed",buildJMSBin("noetaNN","3Dnuminv"))),
        0,
    ]

    ni0 = niList[0]
    ni1 = niList[1]
   
#ni = NumInvTest(resp0)        
ROOT.gROOT.LoadMacro("AtlasStyleUtils.C")
ROOT.SetAtlasStyle()
