Derivation of MC based JES, JMS and eta calibrations
===================================================

This package implements methods to derive the MC based calibration
constants to calibrate the Jet Energy Scale (JES), Jet Mass Scale
(JMS) and eta calibration.


Authors :
- P-A Delsart
- Xuanhong Lou
- Johannes Balz



- [Dependencies](#user-content-dependencies-)
- [Deriving EtaJES and JMS corrections](#user-content-deriving-etajes-and-jms-corrections)
   - [Inputs](#user-content-inputs)
   - [Derivation steps](#user-content-derivation-steps)
   - [Outputs](#user-content-outputs)
   - [Run](#user-content-run)
   - [interactive run](#user-content-interactive-run)
- [Usage](#user-content-usage-)
- [Code organisation](#user-content-code-organisation)
   - [Helper classes](#user-content-helper-classes)
   - [CalibrationObjects](#user-content-calibrationobjects)
   - [ResponseBuilder](#user-content-responsebuilder)
   - [ResponseFitter](#user-content-responsefitter)
   - [JES_BalanceFitter](#user-content-jes_balancefitter)
   - [BinnedPhaseSpace](#user-content-binnedphasespace)
- [Interactive use example](#user-content-interactive-use-example)
- [Details on JMS derivation](#user-content-details-on-jms-derivation-)
   - [Response definition](#user-content-response-definition)
   - [Evaluation of an average response per bin](#user-content-evaluation-of-an-average-response-per-bin-)
   - [Merging of very low stat bins at low mass](#user-content-merging-of-very-low-stat-bins-at-low-mass)
   - [Low mass re-evaluation](#user-content-low-mass-re-evaluation)
   - [Response maps](#user-content-response-maps)
   - [Smoothing procedure](#user-content-smoothing-procedure)
   

Package history
=======================

### JES and eta calibration

The code largely follows or completely reproduce the older package
documented in
[DeriveMCJES2015](https://twiki.cern.ch/twiki/bin/view/AtlasProtected/DeriveMCJES2015)
available from :
svn+ssh://svn.cern.ch/reps/atlasperf/CombPerf/JetETMiss/Run2/Jet/Calibration/JetCalibrationTools/MC/DeriveJES/trunk


### JMS calibration

The code follows the num inv procedure from the JES above also
re-using many ideas from the r20 JMS calibration procedure.

Several alternative derivation flows are available as options.

Dependencies 
===========
2 packages :

 * JES_ResponseFitter https://gitlab.cern.ch/atlas-jetetmiss-jesjer/tools/JES_ResponseFitter
 * BinnedPhaseSpace   https://gitlab.cern.ch/atlas-jetetmiss/BinnedPhaseSpace


Introduction
================
The energy and mass calibration derived by this package are obtained
by the Numerical Inversion (NI) procedure. In this introduction the
principles of NI and the related concepts are described.

First we assume each reconstructed jet ("reco jet") in the samples used to derive the calibration
is associated to one particle level ("truth jet"). So each
reconstructed quantity $`X_{reco}`$ (where $`X`$=Mass or $`X`$=energy) has a
corresponding $`X_true`$.

Response
---------
For a given jet, the ratio $`r=\frac{X_{reco}}{X_{true}}`$ is the
individual response for quantity $`X`$. For a same truth-jet, the
corresponding reconstructed jets will have their $`X_{reco}`$ fluctuating
around $`X_{true}`$ and the distribution of individual response $`r`$ will
typically be gaussian like. 
In the exact gaussian case, the mean of the gaussian (equals to the
arithmetic means, the mode and the median of the distribution) is then
called the "average response" $`R`$. Because of various experimental
reasons, $`R`$ will depend on multiple parameters : E, eta, m, internal
composition of the jet...

Depending on the context the word "response" may thus refer to the
"individual response" $`r`$ or to the "average response" $`R`$.

Response evaluation
-------------------
In practice we evaluate the average response in "bins" in a phase
space of reco or truth variables. An example of a 2D bin used in the
E calibration would be $`(E \in [100,200GeV], \eta_{det} \in [0.1, 0.2])`$.
In each bin the distribution of $`r`$ is obtained from MC samples. In practice this
distribution is not always a gaussian and a choice has to be made to
evaluate an "average response". The default choice is to fit a
gaussian function and take the fitted mean, thus actually evaluating
the mode when the distribution is not symmetric. 

As a result we obtain an evaluation of $`R(p_0, p_1, ...)`$ where the
$`p_i`$ are the parameters of the phase space. 

Calibration
--------------
The goal of the package is to obtain a calibration function $`C`$
depending only on reco quantities $`p_0,p_1,...`$ and which defines the calibrated
$`X`$ as $`X_{calib}=C(p_0,p_1,...) X_{reco}`$. 
This in turn defines calibrated individual responses
```math
r_{calib} = \frac{X_{calib}}{X_{true}} = C(p_0,p_1,...) r_{reco}
```

and a calibrated average response $`R_{calib}`$ . 

We then say the calibration **closes** when $`R_{calib}=1`$ on all the
phase space. 

Numerical inversion
-------------------
The Numerical Inversion is a procedure to obtain a calibration
function when the average response $`R`$ depends on the quantity being
calibrated. 

Igoring other dependency, assume we have a obtained a response function
$`R()`$ depending on the true quantity $`X_{true}`$. The NI procedure defines the
calibration function as 
```math
C(X R(X)) = \frac{1}{R(X)}
```

The formula can be interpreted as follows. For $`X=X_{true}`$, $`XR(X)`$
is the average reconstructed $`X`$ at $`X_{true}`$ or the "numerically
inversed X" : $`X_{NI} = X_{true}R(X_{true})`$. The formula simply
states that the calibration at a $`X_{reco}`$ corresponding to a
numerically inversed $`X_{true}`$ is the inverse of the average response
at this point (that is $`1/R(X_{true})`$). 

Numerical inversion limitations
-------------------

It has been proved the NI produces a calibration function with the
desired property of closure when the response function $`R`$ is linear,
independently of the exact shape of the $`r`$ distribution [1].

However it is also proven that, depending on how $`R`$ is evaluated
(mean, mode or median), closure is not achieved or not guaranteed when
$`R`$ is not linear.

Further, when $`R`$ varies strongly with respect to the width of the $`r`$
distribution, the NI calibrated distribution $`r_{calib}`$ can be strongly
distorded such as presenting 2 distinctive peaks, even when starting
from a gaussian uncalibrated distribution.
It has been shown this is the case for the jet mass $`R`$ function in
some region of the (E,m,$\eta$) phase space.

[1] : https://arxiv.org/abs/1609.05195


Overview of implementations
-----------------------

The JES and JMS implementations of NI differ. This is due to
historical reasons and the necessity to be able to reproduce at ~100%
earlier results. 

However, they follow the same principles and share common utilities.

  1 **Response evaluation**. A loop over all jets is run. Each jet is
    associated to a bin in the phase space (2dim for JES, 3dim for
    JMS) and its individual response $`r=\frac{X_{reco}}{X_{true}} `$
    is contributing to the distribution in this bin. Then in in each
    bin the average response $`R`$ is fitted from the distribution,
    thus providing a numerical evaluation of the function $`R()`$
  1 **Numerical inversion**. The calibration function $`C()`$ is
    evaluated. This can be done in 2 ways :
    * directly evaluating $`C()`$ numerically using the above formula
    * by rerunning a loop in as in 1 but filling bins located at point
      $`X_{true}R(X_{true})`$ rather than simply $`X_{true}`$. Fitting
      the corresponding distributions then gives a function
      $`R_{NI}()`$ which is just the inverse of $`C()`$.
  1 **Closure test**. Same as in 1 but filling distributions of
    $`r_{calib}`$ instead of $`r`$.


Deriving EtaJES and JMS corrections
==============

### Inputs
The system use ntuples (they can be in xAOD format) which contain `vector<double>` branches 
(float support is easy by hacking 1 line in ResponseBuilder.h). 
 * There must be at least 6 branches related to jets : for mass,
 energy and eta and for reco and truth jets.
 * The name of the branches are configurable (see the m_X_reco_bname
 and m_X_truth_bname strings in the existing config files under scripts/)
 * the truth and reco quantities must correspond to DR matched
 truth and reco jets and must be aligned, that is e_reco[i] must
 correspond to e_truth[i].
 * Optionnaly a *total* event weight branch can be used. This weight must take into account
 all weights applicable : probably mc-generator & x-section
 normalization, but possibly others depending on the sample. If no
 valid branch exists, the weight defaults to 1 for all events.

The ntuples can be spread amongst several TFiles.


### Derivation steps
Both the EtaJES and JMS calibration derivation procedures are divided
in sequential steps. Each step produces some histograms or graphs
which are saved in a ROOT file. 

A derivation job can run all the steps sequentially or only a few
specific steps. See the command line options below.

Steps are identified by a string in the form "MajorStep.Substep".
Major steps can be : "EtaJES", "JMS", "Plots", "Debug", "NoRun".

Sub-steps of "EtaJES" and "JMS" are :
 * "rawresp" : loop over all events, compute un-calibrated response in
 true bins
 * "rawfits" : fit responses from previous steps, fit <R> vs pT or
 smooth resulting histograms.
 * "niresp" : loop over all events, compute un-calibrated response in
 numerically inversed bins.
 * "nifits" : fit responses from previous steps, fit <R> vs pT or
 smooth resulting histograms.
 * "closureresp" : loop over all events, compute calibrated response
 (using corrections obtained from the nifits step) in true bins.
 * "closurefits" : fit responses from previous steps, fit <R> vs pT or
 smooth resulting histograms.

Sub-steps of "Plots" are "jes" or "jms" : this triggers the drawing
and saving in pdf format of all plots relevant to JES or JMS. This
also creates the final config file or root file to be used by the
official JetCalibTools.

### Outputs

A full derivation job will create 2 ROOT output files containing several histos
related to the JES and JMS calibrations.

If the "Plots" step is invoked histograms and graphs plots will be
saved in various pdf files as well as final config file or root file to be used by the
official JetCalibTools.


### Run 

```shell 
python deriveJES.py AntiKt10LCTopoTrimmedPtFrac5SmallR20 --inputPattern 'JZ*root'  -t AntiKt10LCTopoTrimmedPtFrac5SmallR20Jets -s EtaJES+JMS
```

This example will run the derivation for
AntiKt10LCTopoTrimmedPtFrac5SmallR20 jets (1st argument), using all
the JZ*root in the current directory (--inputPattern option), using
"AntiKt10LCTopoTrimmedPtFrac5SmallR20Jets" as the tree name inside the
TFiles (-t option) and running both the EtaJES and JMS derivation (-s
option).

Do `python deriveJES.py --help` for more option



### interactive run
For debugging and testing it's useful to run interactively and only
some sub-steps of the derivation. This is possible for example by
using `ipython` and specifying which sub-steps to run :

```shell
ipython
In [1]: %run deriveJES.py AntiKt10LCTopoTrimmedPtFrac5SmallR20 -t
AntiKt10LCTopoTrimmedPtFrac5SmallR20Jets --inputPattern "'JZ*root'" -s JMS.rawresp+JMS.rawfits
``` 

This will run only 2 substep of JMS : the raw response calculation
(including loop over all evnts) and the fit of this responses and
preparation of 2D response maps.
*Note* the double quotes around the `--inputPattern` argument !

#### ipython not available
On lxplus, setting up (Ath)AnalysisBase may break `ipython`. There's
an alternative in `utils/ipython.py`. Simply replace `python deriveJES.py ...` by `python -i  utils/ipython.py deriveJES.py ...` 




Usage 
=======

The code is composed of C++ classes which are invoked by a python
script. 

The script is  `scripts/deriveJES.py` :

``` 
> python deriveJES.py --help
usage: deriveJES.py [-h] [--maxEvents MAXEVENTS]
                    [--inputPattern [INPUTPATTERN]] [-c [CONFIG]]
                    [-t [TREENAME]] [-s [CALIBSTEPS]]
                    jetType

derive JES and Eta calibrations
Invocation example :
python deriveJES.py AntiKt10LCTopoTrimmedPtFrac5SmallR20 --inputPattern 'JZ*root'  -t AntiKt10LCTopoTrimmedPtFrac5SmallR20Jets

  --> derive EtaJES correction for standard large-R jets

positional arguments:
  jetType               The type of jets. Ex: 'AntiKt4LCTopo' or 'AntiKt10LCTopoTrimmedPtFrac5SmallR20' 

optional arguments:
  -h, --help            show this help message and exit
  --maxEvents MAXEVENTS
  --inputPattern [INPUTPATTERN]
                        A pattern to glob the input ntuples. Put it within single quotes like :  '/some/path/JZ*root' 
  -c [CONFIG], --config [CONFIG]
                        The configuration file defining the calib derivation. If left blank, will be set to 'jetType+.py' 
  -t [TREENAME], --treeName [TREENAME]
                        The tree name inside the input files. If left blank, will be set to 'jetType' 
  -s [CALIBSTEPS], --calibSteps [CALIBSTEPS]
                        What calibration derivation steps to perform. Steps are separted by '+'.
                                            Ex : 'EtaJES+JMS.rawresp' means all EtaJES steps and 'rawresp' of JMS. default is 'EtaJES+JMS+Plots'

```


Input to the script needs to be given in the form of ntuples (see
above for details).

Code organisation
==================

The code consists of a main python script which uses other python
modules or C++ classes through pyROOT.
The main script is `deriveJES.py`. It reads in the command line
arguments, executes a user given configuration files and instantiates
a `CalibrationDerivation` object and call the `runDerivation()` method.
`CalibrationDerivation` is the python class implementing the derivation flow
dividing it in different steps and allowing the user to choose which
steps to execute.

Each derivation steps is implemented in python or in C++ helper
classes.

Before starting the derivation flow, the main script will execute a
configuration script. The configuration script is expected to define a
`configure()` function which returns a `CalibrationObjects` instance
which hold all the information needed for the derivation (including
options, histograms, configured helper classes,...).


Helper classes
--------------
    
## CalibrationObjects
C++ and Python

Defined in `DeriveJetScales/Utilities.h` `python/CppExtensions.py`

This class defines an object centralizing all information needed to
run JES and JMS derivation.
It's a C++ class, so instances can be used in C++ helpers, but it is
also "augmented" in python by a few python methods and attributes.

## ResponseBuilder
C++

Defined in `DeriveJetScales/ResponseBuilder.h`

Implements the loop over input ntuple during which responses (e, mass,
eta) are calculated and filled into histograms.

## ResponseFitter
C++

Defined in `DeriveJetScales/ResponseFitter.h`

Recopied from the older JES derivation package. Performs the fit of
E or DeltaEta responses and then the fits of <R> as a function of E.

## JES_BalanceFitter
C++

Defined in the `JES_ResponseFitter` package.
Implements the recursive fit of a gaussian distribution.

## BinnedPhaseSpace
C++ and python

Defined in the `BinnedPhaseSpace` package.

Utility to handle bins in a multi-dimensionnal phase space : bin
indexing, bin finding, projection into sub-space, save/load in ROOT
file, etc...




Interactive use example
=====================

JES
----

Let's assume we have already run all the steps of the derivation and
we now want to inspect some particular histograms/graphs produced
during the derivation.
We'll use ipython and have it execute the *exact* same command line we
used to run the derivation except we'll set the "step" option to
"Debug".

So this will look like :
```
ipython
In [1]: %run deriveJES.py  AntiKt10LCTopoTrimmedPtFrac5SmallR20 --inputPattern  "'JZ*root'" -c AntiKt10LCTopoTrimmedPtFrac5SmallR20.py -t AntiKt10LCTopoTrimmedPtFrac5SmallR20Jets -s Debug 
```
Note ipython needs to have double quotes around the `'JZ*root'` argument.

After this command, we
have access to all objects used for the derivation, configured exactly
as they were during the actual derivation. The objects will be
accessible from the `objs` central object.


The objects are defined, but we still need to load their content
from the file in which they were saved. For EtaJES derivation this is :
```
In [2]: objs.readEtaJESFromFile(loadRespPerBin=True)
```
The `loadRespPerBin=True` option tells the function to also load all 
response histos in each bins (there are many, by default those are not
loaded).

Next we use the auto-completion
feature of ipython to quickly see what objects are available. for
example energy response objects are named "R_vs_Exxxx", so we type
```
In [3]: objs.R_<type TAB>
objs.R_vs_Enuminv                objs.R_vs_Enuminv_GroomExt_Func  objs.R_vs_Etrue_Func             objs.R_vs_Etrue_SLGraph
objs.R_vs_Enuminv_Func           objs.R_vs_Enuminv_SLGraph        objs.R_vs_Etrue_Graph            objs.R_vs_Etrue_perEtaBin
objs.R_vs_Enuminv_Graph          objs.R_vs_Etrue                  objs.R_vs_Etrue_GroomExt_Func    
In [3]: objs.R_vs_E 
```
For example to draw the fitted `<R> vs Etrue` function in eta slice 30 :
```
In [19]: objs.R_vs_Etrue_Func[30].Draw()

```


Now let's say we want to look at the response histograms of the 1st pt bin
of  eta slices 23 and 24. Because they are so many of them, the response histos are stored in a  ROOT
sub-directory within the file and we access them indirectly from
`objs` as in : 
```
In [4]: objs.jes_respDir.R_vs_Etrue_23_0
Out[4]: <ROOT.TH1D object ("R_vs_Etrue_23_0") at 0xbe3e780>
```

So we can do for example :
```
In [6]: objs.jes_respDir.R_vs_Etrue_23_0.Draw()
In [7]: objs.jes_respDir.R_vs_Etrue_24_0.SetLineColor(2)
In [8]: objs.jes_respDir.R_vs_Etrue_24_0.Draw("same")
```

JMS
----


JMS histos/graphs can be accessed similarly. The difference is they are
saved in dedicated containers which are aware of the phase space they
represent. For example :
```
In [25]: objs.mR_in_PtEtaMtrue # <--- the TH1F container
Out[25]: <ROOT.BPS::BinnedHistos<TH1F> object ("BPS::BinnedHistos<TH1F>") at 0x4eb6988>

In [26]: objs.mR_in_PtEtaMtrue.m_phaseSpace # <-- the container knows about its phase space
Out[26]: <ROOT.BPS::BinnedPhaseSpace* object ("BPS::BinnedPhaseSpace") at 0xadc1640>
In [27]: objs.mR_in_PtEtaMtrue.m_phaseSpace.showStruct() 
                                      pt : 18 bins 
                                       m : 24 bins 
                                  abseta : 4 bins 

```

The container is available but its content, the TH1F, is not loaded by default
(many containers x many histos/graphs). So load it and then access one
histogram at bin (10,4,0) :
```
In [28]: objs.mR_in_PtEtaMtrue_py.loadContent()
loading  mR_in_PtEtaMtrue <ROOT.TFile object ("jms_PtMBins/AntiKt10LCTopoTrimmedPtFrac5SmallR20_JMS_mBins.root") at 0xa624250>

In [29]: objs.mR_in_PtEtaMtrue_py.at(10,4,0)
Out[29]: <ROOT.TH1F object ("JMSbins_mR_in_PtEtaMtrue_10_4_0") at 0xdc4caf0>

In [30]: objs.mR_in_PtEtaMtrue_py.at(10,4,0).Draw()
```

Plotting
----------
The `Plotting.py` module contains helper classes to produce series of
validation plots.



Details on JMS derivation 
==========================

Response definition
--------------------
As discussed in the introduction, invidual jet responses are grouped
in bins of jet quantities defining the phase space of the calibration.

Several types of phase space or "bin system" are available. An example
 is $`(p_T,m,\eta)`$ but they are several options and the default
 binning system is $`(E,log(m)/E,\eta)`$ (see the option
 `objs.binSystem` in the config files).
 

As discussed also in the introduction, bins are evaluated using true mass (when evaluating raw responses or
closure responses in `ResponseBuilder::jmsRvsEtrue()` or
`ResponseBuilder::jmsClosure()`) or in numerically inversed mass (in `ResponseBuilder::jmsRvsNuminv()`).
E and eta are always evaluated in reco bin : it is assumed that thanks
to the EtaJES calibration, this is no different than using true bins.


Very Fine Bins variant
----------------------

In order to improve the smoothness of the response maps an optional
"very fine bins" technique is available.

The idea is to use much smaller bins in all 3 dimensions and to
compensate the lower statistics per bin by allowing a jet to
contribute to several neighbouring bins.
More precisely if a jet (m,pT,eta) belongs to a bin 'bin0' centered on
(m0,pT0,eta0) then this jet will contribute to all the bins in a cube
around bin0 with a weight wi such as 
```math
w_i \sim e^{ \frac{(m-m_i)^2}{\sigma(m_i)^2} +
\frac{(p-pT_i)^2}{\sigma(pT_i)^2} +\frac{(\eta-\eta_i)^2}{\sigma(\eta_i)^2} )}
```

And each response histograms is filled like :

```
for i in bins_around(bin0) {
    histo_i->Fill(mass_response, w_i*w_event);
 }
```

As a result we obtain very dense and pre-smoothed response maps (see below) in
which each point as a relatively small stat errors (which are then 
correlated but this does not matter in this context). The smoothing
procedure applied on these almost regular maps is then much less
likely  to produce artifacts due to low stats or outlying points.
(C++ class : `FineResponseBuilder`, configuration script
  `AntiKt10LCTopoTrimmedPtFrac5SmallR20_finebins.py`).


Evaluation of an average response per bin 
-------------------

Once the response histograms in each bin are filled, we evaluate an
"average" response for each of them.
 - perform a 3-pass recursive gaussian fit (done in fitJMS(), which
 uses the `JES_ResponseFitter` package. Configured as
 objs.balanceFitter_jms. Default TH1::Fit() option : "RWLQ0S"). The
 mean of the fitted gaussian is taken as the response for the bin.
 - if the fit fails, we evaluate the mode of the response distribution. A special procedure to
 avoid fluctuations is applied (see `uniModalSmoothMax()` in Utilities.cxx)
 - if statistics are too low (GetEffectiveEntries()<100) we just take the
 mean as the fit procedure is not reliable in this case.
 - if the stat is *very* low (GetEffectiveEntries() < 8), the bin is
 marked as invalid, a response ==1 is assigned with 100% error. 


Merging of very low stat bins at low mass
-------------------------
(This part is not necessary and not performed when using the very fine
binnning option)


As described above bins with very low stats are marked as
invalid. Ignoring them or assigning arbitrary value even with large
errors  can distrub the smoothing procedure below.
To avoid this we merge contiguous low mass/low stat bins until the
merger contains enough stats.
We then assign the response evaluated on the merger to each individual
bins part of the merger.

See `mergeVeryLowMassLowStatBins()` in deriveJES.py


Low mass re-evaluation
-----------------------
(This part is not necessary and not performed when using the very fine
binnning option)

In the lowest mass (or log(m/E)) bin, the statistics is very low and responses are
very wide. As we observe that the mass response is generally
monotonous with the (true) mass  (or log(m/E)), we apply a regulation procedure to
replace any fitted response which breaks this monotony, so that in the
end we always have `response_i > response_j if truemass_i < truemass_j`. 

By default the procedure is to work on each (pt,eta) slice. In such
slices, we take the 3 lowest mass points for which
the response decrease monotically and fit them by a straight line
(response vs mass). Then we replace the responses at mass points which
break monotony with the evaluation at the fitted line.

Optionnaly we can instead fit with a parrabola, or do a very simple
linear extrapolation.

This procedure is not applied on numerically inversed mass because the
response may not be monotonous.

This is done by the `reEvalJMSatLowM()` function in deriveJES.py


Response maps
-------------
For each eta bin we then create a map of the response vs (pt,m) (or
whatever binning coordinates is used).
A `TGraph2DErrors` is created for each eta bin where the `(x,y,z, z_err)`
points correspond to `(ptbinCenter_i, massbinCenter_i, mresp_i,
mresp_err_i)`.

Additionnal points are added at low mass and  low pT (that is below the 1st pt
and m bins) to avoid freezing effects in num inv procedures. This
additionnal points are just linearly extrapolated from the 2 lowest
bins. ONLY when binSystem=="PtMBins".

Done with `buildJMSvsPtM()` in deriveJES.py


Smoothing procedure
-------------------

A 2D gaussian kernel smoothing procedure is apply to each
TGraph2DErrors build in the previous step.
That is we create a smooth function of mass response vs (pT,m) 
(or vs (E,log(m/E)) for each eta bin.
The smoothed version are returned in the form of TH2F histos stored in
a BinnedTH2 container.

See `smoothJMSvsPtM()` in deriveJES.py and KernelSmoothing.h/cxx.
The effective gaussian widths in the 2D plan are set in the
configuration file (see buildSmoother() invocation in config files).


Additional documentation and examples
=================================
See the `doc/` directory in this package.
