## ####################################
## This module defines utilities to draw plots comparing
## JMS results obtained by different strategies.
## 
import ROOT

from DeriveJetScales.Plotting import JMSPlots

class Comparator(JMSPlots):
    """Helper class to to draw plots comparing different JMS results.

    The JMS results to be compared are retrieved as part of CalibrationObjects, one per JMS result
    to be compared.
    CalibrationObjects are kept in member `objsList`

    CalibrationObjects are instantiated from PyHelpers.voidCalibrationObjects and
    are then pointed to existing JMS data in directories like 'jms_XXXXX/JetContainer_JMS.root'
    See helper functions below.

    Example usage :
    # edit the defaultList() function to set the CalibrationObjects to be compared.
    # (or call comp.setObjsList() below)
    from DeriveJetScales.JMSComparator import Comparator, comp
    # compare 
    comp.drawLargeMassBins("mR",suffix='pdf' )
    comp.drawLargeMassBins("mIQR",suffix='pdf' )
    


    """
    objsList = []

    def __init__(self, objs):
        JMSPlots.__init__(self, objs)

    def drawAllResp(self, bin=None, suffix="pdf", respContainer="mR_in_PtEtaMtrue_py", details=False, normalize=False, drawUncalib=True, outTag="allRespComp"):
        self.defaultList()
        self.preApplyStyle = False
        oList= []
        if drawUncalib:
            oList = [ getattr(self.objsList[0], respContainer.replace("closure_",""))  ]
            oList[0].updateHistoDesc(Title="uncalibrated", LineColor=ROOT.kBlack)
        for o in self.objsList :
            cont = getattr(o, respContainer)
            cont.updateHistoDesc(**o.localStyle)
            if normalize:
                cont.loadContent()
                for h in cont.m_binContent:
                    n = h.Integral()
                    if n>0 : h.Scale( 1./n )
            oList += [ cont ] 
        JMSPlots.drawAllResp(self,  bin=bin , oList =  oList, drawFitFunc=False, suffix=suffix, details=details, outTag=outTag)


    def drawLargeMassBins(self,var = "mR", bin=None,suffix='png', outName = '_Compare_{var}_vsPtLargeMassBins',
                          graphContName=None, refGraphName=None, refHistoStyle={}, yrange=(0., 1.2), legPos=(0.6,0.4, 0.85, 0.55)) :
        self.defaultList()
        o0=self.objsList[0]
        self.preApplyStyle = False
        from DeriveJetScales.DrawUtils import buildPage
        if graphContName is None:
            graphContName = "closure_"+var+"_vs_pttrue_in_largeMassBins"
            if refGraphName is None:
                refGraphName = var+"_vs_pttrue_in_largeMassBins"
        for o in self.objsList :
            getattr(o,graphContName).updateHistoDesc(**o.localStyle)
        
        if refGraphName !="":
            getattr(o0,refGraphName).updateHistoDesc(**refHistoStyle)
        else:
            getattr(self.objsList[0],graphContName).updateHistoDesc(**refHistoStyle)

        graphList = []
        if refGraphName != "":
            gCont = getattr(o0,refGraphName)
            gCont.updateHistoDesc(Title="uncalibrated")
            graphList += [self.prepare( gCont)]
        graphList += [ self.prepare( getattr(o,graphContName) ) for o in self.objsList ]
        npage = 2 if bin is None else 1

        self.drawMultiGraphs(graphList,                              
                             page= buildPage(suffix,npage, self.objs.jmsFile(outName.format(var=var) )),
                             legPos=legPos,
                             bin=bin, yrange=yrange, )
        
                          


    def compareJMSCorrections(self, X, fixedCoords, fixedValues, yrange=(0.2,1.2) ,**args):
        grList = []
        g = ROOT.TMultiGraph()
        self.defaultList()
        useTrueBins = args.get("useTrueBins",False)
        jmsBinGetter = lambda o : o.respBuilder.m_jmsTrueBin if useTrueBins else o.respBuilder.m_jmsRecoBin
        for o in self.objsList:
            jmsBins = jmsBinGetter(o)
            if not jmsBins.m_correctionFactors3d:
                jmsBins.m_correctionFactors3d = o.mRsmooth_3Dnuminv
            jmsBins.initCorrections()
            func = args.get('jmsCorrection',None)
            if func is not None:
                # translate into a function dedicated to current object o
                func = getattr(jmsBins, func)                
            else: func = jmsBins.jmsCorrection
            gr = self.calibFactorToGraph( X, fixedCoords, fixedValues, calibObjs=o, titleSuff=" "+o.tag, jmsCorrection=func)
            grList.append(gr)
            g.Add(gr)
        g.Draw("a plc ")
        g.GetXaxis().SetTitle(grList[0].GetXaxis().GetTitle())
        g.GetYaxis().SetTitle(args.get("ytitle","JMS calib factor") )
        g.SetMaximum(yrange[1])
        g.SetMinimum(yrange[0])
        from DeriveJetScales.Plotting import buildLegend
        if 'legPos' in args:
            leg = buildLegend(grList, legPos=args['legPos'])
        else:
            leg = buildLegend(grList )
        leg.Draw()
        return g, leg


    def multiCompareJMSCorr(self, X, fixedCoords, fixedValues,yrange=(0.2,1.2) , suffix='pdf', npage=2, **args ):
        from DeriveJetScales.DrawUtils import buildPage
        page =  buildPage(suffix,npage, 'calibFactorVs'+X+"At"+'_'.join(fixedCoords))
        objs = []
        for fvalues in fixedValues:
            objs.append( self.compareJMSCorrections(X,fixedCoords, fvalues, yrange, **args))
        
            page.nextPad()
        page.forceSave()
        page.close()
        return page, objs
        
    # the function belows are used to define which jms results to compare
    # hack the lists or make use of setObjsList() in a script or interactive session.
    def defaultList(self):
        if self.objsList == []:
            self.setObjsList( [
                #newObjs_mBins(),
                newObjs_ElogEBins_ni("_EbinCalib/"),
                newObjs_ElogEBins_ni_etcalib("_EtbinCalib"),
                #newObjs_EtlogEtBins_ni(),
                #newObjs_ElogEBins_noni(),
                ] )

    def setObjsList(self, l):
        self.objsList = l
        self.objs  = l[0]



def newObjs(fname="", **args):
    """Create a void CalibrationObjects from file fname.
    the optionnal arguments are passed to PyHelpers.voidCalibrationObjects() and allow to
    set some desired JMS calibration options onto the newly created CalibrationObjects
    
    """
    from DeriveJetScales.PyHelpers import voidCalibrationObjects
    tag = args.pop('tag', None)
    newobjs = voidCalibrationObjects(**args)
    newobjs.tag = tag
    
    newobjs.localStyle = dict()
    
    if fname != "":
        newobjs.fOut_jms = ROOT.TFile(fname)    
        newobjs.readJMSFromFile(newobjs.fOut_jms, False)
        if hasattr(newobjs, 'mRsmooth_3D'):
            newobjs.respBuilder.m_jmsRecoBin.initCorrections(newobjs.mRsmooth_3D)
    return newobjs



# default style
styleD = dict( LineWidth = 2 , FillColor=ROOT.kWhite)

def updateStyle(o, **args):
    """Set graphic options in args into the localStyle of the CalibrationObjects o.
    returns o
    """
    # if o.tag exists ,duplicate as Title
    if 'Title' not in args and hasattr(o,'tag'):
        args['Title'] = o.tag
    localStyle = dict( styleD)
    localStyle.update( **args)
    o.localStyle = localStyle
    return o


####################################
# Short cut functions to create various CalibrationObjects corresponding to
# different JMS calibration options
#
def newObjs_mBins( ):
    o = updateStyle(newObjs( "jms_PtMBins_ni//AntiKt10LCTopoTrimmedPtFrac5SmallR20_JMS.root", binSystem="PtMBins",
                             useNoMNumInv = False,),                             
                    LineColor = ROOT.kBlue, Title = "(m,p_{T}) bins" )
    o.tag = "(m,p_{T})"
    return o


def newObjs_ElogEBins_ni( modif="" ):
    o= updateStyle(newObjs( "jms_ELogMoEBins_ni"+modif+"/AntiKt10LCTopoTrimmedPtFrac5SmallR20_JMS.root",binSystem="ELogMoEBins",
                             useNoMNumInv = False,),
                   LineColor = ROOT.kViolet, Title = "(E,log(m/E)) calib, NI " )
    o.tag = "(E,log(m/E))"
    return o


def newObjs_ElogEBins_ni_etcalib( modif="_EtbinCalib" ):
    o= updateStyle(newObjs( "jms_ELogMoEBins_ni"+modif+"/AntiKt10LCTopoTrimmedPtFrac5SmallR20_JMS.root",binSystem="ELogMoEBins",
                             useNoMNumInv = False,),
                   LineColor = ROOT.kRed, Title = "(Et,log(m/Et)) calib, NI " )


    # create a EtLogMoEtBins object to have the right phase space and correction factors needed to
    # properly compare jms corrections in E-bins vs Et-Bins
    otmp=newObjs("jms_EtLogMoEtBins_ni/AntiKt10LCTopoTrimmedPtFrac5SmallR20_JMS_calibFactors.root", binSystem="EtLogMoEtBins", useNoMNumInv=False)
    print otmp.jmsBPS
    o.mRsmooth_3D = otmp.mRsmooth_3D
    
    o.respBuilder.m_jmsRecoBin = ROOT.DJS.EtLogMovEtBinning()
    o.respBuilder.m_jmsRecoBin.m_bps = otmp.jmsBPS
    o.respBuilder.m_jmsRecoBin.initCorrections(o.mRsmooth_3D)
    o.tag = "(Et,log(m/Et))"
    return o


def newObjs_EtlogEtBins_ni( ):
    o= updateStyle(newObjs( "jms_EtLogMoEtBins_ni/AntiKt10LCTopoTrimmedPtFrac5SmallR20_JMS.root",binSystem="EtLogMoEtBins",
                             useNoMNumInv = False,),
                   LineColor = ROOT.kRed, Title = "(Et,log(m/Et)) calib, NI " )
    o.tag = "(Et,log(m/Et))"
    return o
                    
                    

def newObjs_ElogEBins_noni( ):
    o= updateStyle(newObjs( "jms_ELogMoEBins_noni/AntiKt10LCTopoTrimmedPtFrac5SmallR20_JMS.root" ,binSystem="ELogMoEBins",
                            useNoMNumInv = True,),
                   LineColor = ROOT.kGreen, Title = "(E,log(m/E)) calib, no NI" )
    o.tag = "(E,log(m/E)) no NI"
    return o


# predefine an isntance
comp = Comparator(None)
