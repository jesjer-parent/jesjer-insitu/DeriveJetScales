import ROOT

def buildLegend(gObjects, legPos=(0.3,0.21,0.5,0.41) , ncol=1, style='pl'):
    leg = ROOT.TLegend(*legPos )
    leg.SetNColumns(ncol)
    for o in gObjects:
        leg.AddEntry(o, o.GetTitle(), style)
    leg.Draw()
    return leg

class Page(object):
    """Base class to draw and save series of pages containing 1 or several plots each.
    This class is not to be instantiated alone.
    It is expected to be be inherited by a concrete class in the form
    class ConcretePage(Page,FormatClass) : ...
    where FormatClass is one of the class below and is used to specify how the multiple pages
    will be saved (1 single pdf file or multiple pdf/svg/png/root files).

    The buildPage() function below does that automatically to instantiate a concrete page object.

    
    """
    nplots=1
    pname = ""
    curpad=0
    savedImg = []

    graphObjects = [] # a list to keep temporary graphic objects 

    dir = ''
    
    def __init__(self, nplots=1):
        self.nplots = nplots
        self.savedImg = []
        self.graphObjects = []

    def pad(self):
        return self.canvas.GetPad(self.curpad)
    
    def nextPad(self):
        """Change current active page to next TPad within the TCanvas  """
        if self.nplots>1:
            self.curpad += 1
        self.saveIfFull()
        p=self.canvas.cd(self.curpad)
        #print " moving to ",p, self.curpad 
        return p

    def resetCanvas(self):
        #print ' resetting canvas !!'
        self.canvas.cd(0)
        p =self.canvas
        self.canvas.Clear()
        #if self.suffix == '.pdf': self.canvas.SetFixedAspectRatio(True)
        self.curpad=0
        self.divide()
        if self.nplots>1:
            self.curpad = 1
            p=self.canvas.cd(1)
            #print '---> ',p
        self.graphObjects = []        
        return p
    
    def saveIfFull(self, n=None):
        """ """
        if self.curpad > self.nplots or self.nplots==1:
            self.save(n)
            self.resetCanvas()

    def forceSave(self, n = None):
        sn = self.pname if n is None else n        
        self.save(sn)
        
    def divide(self):
        pass


    def setNplots(self, i):
        print "Setting nplots ",i
        if isinstance(i,int):
            self.nplots = i
            self.divide = [ self.divide_1,self.divide_2,self.divide_3,self.divide_4 ][i-1]
        else:
            i,j = i
            self.nplots = i*j
            def divid():
                self.canvas.Divide(i,j)
            self.divide = divid
        return self.resetCanvas()
        
    def divide_1(self):
        self.curpad = 0
        pass

    def divide_2(self):
        self.canvas.Divide(1,2)

    def divide_3(self):
        self.canvas.Divide(2,2)

    def divide_4(self):
        self.canvas.Divide(2,2)

    def currentPad(self):
        return self.canvas.GetPad(self.curpad)


class ImgSaver(object):
    suffix = '.png'
    def open(self,n):
        self.dir = n
        
    def save(self, pname=None):
        if pname is None: pname = self.pname
        sn = self.dir+pname+self.suffix
        self.canvas.SaveAs(sn)
        self.savedImg.append(sn)

    def close(self):
        pass

class PdfSaver(object):
    suffix = '.pdf'


    def open(self,name):

        self.name = name            
        self.canvas.Print(self.name+'[') #open
        self.nPagePrinted = 0
        ROOT.gErrorIgnoreLevel = ROOT.kInfo

    def save(self, pname):
        
        pname = "" if pname is None else pname
        print ' saving ',pname , self.canvas
        self.canvas.Print(self.name, "Title:"+ pname)
        self.nPagePrinted += 1

    def close(self):
        ROOT.gErrorIgnoreLevel = ROOT.kPrint
        self.canvas.Print(self.name+']') #close


class NoSaver(ImgSaver):
    def saveIfFull(self):
        if self.curpad>self.nplots:
            self.curpad =0
        pass
    def save(self, pname=None):
        pass
    
class SvgSaver(ImgSaver):
    suffix = '.svg'

class PdfImgSaver(ImgSaver):
    suffix = '.pdf'

class EpsImgSaver(ImgSaver):
    suffix = '.eps'

class RootMacroSaver(ImgSaver):
    suffix = '.root'

def buildPage(format, nplots, name, canvas=None):
    print 'buildPage ', format, nplots, name
    if format is None or format == "":
        format = NoSaver
    if format =='png':
        format = ImgSaver
        name = name
    if format =='pdf':
        format = PdfSaver
        if not name.endswith('.pdf'): name = name+'.pdf'        
    if format =='svg':
        format = SvgSaver
        name = name
        
    if canvas is None:
        try:
            canvas=ROOT.hCanvas
        except:
            canvas = ROOT.TCanvas("hCanvas","hCanvas",1024,768) # Use ~ standard width and height, otherwise strange pdf page
                                                                # formating happens and depends on screen resolution (!?)
    # Generate a new type 'Pager' combining Page and the output format :
    Pager = type("Pager",(format,Page,), {}) 
    page = Pager()
    page.canvas = canvas
    page.setNplots(nplots)
    page.resetCanvas()
    page.open( name)
    return page
