####################################################3
##
## This module implements numerical inversion techniques for JMS
## Contrary to the original techniques in CalibratioDerivation.py, the
## numerical inversion is not evaluated by looping over jets and collecting responses in
## numericaly inversed bin.
## Instead the response in true bins are directly re-assigned to bins defined by 'M*R(M)'
## 
## Various options are implemented to mitigate the non-closure which necessarily occur when R(Mtrue) is varying
## fast w.r.t the Mreco/Mtrue distributions resolution at each Mtrue point.
##
import ROOT

# we use numpy as a convenient and fast way to manipulate arrays
import numpy as np


def graphAsNPArray(g):
    """ Returns the TGraph as numpy arrays. The arrays are pointing on the TGraph data, so modifying 
    the arrays will modify the TGraph.
    returns a tuple (x,y) if g is a TGraph
    returns a tuple (x,y,z) if g is a TGraph2D
    """
    from numpy import frombuffer
    resizeBuff = ROOT.gROOT.GetVersionInt() < 61200
    n = g.GetN()
    bx = g.GetX()
    by = g.GetY()
    if resizeBuff:
        bx.SetSize(n*8) # numb of bytes
        by.SetSize(n*8) # numb of bytes
    res = (frombuffer(bx,dtype=np.double), frombuffer(by,dtype=np.double))
    if hasattr(g,'GetZ'):
        bz = g.GetZ()
        if resizeBuff: bz.SetSize(n*8) # numb of bytes
        res = res + (frombuffer(bz,dtype=np.double),)
    return res
        

def histoAsNPArray(h2):
    """ Returns an array pointing on the bin content of the histogeam.
    modifying the array will modify the content of the histogram.
    """
    ndim, dtype = {
        ROOT.TH2F : (2 , np.float32),
        ROOT.TH2D : (2 , np.float64),
    }[h2.__class__]
    shape = [
        lambda x:(), lambda h : (h.GetNbinsX()+2,), lambda h : (h.GetNbinsX()+2,h.GetNbinsY()+2),
    ][ndim](h2)
    
    a = np.ndarray( shape, dtype, h2.GetArray() , order='F')
    return a

class DirectNumInverser:
    """Class implementing a direct numerical inversion from response stored in a DJS::CalibrationObjects 
    
    For now the num inv is perform from the true responses stored in  'mRsmooth_vs_PtMtrue_in_Eta' container.
    """
    def __init__(self, objs,  outTag="3Dnuminv", etaAxis=None, ):
        self.inputBPS = objs.jmsBPS
        self.etaAxis = etaAxis if etaAxis is not None else self.inputBPS.m_a3
        self.objs = objs
        self.niOpts = objs.jmsNIopts
        self.outTag = outTag
        
        self.respHistoContainer = objs.mRsmooth_vs_PtMtrue_in_Eta_py
        self.iqrHistoContainer  = objs.mIQRsmooth_vs_PtMtrue_in_Eta_py
        self.respHistoContainer.loadContent()
        self.iqrHistoContainer.loadContent()
        # create a vector<TH2F> to hold the num inv histos :
        self.nirespHistoContainer = self.respHistoContainer.m_binContent.__class__()
        
        
    def doNumInv(self, writeOuput=True ):
        """Perform num inversion and 'flatening' of inversed response according to options passed in objs.jmsNIopts """
        if self.objs.binSystem != "ELogMoEBins":
            print " ERROR ! bining type ", objs.binSystem, " not supported yet "
            print "         use ELogMoEBins  or updte the implementation of DirectNumInverser "
            print ""

        niOpts = self.niOpts
        print niOpts
        for ieta in range(self.etaAxis.nBins()):
            print " direct numerical inversion eta slice ", ieta
            if niOpts.forceSuperIncrResp : self.forceSuperIncreasingResp(ieta)
            h2 = self.createNIRespHisto_LogMoE(ieta )
            h2_c = h2
            if niOpts.flattenCalib :h2_c = self.flattenResponse(ieta,h2,)
            if niOpts.adjustFlatCalib: self.nonClosureCombinaison(ieta, h2_c)
            self.nirespHistoContainer.push_back(h2_c)
            
        self.nirespHistoList = list(self.nirespHistoContainer) # keep a python list just to make sure the histos are kept alive.

        return self.saveFinalTH3("mRsmooth_"+self.outTag , writeOuput=writeOuput)
        


    def forceSuperIncreasingResp(self, ieta):
        """ Enforce super increasing toward low masses
        more precisely, we enforce the derivative is increasing.
        ==>  The response histo at ieta is modified 
        """
        hResp = self.respHistoContainer[ieta]
        # use numpy arrays (easier to manipulate)
        respA = histoAsNPArray(hResp)
        stop = 30 # number of mass bins for which we enforce increasing 
        fbin=0  # mass bin at which enforced increasing starts
        ePos = hResp.GetXaxis().GetBinCenter
        mPos = hResp.GetYaxis().GetBinCenter
        from math import exp, log

        for ie in xrange(1,hResp.GetNbinsX()+1 ):
            E = ePos(ie)
            resp =  respA[ie,:][1:-1] #  '[1:-1]' is to exclude overflow bins of TH2 
            delta = resp[fbin:stop-1+fbin] - resp[fbin+1:stop+fbin]

            # make sure delta are increasing when low mass bins decrease (hence the 'reversed' below)
            for j,d in reversed(list(enumerate(delta))): 
                if j== (stop-2): continue
                if d < delta[j+1]:
                    #print 'changing ',i,j, v[j], d
                    delta[j] = delta[j+1]

            # now re-integrate the modified deltas (cumsum = cumulative sum), doing it backward from stop point
            # (hence double usage of flipudx)
            resp[fbin:stop-1+fbin] = np.flipud(np.cumsum(np.flipud(delta)))+resp[stop-1+fbin]
            
            
    def createNIRespHisto_LogMoE(self, ieta ):
        """returns a new  num inv of the TH2 at eta slice ieta """
        hResp = self.respHistoContainer[ieta]

        # create a graph from the response histos
        g2 = ROOT.TGraph2D(hResp)
        # retrieve graph content as numpy array
        ee, logMoE, resp = graphAsNPArray(g2)
        # perform numerical inversion :
        logMoE  += np.log(resp)         
        # --> g2 has now been modified, its Y axis contain log(mR(m)/E) instead of log(m/E).
        
        # convert the TGraph back to a smoothed TH2
        g2.SetName("mRsmooth_ni_"+str(ieta)) 
        g2.SetNpx(hResp.GetNbinsX())
        g2.SetNpy(hResp.GetNbinsY())
        hResp = self.smoothGraph(g2)
        ROOT.DJS.fillEmptyBin(hResp)        
        return hResp

    def flattenResponse(self, ieta, hNIResp):
        """ Flatten the num inversed response given by hNIResp at position ieta.
         The flattening procedure is as follow.
          let : f = d(calib)/dm , we enforce  f(m*R(m))*resol(m)*m < 0.1 by
          numericaly computing f, evaluating at each m*R(m) and replacing it by 0.1/reslo(m)*m if needed.
          We then re-sum the modified f to obtain the modified response.
        """
        hResp = self.respHistoContainer[ieta]
        hIQR  = self.iqrHistoContainer[ieta]

        calib = lambda E,m : 1./hNIResp.Interpolate(E,m)

        # create a copy of responses which will be flattened 
        hNiResp_flat = hNIResp.Clone(hNIResp.GetName()+'corr')

        # alias some functions
        mPos = hResp.GetYaxis().GetBinCenter
        ePos = hResp.GetXaxis().GetBinCenter
        etaPos = self.objs.jmsBPS.m_a3.binCenter
        nM = hNIResp.GetNbinsY()
        
        # access histos as numpy array for easier handling
        nirespA = histoAsNPArray(hNiResp_flat)
        respA  = histoAsNPArray(hResp)
        resolA = histoAsNPArray(hIQR)


        # Flattening procedure :
        # let : f = d(calib)/dm 
        # below, we enforce the inequality :  f(m*R(m))*resol(m)*m < T0 (T0=0.1 by default)
        #
        T0 = self.niOpts.flattenT0
        from math import exp, log
        # do it for each energy slice
        for ie in xrange(1,hResp.GetNbinsX()+1 ):
        #for ie in [10]:
            E = ePos(ie)
            # retrieve arrays corresponding to this energy slice :
            masses = np.array( [ exp(mPos(i))*E for i in xrange(nM) ], dtype=np.float32)
            niresp = nirespA[ie,:][1:-1] #  '[1:-1]' is to exclude overflow bins
            calib = 1./niresp
            resp = respA[ie,:][1:-1] #  '[1:-1]' is to exclude overflow bins
            resol = resolA[ie,:][1:-1] #  '[1:-1]' is to exclude overflow bins
            
            deltamasses = (masses[:-1] - masses[1:])
            dcalib = (calib[:-1] - calib[1:])/ deltamasses # this is (dcalib/dm)(m)
            mass_mid = 0.5*(masses[:-1] + masses[1:])
            deltamasses = np.concatenate( [deltamasses, np.array([deltamasses[-1]])])
            dcalib_m = np.interp( masses , mass_mid, dcalib  ) # this is still (dcalib/dm)(m)...
            # ..but dcalib_m values are now aligned with masses, not mass_mid
            # (we just shifted the mass back to the original mass points)

            #return masses.astype(np.float64), calib.astype(np.float64), dcalib.astype(np.float64) , dcalib_m.astype(np.float64) 
            massresp = masses*resp
            #massW = massresp*resol
            massW = masses*resol
             
            dcalib_mr = np.interp( massresp , masses, dcalib_m)# this is (dcalib/dm)(m*R(m))
            testedValues = dcalib_mr*massW

            #dcalib_m2 = dcalib_m.copy() # for debugging
            #return masses.astype(np.float64), calib.astype(np.float64), dcalib_m.astype(np.float64) ,testedValues.astype( np.float64) #dcalib_mr.astype(np.float64)
            #return masses.astype(np.float64), calib.astype(np.float64), dcalib_m.astype(np.float64) ,dcalib_m2.astype(np.float64)

            T = T0 +0.01*etaPos(ieta)
            pos = (testedValues > T) # find positions violating our inequality
            dcalib_mr[ pos ] = T / (massW)[pos] # enforce the inequality at these positions            
            #return masses.astype(np.float64), calib.astype(np.float64), dcalib_mr.astype(np.float64) ,dcalib_m.astype(np.float64)            

            # now re-evaluate the values of the (dcalib/dm) function for points corresponding to masses instead of masses*resol :
            dcalib_m = np.interp(masses, massresp, dcalib_mr)

            #return masses.astype(np.float64), dcalib_m.astype(np.float64), dcalib_m2.astype(np.float64) ,dcalib_mr.astype(np.float64)

            # we have our modified derivative, now re-integrate it 
            # start from a late bin (high mass) since it's the where the response/calib is  most reliable
            # and do it backward toward lowmasses (hence the double flipud)
            c_start = -20
            #print ieta, ie, E,  ' mass start ', masses[c_start]
            calib_c = calib[c_start] + np.flipud(np.cumsum(np.flipud((dcalib_m*deltamasses)[1:c_start+1])))

            #print  np.argmax(massresp[-1]<masses), nM+c_start
            #return masses.astype(np.float64), calib.astype(np.float64), dcalib_m.astype(np.float64) ,calib_c.astype(np.float64)
            #return masses.astype(np.float64), calib.astype(np.float64), dcalib_m.astype(np.float64) ,dcalib_m2.astype(np.float64)
            #calib2_ = niresp.copy() # for debuging

            # update the response according to the corrected calibration 
            niresp[:c_start] = 1./calib_c
            # (changing  niresp actually changes the content of hNiResp_flat)
            
            #return masses.astype(np.float64), calib2_.astype(np.float64), niresp.astype(np.float64) ,calib_c.astype(np.float64)
            
        return hNiResp_flat

    def nonClosureCombinaison(self, ieta, h2_c):
        calibA = histoAsNPArray(h2_c)
        self.objs.closure_mRsmooth_vs_PtMtrue_in_Eta_py.loadContent()
        closureResp =   self.objs.closure_mRsmooth_vs_PtMtrue_in_Eta_py[ieta]            
        closureA = histoAsNPArray(closureResp)            
        for ie in xrange(1,closureResp.GetNbinsX()+1 ):
            calib_ = calibA[ie,:][1:-1] #  '[1:-1]' is to exclude overflow bins
            closureW = 1+abs(1-closureA[ie,:][1:-1]) #  '[1:-1]' is to exclude overflow bins
            calib_[:] = (calib_+closureW*calib2_)/(1+closureW)
            #calib3_ = (calib_+closureW*calib2_)/(1+closureW)
            #print closureW
        
                    
    def saveFinalTH3(self, outTH3name, writeOuput=True ):
        objs = self.objs
        objs.mRsmooth_vs_PtMnuminv_in_Eta.m_binContent.swap(self.nirespHistoContainer)
        h3 = objs.derObject.buildResponseAsTH3(objs.mRsmooth_vs_PtMnuminv_in_Eta, saveAs="")

        objs.derObject.fOut_jms.cd()
        if writeOuput:
            h3.Write(outTH3name, ROOT.TObject.kOverwrite)
            print 'saved ', outTH3name, " in ", objs.derObject.fOut_jms.GetName()
        setattr(objs, outTH3name, h3)
        # also save mRsmooth_vs_PtMnuminv_in_Eta with the new histos.
        # first set correct name and phasespace to mRsmooth_vs_PtMnuminv_in_Eta since it can be uninitialize
        # a this stage 
        objs.mRsmooth_vs_PtMnuminv_in_Eta.m_phaseSpace = self.respHistoContainer.m_phaseSpace
        objs.mRsmooth_vs_PtMnuminv_in_Eta.setName( self.respHistoContainer.name().replace('Mtrue','Mnuminv'))
        # save :
        objs.saveJMSObject(objs.derObject.fOut_jms, objs.mRsmooth_vs_PtMnuminv_in_Eta)
        return h3
        
    def smoothGraph(self,g2, w=0.05):
        bps = self.inputBPS

        # re-use the default smoother
        smoother = self.objs.jms_tg2_smoother
        smoother.m_useErrors = False
        smoother.m_kernel.SetParameter(2,w)
        
        ptmin,ptmax = bps.axis(0).min()  , bps.axis(0).max()
        mmin,mmax = bps.axis(1).min() , bps.axis(1).max()
        model =ROOT.TH2F(g2.GetName()+"h" , "Smoothed mass resp",g2.GetNpx(),ptmin,ptmax, g2.GetNpy() , mmin, mmax )

        # get an histo from the graph
        htmp = g2.GetHistogram()
        # and make sure *all* bins are filled (empty bins will be filled according to closest non-empty bins)
        ROOT.DJS.fillEmptyBin(htmp)        

        # perform smoothing
        smoother.histoToHisto( htmp , model)
        if self.objs.binSystem=="ELogMoEBins":
            self.objs.jms_th1_smoother.smoothAllSlices(model, 0)
        
        return model
