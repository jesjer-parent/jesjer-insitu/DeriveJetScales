import ROOT

# by default, set all stretch factors to 1.
ROOT.KS.initStretchFactor()

def buildSmoother( log=[False], npoints=[100,], kernel=None, gausW=0.07,
                   stretchF = None,
                   maxNpoints = None, maxD2=None, useErrors=False, useTwoPoints=False):
    N=len(log)
    m = ROOT.KS.Smoother(N)()

    if kernel is None:
        kernel = ROOT.TF1("g","gaus",0,1)
        kernel.SetParameter(0,1)
        kernel.SetParameter(1,0)
        kernel.SetParameter(2,gausW)
    m.m_kernel = kernel
    m.m_useErrors = useErrors
    m.m_useTwoPoints = useTwoPoints

    if stretchF :
        m.setStretchFactor(0, stretchF[0] )
        m.setStretchFactor(1, stretchF[1] )

    
    for i, (l,n) in enumerate(zip(log,npoints)):
        m.m_logCoord[i] = l
        m.m_npoints[i] = n

    if maxNpoints: m.m_maxClosePoint = maxNpoints
    if maxD2 is None : maxD2 = (gausW*5)**2
    m.m_maxD2 = maxD2

    return m




def showGausWidths2D(smoother):
    c2=ROOT.KS.CoordND(2,2)
    wg = smoother.m_kernel.GetParameter(2)
    print " width0 = ", wg*smoother.m_stretchFactor[0]
    print " width1 = ", wg*smoother.m_stretchFactor[1]


## extend smoother in 1D

Smoother1D = ROOT.KS.Smoother(1)

def smooth1DSlice( self, h2, pos, smoothedDim=0, htmp=None):
    """ Smooth in place 1 slice of the input TH2 'h2'
    the slice is smoothed along 'smoothedDim' and at index 'pos' in the other dimension.
    """
    axis = h2.GetXaxis() if smoothedDim==0 else h2.GetYaxis()
    pos2d = lambda i : (i,pos) if smoothedDim==0 else lambda i : (pos,i)
    if htmp is None: htmp = ROOT.TH1F()
    ROOT.DJS.sliceTH2(h2, htmp, smoothedDim, pos)
    self.histoToHisto(htmp, htmp)
    for i in xrange(1,axis.GetNbins()+1):
        b = h2.GetBin(*pos2d(i))
        h2.SetBinContent(b, htmp.GetBinContent(i))
    return htmp

Smoother1D.smooth1DSlice =  smooth1DSlice

def smoothAllSlices(self, h2,  smoothedDim=0):
    """ Smooth all 1D slices in the input TH2 by smoothing along smoothedDim"""
    axis = h2.GetXaxis() if smoothedDim==0 else h2.GetYaxis()
    oaxis = h2.GetXaxis() if smoothedDim==1 else h2.GetYaxis()
    htmp = ROOT.TH1F()
    for i in xrange(1,oaxis.GetNbins()+1):
        self.smooth1DSlice(h2,i, smoothedDim, htmp)
    return htmp
Smoother1D.smoothAllSlices =  smoothAllSlices
