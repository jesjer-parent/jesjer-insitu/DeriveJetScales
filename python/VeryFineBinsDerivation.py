###################################################3
##
## This module implements the JMS derivation logic for very fine bins.
## Very fine bins derivation makes use of a different type of histo container (HistoClones instead of BinnedTH1)
## and thus requires some small differences in the derivation logic w.r.t using coarse bins.
##

from BinnedPhaseSpace.BinnedPhaseSpaceConfig import AxisSpec , BinnedPhaseSpace, BinnedTH1, BinnedTH2, BinnedTGraph2DErrors, h1, h2
import ROOT


class FilteredBinnedPhaseSpace(BinnedPhaseSpace):
    """A class to deal with binned phase space where only some of the bins are non-empty.

    When using very fine bins, the phase space is uge (10k to 100K bins) and overlapping. When plotting we want
    only a limited number of relevant plots, not 100K plots, so we use partially filled containers. 

    This class helps to define which bins we want to fill/plot and to retrieve them amongst all possible bins.
    
    """
    def __init__(self, bps , b1, b2, b3):
        """bps is the original BinnedPhaseSpace b1, b2 and b3 define which bin along each dimension will be valid in
        this FilteredBinnedPhaseSpace.
         * if bi is an int, the valid bins aling dim i are range(0,len(dimi), bi) that is 1 bin every bi bins.
         * else bi is expected to be the ordered list of valid bins along dim         
         """
        BinnedPhaseSpace.__init__(self)
        self.__assign__(bps)
        def validBins(dim, d):
            if isinstance(d,int): return range(0,bps.axis(dim).nBins(), d) 
            else : return d
        self.filteredBins1 = validBins(0,b1)
        self.filteredBins2 = validBins(1,b2)
        self.filteredBins3 = validBins(2,b3)

        self.filteredBins1_set = set( self.filteredBins1)
        self.filteredBins2_set = set( self.filteredBins2)
        self.filteredBins3_set = set( self.filteredBins3)

    def isValid(self, bin):
        return  bin[0] in self.filteredBins1_set and bin[1] in self.filteredBins2_set and bin[2] in self.filteredBins3_set

    def isIndexValid(self, index):
        bin = self.iteratorAtIndex( index).asTuple()
        return isValid(bin)

    def buildHistoIfValid(self, testedBin, binnedTH1, index, model):
        h = None
        if self.isValid(testedBin.asTuple() ):
            #print "buildHistoIfValid valid at ", testedBin.asTuple() , index, binnedTH1
            h = binnedTH1.setClone(index, model)
            #h = model.Clone( binnedTH1.nameAt( index ))
            #h.SetName( binnedTH1.nameAt( index ))
            ROOT.SetOwnership(h , False)
            h.SetTitle(binnedTH1.addTagAt(model.GetTitle(), index ))
            #binnedTH1.setObj(index, h)
            
        return h

    def toFullBin(self, bin):
        """ convert a tuple 'bin' describing a position amongs the valid bins to the position in the full original bin space  """
        return (self.filteredBins1[bin[0]] ,
                self.filteredBins2[bin[1]] ,
                self.filteredBins3[bin[2]] ,
                )

    def fullRange(self):
        """Return an iterator on all the valid bins """
        for it in BinnedPhaseSpace.fullRange(self):
            if self.isValid(it.asTuple()):
                yield it

    def range(self, *l):
        """Return an iterator on all the  bins specified by l """
        r = BinnedPhaseSpace.range(self,*l)
        for it in r:
            if self.isValid(it.asTuple()):
                yield it



from DeriveJetScales.CalibrationDerivation import CalibrationDerivation

class FineBinCalibDerivation(CalibrationDerivation):
    """
    An adpatation of the CalibrationDerivation base class needed to deal with the special
    objects used to enable very fine binning (HistoClones).

    FineBinCalibDerivation only redefine the the jms steps.
    """

    
    def jmsRawResp(self):
        objs = self.objs 
        # make sure we have the EtaJES constants loaded :
        objs.readEtaJESFromFile(self.fOut_jes, ["R_vs_Enuminv_Func", "R_vs_Etrue_Func", "DEta_vs_Etrue_Func"] )

        # set the appropriate running mode :
        objs.respBuilder.m_mode = ROOT.DJS.ResponseBuilder.JMS_RvsEtrue
        # run : assign each jet mass response to a bin of the bin space
        objs.respBuilder.processTree(self.tree)
        
        # make sure we write responses out
        self.fOut_jms.cd()
        objs.jmsBPS.write()
        self.fOut_jms.cd()
        if objs.jmsNIopts.noNumInv:
            objs.mR_in_EMEtanuminv_py.saveContent(self.fOut_jms)
        else:
            objs.mR_in_EMEtatrue_py.saveContent(self.fOut_jms)
        
        
    def jmsRawFits(self):
        
        objs = self.objs
        self.fOut_jms.cd()
        self.readJMSFromFile()

        # load the HistoClones (this won't overwrite anything)
        objs.mR_in_EMEtatrue_py.load(self.fOut_jms)
        
        # Fit
        self.fitJMS(objs.mR_in_EMEtatrue_py, saveFuncs=False, responseEval=objs.jmsRespEvaluation)
        # build maps per eta slices
        self.buildJMSvsPtM( (objs.mR_in_EMEtatrue_py, "mR_vs_PtMtrue_in_Eta") )
        self.smoothJMSvsPtM( objs.mR_vs_PtMtrue_in_Eta, "mRsmooth_vs_PtMtrue_in_Eta" , doSave=False)

        if objs.binSystem=="ELogMoEBins":
            # then it is better to re-smooth along the E dimension only
            for h2 in objs.mRsmooth_vs_PtMtrue_in_Eta.m_binContent:
                # perform the 1D smoothing on each E (dimension 0) slices.
                # (smoothAllSlices is an extended function, see KernelSmoothing.py)
                objs.jms_th1_smoother.smoothAllSlices(h2, 0)
        objs.saveJMSObject(self.fOut_jms, objs.mRsmooth_vs_PtMtrue_in_Eta)
        objs.mRsmooth_vs_PtMtrue_in_Eta_py = objs.mRsmooth_vs_PtMtrue_in_Eta 
        
        self.buildJMSvsPtM( (objs.mR_in_EMEtatrue_py, "mIQR_vs_PtMtrue_in_Eta"), useIQR=True )
        self.smoothJMSvsPtM( objs.mIQR_vs_PtMtrue_in_Eta, "mIQRsmooth_vs_PtMtrue_in_Eta" ,doSave=False)
        if objs.binSystem=="ELogMoEBins":
            # then it is better to re-smooth along the E dimension only
            for h2 in objs.mIQRsmooth_vs_PtMtrue_in_Eta.m_binContent:
                # perform the 1D smoothing on each E (dimension 0) slices.
                # (smoothAllSlices is an extended function, see KernelSmoothing.py)
                objs.jms_th1_smoother.smoothAllSlices(h2, 0)
        objs.saveJMSObject(self.fOut_jms, objs.mIQRsmooth_vs_PtMtrue_in_Eta)
        objs.mIQRsmooth_vs_PtMtrue_in_Eta_py = objs.mIQRsmooth_vs_PtMtrue_in_Eta
                
        
        # extract and save the TH3
        h3 = self.buildResponseAsTH3( objs.mRsmooth_vs_PtMtrue_in_Eta, "mRsmooth_3Dtrue")

        filterFunc = objs.plotsBPS.buildHistoIfValid # just an alias
        templName = '{value}_vs_{var}true_in_PtEtatrue'
        self.buildJMSvsX(1, step=(objs.mR_in_EMEtatrue_py, templName), binFilter=filterFunc )# <mR> vs m
        self.buildJMSvsX(1, step=(objs.mR_in_EMEtatrue_py, templName), value='mIQR', binFilter=filterFunc) # <mIQR> vs m

        self.buildJMSvsX(0, step=(objs.mR_in_EMEtatrue_py, templName), binFilter=filterFunc) # <mR> vs pt
        self.buildJMSvsX(0, step=(objs.mR_in_EMEtatrue_py, templName), value='mIQR', binFilter=filterFunc) # <mIQR> vs pt

        # Save the fits we produced, without overwriting the response histograms
        objs.saveJMSObject(self.fOut_jms, objs.mR_in_EMEtatrue, writeObjs=False) # re-save only the new info we added in this step
        

    def jmsNiResp(self):
        if self.objs.jmsNIopts.directNumInv :
            # direct numerical inversion : we skip this step
            return

        objs = self.objs
        objs.readEtaJESFromFile(self.fOut_jes, ["R_vs_Enuminv_Func", "R_vs_Etrue_Func", "DEta_vs_Etrue_Func"] )

        self.fOut_jms.cd()
        self.readJMSFromFile()
        objs.respBuilder.m_jmsTrueBin.initCorrections( objs.mRsmooth_3Dtrue )

        objs.respBuilder.m_mode = ROOT.DJS.ResponseBuilder.JMS_RvsNuminv
        objs.respBuilder.processTree(self.tree)

        objs.mR_in_EMEtanuminv_py.saveContent(self.fOut_jms)


    def jmsNiFits(self):
        objs = self.objs
        self.readJMSFromFile()

        if objs.jmsNIopts.directNumInv :
            # invoke the special object
            from DeriveJetScales.DirectNumericalInversion import DirectNumInverser
            ni = DirectNumInverser(objs, outTag="3Dnuminv")
            ni.doNumInv()
            return

        self.fOut_jms.cd()
        # load the HistoClones (this won't overwrite anything)
        objs.mR_in_EMEtanuminv_py.load(self.fOut_jms)

        self.fitJMS(objs.mR_in_EMEtanuminv_py, saveFuncs=False, responseEval=objs.jmsRespEvaluation)
        self.buildJMSvsPtM( (objs.mR_in_EMEtanuminv_py, "mR_vs_PtMnuminv_in_Eta") )
        self.smoothJMSvsPtM( objs.mR_vs_PtMnuminv_in_Eta, "mRsmooth_vs_PtMnuminv_in_Eta" )
        h3 = self.buildResponseAsTH3( objs.mRsmooth_vs_PtMnuminv_in_Eta, "mRsmooth_3Dnuminv")

    def loadMassCombConstants(self):
        objs = self.objs
        if objs.respBuilder.m_jmsRecoBin.m_correctionFactors3d:
            # assume everything was set correctly
            return
        fcalo = ROOT.TFile(objs.jmsFile().replace("mComb.root", "mCalo.root"))
        objs.respBuilder.m_jmsRecoBin.initCorrections( fcalo.mRsmooth_3Dnuminv )
        objs.respBuilder.m_jmsRecoBin.m_sigmas3d = fcalo.IQRsmooth3D
        fTA = ROOT.TFile(objs.jmsFile().replace("mComb.root", "mTA.root"))
        fTA.mRsmooth_3Dnuminv.SetDirectory(0)
        objs.mCombiner.m_mTACalib.initCorrections(fTA.mRsmooth_3Dnuminv)
        objs.mCombiner.m_mTACalib.m_sigmas3d = fTA.IQRsmooth3D

        objs.mcomb_files = (fcalo, fTA)

    def jmsClosureResp(self):
    
        objs = self.objs
        objs.readEtaJESFromFile(self.fOut_jes, ["R_vs_Enuminv_Func", "R_vs_Etrue_Func", "DEta_vs_Etrue_Func"] )

        self.fOut_jms.cd()
        self.readJMSFromFile(loadHistos=False)


        # enable jmsCorrection using TH3F,
        # Associate a TH3F to our JMSBinning helper if needed an make sure it is properly inited :
        if not objs.respBuilder.m_jmsRecoBin.m_correctionFactors3d:
            objs.respBuilder.m_jmsRecoBin.m_correctionFactors3d = objs.mRsmooth_3Dnuminv
        objs.respBuilder.m_jmsRecoBin.initCorrections()

        objs.respBuilder.m_mode = ROOT.DJS.ResponseBuilder.JMS_Closure
        objs.respBuilder.processTree(self.tree)

        objs.jmsinclPtBPS.write()
        objs.saveJMSObject(self.fOut_jms, objs.closure_mR_in_inclPtEtatrue)
        objs.saveJMSObject(self.fOut_jms, objs.mR_in_inclPtEtatrue)

        objs.mR_in_EMEtareco_py.saveContent(self.fOut_jms)
        objs.closure_mR_in_EMEtatrue_py.saveContent(self.fOut_jms)

        

    def jmsClosureFits(self):
        objs = self.objs
        self.fOut_jms.cd()
        self.readJMSFromFile(loadHistos=False)

        # closure fits and graphs
        # load the HistoClones (this won't overwrite anything)
        objs.closure_mR_in_EMEtatrue_py.load(self.fOut_jms)        
        self.fitJMS(objs.closure_mR_in_EMEtatrue, saveFuncs=False, responseEval=objs.jmsRespEvaluation)

        fOut_jms=self.fOut_jms
        # alias to workaround C++/python references issues.            
        objs.mR_in_inclPtEtatrue_py = objs.mR_in_inclPtEtatrue
        objs.closure_mR_in_inclPtEtatrue_py = objs.closure_mR_in_inclPtEtatrue
        objs.mR_in_inclPtEtatrue_py.loadContent( fOut_jms)
        objs.closure_mR_in_inclPtEtatrue_py.loadContent(fOut_jms)

        self.fitJMS(objs.mR_in_inclPtEtatrue_py, excludeLargeMoPtBins=False, saveFuncs=True, responseEval=objs.jmsRespEvaluation)
        self.fitJMS(objs.closure_mR_in_inclPtEtatrue_py, excludeLargeMoPtBins=False, saveFuncs=True, responseEval=objs.jmsRespEvaluation)
        objs.mR_in_inclPtEtatrue_py.saveContent(fOut_jms, writeObjs=False) # re-save only the new info we added in this step
        objs.closure_mR_in_inclPtEtatrue_py.saveContent(fOut_jms, writeObjs=False) # re-save only the new info we added in this step


        templName = '{value}_vs_{var}true_in_largeMassBins'
        self.buildJMSvsX( 0, step=(objs.mR_in_inclPtEtatrue_py,templName) ) # <mR> vs pt
        self.buildJMSvsX( 0, step=(objs.mR_in_inclPtEtatrue_py,templName), value='mIQR') # <mIQR> vs pt

        templName = 'closure_{value}_vs_{var}true_in_largeMassBins'
        self.buildJMSvsX( 0, step=(objs.closure_mR_in_inclPtEtatrue_py,templName) ) # <mR> vs pt
        self.buildJMSvsX( 0, step=(objs.closure_mR_in_inclPtEtatrue_py,templName), value='mIQR') # <mIQR> vs pt

        templName = '{value}_vs_{var}true_in_largeMassBins'
        self.buildJMSvsX( 2, step=(objs.mR_in_inclPtEtatrue_py,templName) ) # <mR> vs eeta
        self.buildJMSvsX( 2, step=(objs.mR_in_inclPtEtatrue_py,templName), value='mIQR') # <mIQR> vs eta

        templName = 'closure_{value}_vs_{var}true_in_largeMassBins'
        self.buildJMSvsX( 2, step=(objs.closure_mR_in_inclPtEtatrue_py,templName) ) # <mR> vs eta
        self.buildJMSvsX( 2, step=(objs.closure_mR_in_inclPtEtatrue_py,templName), value='mIQR') # <mIQR> vs eta
        


        filterFunc = objs.plotsBPS.buildHistoIfValid ## just an alias
        templName = 'closure_{value}_vs_{var}true_in_PtEtatrue'
        self.buildJMSvsX(1, step=(objs.closure_mR_in_EMEtatrue_py, templName), binFilter=filterFunc ) #<mR> vs m
        self.buildJMSvsX(1, step=(objs.closure_mR_in_EMEtatrue_py, templName), value='mIQR', binFilter=filterFunc)  #<mIQR> vs m

        self.buildJMSvsX(0, step=(objs.closure_mR_in_EMEtatrue_py, templName), binFilter=filterFunc) # <mR> vs pt
        self.buildJMSvsX(0, step=(objs.closure_mR_in_EMEtatrue_py, templName), value='mIQR', binFilter=filterFunc) # <mIQR> vs pt

        self.buildJMSvsPtM( (objs.closure_mR_in_EMEtatrue, "closure_mR_vs_PtMtrue_in_Eta") )
        self.smoothJMSvsPtM( objs.closure_mR_vs_PtMtrue_in_Eta, "closure_mRsmooth_vs_PtMtrue_in_Eta" )

        self.buildJMSvsPtM( (objs.closure_mR_in_EMEtatrue_py, "closure_mIQR_vs_PtMtrue_in_Eta"), useIQR=True )
        self.smoothJMSvsPtM( objs.closure_mIQR_vs_PtMtrue_in_Eta, "closure_mIQRsmooth_vs_PtMtrue_in_Eta" ,doSave=False)
        objs.saveJMSObject(self.fOut_jms, objs.closure_mIQRsmooth_vs_PtMtrue_in_Eta)
        

    def jmsIQRFits(self):
        objs = self.objs
        self.fOut_jms.cd()
        self.readJMSFromFile(loadHistos=False)

        objs.mR_in_EMEtareco_py.loadContent()
        
        self.fitJMS(objs.mR_in_EMEtareco_py, saveFuncs=False, responseEval="gaus")

        self.buildJMSvsPtM( (objs.mR_in_EMEtareco_py, "IQR_vs_PtMreco_in_Eta"), useIQR=True )
        self.smoothJMSvsPtM( objs.IQR_vs_PtMreco_in_Eta, "IQRSmooth_vs_PtMreco_in_Eta") 

        self.buildJMSvsPtM( (objs.mR_in_EMEtareco_py, "mR_vs_PtMreco_in_Eta") )
        self.smoothJMSvsPtM( objs.mR_vs_PtMreco_in_Eta, "mRSmooth_vs_PtMreco_in_Eta" ) 

        for i,h in enumerate(objs.IQRSmooth_vs_PtMreco_in_Eta.m_binContent):
            h.Divide(objs.mRSmooth_vs_PtMreco_in_Eta[i])
        h3 = self.buildResponseAsTH3( objs.IQRSmooth_vs_PtMreco_in_Eta, "IQRsmooth3D")

    def jmsClosureMassCombResp(self):
        
        objs = self.objs
        if objs.massType != "mComb":
            print "    ******* ERROR ********* jmsClosureMassCombResp must be run using massType=mComb, not " , objs,massType 

        objs.readEtaJESFromFile(self.fOut_jes, ["R_vs_Enuminv_Func", "R_vs_Etrue_Func", "DEta_vs_Etrue_Func"] )

        self.fOut_jms.cd()
        #self.readJMSFromFile(loadHistos=False)

        # enable jmsCorrection using TH3F,
        # Associate a TH3F to our JMSBinning helper if needed an make sure it is properly inited :
        self.loadMassCombConstants()


        objs.respBuilder.m_mode = ROOT.DJS.ResponseBuilder.JMS_ClosureMassComb
        objs.respBuilder.processTree(self.tree)

        objs.saveJMSObject(self.fOut_jms, objs.closure_mR_in_inclPtEtatrue)
        objs.saveJMSObject(self.fOut_jms, objs.mR_in_inclPtEtatrue)

        objs.mR_in_EMEtareco_py.saveContent(self.fOut_jms)
