## **********************************************
## Pyhton helper modules
##
## This module define a few helper functions
## used in the configuration and/or derivation of
## JES and JMS

## ********************************************

import ROOT


## ***************************************
# utilities to conveniently group options together
from collections import namedtuple

# gather numerical inversion options in one flag container :
NIOptions_ = namedtuple('NIOptions', 'noNumInv directNumInv flattenCalib adjustFlatCalib forceSuperIncrResp flattenT0')
class NIOptions(NIOptions_):
    def __new__(cls, **args):
        defaultsOpt =  dict(noNumInv=False, directNumInv=False, flattenCalib=False, adjustFlatCalib=False, forceSuperIncrResp=False,
                            flattenT0 = 0.1, 
        )
        defaultsOpt.update(**args)
        return NIOptions_.__new__(cls,**defaultsOpt)
    def clone(self, **args):
        return self._replace(**args)
        
        
## ***************************************
## generic helper functions
def vectorFloat( l ):
    """converts a list into a vector<float> """
    v= ROOT.vector(float)(len(l))
    for i,val in enumerate(l):
        v[i] = val
    return v


def seq(start, stop, step=1):
    """returns a list [start, start+step, start+2*step, ... ,stop] """
    from math import floor
    n = int(floor((stop - start)/float(step)))
    if n >= 1:
        return [start + step*i for i in range(n+1)] 
    else:
        return [] 

def quantilesFromH(h,f=0.68):
    """h is a TH1. returns f0,med,f1 where
      * med is the median of h
      * [f0,f1] contain the fraction f of events in the distribution h.
      By default f1-f0 is thus the IQR.
      """
    from array import array
    a = 0.5-f/2.
    q_pos = array( 'd',[ a, 0.5, 1-a ] ) # 68% quantiles
    q = array( 'd',[ 0., 0., 0. ] )
    h.GetQuantiles(3,q,q_pos)
    return q[0], q[1] , q[2]

from array import array
def calcMedIQR(h):
    """ returns (med, med_err, IQR, IQR_err)
    The C++ implementation is in Root/Utilities.cxx .
    """
    m = array( 'f',[ 0, 0,0,0 ] ) 
    ROOT.DJS.calcMedIQR(h, m )
    return tuple(m)

def calcSmoothMode(h, startBin=1, outlierF=0.7):
    """Evaluates the mode of h in a smooth way.
    The C++ implementation is in Root/Utilities.cxx .
    """
    m = array( 'f',[ 0, 0,0,0 ] ) # 68% quantiles
    ROOT.DJS.uniModalSmoothMax(h,m, outlierF, startBin)
    return tuple(m)
# ***************************************************



def voidCalibrationObjects( **args):
    """ Create a void CalibrationObjects
    This is for testing/debugging purpose only. The object created here is NOT to be used in any actual derivation.
    """
    from BinnedPhaseSpace.BinnedPhaseSpaceConfig import AxisSpec , BinnedPhaseSpace, BinnedTH1, BinnedTH2, BinnedTGraph2DErrors, h1, h2

    CalibrationObjects = args.get('CalibrationObjects', ROOT.DJS.CalibrationObjects)
    ResponseBuilder = args.get('ResponseBuilder', ROOT.DJS.ResponseBuilder)

    objs = CalibrationObjects()
    # this is a C++ object (see Utilities.h) but we will augment it with pure python attributes

    objs.jetType = args.get('jetType', "AntiKt10LCTopoTrimmedPtFrac5SmallR20")
    objs.radius  = args.get('radius' , 1.0)
    objs.massType = args.get('massType', 'mCalo')
    
    respBuilder = ResponseBuilder()
    objs.respBuilder = respBuilder
    etaBins = AxisSpec("EtaBins", "#eta", seq(-3. , 3, 0.1 ) )
    etaBins.m_isGeV = True
    objs.etaBins = etaBins


    respBuilder.m_etaBins = etaBins
    respBuilder.m_objects = objs

    # ********************************************
    # configure the fitters
    eRespFitter = ROOT.DJS.ResponseFitter()
    eRespFitter.m_minNEffP1 = -3.1
    eRespFitter.m_minNEffP2 = 1.05
    eRespFitter.m_minNEffP3 = 400
    eRespFitter.m_NSigmaForFit = 1.5 # this is for energy !!
    eRespFitter.m_minEtForMeasurement = 100

    eRespFitter.m_useBetterChi2 = False
    eRespFitter.m_useGeoDetCorr = True
    eRespFitter.m_minGeoDetCorr = 2.5
    eRespFitter.m_maxGeoDetCorr = 3.5

    # not set for AntiKt10 :
    #eRespFitter.m_etaException
    #eRespFitter.m_JES_MaxOrderException 


    #eRespFitter.m_JES_Function = "polBestChi2"
    eRespFitter.m_JES_Function = "polBestChi2+groom"
    eRespFitter.m_JES_MaxOrderP1 = -3.1
    eRespFitter.m_JES_MaxOrderP2 = -0.05
    eRespFitter.m_JES_MaxOrderP3 = 5.5
    eRespFitter.m_enableEMinExtrapolation = False    
    eRespFitter.m_enableEMaxExtrapolation = True # (default value :True)
    #eRespFitter.m_enableEMaxExtrapolationException -> not set for AntiKt10
    #eRespFitter.m_enableEMinExtrapolationException -> not set for AntiKt10
    
    eRespFitter.m_maxAcceptableYDeviationFitVSMinGroom = 0.3

    # In case the number of data points in the R vs. E graph < MinNDataPointsForMinExtrapolation
    # => EnableEMinExtrapolation=true forced ("JES_Function: polBestChi2+groom" only)
    eRespFitter.m_minNDataPointsForMinExtrapolation = 9

    eRespFitter.m_minEtForExtrapolation = 50
    eRespFitter.m_maxEtForFits = 5000
    
    balanceFitter = ROOT.JES_BalanceFitter(eRespFitter.m_NSigmaForFit)
    balanceFitter.SetGaus()
    balanceFitter.SetFitOpt("REQS")
    eRespFitter.m_respFitter = balanceFitter

    eRespFitter.m_etaBins = etaBins

    objs.eRespFitter = eRespFitter

    # ******************************
    # config a fitter for delta Eta
    dEtaFitter = ROOT.DJS.ResponseFitter(eRespFitter)
    # same config as eRespFitter except the following :
    dEtaFitter.m_NSigmaForFit = 1.4

    balanceFitter = ROOT.JES_BalanceFitter(dEtaFitter.m_NSigmaForFit)
    balanceFitter.SetGaus()
    balanceFitter.SetFitOpt("REQS")
    dEtaFitter.m_respFitter = balanceFitter
    dEtaFitter.m_etaBins = etaBins
    dEtaFitter.m_respHistTitle = "#Delta Eta, %.1f < E_{true} < %.1f, etabin=%d";
    objs.dEtaFitter = dEtaFitter




    # ######################################################
    # ######################################################
    # JMS configuration 

    # ******************************
    # optionnal JMS strategies. By default, all are set to False
    objs.validateVarPairs = False #  (need to run JMS.closureresp+JMS.closurefits)
    objs.useDirectNumInv  = args.get('useDirectNumInv', False)
    objs.jmsPreCorr       = False
    objs.useNoMNumInv     = args.get('useNoMNumInv', False)
    objs.jmsRespEvaluation = args.get("jmsRespEvaluation","gaus") # default is "gaus", alternative is "mode"
    objs.binSystem = args.get("binSystem","ELogMoEBins")
    # ******************************

    jmsEtaBins = [0,0.2,0.6, 1.0 , 1.4, 2., 3  ]
    if objs.binSystem=="PtMBins":
        BinningClass = ROOT.DJS.PtMBinning
        objs.pt_desc = 'p_{T} [GeV]'
        objs.m_desc = 'm [GeV]'
        jmsBPS = BinnedPhaseSpace( "JMSbins",
                                   # bins from BinHadler/BinHandler.cxx
                                   AxisSpec("pt", "p_{T} true", seq(200,1000,100)+seq(1200,2000,200)+[2500,3000,4000,5000], isGeV=True ),
                                   AxisSpec("m", "m true",seq(0,200,10)+[225,250,275,300], isGeV=True),
                                   AxisSpec("abseta", "|eta|",[0,0.4,0.8, 1.2,2. ], isGeV=False)
                               )
    elif objs.binSystem=="ELogMoEBins":
        BinningClass = ROOT.DJS.ELogMovEBinning
        objs.pt_desc = 'E [GeV]'
        objs.m_desc = 'log(m/E)'
        jmsBPS = BinnedPhaseSpace( "JMSbins",
                                   # bins from BinHadler/BinHandler.cxx
                                   AxisSpec("E", "E", seq(200,1000,200)+seq(1200,4000,400)+[6000], isGeV=True ),
                                   AxisSpec("logmopt", "log(m/E)",[-6,-4.5, -4, -3.75, -3.5, -3.25, -3,-2.75, -2.5, -2.25, -2,-1.75,-1.5, -1.25,-1,-0.8,0], isGeV=False), # m<40GeV for all log(m/E)<-4/5
                                   AxisSpec("abseta", "|eta|",[0,0.4,0.8, 1.2,2. ], isGeV=False)
                                   )

    elif objs.binSystem=="EMBins":
        BinningClass = ROOT.DJS.EMBinning
        objs.pt_desc = 'E [GeV]'
        objs.m_desc = 'm [GeV]'
        jmsBPS = BinnedPhaseSpace( "JMSbins",
                                   # bins from BinHadler/BinHandler.cxx
                                   AxisSpec("E", "E", seq(200,1000,200)+seq(1200,2000,400)+[3000,4000,6000], isGeV=True ),
                                   AxisSpec("m", "m_{true}",seq(0,100,10)+[120,140,200, 250, 300], isGeV=True),
                                   AxisSpec("abseta", "|eta|",jmsEtaBins, isGeV=False)
                               )
    elif objs.binSystem=="MLogMoEBins":
        BinningClass = ROOT.DJS.MLogMovEBinning
        objs.pt_desc = 'm [GeV]'
        objs.m_desc = 'log(m/E)'
        jmsBPS = BinnedPhaseSpace( "JMSbins",
                                   # bins from BinHadler/BinHandler.cxx
                                   AxisSpec("m", "m true",seq(0,200,10)+[225,250,275,300], isGeV=True),
                                   AxisSpec("logmopt", "log(m/E)",[-6,-4.5, -4, -3.75, -3.5, -3.25, -3,-2.75, -2.5, -2.25, -2,-1.75,-1.5, -1.25,-1,-0.8,0], isGeV=False), # m<40GeV for all log(m/E)<-4/5
                                   AxisSpec("abseta", "|eta|",[0,0.4,0.8, 1.2,2. ], isGeV=False)
                                   )


    if objs.binSystem=="EtLogMoEtBins":
        BinningClass = ROOT.DJS.EtLogMovEtBinning
        objs.pt_desc = 'E [GeV]'
        objs.m_desc = 'log(m/E)'
        jmsBPS = BinnedPhaseSpace( "JMSbins",
                                   # bins from BinHadler/BinHandler.cxx
                                   AxisSpec("Et", "Et", seq(200,1000,200)+seq(1200,4000,400)+[6000], isGeV=True ),
                                   AxisSpec("logmoet", "log(m/Et)",[-6,-4.5, -4, -3.75, -3.5, -3.25, -3,-2.75, -2.5, -2.25, -2,-1.75,-1.5, -1.25,-1,-0.8,0], isGeV=False), # m<40GeV for all log(m/E)<-4/5
                                   AxisSpec("abseta", "|eta|",[0,0.4,0.8, 1.2,2. ], isGeV=False)
                                   )



    objs.jmsWorkDir = "jms_"+objs.binSystem+('_noni/' if objs.useNoMNumInv else '_ni/')
    objs.jmsQualifier = ""
    

    if objs.useNoMNumInv:
        # when NOT doing JMS numerical inversion, we fill the 'numinv' histos directly in the JMS.rawresp step
        # we thus don't need the niresp step
        objs.vetoedCalibSteps = ["JMS.niresp"]

    respBuilder.m_noMassNumInv = objs.useNoMNumInv

    respBuilder.m_jmsTrueBin = BinningClass()
    respBuilder.m_jmsRecoBin = BinningClass()
    respBuilder.m_jmsRecoBin.m_correctionFactors = objs.mRsmooth_vs_PtMnuminv_in_Eta

    objs.jmsBPS = jmsBPS
    respBuilder.m_jmsBPS = jmsBPS
    respBuilder.m_jmsRecoBin.m_bps = jmsBPS # for interactive use 
    respBuilder.m_jmsTrueBin.m_bps = jmsBPS # for interactive use 


    # Define JMS histogram containers .

    objs.mR_in_PtEtaMtrue         = BinnedTH1("mR_in_PtEtaMtrue", jmsBPS, None, Title="Mass Response", var_x='m response')
    objs.mR_in_PtEtaMnuminv       = BinnedTH1("mR_in_PtEtaMnuminv", jmsBPS, None, Title="Mass Response", var_x='m response')
    objs.closure_mR_in_PtEtaMtrue = BinnedTH1("closure_mR_in_PtEtaMtrue", jmsBPS, None, Title="Mass Response closure", var_x='m response')
    # --------



    # --------
    # define a phase space used in final comparisons 
    jmsinclPtBPS = BinnedPhaseSpace( "JMSinclPt",
                                     AxisSpec("pt", "p_{T} true", seq(200,1000,100)+seq(1200,3000,200), isGeV=True ),
                                     AxisSpec("m", "mass true", [40, 80, 100,140,200], isGeV=True ),
                                     AxisSpec("abseta", "|eta|",[0,0.8, 1.2, 2. ], isGeV=False),
                                     )
    # define corresponding histogram containers
    objs.mR_in_inclPtEtatrue         = BinnedTH1("mR_in_inclPtEtatrue", jmsinclPtBPS, None, Title="Mass Response", var_x='m response')
    objs.closure_mR_in_inclPtEtatrue = BinnedTH1("closure_mR_in_inclPtEtatrue", jmsinclPtBPS, None, Title="Mass Response", var_x='m response')
    objs.jmsinclPtBPS = jmsinclPtBPS
    # --------

    

    # --------
    # JMS Response fitter.
    balanceFitter_jms = ROOT.JES_BalanceFitter(1.3) #(1.6)
    balanceFitter_jms.SetGaus()
    balanceFitter_jms.SetDefaultMinMax(0. , 4. )
    #balanceFitter_jms.SetFitOpt("RQE0S")
    balanceFitter_jms.SetFitOpt("RWLQ0S") # for JMS, use WL="Weighted Likelihood" because we're not rebinning
    balanceFitter_jms.StopWhenFitFails(True)
    objs.balanceFitter_jms =balanceFitter_jms
    # --------


    # --------
    # JMS smoothers
    from DeriveJetScales.KernelSmoothing import buildSmoother
    jms_th1_smoother = buildSmoother(gausW=0.07, log=[True], npoints=[38] )        
    objs.jms_th1_smoother = jms_th1_smoother

    # for mR vs pt,m smoothing
    if objs.binSystem in ["ELogMoEBins" , "EtLogMoEtBins" ]:
        jms_tg2_smoother = buildSmoother(gausW=0.09, log=[False,False], npoints=[60,60] ,
                                         useErrors=True, maxNpoints=30,
                                         stretchF=(1,1) )        
    else: # objs.binSystem=="PtMBins":
        jms_tg2_smoother = buildSmoother(gausW=0.07, log=[False,False], npoints=[60,60] ,
                                         useErrors=True, maxNpoints=30,
                                         stretchF=(1.3,0.6) )        
        
    objs.jms_tg2_smoother = jms_tg2_smoother
    # --------


    return objs



