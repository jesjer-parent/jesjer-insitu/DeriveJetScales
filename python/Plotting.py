import ROOT
from ROOT.TMath import CosH,Sqrt,Exp
ROOT.gROOT.LoadMacro("AtlasStyleUtils.C")

from DeriveJetScales.PyHelpers import seq 
from BinnedPhaseSpace.BinnedPhaseSpaceConfig import h1, h2
from DeriveJetScales.DrawUtils import buildLegend

# ************************************************
class Plots(object):
    """Base class for  plots"""
    preApplyStyle = True
    def __init__(self, objs):
        self.objs = objs
        self._defaultPage = None

        
    def defaultPage(self):
        if self._defaultPage: return self._defaultPage
        from DeriveJetScales.DrawUtils import buildPage, NoSaver            
        page = buildPage(NoSaver, 1, "")
        self._defaultPage = page
        return page

    def setStyles(self):
        ROOT.gStyle.SetOptTitle(False)
        ROOT.SetAtlasStyle()
        ROOT.gStyle.SetMarkerStyle(1) # overwrite atlas
        
        ROOT.BPS.AxisSpec.geVFactor = 1.
        ROOT.gStyle.SetLegendFillColor(0)
        ROOT.gStyle.SetLegendBorderSize(0)
        from collections import defaultdict
        self.styles = defaultdict(dict)
        return self.styles


# ************************************************
class JMSPlots(Plots):
    """This object defines several function to create plots of the various histograms and graphs
    used as part of the JMS derivation
    """
    
    def __init__(self, objs):
        Plots.__init__(self,objs)
        self.objs = objs
        self.setStyles()

    def prepare(self, n, doLoad=True ):
        # make sure the object n has its content loaded and the styles applied
        # (ROOT does not save style attributes in .root files)
        if isinstance(n,str):
            o = getattr(self.objs, n)
        else:
            o = n
            n = o.name()
        if doLoad: o.loadContent()
        
        if self.preApplyStyle:
            o.updateHistoDesc( ** self.styles[n] )
        o.applyStyle()
        return o


    def setStyles(self):
        styles = Plots.setStyles(self)

        closure_objs = [
            "closure_mR_in_PtEtaMtrue",
            "closure_mR_in_inclPtEtatrue",
            "closure_mR_vs_pttrue_in_PtEtatrue",
            "closure_mR_in_PtEtaMtrue_func",
            "closure_mR_vs_mtrue_in_PtEtatrue",
            "closure_mIQR_vs_mtrue_in_PtEtatrue",
            "closure_mIQR_vs_pttrue_in_PtEtatrue",
            'closure_mR_vs_Pt_in_Etatrue',
            'closure_mIQR_vs_Pt_in_Etatrue',
            "closure_mR_vs_pttrue_in_largeMassBins",
            "closure_mIQR_vs_pttrue_in_largeMassBins",
            "closure_mR_in_EMEtatrue",
            ]            
        numinv_objs = [
            "mR_in_PtEtaMnuminv",
            "mR_in_PtEtaMnuminv_func",
            "mR_vs_PtMnuminv_in_Eta",
            "mR_vs_mnuminv_in_PtEtanuminv",
            "mR_vs_mnuminv_in_PtEtanuminv_noExtrap",
            "mIQR_vs_mnuminv_in_PtEtanuminv",
            "mRsmooth_vs_PtMnuminv_in_Eta",
            #"m_in_PtEtaMnuminv",
        ]

        raw_objs= [
            #m_in_PtEtaMtrue
            "mR_in_PtEtaMtrue",
            "mR_in_PtEtaMtrue_func",
            "mR_in_inclPtEtatrue",
            "mR_vs_PtMtrue_in_Eta",
            "mR_vs_mtrue_in_PtEtatrue",
            "mR_vs_pttrue_in_PtEtatrue",
            "mIQR_vs_mtrue_in_PtEtatrue",
            "mIQR_vs_pttrue_in_PtEtatrue",
            "mRsmooth_vs_PtMtrue_in_Eta",
            'mR_vs_Pt_in_Etatrue',
            'mIQR_vs_Pt_in_Etatrue',
            "mR_vs_pttrue_in_largeMassBins",
            "mIQR_vs_pttrue_in_largeMassBins",
        ]

        for o in numinv_objs+closure_objs+raw_objs:
            l = 1 if 'vs_PtM' in o else 2
            styles[ o ].update(LineWidth = l , FillColor=ROOT.kWhite)
        for o in closure_objs:
            styles[ o ].update(LineColor = ROOT.kRed , Title="calibrated")
        for o in raw_objs:
            styles[ o ].update( Title="uncalibrated")
        self.styles = styles
        
    def addLabel(self,x,y, Atlaslabel):
        ROOT.ATLASLabel(x,y,Atlaslabel,1)
        y=y-0.07
        for desc in self.objs.jetTypePublic:
            ROOT.myText(x,y,1,desc)
            y=y-0.07
    
    def drawMultiGraphs( self, binnedGraphs, page=None, addHistos=[], yrange=(0,6) , bin=None, legPos=(0.6, 0.6, 0.85, 0.75),
                         legHeader=None):
        """generic function to draw a list of 1D graphs on the same TPad. The list of graphs can be
        drawn for 1 or all the bins in the phasespace and saved on 1 pdf file or (multiple) png file.
                
        """
        bg0 = binnedGraphs[0]
        bgOthers = binnedGraphs[1:]
        bps = bg0.m_phaseSpace 

        if page is None:
            page = self.defaultPage()
            page.resetCanvas()

        #print 'drawMultiGraphs ', binnedGraphs, bin
        drawMassLine = 'LogMo' in self.objs.binSystem and (bps.m_a1.name()[0]=='E' or bps.m_a1.name()=='logmopt')
        if drawMassLine:            
            if bps.m_a1.name()[0]=='E':
                log = ROOT.TMath.Log
                mmaxFunc = lambda e : log(40/e)
            else:
                exp  = ROOT.TMath.Exp
                mmaxFunc = lambda e : 40*exp(-e)
            ax = bps.m_a1
            page.lines = []
        singlePlot = False
        if isinstance(bin, int):
            bin = [bps.iteratorAtIndex(bin)]
            singlePlot=True
        elif isinstance(bin, tuple):
            bin = [bps.iteratorFrom(*bin)]
            singlePlot = True
        psrange = bin if bin is not None else bps.fullRange()
        x0 = legPos[0]
        y0 = legPos[1]-0.07
        for it in psrange:
            index = it.index()
            g0 = bg0.m_binContent[ index ]
            if not g0: continue
            g0.SetMaximum(yrange[1])
            g0.SetMinimum(yrange[0])

            g0.Draw("AL")
            for bg in bgOthers:
                g = bg.m_binContent[index]
                g.Draw("L")

            descs = bps.describeBin(it)
            l=page.currentPad().BuildLegend(*legPos)
            if legHeader: l.SetHeader(legHeader)

            if drawMassLine:
                emax = ax.binUpEdge(it.i)
                #mmax = log(40/emax)
                mmax = mmaxFunc(emax)
                print ' drawMassLine at ', emax, yrange
                line = ROOT.TLine( mmax, yrange[0], mmax, yrange[1] )
                line.SetLineStyle(2)
                line.SetLineColor(ROOT.kBlue)
                line.Draw("same")
                p = page.currentPad()
                p.Update()
                mmaxNDC = (mmax-p.GetX1())/(p.GetX2()-p.GetX1()) -0.06
                page.lines.append(line)
                #print "   ",mmaxNDC, "__", mmax, p.GetX1()
                ROOT.myText(mmaxNDC, 0.85, ROOT.kBlue, "m<40GeV")

            ROOT.myText(x0, y0, 1, descs[0] )
            dy = 0.07
            if len(descs)==2:
                ROOT.myText(x0, y0-dy, 1, descs[1] )
                dy +=dy
            ROOT.myText(x0, y0-dy, 1, str(it.asTuple()) )
            page.currentPad().SetGridy(True)
            page.pname = bps.binTag( index ) 
            if not singlePlot: page.nextPad()
            else : page.save()

        if page.suffix == '.pdf': page.forceSave()
        if singlePlot:
            # we're likely inspecting things interactively. So save the page
            self.last_page = page
        else:
            page.close()


    def drawVvsX(self , dim, value="mR", otherObjs=[], fromTrueBins = True, **args):
        """wrapper around drawMultiGraphs to simply draw calibrated & unclaibrated values on the
        same plot."""
        var = ["pt","m","eta"][dim]

        return self.drawMultiGraphs( [ self.prepare( value+"_vs_"+var+"true_in_PtEtatrue"),
                                       self.prepare("closure_"+value+"_vs_"+var+"true_in_PtEtatrue") ], #+[self.prepare(o)],
                                     **args)
    


    def drawmRvsPtM(self, page, mR_vs_PtM, bin=None):
        self.prepare(mR_vs_PtM)
        bps=mR_vs_PtM.m_phaseSpace
        bps.axis(0).m_isGeV = False
        opt = "surf1" if isinstance(mR_vs_PtM[0], ROOT.TH2F) else "tri1"
        if page is None:
            page = self.defaultPage()
            page.resetCanvas()

        if isinstance(bin, int): bin = [bps.iteratorAtIndex(bin)]
        elif isinstance(bin, tuple): bin = [bps.iteratorFrom(*bin)]
        psrange = bin if bin is not None else bps.fullRange()
        for it in psrange:
            h2 = mR_vs_PtM[it.index()]
            desc = bps.axis(0).describeBin(it.index())
            h2.Draw(opt)
            page.currentPad().SetTheta(20.6)
            page.currentPad().SetPhi(120)
            page.pname = bps.binTag(it.index())
            ROOT.myText(0.03,0.93,1,desc)
            print 'page ',page.pname , 'suffix=',page.suffix, 'dir=',page.dir
            page.nextPad()
            
        if page.suffix == '.pdf':
            page.forceSave()
        
        page.close()


    def drawAllResp(self, page=None, bin=None, oList=None, drawFitFunc=1 , outTag='allResp', suffix='pdf', details=True,drawCopy=False):
        """ Draw mass response histograms in 1 or many derivation bins.
        Response histograms are obtained from histo containers (BinnedHistos) passed in a list 'oList' . 
        Each plot contains 1 histo per container in 'oList'. 
        By default 'oList' is set to '[objs.mR_in_PtEtaMtrue_py , objs.closure_mR_in_PtEtaMtrue_py]' and thus plots uncalib response & calibrated response in true bins.
        'bin' governs which bin to plot. It can be specified as an int, a tuple (integer coordinates of the bin) or a BPS::BinnedPhaseSpace::BinRange (that is a set of bins)
             by default 'bin' is set to *all* the bins in the phase space.
        """
        objs = self.objs
        if page is None:
            from DeriveJetScales.DrawUtils import buildPage
            npage = 1 if isinstance(bin, tuple) else 4
            if suffix=='pdf':
                outName=objs.jmsFile(outTag+'.pdf')
            else:
                outName=objs.jmsFile(outTag+'_at')
            page = buildPage(suffix, npage, outName)

        if oList is None :
            oList= [objs.mR_in_PtEtaMtrue_py , objs.closure_mR_in_PtEtaMtrue_py] 
        
        for o in oList:
            self.prepare( o )

        # retrieve the fit function
        if isinstance(drawFitFunc,int) :
            drawFitFunc = drawFitFunc if len(oList)>drawFitFunc else 0
            mR_fitfunc = getattr(objs, oList[drawFitFunc].name()+"_func")
            # self.prepare(objs.mR_in_PtEtaMtrue_func_py) --> no. some funcs are empty
            mR_fitfunc.loadContent()
            # so we draw :
            biCont0 = oList[drawFitFunc].infoContent
            drawFitFunc = True
        else:
            drawFitFunc = False
            biCont0 = oList[0].infoContent
            
        bps = oList[0].m_phaseSpace
        nPtBin = bps.axis(0).nBins()-1
        hCont0 = oList[0].m_binContent
        hContList = [o.m_binContent for o in oList[1:] ]
        if isinstance(bin, int): bin = [bps.iteratorAtIndex(bin)]
        elif isinstance(bin, tuple): bin = [bps.iteratorFrom(*bin)]
        psrange = bin if bin is not None else bps.fullRange()
        l=ROOT.TLegend(0.63,0.35,0.9,0.50)

        def drawText(x,y,i,s):
            ROOT.myText(x,y,i,s)
            return y - 0.06

        histoDraw = ROOT.TH1.DrawCopy if drawCopy else ROOT.TH1.Draw
        hcopies = []

        line1 = ROOT.TLine(5,0.1,5,0.9)
        line1.SetNDC(True)
        line1.SetLineColor(ROOT.kBlue)
        line1.SetLineStyle(2)
        xline1 = -1
        for it in psrange:
            i = it.index()            
            h = hCont0[i]

            l.Clear()
            hcopies+= [histoDraw(h)] # calls TH1::Draw
            l.AddEntry(h)
            for hc in hContList:
                hi = hc[i]
                hcopies+= [histoDraw(hi,"same")]

                l.AddEntry(hi)
            descs = bps.describeBin(it)
            l.Draw()
            if drawFitFunc:
                try:
                    mR_fitfunc.m_binContent[i].Draw("same")
                except:
                    pass
            if xline1<0:
                page.currentPad().Update()
                pad0=page.currentPad()
                w = pad0.GetX2() - pad0.GetX1()
                xline1  = 1./w*(1.-pad0.GetX1())
                print "  uuu ", xline1
                line1.SetX1(xline1 )
                line1.SetX2(xline1 )
            line1.DrawLineNDC(xline1,0.15, xline1, 0.95)
            page.currentPad().Update()
            y0=0.85
            if details : y0=drawText(0.63, y0, 1, str(it.asTuple()[:-1]) ) # draw bin indices
            y0=drawText(0.63, y0 ,1, descs[0] )
            y0=drawText(0.63, y0 ,1, descs[1] )
            y0=drawText(0.63, y0 ,1, descs[2] )            
            if details:
                y0=drawText(0.63, y0 ,1, "Entries= %d (%.1f)"%( h.GetEntries() , h.GetEffectiveEntries() ) )
                bi = biCont0[i]
                y0=drawText(0.63, y0, 1, "resp=%1.2f\pm %1.2f"%(bi.mresp, bi.mresp_err) )
            page.pname = bps.binTag(i)
            if it.i==nPtBin:
                page.forceSave()
                if bin is None: page.resetCanvas()
                if drawCopy:
                    for h in hcopies:
                        h.Delete()
                hcopies=[]
            else:
                page.nextPad()
        if bin is None: page.forceSave() # case where we're saving a full pdf
        page.close()
        page.l =  l
        return page

    def drawAllRespVFB(self, **args):
        """ A wrapper function around drawAllResp to deal with special cases of 'very fine bins' derivation and
        the corresponding special objects"""
        objs = self.objs
        oList = args.setdefault('oList',[])
        if oList==[]: oList += [objs.mR_in_EMEtatrue_py,objs.closure_mR_in_EMEtatrue_py]
        for o in oList:
            o.load(objs.derObject.fOut_jms)
            o.createFakeVector()
        bi = args.get('bin')
        if bi is None:
            args['bin'] = objs.plotsBPS.fullRange()
        args.update(drawFitFunc=None, details=False, drawCopy=True)
        print args
        return self.drawAllResp(**args)

    

    def jmsPlots(self, plotsToDraw, bin=None, suffix=None, npage=2):
        """Main JMS plotting function.
        plotsToDraw : a list of strings where each string identifies a type of plot to be drawn and saved.

        bin : is None or a tuple identifying a bin of the phasespace at wich plots must be drawn.
        If None, plots for all bins will be drawn into pdf files (suffix will be overwritten).

        suffix : format to save the plots. if suffix==None and bin!=None  then nothing will be saved

        npage: number of plots per page/canvas.

        This function also acts as a list of recipe to produce plots using lower-level functions.
        """
        self.objs.readJMSFromFile()
    
        ROOT.gStyle.SetOptStat(0)
        page = None
        from DeriveJetScales.DrawUtils import buildPage, PdfSaver, ImgSaver

        # alias
        objs= self.objs
                    
        if bin is None:
            if suffix is None: suffix = 'pdf'

        if suffix is None:
            def pageBuilder(tag, npage=npage):
                return None
        else:
            def pageBuilder(tag, npage=npage):
                p = buildPage(suffix, npage, objs.jmsFile( tag ) )
                self._pdf_page = p
                return p
            
        if "mR_vs_m_closure" in plotsToDraw:
            self.drawVvsX(1,value="mR",page= pageBuilder( "_mRvsM" ),
                         bin=bin)

        if "mR_vs_pt_closure" in plotsToDraw:
            self.drawVvsX(0,value="mR",page= pageBuilder( "_mRvsPt" ),
                          bin=bin, yrange=(0,2.))

        if "mR_vs_eta" in plotsToDraw:
            self.drawVvsX(2,value="mR",page= pageBuilder( "_mRvsEta" ),
                         bin=bin)

        if "mIQR_vs_m" in plotsToDraw:
            self.drawVvsX(1,value="mIQR",page= pageBuilder( "_mIQRvsM" ),
                         bin=bin, yrange=(0,2.))

        if "mIQR_vs_pt" in plotsToDraw:
            self.drawVvsX(0,value="mIQR",page= pageBuilder( "_mIQRvsPt" ),
                         bin=bin, yrange=(0,2.))

        if "mR_vs_pt_largeMassBins" in plotsToDraw:            
            self.drawMultiGraphs( [ self.prepare("mR_vs_pttrue_in_largeMassBins"),
                                    self.prepare("closure_mR_vs_pttrue_in_largeMassBins") ],
                                  page= pageBuilder( "_mRvsPtLargeMassBins" ),
                                  bin=bin, yrange=(0.5, 1.3),
                                  legPos=(0.6,0.4, 0.85, 0.55),
                                  )

        if "mIQR_vs_pt_largeMassBins" in plotsToDraw:            
            self.drawMultiGraphs( [ self.prepare("mIQR_vs_pttrue_in_largeMassBins"),
                                    self.prepare("closure_mIQR_vs_pttrue_in_largeMassBins") ],
                                  page= pageBuilder( "_mIQRvsPtLargeMassBins" ),
                                  legPos=(0.3, 0.6, 0.55, 0.75),
                                  bin=bin, yrange=(0., 1.2),
                                  )


        if "mR_vs_PtMtrue" in plotsToDraw:
            p = pageBuilder( "_mRvsPtMtrue",npage=1 )
            self.drawmRvsPtM(p,
                             objs.mR_vs_PtMtrue_in_Eta_py , bin=bin,)

        if "mR_vs_PtMnuminv" in plotsToDraw:
            self.drawmRvsPtM(pageBuilder( "_mRvsPtMnuminv",npage=1 ),
                             objs.mR_vs_PtMnuminv_in_Eta_py,bin=bin, )

        if "mRsmooth_vs_PtMtrue" in plotsToDraw:
            self.drawmRvsPtM(pageBuilder( "_mRSmoothvsPtMtrue",npage=1 ),
                             objs.mRsmooth_vs_PtMtrue_in_Eta_py, bin=bin, )

        if "mRsmooth_vs_PtMnuminv" in plotsToDraw:
            self.drawmRvsPtM(pageBuilder( "_mRSmoothvsPtMnuminv",npage=1 ),
                             objs.mRsmooth_vs_PtMnuminv_in_Eta_py, bin=bin, )

        if "IQR_vs_PtMreco" in plotsToDraw:
            self.drawmRvsPtM(pageBuilder( "_IQRvsPtMreco",npage=1 ),
                             objs.IQR_vs_PtMreco_in_Eta, bin=bin, )
        if "IQRsmooth_vs_PtMreco" in plotsToDraw:
            self.drawmRvsPtM(pageBuilder( "_IQRSmoothvsPtMreco",npage=1 ),

                             objs.IQRSmooth_vs_PtMreco_in_Eta, bin=bin, )

        if "calibFactors" in plotsToDraw:
            if not bool(objs.respBuilder.m_jmsRecoBin.m_correctionFactors3d):
                objs.respBuilder.m_jmsRecoBin.initCorrections(objs.mRsmooth_3Dnuminv)
            else:
                objs.respBuilder.m_jmsRecoBin.initCorrections()

            page = pageBuilder( "_JMSFactors", npage=1 )
            page.currentPad().SetRightMargin(0.13)

            page.setNplots(1)
            self.drawJMSCorrectionsMap( ("E","Eta") ,("M",80.), page=page)
            self.drawJMSCorrectionsMap( ("Et","Eta") ,("M",80.), page=page)
            self.drawJMSCorrectionsMap( ("Pt","Eta") ,("M",80.), page=page)
            
            self.drawJMSCorrectionsMap( ("E","Eta") ,("M",120.), page=page)
            self.drawJMSCorrectionsMap( ("Et","Eta") ,("M",120.), page=page)
            self.drawJMSCorrectionsMap( ("Pt","Eta") ,("M",120.), page=page)
            page.currentPad().SetRightMargin(0.05)
            
            r=self.drawJMSCorrectionsVsX("M", ("E", [200, 500, 1000,2000, 3000,] ), ("Eta",0.2) )
            page.save("calibfactors_vs_M_eta02")
            r=self.drawJMSCorrectionsVsX("M", ("E", [200, 500, 1000,2000, 3000,] ), ("Eta",0.8) )
            page.save("calibfactors_vs_M_eta08")
            r=self.drawJMSCorrectionsVsX("M", ("E", [200, 500, 1000,2000, 3000,] ), ("Eta",1.4) )
            page.save("calibfactors_vs_M_eta14")

            r=self.drawJMSCorrectionsVsX("Pt", ("M", [60, 80, 125, 175] ), ("Eta",0.2) )
            page.save("calibfactors_vs_Pt_eta02")
            r=self.drawJMSCorrectionsVsX("Pt", ("M", [60, 80, 125, 175] ), ("Eta",0.8) )
            page.save("calibfactors_vs_Pt_eta08")
            r=self.drawJMSCorrectionsVsX("Pt", ("M", [60, 80, 125, 175] ), ("Eta",1.4) )
            page.save("calibfactors_vs_Pt_eta14")


            r=self.drawJMSCorrectionsVsX("Eta", ("E", [200, 500, 1000,2000, 3000,] ), ("M",80.4) , yrange=(0.5,1.2) )
            page.save("calibfactors_vs_Eta_M80")
            r=self.drawJMSCorrectionsVsX("Eta", ("E", [200, 500, 1000,2000, 3000,] ), ("M",125)  , yrange=(0.5,1.2) )
            page.save("calibfactors_vs_Eta_M125")

            page.close()

        if "closure_mR_map" in plotsToDraw:
            objs.respBuilder.m_jmsRecoBin.initCorrections(objs.closure_mRsmooth_3D)

            page = pageBuilder( "_closuremR", npage=1 )
            page.setNplots(1)
            EPt_slices = [150., 200., 300., 400., 500, 800., 1000., 1500., 2000., 3000., 4000.]
            for var in EPt_slices:
                self.drawJMSCorrectionsMap( ("LogMoE","Eta") ,("E",var), page=page, correcFunc=objs.respBuilder.m_jmsRecoBin.jmsResponse, zmax=1.5, zTitle="colsure mR", mass_cut = 50)
                self.drawJMSCorrectionsMap( ("LogMoE","Eta") ,("Pt",var), page=page, correcFunc=objs.respBuilder.m_jmsRecoBin.jmsResponse, zmax=1.5, zTitle="colsure mR", mass_cut = 50)
            Eta_slices = [0., 0.4, 0.8, 1.2, 1.6, 2.0, 2.4, 2.8]
            for var in Eta_slices:
                self.drawJMSCorrectionsMap( ("E","LogMoE") ,("Eta",var), page=page, correcFunc=objs.respBuilder.m_jmsRecoBin.jmsResponse, zmax=1.5, zTitle="colsure mR", mass_cut = 50)
            page.close()

        if "closure_significance_map" in plotsToDraw:
            objs.respBuilder.m_jmsRecoBin.initSignificance(objs.closure_mRsmooth_3D, objs.closure_mIQRsmooth_3D)

            page = pageBuilder( "_significance", npage=1 )
            page.setNplots(1)
            EPt_slices = [150., 200., 300., 400., 500, 800., 1000., 1500., 2000., 3000., 4000.]
            for var in EPt_slices:
                self.drawJMSCorrectionsMap( ("LogMoE","Eta") ,("E",var), page=page, correcFunc=objs.respBuilder.m_jmsRecoBin.jmsSignificance, zmax=1.5, zTitle="significance", mass_cut = 50)
                self.drawJMSCorrectionsMap( ("LogMoE","Eta") ,("Pt",var), page=page, correcFunc=objs.respBuilder.m_jmsRecoBin.jmsSignificance, zmax=1.5, zTitle="significance", mass_cut = 50)
            Eta_slices = [0., 0.4, 0.8, 1.2, 1.6, 2.0, 2.4, 2.8]
            for var in Eta_slices:
                self.drawJMSCorrectionsMap( ("E","LogMoE") ,("Eta",var), page=page, correcFunc=objs.respBuilder.m_jmsRecoBin.jmsSignificance, zmax=1.5, zTitle="significance", mass_cut = 50)
            page.close()

        if "iqrValues" in plotsToDraw:
            jmsBinning = objs.respBuilder.m_jmsRecoBin
            if not bool(jmsBinning.m_correctionFactors3d): jmsBinning.initCorrections( objs.mRsmooth_3Dnuminv)
            else: jmsBinning.initCorrections( )
            jmsBinning.m_sigmas3d = objs.IQRsmooth3D

            iqrFunc = jmsBinning.jmsSigma
            
            page = pageBuilder( "_IQRValues", npage=1 )
            page.currentPad().SetRightMargin(0.13)

            page.setNplots(1)
            args = dict(page=page, correcFunc=iqrFunc, zTitle="#sigma(m)")
            self.drawJMSCorrectionsMap( ("E","Eta") ,("M",80.), **args)
            self.drawJMSCorrectionsMap( ("Pt","Eta") ,("M",80.), **args)

            self.drawJMSCorrectionsMap( ("E","Eta") ,("M",120.), **args)
            self.drawJMSCorrectionsMap( ("Pt","Eta") ,("M",120.), **args)

            r=self.drawJMSCorrectionsVsX("Pt", ("M", [60, 80, 125, 175] ), ("Eta",0.) , yrange=(0.,0.5),jmsCorrection=iqrFunc)
            page.save("iqrvalues_vs_Pt_eta0")
            r=self.drawJMSCorrectionsVsX("Pt", ("M", [60, 80, 125, 175] ), ("Eta",1.) , yrange=(0.,0.5),jmsCorrection=iqrFunc)
            page.save("iqrvalues_vs_Pt_eta1")

            page.close()


        if "compareCombMass" in plotsToDraw:
            if objs.massType != "mCalo":
                print "ERROR  can draw combined mass comparisons only starting from a mCalo configuration"
                return
            from DeriveJetScales.JMSComparator import comp, newObjs
            # prepare void calibration objects to load the mTA and mComb histos/graphs
            # and load them in the comparator
            comp.setObjsList(
                [
                updateStyle(objs,LineColor=ROOT.kGreen,Title="mCalo"),
                updateStyle(newObjs(objs.jmsFile().replace("mCalo", "mTA"),  binSystem=objs.binSystem, tag='mTA' ), LineColor=ROOT.kBlue),
                updateStyle(newObjs(objs.jmsFile().replace("mCalo", "mComb"),binSystem=objs.binSystem, tag='mComb' ) ,LineColor=ROOT.kRed),    
                ]  )
            comp.drawLargeMassBins("mIQR", suffix='pdf',  refHistoStyle=dict(Title="mCalo uncal") )
            comp.drawLargeMassBins("mR", suffix='pdf',  refHistoStyle=dict(Title="mCalo uncal") )
            
            
        if "allMassWindowResp" in plotsToDraw:
            self.drawAllResp(outTag="_allMassWindowResp", oList=[objs.mR_in_inclPtEtatrue_py, objs.closure_mR_in_inclPtEtatrue_py])

        if "allResp" in plotsToDraw:
            if not objs.jmsVeryFineBinning:                
                self.drawAllResp(outTag="_allResp" )
            else:
                self.drawAllRespVFB(outTag="_allResp")
                
        if "allNumInvResp" in plotsToDraw:
            if not objs.jmsVeryFineBinning:                
                objs.mR_in_PtEtaMnuminv_py.loadContent()
                self.drawAllResp(oList=[objs.mR_in_PtEtaMnuminv_py], outTag="_numinvResp" )
            else:
                self.drawAllRespVFB(oList=[objs.mR_in_EMEtanuminv_py], outTag="_numinvResp")

        if "allTrueResp" in plotsToDraw:
            objs.mR_in_PtEtaMnuminv_py.loadContent()
            self.drawAllResp(oList=[objs.mR_in_PtEtaMtrue_py], outTag="_trueResp" )

        if "vfbRecoResp" in plotsToDraw:
            self.drawAllRespVFB(oList=[objs.mR_in_EMEtareco_py ], outTag="_recoResp")
            


        if "extractFile" in plotsToDraw:
            objs.mRsmooth_vs_PtMnuminv_in_Eta_py.loadContent()
            f = ROOT.TFile(objs.jmsFile("_calibFactors.root"), "recreate")
            f.cd()
            objs.mRsmooth_3Dnuminv.Write("mRsmooth_3D")
            f.Close()




    def calibFactorToGraph(self, X, fixedCoords, fixedValues, titleSuff='' , calibObjs=None , jmsCorrection=None ):
        """create a TGraph of jms calib factors vs X  fixing other parameters  (Y,Z)==fixedCoords at values (Y_value,Z_value)==fixedValues
         if dim== 0 then X=pt else X=m.

         calibObjs is a calibrationObjects (defaulting to global 'objs') and the calib factors evaluated from calibObjs.m_jmsRecoBin.

         jmsPlots.calibFactorToGraph('Pt',('M','Eta'), (100,0.5) ) -->  calibF vs pt at m=100GeV,eta=0.5
         
        """
        coordSystem  = JMSCoord.newCoord( X, *fixedCoords )

        if calibObjs is None : calibObjs = self.objs # use the global objs
        if jmsCorrection is None: jmsCorrection= calibObjs.respBuilder.m_jmsRecoBin.jmsCorrection

        gr = coordSystem.calibFactorsVsX(X, fixedCoords, fixedValues, jmsCorrection, doTGraph=True)
        gr.SetLineWidth(2)
        gr.SetTitle( gr.GetTitle()+titleSuff )

        return gr

    def drawJMSCorrectionsMap(self, coordNames, fixedCoord , calibObjs=None, page=None , suffix="", correcFunc=None, zmax=1.2, outputTag="JMSFactors_", zTitle="JMS calib factor", mass_cut = None):
        """create and draw a TH2F of jms calib factors vs (X,Y) at Z fixed where
          (X,Y) == coordNames
          (Z,Z_Value) == fixedCoord
        'calibObjs' is a CalibrationObjects (defaulting to the global 'objs').
        The calib factors are evaluated from calibObjs.respBuilder.m_jmsRecoBin.
        
         Ex : h=jmsplots.drawJMSCorrectionsMap( ('Pt','Eta') ,('M',80.) )
              h=jmsplots.drawJMSCorrectionsMap( ('E','Eta') ,('M',80.) )
         
        """
        coordSystem  = JMSCoord.newCoord( fixedCoord[0], *coordNames )

        if calibObjs is None : calibObjs = self.objs # use the global objs
        calibObjs.mRsmooth_vs_PtMnuminv_in_Eta_py.loadContent()
        if page is None:
            if suffix=='' :
                page = self.defaultPage()
                page.resetCanvas()
            else:
                from DrawUtils import buildPage
                page = buildPage(suffix, nplots=1, name=self.objs.jmsFile( outputTag ) )
                self._defaultPage = page

        page.currentPad().SetRightMargin(0.13)                
        if correcFunc is None :
            correcFunc = calibObjs.respBuilder.m_jmsRecoBin.jmsCorrection

        h = coordSystem.calibFactorsToTH2( coordNames, fixedCoord,  correcFunc , zTitle )
        optT = ROOT.gStyle.GetOptTitle()
        ROOT.gStyle.SetOptTitle( True )
        ROOT.gStyle.SetTitleStyle(0)
        ROOT.gStyle.SetTitleFillColor(0)
        ROOT.gStyle.SetTitleBorderSize(1)
        h.SetMaximum(zmax)
        h.Draw("colz")
        if mass_cut is not None and fixedCoord[0] == "E":
            cutOnX = ROOT.TMath.Log(float(mass_cut/fixedCoord[1]) )
            line = ROOT.TLine(cutOnX, 0., cutOnX, 3.)
            line.SetLineStyle(2)
            line.SetLineColor(ROOT.kRed)
            line.Draw("same")
            p = page.currentPad()
            p.Update()
            labelpos = (cutOnX-p.GetX1())/(p.GetX2()-p.GetX1()) -0.15
            ROOT.myText(labelpos, 0.2, ROOT.kRed, "m = "+str(mass_cut)+" GeV")
        self.addLabel(0.2,0.85,self.objs.AtlasLabel)
        ZAxisDim = coordSystem.coordIndex(fixedCoord[0])
        az = coordSystem.axis[ZAxisDim[0]]
        ROOT.myText(0.2,0.5,1,az.title + "="+str(fixedCoord[1])+az.unit)
        page.save( h.GetName() )
        ROOT.gStyle.SetOptTitle( optT )
        page.currentPad().SetRightMargin(0.05)                

        return h
        


    def drawJMSCorrectionsVsX(self, X, variedCoord, fixedCoord, yrange=(0.2,1.2) , jmsCorrection=None, legPos=(0.3,0.21,0.5,0.41)):
        """ draw several graphs of  calib factors vs X. Several curves are drawn at different Y
        values, all at a fixed Z value where :

          - variedCoord == (Y, [ values...] ) Y is a variable name. One curve will be drawn for each value.
          - fixedCoord == (Z, value)

         ex :
          drawJMSCorrectionsVsX("Pt", ("M", [60, 80, 125, 175] ), ("Eta",0.) )
          ---> draw 4 JMS vs Pt curves at M=60, 80, 125, 175 and at Eta=0.

        See also calibFactorToGraph which builds each graph.
        """
        grList = []
        g = ROOT.TMultiGraph()
        fixedCoords=(fixedCoord[0], variedCoord[0])
        fixedValues=[fixedCoord[1], 0]
        for m in variedCoord[1]:
            fixedValues[1] = m
            gr = self.calibFactorToGraph(X, fixedCoords, fixedValues, jmsCorrection=jmsCorrection)
            ROOT.SetOwnership(gr,False)
            grList.append(gr)
            g.Add(gr)
        g.Draw("a plc ")
        g.GetXaxis().SetTitle(grList[0].GetXaxis().GetTitle())
        g.SetMaximum(yrange[1])
        g.SetMinimum(yrange[0])
        leg = buildLegend(grList, legPos=legPos)
        leg.Draw()
        return g, leg
            



# ************************************************
class JESPlots(Plots):
    """This object defines several function to create plots of the various histograms and graphs
    used as part of the JES derivation
    """
    
    def __init__(self, objs, refConfig = ''):
        Plots.__init__(self,objs)
        self.objs = objs
        self._needReadFiles = True
        self.addRefResposeFunc = (refConfig != '')
        if self.addRefResposeFunc:
            self.RefJES, self.RefEtaCorr = self.configFileToTF1(refConfig)

    def setStyles(self):
        styles = Plots.setStyles(self)
        #Use invered bird. Better in grayscale
        ROOT.DJS.SetInvertedBird()
        ROOT.gStyle.SetPadRightMargin(0.13)
        closure_objs = [
            "closure_R_vs_Etrue_Graph",
            "closure_DEta_vs_Etrue_Graph",
            "closure_R_vs_pTtrue_Graph",
            
            ]        

        numinv_objs = [
            "R_vs_Enuminv_Graph",

            ]
        raw_objs = [
            "R_vs_Etrue_Graph",
            "R_vs_pTtrue_Graph",
            "DEta_vs_Etrue_Graph",
            ]        

        for o in closure_objs+numinv_objs+raw_objs:
            styles[ o ].update(LineWidth = 2 , FillColor=ROOT.kWhite, MarkerStyle=4)

        for o in closure_objs:
            styles[ o ].update(LineColor = ROOT.kRed , MarkerColor=ROOT.kRed, Title="calibrated")
        for o in raw_objs:
            styles[ o ].update( Title="uncalibrated")
        self.styles = styles

        for o in closure_objs+numinv_objs+raw_objs:
            styleDic = styles[o]
            if not hasattr(self.objs,o): continue
            for h in getattr(self.objs,o):
                setHistoProp(h, styleDic)

    #This function (copied from compareJES) turn the calibration config file containing calibration parameters into response function.
    #Use this to compare JES from another result
    @classmethod
    def configFileToTF1(cls, fname, altPrefix='', erange=(20,4000)):
        jesList = []
        etaList = []
        f = open(fname)
        terms = [ '[p0]','[p1]*pow(log(x),1)', '[p2]*pow(log(x),2)','[p3]*pow(log(x),3)','[p4]*pow(log(x),4)', '[p5]*pow(log(x),5)','[p6]*pow(log(x),6)','[p7]*pow(log(x),7)','[p8]*pow(log(x),8)']
        for l in f:
            prefix = ''
            if l.startswith('JES'):
                prefix = altPrefix+'JES'
                theList = jesList
            elif l.startswith('EtaCorr'):
                prefix = altPrefix+'EtaCorr'
                theList = etaList
            else:
                continue

            words=l[:-1].strip().split()[1:]
            params = []
            for w in words:
                if w !='':
                    p = float(w)
                    if p != 0.0:
                        params.append(p)
                    else: break
            maxOrder = len(params)
            formula = '+'.join(terms[:maxOrder] )
            func = ROOT.TF1( prefix+str(len(theList)), formula, *erange)
            func.SetTitle("Bin "+str(len(theList)))
            for i,p in enumerate(params):
                func.SetParameter(i,p)
            theList.append(func)

        return jesList, etaList

    def readFiles(self):
        if not self._needReadFiles: return
        objs = self.objs
        self.objs.readEtaJESFromFile( loadRespPerBin=True)
    
        etaBins = self.objs.etaBins
        etaBins.m_isGeV = False
        for i in range(etaBins.nBins() ):
            self.objs.R_vs_Etrue_Graph[i].SetBit(ROOT.TGraph.kIsSortedX)
        self.setStyles()
        self._needReadFiles = False
        

    def drawAll(self):
        from DeriveJetScales.DrawUtils import buildPage, PdfSaver
        objs = self.objs
        outName = self.objs.jesFile(".pdf") 
        self.readFiles()
        
        page = buildPage(PdfSaver, 1, outName)
        ptMin = 140 if objs.radius > 0.4 else 15
        p,mg=self.drawVsEta(page=page, ptMin=ptMin)
        p,mg=self.drawVsEta(page=page, R_vs_E_Graph = objs.closure_R_vs_Etrue_Graph, ptMin=ptMin)
        p,mg=self.drawVsEta(page=page, R_vs_E_Graph = objs.R_vs_mu_Graph, ePoints=[15,20,25,30,35], unit="", colors=[1,2,3,4,5], markerStyles=[24,25,26,27,28], ptMin=0)
        self.drawJESMap(page=page)
        self.drawJESMap(page=page,rawResp=False)
        etaBins = self.objs.etaBins
        for ieta in xrange(etaBins.nBins()):
            page.setNplots( 1 )
            
            eta= etaBins.binCenter( ieta )
            binDesc = self.objs.etaBins.describeBin(ieta)
            print "ieta = _ ",ieta, binDesc
            ROOT.myText(0.3, 0.5, 1, "Eta bin "+str(ieta)+" "+binDesc )
            page.nextPad()

            page.setNplots( 2 )
            self.drawRawResp2D(ieta,page)
            p=page.nextPad()
            self.drawRawRespVsE(ieta,page)
            p=page.nextPad()

            page.setNplots( (3,3) )
            self.drawResponseHistos(ieta,page)
            p=page.nextPad()

            p=page.setNplots( 2 )
            self.drawNIRespVsE(ieta,page)
            p=page.nextPad()
            self.drawClosureRespVsE(ieta,page)
            p=page.nextPad()

            page.setNplots( 2 )
            self.drawDEta2D(ieta, page)
            p=page.nextPad()
            self.drawDEtavsE(ieta, page)
            #self.drawDEtavsE(ieta, page)
            p=page.nextPad()

            page.setNplots( (3,3) )
            self.drawDEtaHistos(ieta, page)
            p=page.nextPad()

            p=page.setNplots( 2 )        
            self.drawClosureDEtavsE(ieta, page)
            #self.drawClosureDEtavsE(ieta, page)
            page.nextPad()
            page.forceSave()
        page.close()

    def dumpConstants(self):
        self.readFiles()
        jesParams = []
        etaParams = []
        objs = self.objs
        jetType = self.objs.jetType
        for i in xrange(self.objs.etaBins.nBins()):
            # *******************
            # Collect constants
            jesParams.append( tf1Params( self.objs.R_vs_Enuminv_Func[i] ) )
            etaParams.append( tf1Params( self.objs.DEta_vs_Etrue_Func[i] ) )

        # *******************
        # save constants
        outName = self.objs.jesFile(".pdf") 
        
        npar = 9 # fixed number of parameter to output
        py_line = '[ '+ '%11.4e ,'*npar +'], ## bin %d \n'
        config_line = '%s.%s_Bin%d: '+'%11.4e '*npar + '\n'
        constPyFile = open(outName.replace('.pdf','_constants.py') ,'w')
        constConfigFile = open(outName.replace('.pdf','_constants.config') ,'w')

        constPyFile.write("## Jet energy calibration\n")
        constPyFile.write("##   [0]+[1]*log(x)+[2]*log(x)^2+...\n")
        constPyFile.write("energyCorrDict[ '%s_%s' ] = [\n"%( jetType, "LC") )
        constConfigFile.write("## Jet energy calibration\n")



        jesParams.append( jesParams[-1] ) # extend by 1 (compatibility with R20...)
        for i, params in enumerate(jesParams):
            while( len(params) < npar) : params.append( 0. ) 
            constPyFile.write( py_line%( tuple(params)+(i,) ) )
            constConfigFile.write(config_line % ( ('JES', jetType, i)+tuple(params) ) )

        constPyFile.write("]\n")
        constConfigFile.write('\n')

        constPyFile.write("## Jet eta correction\n")
        constPyFile.write("##   [0]+[1]*log(x)+[2]*log(x)^2+...\n")
        constPyFile.write("etaCorrDict[ '%s_%s' ] = [\n"%(jetType, "LC") );

        constConfigFile.write("## Jet eta correction\n")

        etaParams.append( etaParams[-1] )
        for i, params in enumerate(etaParams):
            while( len(params) < npar) : params.append( 0. )
            constPyFile.write( py_line%( tuple(params)+(i,) ) )
            constConfigFile.write(config_line % ( ('EtaCorr', jetType, i)+tuple(params) ) )

        constPyFile.write("]\n")
        constConfigFile.write('\n')


        constPyFile.close()
        constConfigFile.close()

    def makeEResponseVsEtaGraphs(self, R_vs_E_Graph, 
                                 ePoints = [150 , 200, 300 , 400, 500, 800, 1000, 1500, 2000, 3000, 4000 ], unit = " GeV",
                                 colors = [1, 2, 3, 4, 5, 6, 7, 8, 9, 11, 12, 28, 32, 38, 41, 42, 45, 46, 49],
                                 markerStyles = [24, 25, 26, 27, 28, 30, 32, 33, 22, 23, 20, 21], ptMin=20):
        etaBins = self.objs.etaBins
        # We create  TGraphs  containing (x,y+err) instead of (x,y)
        #  these new graphs will be used to interpolate the errors at the E we want.
        # (We can't do this from R_vs_E_Graph because TGraphErrors do not have Eval() for errors)
        R_vs_E_Graph_errp = ROOT.vector("TGraph")(R_vs_E_Graph.size())
        for ieta in xrange(etaBins.nBins() ):
            x, y, y_err = R_vs_E_Graph[ieta].GetX(), R_vs_E_Graph[ieta].GetY(), R_vs_E_Graph[ieta].GetEY()
            R_vs_E_Graph_errp[ieta].Set(R_vs_E_Graph[ieta].GetN())
            for i in range(R_vs_E_Graph[ieta].GetN()):
                R_vs_E_Graph_errp[ieta].SetPoint(i, x[i], y[i]+y_err[i])
        
        allGvsEta = ROOT.TMultiGraph()
        #ncolF = ROOT.TColor.GetNumberOfColors()/len(ePoints)
        cosh = ROOT.TMath.CosH
        for ip,E in enumerate(ePoints):
            g = ROOT.TGraphErrors(etaBins.nBins())
            npoint=0
            ROOT.SetOwnership(g,False)
            for i in xrange(etaBins.nBins() ):
                if E/cosh(etaBins.binCenter(i)) < ptMin: continue
                print E, i , ' --> ', E/cosh(etaBins.binCenter(i)), ptMin
                y = R_vs_E_Graph[i].Eval(E)
                err = R_vs_E_Graph_errp[i].Eval(E) - y
                g.SetPoint(npoint, etaBins.binCenter( i ),  y )
                g.SetPointError(npoint, 0,  err )
                npoint+=1
            g.Set(npoint)
            g.SetTitle(str(E)+unit)
            g.SetFillStyle(0)
            g.SetLineWidth(2)
            g.SetLineColor(colors[ip])
            g.SetMarkerColor(colors[ip])
            #g.SetLineColor(ROOT.TColor.GetColorPalette(ip*ncolF))
            #g.SetMarkerColor(ROOT.TColor.GetColorPalette(ip*ncolF))
            g.SetMarkerStyle( markerStyles[ip] )
            allGvsEta.Add(g)

        return allGvsEta


    def drawVsEta(self, page=None, suffix='.png', R_vs_E_Graph=None, ePoints=None, unit=" GeV", colors=None, markerStyles=None, ptMin=20):
        """ Draw several E response curves vs eta at energy fixed, showing only points with pT> ptMin
        (so curves at low E only show points at low eta). 
        """
        self.readFiles()
        from DeriveJetScales.DrawUtils import buildPage, PdfSaver, ImgSaver
        if page is None:
            page  = buildPage(suffix[1:], 1, self.objs.jesFile( "_vsEta" ) )
            
        etaBins = self.objs.etaBins

        if R_vs_E_Graph is None:
            R_vs_E_Graph = self.objs.R_vs_Etrue_Graph
        if ePoints is None:
            ePoints= [150 , 200, 300 , 400, 500, 800, 1000, 1500, 2000, 3000, 4000 ]
            colors = [1, 2, 3, 4, 5, 6, 7, 8, 9, 11, 12, 28, 32, 38, 41, 42, 45, 46, 49]
            markerStyles = [24, 25, 26, 27, 28, 30, 32, 33, 22, 23, 20, 21]

        allGvsEta = self.makeEResponseVsEtaGraphs(R_vs_E_Graph=R_vs_E_Graph, ePoints=ePoints, unit=unit, colors=colors, markerStyles=markerStyles, ptMin=ptMin)
        allGvsEta.Draw("ALP")
        allGvsEta.GetXaxis().SetTitle("#eta reco")
        allGvsEta.GetYaxis().SetTitle("E response")
        allGvsEta.SetMaximum(allGvsEta.GetHistogram().GetMaximum()*1.1)
        allGvsEta.SetMinimum( min( allGvsEta.GetHistogram().GetMinimum()*0.99, 0.97) )

        self.addLabel(0.2,0.88,self.objs.AtlasLabel)
        page.lines_RvsEta = drawLineAtClosure(allGvsEta.GetHistogram())
        page.legend_R_vs_eta = buildLegend([ g for g in allGvsEta.GetListOfGraphs() ], legPos=(0.65,0.6,0.9,0.9), ncol=2)
        page.legend_R_vs_eta.AddEntry(None, "p_{T}>%dGeV"%(ptMin,) , "")
        ROOT.SetOwnership(page.legend_R_vs_eta,False)
        page.currentPad().SetLogx(False)
        page.currentPad().SetRightMargin(0.05)
        page.forceSave()
        return page, allGvsEta


    def drawJESMap(self, page=None, suffix='.png', rawResp=True):
        self.readFiles()
        from DeriveJetScales.DrawUtils import buildPage, PdfSaver, ImgSaver
        if page is None:
            page = buildPage(suffix[1:], 1, self.objs.jesFile( "_vsEtrueEta" ))
        etaBins = self.objs.etaBins

        g = ROOT.TGraph2D()
        R_vs_E_Graph = self.objs.R_vs_Etrue_Graph if rawResp else self.objs.closure_R_vs_Etrue_Graph
        pointN=0
        for ieta in xrange(etaBins.nBins() ):
            x, z = R_vs_E_Graph[ieta].GetX(), R_vs_E_Graph[ieta].GetY()
            for i in range(R_vs_E_Graph[ieta].GetN() ):
                y = etaBins.binCenter(ieta)
                g.SetPoint(pointN,x[i],y,z[i])
                pointN=pointN+1
        g.SetTitle(";E_{true} [GeV];#eta;R_{E}") if rawResp else g.SetTitle(";E_{true} [GeV];#eta;closure R_{E}")
        g.Draw("surf1")
        page.forceSave()

    def addLabel(self,x,y, Atlaslabel):
        ROOT.ATLASLabel(x,y,Atlaslabel,1)
        y=y-0.06
        for desc in self.objs.jetTypePublic:
            ROOT.myText(x,y,1,desc)
            y=y-0.06

    def addText(self,x,y, binDesc, text):
        ROOT.myText(x, y ,  1, binDesc)
        ROOT.myText(x, y +0.06, 1, text)
        ROOT.myText(x, y +0.12, 1, self.objs.jetType)
        

    def drawRawResp2D(self, i, page, binDesc=None):
        if binDesc is None:binDesc = self.objs.etaBins.describeBin(i)

        h=self.objs.R_vs_Etrue[i]
        h.SetXTitle("Etrue, "+binDesc)
        h.SetYTitle("Response")
        h.Draw("colz")
        page.currentPad().SetLogx()
        page.currentPad().SetLogz()
        page.currentPad().SetGridy()
        z=h.GetZaxis()
        z.SetRangeUser( h.GetMinimum(0), h.GetMaximum() )
        

    def drawRawRespVsE(self, i, page, binDesc=None):
        if binDesc is None:binDesc = self.objs.etaBins.describeBin(i)

        etaMin = self.objs.etaBins.binLowEdge(i)
        etaMax = self.objs.etaBins.binUpEdge(i)

        page.legend_RvsE = buildLegend([], legPos=(0.6,0.45,0.8,0.7))

        #Response fit function: blue lines
        #Draw this first to show the extrapolated range
        g_R_vs_E_Func = self.objs.R_vs_Etrue_Func[i]
        g_R_vs_E_Func.SetLineColor(ROOT.kBlue)
        g_R_vs_E_Func.SetMaximum(1.1)
        g_R_vs_E_Func.Draw()
        page.legend_RvsE.AddEntry(g_R_vs_E_Func, "Fitted Response", "l")

        #R vs Ereco(num inv): black points
        g_R_vs_Etrue=self.objs.R_vs_Etrue_Graph[i]
        g_R_vs_Etrue.SetMarkerSize(1)
        g_R_vs_Etrue.SetMarkerStyle(ROOT.kFullCircle)
        g_R_vs_Etrue.Draw("P")
        page.legend_RvsE.AddEntry(g_R_vs_Etrue, "Response", "p")

        page.ptline_RvsE = self.drawpTLineOnEAxis(i, page, g_R_vs_E_Func, pT=20, color=ROOT.kMagenta, lineMargin=0.05)
        page.legend_RvsE.AddEntry(page.ptline_RvsE, "p_{T}=20 GeV", "l")
        
        self.addText(0.5, 0.2, binDesc,"Fitted response vs E_{true}")
        p=page.currentPad()
        p.SetLogx()
        g_R_vs_E_Func.GetXaxis().SetTitle("E_{true} [GeV]") # do it after SetLogx() or doesn't work !!??
        g_R_vs_E_Func.GetYaxis().SetTitle("Fitted response")
        page.lines_RvsE = drawLineAtClosure(g_R_vs_E_Func)
        page.legend_RvsE.Draw()

    def drawResponseHistos(self, i, page, binDesc=None):
        if binDesc is None:binDesc = self.objs.etaBins.describeBin(i)

        for j,h in enumerate(self.objs.R_vs_Etrue_perEtaBin[i]):
            h.Draw()
            page.nextPad()
        page.forceSave()

    def drawSingleResponseHisto(self, ieta, ie, page, binDesc=None):
        if binDesc is None:binDesc = self.objs.etaBins.describeBin(ieta)
        page.currentPad().SetGridx()
        h = self.objs.R_vs_Etrue_perEtaBin[ieta][ie]
        h.Draw()
        hc = self.objs.closure_R_vs_Etrue_perEtaBin[ieta][ie]
        hc.SetLineColor(0)
        hc.Draw("same")
        self.addText(0.65, 0.5, binDesc,"")
        page.nextPad()
        
    def drawResponseDistAtFixedE(self, ebin, page=None, etamin=-2, etamax=2):
        if page is None:
            from DeriveJetScales.DrawUtils import buildPage            
            page = buildPage('pdf', 1, self.objs.jesFile( "_RespDistAtEbin"+str(ebin) ))
        page.setNplots( (3,3))
        for ieta in range(self.objs.etaBins.nBins()):
            eta = self.objs.etaBins.binCenter(ieta)
            if eta< etamin or eta>etamax: continue
            self.drawSingleResponseHisto(ieta, ebin, page)
        page.forceSave()
        page.close()
        
    def drawNIRespVsE(self, i, page, binDesc=None):
        if binDesc is None:binDesc = self.objs.etaBins.describeBin(i)

        etaMin = self.objs.etaBins.binLowEdge(i)
        etaMax = self.objs.etaBins.binUpEdge(i)

        page.legend_NIRvsE = buildLegend([], legPos=(0.43,0.2,0.63,0.4))

        #Draw this first to show the extrapolated range
        #Groom function: red circles
        g_R_vs_Enuminv_SLGraph = self.objs.R_vs_Enuminv_SLGraph[i]
        g_R_vs_Enuminv_SLGraph.SetMaximum(1.05)
        g_R_vs_Enuminv_SLGraph.SetMinimum(0.2)
        g_R_vs_Enuminv_SLGraph.SetMarkerSize(2)
        g_R_vs_Enuminv_SLGraph.SetMarkerColor(ROOT.kRed)
        g_R_vs_Enuminv_SLGraph.SetMarkerStyle(24)
        g_R_vs_Enuminv_SLGraph.SetFillColor(0)
        g_R_vs_Enuminv_SLGraph.Draw("AP")
        page.legend_NIRvsE.AddEntry(g_R_vs_Enuminv_SLGraph, "Response Fit Points", "p")

        #Actual response fit function to be used: blue lines
        g_R_vs_Enuminv_Func = self.objs.R_vs_Enuminv_Func[i]
        g_R_vs_Enuminv_Func.SetLineColor(ROOT.kBlue)
        g_R_vs_Enuminv_Func.Draw("same")
        page.legend_NIRvsE.AddEntry(g_R_vs_Enuminv_Func, "Fitted Response", "l")

        #If given, plot the reference response
        if self.addRefResposeFunc:
            g_R_vs_Enuminv_Func_Ref = self.RefJES[i]
            g_R_vs_Enuminv_Func_Ref.SetRange(g_R_vs_Enuminv_SLGraph.GetXaxis().GetXmin(), g_R_vs_Enuminv_SLGraph.GetXaxis().GetXmax())
            g_R_vs_Enuminv_Func_Ref.SetLineColor(ROOT.kGreen)
            g_R_vs_Enuminv_Func_Ref.SetLineStyle(7)
            g_R_vs_Enuminv_Func_Ref.Draw("same")
            page.legend_NIRvsE.AddEntry(g_R_vs_Enuminv_Func_Ref, "Ref Fitted Response", "l")

        #R vs Ereco(num inv): black points
        g_ni=self.objs.R_vs_Enuminv_Graph[i]
        g_ni.SetMarkerSize(1)
        g_ni.SetMarkerStyle(ROOT.kFullCircle)
        g_ni.Draw("P")
        page.legend_NIRvsE.AddEntry(g_ni, "Response", "p")

        # red dashed line
        g_R_vs_Enuminv_GroomExt_Func = self.objs.R_vs_Enuminv_GroomExt_Func[i]
        g_R_vs_Enuminv_GroomExt_Func.SetLineColor(ROOT.kRed)
        g_R_vs_Enuminv_GroomExt_Func.SetLineStyle(3)
        g_R_vs_Enuminv_GroomExt_Func.Draw("same")
        page.legend_NIRvsE.AddEntry(g_R_vs_Enuminv_GroomExt_Func, "Groom Fit", "l")

        page.ptline_NIRvsE = self.drawpTLineOnEAxis(i, page, g_R_vs_Enuminv_SLGraph.GetHistogram(), pT=20, color=ROOT.kMagenta, lineMargin=0.05)
        page.legend_NIRvsE.AddEntry(page.ptline_NIRvsE, "p_{T}=20 GeV", "l")
        
        self.addText(0.65, 0.2, binDesc,"Fitted response vs E_{reco (NI)}")
        p=page.currentPad()
        p.SetLogx()
        g_R_vs_Enuminv_SLGraph.GetXaxis().SetTitle("E_{reco (NI)} [GeV]") # do it after SetLogx() or doesn't work !!??
        g_R_vs_Enuminv_SLGraph.GetYaxis().SetTitle("Fitted response")
        page.lines_NIRvsE = drawLineAtClosure(g_R_vs_Enuminv_SLGraph.GetHistogram())
        page.legend_NIRvsE.Draw()

    def drawClosureRespVsE(self, i, page, binDesc=None):
        if binDesc is None:binDesc = self.objs.etaBins.describeBin(i)

        p=page.currentPad()
        page.legend_clRvsE = buildLegend([], legPos=(0.65,0.45,0.85,0.65))

        g_cl=self.objs.closure_R_vs_Etrue_Graph[i]
        g_cl.GetXaxis().SetTitle("E_{true} [GeV]")
        g_cl.GetYaxis().SetTitle("Fitted response")
        g_cl.SetMinimum(0.96)
        g_cl.SetMaximum(1.14)
        g_cl.Draw("AP")
        page.legend_clRvsE.AddEntry(g_cl, "Calibrated Response", "P")

        page.lines_clRvsE = drawLineAtClosure(g_cl)
        page.legend_clRvsE.AddEntry(page.lines_clRvsE[1], "10% Response", "L")

        page.ptline_clRvsE = self.drawpTLineOnEAxis(i, page, g_cl.GetHistogram(), pT=20, color=ROOT.kGreen, lineMargin=0.1)
        page.legend_clRvsE.AddEntry(page.ptline_clRvsE, "p_{T}=20 GeV", "l")

        self.addText(0.7, 0.7,binDesc, "Closure response" )

        p.SetLogx()
        page.legend_clRvsE.Draw()


    def drawClosureRespVsPt(self, i, page, binDesc=None):
        if binDesc is None:binDesc = self.objs.etaBins.describeBin(i)

        g=self.objs.R_vs_pTtrue_Graph[i]
        g.Draw("ALP")
        g.SetMaximum(1.1)
        g.SetMinimum(0.5)        
        g_cl=self.objs.closure_R_vs_pTtrue_Graph[i]
        page.lines_vspt = drawLineAtClosure(g_cl)
        g_cl.GetXaxis().SetTitle("p_{T} true")
        g_cl.GetYaxis().SetTitle("Fitted response")        
        g_cl.Draw("LP")
        p=page.currentPad()
        page.legend_vspt= buildLegend([g,g_cl])
        self.addText(0.5, 0.2,binDesc, "Closure response" )

        p.SetLogx()


        # Plot DEta responses

    def drawDEta2D(self, i, page, binDesc=None):
        if binDesc is None:binDesc = self.objs.etaBins.describeBin(i)

        h=self.objs.DEta_vs_Etrue[i]
        h.SetXTitle("Etrue, "+binDesc)
        h.SetYTitle("Response")
        h.Draw("colz")
        page.currentPad().SetLogx()
        page.currentPad().SetLogz()


    def drawDEtavsE(self, i, page, binDesc=None):
        if binDesc is None:binDesc = self.objs.etaBins.describeBin(i)

        etaMin = self.objs.etaBins.binLowEdge(i)
        etaMax = self.objs.etaBins.binUpEdge(i)

        page.legend_DEtavsE = buildLegend([], legPos=(0.65,0.2,0.85,0.4))

        #Response fit function: blue lines
        #Draw this first to show the extrapolated range
        g_DEta_vs_E_Func = self.objs.DEta_vs_Etrue_Func[i]
        g_DEta_vs_E_Func.SetMinimum(-0.03)
        g_DEta_vs_E_Func.SetMaximum(0.03)
        g_DEta_vs_E_Func.SetLineColor(ROOT.kBlue)
        g_DEta_vs_E_Func.Draw()
        page.legend_DEtavsE.AddEntry(g_DEta_vs_E_Func, "Fitted Response", "l")
        
        #If given, plot the reference response
        if self.addRefResposeFunc:
            g_DEta_vs_E_Func_Ref = self.RefEtaCorr[i]
            g_DEta_vs_E_Func_Ref.SetMinimum(-0.03)
            g_DEta_vs_E_Func_Ref.SetMaximum(0.03)
            g_DEta_vs_E_Func_Ref.SetRange(g_DEta_vs_E_Func.GetXmin(), g_DEta_vs_E_Func.GetXmax())
            g_DEta_vs_E_Func_Ref.SetLineColor(ROOT.kGreen)
            g_DEta_vs_E_Func_Ref.SetLineStyle(7)
            g_DEta_vs_E_Func_Ref.Draw("same")
            page.legend_DEtavsE.AddEntry(g_DEta_vs_E_Func_Ref, "Ref Fitted Response", "l")

        #dEta vs Etrue: black points
        g_DEta_vs_Etrue=self.objs.DEta_vs_Etrue_Graph[i]
        g_DEta_vs_Etrue.SetMarkerSize(1)
        g_DEta_vs_Etrue.SetMarkerStyle(ROOT.kFullCircle)
        g_DEta_vs_Etrue.Draw("P")
        page.legend_DEtavsE.AddEntry(g_DEta_vs_Etrue, "Response", "p")

        page.ptline_DEtavsE = self.drawpTLineOnEAxis(i, page, g_DEta_vs_E_Func.GetHistogram(), pT=20, color=ROOT.kMagenta, lineMargin=0.05)
        page.legend_DEtavsE.AddEntry(page.ptline_DEtavsE, "p_{T}=20 GeV", "l")
        
        self.addText(0.7, 0.7, binDesc,"Fitted #Delta #eta vs E_{true}")
        p=page.currentPad()
        p.SetLogx()
        g_DEta_vs_E_Func.GetXaxis().SetTitle("E_{true} [GeV]") # do it after SetLogx() or doesn't work !!??
        g_DEta_vs_E_Func.GetYaxis().SetTitle("Fitted #Delta #eta")
        page.lines_DEtavsE = drawLineAtClosure(g_DEta_vs_E_Func, isDEta=True)
        page.legend_DEtavsE.Draw()

    def drawDEtaHistos(self, i, page, binDesc=None):
        if binDesc is None:binDesc = self.objs.etaBins.describeBin(i)

        for j,h in enumerate(self.objs.DEta_vs_Etrue_perEtaBin[i]):
            h.Draw()
            page.nextPad()
        page.forceSave()
        
    def drawClosureDEtavsE(self, i, page, binDesc=None):
        if binDesc is None:binDesc = self.objs.etaBins.describeBin(i)

        p=page.currentPad()
        page.legend_clDEtavsE = buildLegend([], legPos=(0.65,0.2,0.85,0.4))

        g_cl=self.objs.closure_DEta_vs_Etrue_Graph[i]
        g_cl.GetXaxis().SetTitle("E_{true} [GeV]")
        g_cl.GetYaxis().SetTitle("Fitted #Delta #eta")
        g_cl.SetMinimum(-0.03)
        g_cl.SetMaximum(0.03)
        g_cl.Draw("AP")
        page.legend_clDEtavsE.AddEntry(g_cl, "Calibrated Response", "P")

        page.lines_clDEtavsE = drawLineAtClosure(g_cl, isDEta=True)
        page.legend_clDEtavsE.AddEntry(page.lines_clDEtavsE[1], "10% Response", "L")

        page.ptline_clDEtavsE = self.drawpTLineOnEAxis(i, page, g_cl.GetHistogram(), pT=20, color=ROOT.kGreen, lineMargin=0.1)
        page.legend_clDEtavsE.AddEntry(page.ptline_clDEtavsE, "p_{T}=20 GeV", "l")

        self.addText(0.7, 0.7,binDesc, "Closure response" )

        p.SetLogx()
        page.legend_clDEtavsE.Draw()


    def drawpTLineOnEAxis(self, i, page, axisHist, pT, color=ROOT.kBlue+1, linestyle=2, lineMargin=0.25, textShift=0):
        etaMin = self.objs.etaBins.binLowEdge(i)
        etaMax = self.objs.etaBins.binUpEdge(i)
        yMin = axisHist.GetMinimum()
        yMax = axisHist.GetMaximum()
        Dy = lineMargin*(yMax-yMin)

        line = ROOT.TLine()
        line.SetLineColor(color)
        line.SetLineWidth(2);
        line.SetLineStyle(linestyle)

        line.DrawLine(pT*CosH((etaMin+etaMax)/2),yMin+Dy,pT*CosH((etaMin+etaMax)/2),yMax-Dy)
        
        #tex = ROOT.TLatex()
        #tex.SetNDC(0)
        #tex.SetTextColor(color);
        #tex.DrawLatex(pT*CosH((etaMin+etaMax)/2)+textShift,yMin+0.8*Dy,"p_{T}="+str(pT)+"GeV");
        #tex.SetNDC(1);
        return line


# ************************************************

def tf1Params(f):
    buf = f.GetParameters()
    return [buf[i] for i in xrange(f.GetNpar()) ]

def setHistoProp(h, hprop):
    for k,v in hprop.iteritems():
        getattr(h,'Set'+k)(v)

def drawLineAtClosure(g,isDEta=False):
    zeroLine = 0 if isDEta else 1
    minX=g.GetXaxis().GetXmin()
    maxX=g.GetXaxis().GetXmax()
    minY=g.GetMinimum()#GetYaxis().GetXmin()
    maxY=g.GetMaximum()#GetYaxis().GetXmax()
    l0 = ROOT.TLine(minX,zeroLine,maxX,zeroLine)
    lp = ROOT.TLine(minX,zeroLine + 0.01,maxX,zeroLine + 0.01)
    lm = ROOT.TLine(minX,zeroLine - 0.01,maxX,zeroLine - 0.01)
    l0.SetLineStyle(3)
    lp.SetLineStyle(4)
    lm.SetLineStyle(4)
    linesToDraw = [l0]
    if maxY>zeroLine+0.01 or isDEta:
        linesToDraw.append(lp)
    if minY<zeroLine-0.01 or isDEta:
        linesToDraw.append(lm)
    [l.Draw() for l in linesToDraw]
    return (l0,lp,lm)




# #######################################

class JMSCoord(object):
    
    """Helper class to convert from one coordinate system to (E,m,eta) and draw JMS calib factors vs coordinates. """


    # the known coordinate systems mapped by name of their variable
    # format :
    #  (varName1 , varName2) : ( varObject1, varObject2, translationfunction)
    # see below for examples
    coordSystems = {}


    @staticmethod
    def newCoord(n1 , n2 , n3=""):
        """Return a JMSCoord object from variable names n1,n2 and n3 """
        
        # any valid coord system as Eta as part of n1,n2,n3.
        # make sure Eta is not n1 nor n2
        if n3!="":
            if n1=="Eta": n1 =n3
            elif n2=="Eta": n2 = n3

        # look for a coord system based on n1 and n2
        coords = JMSCoord.coordSystems.get( (n2,n1), None)
        if coords is None: coords = JMSCoord.coordSystems[(n1,n2)]
        v1, v2, f = coords
        return JMSCoord(v1,v2)

    
    def __init__(self, v1,v2): # v3 is always eta
        self.v1 = v1
        self.v2 = v2
        self.v3 = var_eta
        self.axis = [v1,v2,var_eta]
        self.toEMEta = self.coordSystems[(v1.name,v2.name)][2]

    def toEMEta(self):
        pass

    def coordIndex(self, *names):
        def findIndex(v):
            if isinstance(v,int): return v
            else: return [i for i in (0,1,2) if self.axis[i].name ==v][0]
        return tuple(findIndex(v) for v in names)
    
    def calibFactorsVsX(self, xdim , fixedCoords , fixedValues, jmsCorrection, doTGraph=False):
        """ returns a TH1 or TGraph of 'jmsCorrection() vs xdim' at fixedCoords set to fixedValues
        """
        # make sure we have index even if we are passed strings
        xdim, = self.coordIndex(xdim)
        fixedCoords = self.coordIndex(*fixedCoords)
        
        ax = self.axis[xdim]
        ay = self.axis[fixedCoords[0]]
        az = self.axis[fixedCoords[1]]
        
        pos0=[0,0,0]
        pos0[fixedCoords[0]] = fixedValues[0]
        pos0[fixedCoords[1]] = fixedValues[1]
        toEMEta = self.toEMEta

        name = ax.name+"_at"+ay.rep%(fixedValues[0],)+az.rep%(fixedValues[1],)
        title = az.title + "="+az.rep%(fixedValues[1],)+az.unit+","+ay.title + "="+ay.rep%(fixedValues[0],)+ay.unit

        if doTGraph:
            if len(ax.range)==3: xrange= seq(ax.range[1], ax.range[2], float(ax.range[2]- ax.range[1])/ax.range[0])
            else : xrange=ax.range[0]
            g = ROOT.TGraph(len(xrange))
            g.SetTitle(title)
            g.GetXaxis().SetTitle(ax.title)
            g.GetYaxis().SetTitle("JMS calib")
            for i,x in enumerate(xrange):
                pos0[xdim] = x
                pos = toEMEta(self, *pos0 )            
                g.SetPoint(i,x,jmsCorrection(*pos) )
            return g

        else:
            title += ";"+ax.title+";JMS calib"
            hbins = h1(* ax.range )
            h = ROOT.TH1F(name, title, *hbins ) 
            
            tax = h.GetXaxis()
            
            for ix in xrange(1,tax.GetNbins()+1):        
                pos0[xdim] = tax.GetBinCenter(ix)
                pos = toEMEta(self, *pos0 )
                b = h.GetBin(ix)                
                h.SetBinContent(b, jmsCorrection(*pos) )
            return h

    def calibFactorsToTH2(self, (xdim,ydim) , (fixedCoord, fixedV), jmsCorrection, zTitle=""):

        # make sure we have index even if we are passed strings
        xdim, ydim, fixedCoord = self.coordIndex( xdim, ydim, fixedCoord )

        ax = self.axis[xdim]
        ay = self.axis[ydim]
        az = self.axis[fixedCoord]
        
        name = ax.name+ay.name+"_at"+az.rep%(fixedV,)
        title =";"+ax.AxisTitle+";"+ay.AxisTitle
        hbins = h2(* (ax.range+ay.range) )
        h = ROOT.TH2F(name, title, *hbins ) 
        
        tax = h.GetXaxis()
        tay = h.GetYaxis()

        pos0=[0,0,0]
        pos0[fixedCoord] = fixedV
        toEMEta = self.toEMEta
        for ix in xrange(1,tax.GetNbins()+1):        
            pos0[xdim] = tax.GetBinCenter(ix)
            for iy in xrange(1,tay.GetNbins()+1):        
                pos0[ydim] = tay.GetBinCenter(iy)
                pos = toEMEta(self, *pos0 )
                b = h.GetBin(ix,iy)                
                h.SetBinContent(b, jmsCorrection(*pos) )
        zax = h.GetZaxis()
        zax.SetTitle(zTitle)
        zax.SetTitleOffset(0.8)
                
        return h


    def EMEta_toEMEta(self, v1,v2,v3):
        return v1,v2,v3
    def EtMEta_toEMEta(self, v1,v2,v3):
        e = v1*CosH(v3)
        eta = v3
        m = v2
        return e,m,eta
    def PtMEta_toEMEta(self,v1 , v2 ,v3):
        e = Sqrt( (v1*CosH(v3))**2 +v2**2)
        eta = v3
        m = v2
        return e,m,eta
    def EtLogMoEtEta_toEMEta(self, v1,v2,v3):
        e = v1*CosH(v3)
        eta = v3
        m = e*Exp(v2)
        return e,m,eta

    def ELogMoEEta_toEMEta(self, v1,v2,v3):
        e = v1
        eta = v3
        m = e*Exp(v2)
        return e,m,eta

    def MLogMoEEta_toEMEta(self, v1,v2,v3):
        eta = v3
        m = v1
        e = m * Exp(-v2)
        return e,m,eta
    
    def PtLogMoEEta_toEMEta(self, v1,v2,v3):
        e = Sqrt( (v1*CosH(v3))**2 / (1-Exp(2*v2)) )
        eta = v3
        m = Sqrt( (v1*CosH(v3))**2 / (Exp(-2*v2)-1) )
        return e,m,eta

from collections import namedtuple

# JMSVar defines a variable used as a coordinate
JMSVar = namedtuple("JMSVar", "name title range unit rep AxisTitle")

var_et = JMSVar("Et", "E_{T}", (seq(150,5000,20),), " GeV", "%d", "E_{T} [GeV]")
var_e  = JMSVar("E", "E", (seq(150,5000,20),), " GeV", "%d", "E [GeV]")
var_pt = JMSVar("Pt", "p_{T}", (seq(150,5000,20),), " GeV", "%d", "p_{T} [GeV]")
var_eta = JMSVar("Eta", "#eta", (100,0,3), "", "%.2f", "#eta")
var_m = JMSVar("M", "M", (100,5,300), " GeV", "%d", "M [GeV]")
var_logmet = JMSVar("LogMoEt", "log(m/E_{T})", (80,-6,0), "", "%.2f", "log(m/E_{T})")
var_logme = JMSVar("LogMoE", "log(m/E)", (80,-6,0), "", "%.2f", "log(m/E)")

JMSCoord.coordSystems={
    (var_et.name, var_m.name) : (var_et, var_m, JMSCoord.EtMEta_toEMEta),
    (var_e.name , var_m.name) : (var_e, var_m,  JMSCoord.EMEta_toEMEta ),
    (var_pt.name, var_m.name) : (var_pt, var_m, JMSCoord.PtMEta_toEMEta),
    (var_et.name,var_logmet.name) : (var_et, var_logmet, JMSCoord.EtLogMoEtEta_toEMEta),
    (var_e.name , var_logme.name) : (var_e, var_logme,  JMSCoord.ELogMoEEta_toEMEta ),
    (var_m.name , var_logme.name) : (var_m, var_logme,  JMSCoord.MLogMoEEta_toEMEta ),
    (var_pt.name, var_logme.name) : (var_pt, var_logme, JMSCoord.PtLogMoEEta_toEMEta),
    }
