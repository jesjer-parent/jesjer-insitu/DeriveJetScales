## ********************************************
## CppExtensions
## 
## This module define additional functions
## for the C++ class defined in the package.
## Python/PyROOT allows to augment the C++ class
## by adding new python-only functions
## We use this below to conveniently define config functions
## which are easier to write in python than C++.
##
## ********************************************

import ROOT

## **********************************************
def _decorateClass(klass, funcs):
    """Add all functions in 'funcs' to the class 'klass' """
    for f in funcs:
        setattr(klass, f.__name__, f)

def _replaceClassFunc(klass, func):
    """replace the function named 'func.__name__' in the class 'klass' by the function func.
    """
    old = getattr(klass, func.__name__)
    setattr(klass, func.__name__, func)
    setattr(klass, func.__name__+'orig', old)



## **********************************************
## Augmenting VecStats objects
##
## **********************************************
def saveContent(self, f, writeObjs=None ):
    f.cd()
    self.Write("",ROOT.TObject.kOverwrite) # "" --> will call GetName() instead
def name(self):
    return self.GetName()

def saveJMSObject(self, f, obj, writeObjs=True):
    obj.saveContent( f ,writeObjs)
    obj.histosLoaded = True
    self.alreadyLoadedObjects.add(obj.name())

_decorateClass( ROOT.DJS.VecStats, [ saveContent, saveJMSObject, name ] )



## **********************************************
## Augmenting CalibrationObjects
##
## **********************************************
# Add global members to the class with default values.
ROOT.DJS.CalibrationObjects.jmsWorkDir='./'
ROOT.DJS.CalibrationObjects.jmsQualifier = ""
ROOT.DJS.CalibrationObjects.jesQualifier = ""
ROOT.DJS.CalibrationObjects.jmsLoadedFromFile = False
ROOT.DJS.CalibrationObjects.alreadyLoadedObjects = set()
ROOT.DJS.CalibrationObjects.vetoedCalibSteps = []

# We define a list of new function to be added to the CalibrationObjects class.
# 
def jmsFile(self,end='.root'):
    return self.jmsWorkDir+self.jetType+"_JMS"+self.jmsQualifier+end

def jesFile(self, end='.root'):
    return self.jetType+"_JES"+self.jesQualifier+end

def preventReloading(self, name):
    self.alreadyLoadedObjects.add(name)

def defineHistos(self, **histoDic ):
    nBins = self.etaBins.nBins()
    # histoDic is a python dictionnary of the form :
    #  { 'histoname' : (arguments to a THX constructor) }
    #
    for hname, harguments in histoDic.iteritems():
        v = getattr(self, hname ) # retrieve the vector of histos
        if 'TH2F' in v.__repr__():
            hClass = ROOT.TH2F
        elif 'TProfile' in v.__repr__():
            hClass = ROOT.TProfile
        # build an histo for each eta bin
        # we call the class constructor with the arguments given in input
        # just changing the histo name
        name = harguments[0]
        for i in range(nBins):
            binarguments = (name+'_'+str(i), )+harguments[1:]
            v.push_back( hClass( *binarguments ) )

def readJMSFromFile(self,fname=None, loadHistos=True):
    """
    reads all JMS related objects from fname.
     - detect and read all BPS::BinnedXX objects
     - For each BPS::BinnedXX object, check if it exists as a C++ member and if so read it in-place
    if loadHistos=True also load in memory all THX associated to BinnedTHX objects. 
    """
    if self.jmsLoadedFromFile: return
    if fname is None: fname = self.jmsFile()
    if isinstance(fname, str) :f = ROOT.TFile(fname)
    else : f = fname # assuming fnale is a TFile
    self.jmsInputFile = f # save it
    f.cd()
    rootKeys = f.GetListOfKeys()
    # first pass : get all BinnedPhaseSpace objects
    bpsName = self.jmsBPS.name()
    allPS = []
    tmp_bps = self.jmsBPS
    del(self.jmsBPS)
    for k in rootKeys:        
        if k.GetClassName()=="BPS::BinnedPhaseSpace": 
            print 'reading BPS ', k.GetName()
            #if k.GetName() == bpsName: del(self.jmsBPS)
            ps = k.ReadObj()
            if ps== None:
                print "Error reading ", k.GetName()
                continue
            setattr(self,ps.name(), ps)
            allPS+=[ps.name()]
            if k.GetName() == bpsName:
                # strange thing happens : self.jmsBPS becomes "None" after it has been read from file.
                # So reset it here :
                self.jmsBPS = ps
                # and also reset wherever it is set :
                self.respBuilder.m_jmsBPS = ps
                self.respBuilder.m_jmsRecoBin.m_bps = ps
                self.respBuilder.m_jmsTrueBin.m_bps = ps
                
    
    # 2nd pas : read other objects
    for k in rootKeys:
        cname = k.GetClassName()
        if cname == "DJS::HistoClones":
            kname = k.GetName()
            hc = getattr(self,kname+"_py", None)
            if hc is not None:
                hc.histoFile = f
        if  cname.startswith("BPS::Binned") and cname != "BPS::BinnedPhaseSpace" :
            kname = k.GetName()
            print 'reading ', kname
            # check if it exists as a member (that is it is a C++ member)
            bh = None
            for ps in allPS:
                if not kname.startswith(ps+"_") : continue
                if hasattr(self, kname[len(ps+"_"):] ):
                    bh = getattr(self, kname[len(ps+"_"):] )
                    break
            if bh :
                # exists as C++ member  : read it in-place
                bh.releaseBPS()
                bh.Read( kname )
            else:
                # create one and associate it : 
                bh = k.ReadObj()
                if bh== None:
                    print "Error reading ", k.GetName()
                    continue
                
                setattr(self,bh.name(), bh)
            bh.initialize()
            hdesc = bh.histoDesc()
            bh.histoFile = f
            if loadHistos: bh.loadHistos()
            # associate an alias with "_py" to workaround pyroot limitations
            setattr(self,bh.name()+'_py', bh)
        elif  k.GetClassName() in ["DJS::VecStats" , "TH3F"] :
            setattr(self,k.GetName(), k.ReadObj() )

    print "uuuuuuuuuuuuuuu ", tmp_bps , bpsName
    if not hasattr(self, "jmsBPS") or self.jmsBPS is None:
        # reset backed up bps:
        self.jmsBPS = tmp_bps
    self.jmsLoadedFromFile = True

def readBinnedObjectsFromFile(self, fname, objname, loadHistos=True):
    """Read a single BinnedObject from file if not already loaded.
    loadHistos=True is always honored.
    """

    if isinstance(fname, str) :f = ROOT.TFile(fname)
    else : f = fname # assuming fnale is a TFile

    if objname in self.alreadyLoadedObjects:
        print objname," already loaded "
        bh= getattr(self, objname)
        if loadHistos: bh.loadHistos(f)

    # in files, BinnedObject are stored as "phasespacename_objname"
    k = [ k for k in f.GetListOfKeys() if k.GetName().endswith( "_"+objname) ][0] # assume there's only one !


    bh = getattr(self, objname, None)
    if bh: # if already exists (it's a X++ member) load it in place.
        f.cd()
        bh.Read( k.GetName() )
    else:
        bh = k.ReadObj()    
        setattr(self, objname, bh )
    if loadHistos: bh.loadHistos(f)
    
    self.alreadyLoadedObjects.add(objname)
    return bh


def saveEtaJESToFile(self,  f, toSave =[]):

    print " Saving out ", f, toSave
    overWrite = ROOT.TObject.kOverwrite    
    objectsToSave = [ getattr(self, n) for n in toSave ]
    f.cd()
    print " Saving out ", f, ROOT.gDirectory
    
    for i in xrange(self.etaBins.nBins() ):
        suff= "_"+str(i)
        for n,k in zip(toSave,objectsToSave):
            try:
                k[i].Write( n+suff , overWrite)
            except ReferenceError:
                pass

    for s in toSave:
        self.alreadyLoadedObjects.add(s)
    

def readEtaJESFromFile(self, fname=None, toBeLoaded = None, loadRespPerBin=False):
    if fname is None: fname = self.jesFile()
    if isinstance(fname, str) :f = ROOT.TFile(fname)
    else : f = fname # assuming fnale is a TFile
    self.jesInputFile = f # save it

    print "Reading EtaJES data from ",f.GetName()
    respDir = f.Get("Responses")
    self.jes_respDir = respDir
    
    etaBins = f.EtaJES_etaBins
    self.etaBins = etaBins
    if toBeLoaded is None:
        toBeLoaded = [ "R_vs_Etrue" ,"R_vs_Etrue_Graph", "R_vs_Etrue_Func" ,"R_vs_Ereco" ,"R_vs_Ereco_Graph", "R_vs_Ereco_Func", "R_vs_mu_Graph", "R_vs_Enuminv_Graph" ,"R_vs_Enuminv_Func", "R_vs_Enuminv_SLGraph", "R_vs_Enuminv_GroomExt_Func", "closure_R_vs_Etrue_Graph",
                       "DEta_vs_Etrue" ,"DEta_vs_Etrue_Graph","DEta_vs_Etrue_Func", "closure_DEta_vs_Etrue_Graph", "R_vs_pTtrue", "closure_R_vs_pTtrue",
                       "R_vs_pTtrue_Graph","closure_R_vs_pTtrue_Graph",
                       ]

    # exclude from loading those already there.
    toBeLoaded = set(toBeLoaded) - self.alreadyLoadedObjects
    self.alreadyLoadedObjects.update(toBeLoaded)
    # prepare python list to store intermediate histos
    self.R_vs_Etrue_perEtaBin = []
    self.closure_R_vs_Etrue_perEtaBin = []
    self.DEta_vs_Etrue_perEtaBin = []
    for k in toBeLoaded:
        print "EtaJES. Will load : ",k

    for k in toBeLoaded:
        if not hasattr(self,k):
            print " WARNING  no object ",k ," exists in input file"
            continue
        vectTObjs = getattr(self,k)
        if not vectTObjs.empty():
            # then read in place
            for i in xrange(etaBins.nBins() ):        
                vectTObjs[i].Read( k+"_"+str(i) )
        else:
            # then it's a vector of pointers. Get pointers from the file
            for i in xrange(etaBins.nBins() ):
                vectTObjs.push_back( f.Get( k+"_"+str(i) ) )
            


    if loadRespPerBin:
        def loadFromResp(hname):
            j=0
            listH=[]
            while True:
                respname = hname+"_"+str(j)
                h=respDir.Get(respname)
                setattr(respDir, respname, h)
                if not h: break
                listH.append(h)
                j=j+1
            return listH
        for i in xrange(etaBins.nBins() ):
            self.R_vs_Etrue_perEtaBin.append( loadFromResp("R_vs_Etrue_"+str(i)) )
            self.DEta_vs_Etrue_perEtaBin.append( loadFromResp("DEta_vs_Etrue_"+str(i)) )
            self.closure_R_vs_Etrue_perEtaBin.append( loadFromResp("closure_R_vs_Etrue_"+str(i)) )


def dumpConfig(self, toFile=True):
    import sys
    if toFile:
        old_stdout = sys.stdout
        log_file = open(self.jmsFile('ConfigDump.txt'),"w")
    
        sys.stdout = log_file

    def dumpV(var):
        print 'objs.'+var, '=', str(eval('self.'+var, {'self':self}))
    dumpV('jetType')
    dumpV('AtlasLabel')
    dumpV('radius')
    dumpV('massType')

    dumpV('inputReader.__class__')
    dumpV('inputReader.m_evntWeight_bname')
    dumpV('inputReader.m_e_true_bname')
    dumpV('inputReader.m_e_reco_bname')
    dumpV('inputReader.m_eta_true_bname')
    dumpV('inputReader.m_eta_reco_bname')
    dumpV('inputReader.m_eta_det_bname')
    dumpV('inputReader.m_m_true_bname')


    print 'etaBins : ', self.etaBins.nBins(), self.etaBins.min(), self.etaBins.max()
    dumpV('etaBins.m_isGeV')
    #print 'eBins', eBins
    #print 'pBins', pBins

    dumpV('respBuilder.__class__')
    dumpV('respBuilder.m_jesCorr.m_minPtExtrapolated')
    dumpV('respBuilder.m_jesCorr.m_minPtETACorr')
    dumpV('respBuilder.m_jesCorr.m_maxEETACorr')

    dumpV('eRespFitter.m_minNEffP1')
    dumpV('eRespFitter.m_minNEffP2')
    dumpV('eRespFitter.m_minNEffP3')
    dumpV('eRespFitter.m_NSigmaForFit')
    dumpV('eRespFitter.m_minEtForMeasurement')
    dumpV('eRespFitter.m_useBetterChi2')
    dumpV('eRespFitter.m_useGeoDetCorr')
    dumpV('eRespFitter.m_minGeoDetCorr')
    dumpV('eRespFitter.m_maxGeoDetCorr')
    dumpV('eRespFitter.m_JES_Function')
    dumpV('eRespFitter.m_JES_MaxOrderP1')
    dumpV('eRespFitter.m_JES_MaxOrderP2')
    dumpV('eRespFitter.m_JES_MaxOrderP3')
    dumpV('eRespFitter.m_etaCorr_MaxOrderP1')
    dumpV('eRespFitter.m_etaCorr_MaxOrderP2')
    dumpV('eRespFitter.m_etaCorr_MaxOrderP3')
    dumpV('eRespFitter.m_enableEMinExtrapolation')
    dumpV('eRespFitter.m_enableEMaxExtrapolation')
    dumpV('eRespFitter.m_maxAcceptableYDeviationFitVSMinGroom')
    dumpV('eRespFitter.m_minNDataPointsForMinExtrapolation')
    dumpV('eRespFitter.m_minEtForExtrapolation')
    dumpV('eRespFitter.m_maxEtForFits')

    dumpV('dEtaFitter.m_NSigmaForFit')

    print '\n JMS******************************* \n'
    dumpV('jmsPreCorr')
    dumpV('jmsRespEvaluation')
    dumpV('binSystem')
    dumpV('jmsVeryFineBinning')
    dumpV('massType')
    dumpV('jmsNIopts')
    print
    print 'phase space structure :'
    self.jmsBPS.showStruct()

    dumpV('jmsWorkDir')
    dumpV('jmsQualifier')
    dumpV('BinningClass')

    if self.jmsVeryFineBinning:
        wb = self.respBuilder.m_jmsWBins
        print 'respBuilder.m_jmsWBins.sigmas = ', (wb.m_sig1 , wb.m_sig2 , wb.m_sig3 , wb.m_sig4 )
        print 'respBuilder.m_jmsWBins.nearestneighbN = ', (wb.m_nn1 , wb.m_nn2 , wb.m_nn3 , wb.m_nn4 )
        print 'respBuilder.m_jmsWBins.fracwidth = ', (wb.m_fWidth1, wb.m_fWidth2)

    dumpV('jms_tg2_smoother.m_maxD2')
    dumpV('jms_tg2_smoother.m_useErrors')
    dumpV('jms_tg2_smoother.m_useTwoPoints')
    print 'objs.jms_tg2_smoother.m_stretchFactor =', [f for f in self.jms_tg2_smoother.m_stretchFactor]
    print 'objs.jms_tg2_smoother.m_npoints =', [f for f in self.jms_tg2_smoother.m_npoints]
    print 'objs.jms_tg2_smoother.m_logCoord =', [f for f in self.jms_tg2_smoother.m_logCoord]

    if toFile:
        sys.stdout = old_stdout

        log_file.close()
        print 'dumped configuration in ', self.jmsFile('ConfigDump.txt')
    
# here we add the functions we defined above to the class :
_decorateClass( ROOT.DJS.CalibrationObjects, [ defineHistos, readEtaJESFromFile, readJMSFromFile, readBinnedObjectsFromFile, jesFile, jmsFile, saveEtaJESToFile, saveJMSObject, dumpConfig] )

# declare additionnal python config variables with default values
configVariables = dict(

    jetType = "AntiKt4LCTopo",
    AtlasLabel = "Simulation Internal",
    radius = 0.4,
    massType = "mCalo",

    pTmin = 5,
    pt_desc = '',
    m_desc = '',
    
    derObject = None,
    inputReader = None,
    # JES variables
    etaBins = None,

    
    # JMS variables
    jmsBPS = None,
    jmsPreCorr = False,
    jmsRespEvaluation = "gaus",
    binSystem = "ELogMoEBins",
    jmsVeryFineBinning = True,
    jmsNIopts = None,
    jmsNoRebin = True,
    BinningClass = None,
)
for k,v in configVariables.iteritems():
    setattr(ROOT.DJS.CalibrationObjects, k, v)

# Add the dict of default to the class.
ROOT.DJS.CalibrationObjects.defaultConfigVariables = configVariables



## **********************************************
## Augmenting JMSBinning
##
## **********************************************
def toBinCoordinates(self,e, m ,eta):
    self.initJet(e,m,eta)
    return self.m_var1 ,self.m_var2, eta
def toBinIndex(self,e, m ,eta):
    self.initJet(e,m,eta)
    return self.m_bin
_decorateClass( ROOT.DJS.JMSBinning , [ toBinCoordinates, toBinIndex ] )




## **********************************************
## Augmenting HistoClones
##
## **********************************************
from BinnedPhaseSpace.BinnedPhaseSpaceConfig import BinInfo

def _decorateClass(klass, funcs):
    """Add all functions in 'funcs' to the class 'klass' """
    for f in funcs:
        setattr(klass, f.__name__, f)

ROOT.DJS.HistoClones.infoContent = None

def histoAt( self, *bin):
    b=self.m_phaseSpace.bin(*bin)
    return self.histoAtIndex( b )

def at(self, i):
    return self.histoAtIndex( i )

def loadHistos(self):
    pass
def loadContent(self):
    self.load()
    pass

def th2SliceAt(self, dim, pos, binCalc, ):
    """Create and return a TH2 for the 2D plane located at position pos on dimension dim in the associated BPS.
    The binning the TH2 is the same as the dimensions dim1,dim2. Content of each bin is calculated by binCalc(binIterator)
    """

    # prepare a full interval for each of the dimensions
    fullInt = ROOT.BPS.AxisSpec.BinInterval.full
    binInt = [ self.m_phaseSpace.m_a1.validInterval( fullInt ),
               self.m_phaseSpace.m_a2.validInterval( fullInt ),
               self.m_phaseSpace.m_a3.validInterval( fullInt ),
               ]

    # limit dimension dim to only the position pos
    binInt[dim] = ROOT.BPS.AxisSpec.BinInterval(pos)

    # extract dimensions others than dim
    dim1 , dim2 = (i for i in (0,1,2) if dim!=i)
    print dim1, dim2
    # build a TH2 from them 
    a1=self.m_phaseSpace.axis(dim1)    
    a2=self.m_phaseSpace.axis(dim2)
    hspec = a1.hspec()+a2.hspec()
    h = ROOT.TH2F("Hclone_%d_%s"%(dim1,dim2),"Hclone_%d_%d"%(dim1,dim2), *hspec)
    
    
    translator = self.m_phaseSpace.subSpaceTranslator(dim)

    for bin in self.m_phaseSpace.range( * binInt ) : # loop over the slice at position pos in dimension dim
        v, v_err  = binCalc(bin, self )
        subspaceBin = translator.toSubspaceCoord(bin)
        h.SetBinContent(subspaceBin.i+1, subspaceBin.j+1, v)
        h.SetBinError(subspaceBin.i+1, subspaceBin.j+1, v_err)

            
    return h


def applyToBins(self, func, range=None):
    """ Call func on each bin as in
    func(iterator, histo, binInfo).
    If 'range' is None, will run on all bins. Else only on those specified by 'range'.
    'range' can be a BinnedPhaseSpace.BinRange or a list of tuples (ex: [ (1,0,1), (2,0,3),... ])
    """
    bps = self.m_phaseSpace
    if range is None:
        # take the full range
        range = bps.fullRange()
    elif not isinstance(range, BinnedPhaseSpace.BinRange):
        # assume range is a list of tuple, convert it to a list of BinIterator:
        range = [ bps.iteratorFrom( *coord ) for coord in range ]
    # else range is already a BinnedPhaseSpace.BinRange : nothing to do,
    
    for it in range:
        index = it.index()
        #func(it, self.m_binContent[index], self.infoContent[index] )
        func(it, self.histoAtIndex(index), self.infoContent[index] )
    
    pass

def initInfoContent(self, clear=False):
    if clear or self.infoContent is None:
        self.infoContent = [BinInfo() for i in xrange(self.m_phaseSpace.nBins())]
        self.m_pyBinContent = ''

def inspect_py(self, showVoid=False, range=None):
    bps = self.m_phaseSpace
    if range is None:
        # take the full range
        range = bps.fullRange()
    elif not isinstance(range, BinnedPhaseSpace.BinRange):
        # assume range is a list of tuple, convert it to a list of BinIterator:
        range = [ bps.iteratorFrom( *coord ) for coord in range ]
    # else range is already a BinnedPhaseSpace.BinRange : nothing to do,
    array = self.m_sumwPerHisto.fArray
    n_void = 0
    for it in range:
        index = it.index()
        if array[index] >0 or showVoid:
            print it.asTuple() , ' -> ', array[index]
            n_void+=1
    print "total non void = ", n_void , " / ", bps.nBins()


ROOT.DJS.HistoClones.needLoading = True
def load(self, tfile=None, **args):
    from BinnedPhaseSpace.BinnedPhaseSpaceConfig import load as loadBH
    if self.needLoading:
        tmpBPS = ROOT.BPS.BinnedPhaseSpace(self.m_phaseSpace)
        if tfile is None:
            try: tfile = self.histoFile
            except: print "Error no file associated to ",self, " please pass a TFile as argument"; return
        loadBH(self,tfile=tfile, **args)
        if not self.m_phaseSpace :
            # if BPS hasn't been properly loaded, the load() will void it
            # reset it in this case
            self.m_phaseSpace = tmpBPS
        if hasattr(tfile, self.name()+'resp'):
            vresp = getattr(tfile, self.name()+'resp')
            vresp_err = getattr(tfile, self.name()+'resp_err')
            vstatus = getattr(tfile, self.name()+'status')
            self.initInfoContent()
            for i, bi in enumerate(self.infoContent):
                bi.status = vstatus[i]
                bi.mresp = vresp[i]
                bi.mresp_err = vresp_err[i]
        self.needLoading = False

def writeTo(self, f, writeHistos=True):
    f.cd()
    if writeHistos:self.Write( self.name(), ROOT.TObject.kOverwrite)
    if self.infoContent is None:
        return
    # else, save anyway the infoContent into simple vectors
    n = len(self.infoContent)
    vresp = ROOT.vector(float)(n)
    vresp_err = ROOT.vector(float)(n)
    vstatus = ROOT.vector(int)(n)
    for i,bi in enumerate(self.infoContent):
        vresp[i] = bi.mresp
        vresp_err[i] = bi.mresp_err
        vstatus[i] = bi.status
    f.WriteObject( vresp, self.name()+'resp', "Overwrite")
    print 'wrote ',vresp, self.name()+'resp'
    f.WriteObject( vresp_err, self.name()+'resp_err', "Overwrite")
    f.WriteObject( vstatus, self.name()+'status', "Overwrite")

def saveContent(self, f, writeObjs=True ):
    self.writeTo(f, writeObjs)
    self.needLoading = False

def updateHistoDesc(self, **args):
    from BinnedPhaseSpace.BinnedPhaseSpaceConfig import HistoDesc
    if hasattr(self,'histoDesc'): histoDesc = self.histoDesc
    else:
        histoDesc = HistoDesc()
        self.histoDesc = histoDesc
    histoDesc.setProp(**args)

def applyStyle(self):
    if hasattr(self,'histoDesc'):
        self.histoDesc.toHisto(self.histoAtIndex(0))


class HCvectorFake:
    """An object that mimics a vector<TH1>. """
    def __init__(self, hc):
        self.hc = hc
    def __getitem__(self,i):
        return self.hc.histoAtIndex(i)

def createFakeVector(self):
    """Add to self a fake vector<TH1>. We do this so HistoClones behaves exactly as BinnedTH1 and we can re-use BinnedTH1
    methods with HistoClones instances"""
    self.m_binContent = HCvectorFake(self)

from BinnedPhaseSpace.BinnedPhaseSpaceConfig import applyToBins, planeToTGraph, planeToTH2, lineToTH1, lineToTGraph, lineToTObj1D, infoAt

_decorateClass( ROOT.DJS.HistoClones, [th2SliceAt, histoAt, initInfoContent, writeTo, inspect_py, loadHistos, at, saveContent, loadContent,
                                       applyToBins, planeToTGraph, planeToTH2, lineToTH1, lineToTGraph, lineToTObj1D, infoAt, load,
                                       updateHistoDesc, applyStyle, createFakeVector
                                       ] )
