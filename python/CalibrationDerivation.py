import os
import ROOT
from DeriveJetScales.PyHelpers import seq, quantilesFromH, calcSmoothMode, calcMedIQR
from BinnedPhaseSpace.BinnedPhaseSpaceConfig import AxisSpec , BinnedPhaseSpace, BinnedTH1, BinnedTH2, BinnedTGraph2DErrors, h1, h2

# ********************************************************************
# ********************************************************************
class CalibrationDerivation(object):
    """ This objects implements the EtaJES and JMS derivation logic.

    It makes use of C++ objects to run over input file (ResponseBuilder), fit histograms (JES_BalanceFitter, ResponseFitter) and smooth them (KernelSmoothing).
    
    The full configuration AND histograms needed to perform the task are attached to a DJS::CalibrationObjects instance.
    The CalibrationObjects class is defined in Utilities.h and 'augmented' by a few python function in CppExtensions.py .
    This script will invoke a configuration file (an other python script) which is expected to contain a function returning
    a fully configured object.

    All operations relevant to defining and handling bins in the (eta,pt,m) phasespace are done using utilities defined in the BinnedPhaseSpace package.
        

    """

    def initialize(self,tree, configFunc, maxEvents=-1, firstEvent=0, calibSteps=[]):
        
        # build a CalibrationObjects object containing all the pieces needed to evaluate the calib.
        # We assume a dedicated config file which defines configure() has
        # correctly been included earlier during the script invocation. Typically such file is named like "AntiKt10LCTopoTrimmedPtFrac5SmallR20.py"
        objs = configFunc()
        self.objs = objs

        self.tree = tree
        self.jetType = objs.jetType
        self.calibSteps = calibSteps
        
        objs.respBuilder.m_maxEvents = maxEvents
        objs.respBuilder.m_firstEvent = firstEvent

        # re-copy some objects into alias (_py suffix)
        #  reason : workaround issue where pyROOT forget about dynamic attributes
        #   set onto C++ objects. 
        objs.mR_in_PtEtaMtrue_py = objs.mR_in_PtEtaMtrue
        objs.mR_in_PtEtaMnuminv_py = objs.mR_in_PtEtaMnuminv
        objs.closure_mR_in_PtEtaMtrue_py = objs.closure_mR_in_PtEtaMtrue


        # prepare output files *****************
        jesmode = "Update"
        fOut_jes = ROOT.TFile(objs.jesFile(),  jesmode)
        self.fOut_jes = fOut_jes

        self.jes_respDir = fOut_jes.mkdir("Responses")
        if self.jes_respDir == None:
            self.jes_respDir = fOut_jes.Responses
            
        if not os.path.exists(objs.jmsWorkDir): os.mkdir( objs.jmsWorkDir)        
        jmsmode = "UPDATE"
        fOut_jms = ROOT.TFile(objs.jmsFile(),  jmsmode)
        self.fOut_jms = fOut_jms
        # ****************************************
        

        fOut_jes.cd()
        objs.etaBins.Write("EtaJES_etaBins", ROOT.TObject.kOverwrite)

        return objs



    def readJMSFromFile(self, loadHistos=True):
        """Unconditionnaly read ALL jms-related object from the jms root file.  """
        self.objs.readJMSFromFile(self.fOut_jms, loadHistos)
        
            
    # ********************************************************************
    # ********************************************************************
    def runDerivation(self, ):
        calibSteps = self.calibSteps
        # ****************************************************************
        # The full derivation flow
        # ****************************************************************
        # Some options in the configuration are incompatible with some steps :
        # we remove the steps vetoed by the configuration
        for step in self.objs.vetoedCalibSteps:
            if calibSteps.count(step)>0:
                print " Configuration is vetoing step",step,". Will not be executed"
                try:
                    while True: calibSteps.remove(step)
                except ValueError:
                    pass
                    
        # ******************************************
        # EtaJES 
        if 'EtaJES.rawresp' in calibSteps:
            self.jesRawResp()

        if 'EtaJES.rawfits' in calibSteps:
            self.jesRawFits()

        # ******************************************
        if 'EtaJES.niresp' in calibSteps:
            self.jesNiResp()

        if 'EtaJES.nifits' in calibSteps:
            self.jesNiFits()

        # We combine EtaJES.closureresp and JMS.rawresp if both are requested
        if ("EtaJES.closureresp" in calibSteps) and "JMS.rawresp" in calibSteps:
            self.jesClosureRespAndjmsRawResp()
        elif "EtaJES.closureresp" in calibSteps:
            self.jesClosureResp()
        elif "JMS.rawresp" in calibSteps:
            self.jmsRawResp()

        if 'EtaJES.closurefits' in calibSteps:
            self.jesClosureFits()

        if 'JMS.rawfits' in calibSteps:
            self.jmsRawFits()

        # ******************************************
        # JMS pass 2
        if 'JMS.niresp' in calibSteps:
            self.jmsNiResp()

        if 'JMS.nifits' in calibSteps:
            self.jmsNiFits()

        # ******************************************
        # JMS pass 3
        if 'JMS.closureresp' in calibSteps:
            self.jmsClosureResp()

        if 'JMS.closurefits' in calibSteps:
            self.jmsClosureFits()

        #if 'EtaJES' in calibSteps: self.fOut_jes.Close()
        #if 'JMS' in calibSteps: fOut_jms.Close()

        return # end of runDerivation() .
    

    # ******************************************
    # EtaJES pass 1 and 2
    def jesRawResp(self):
        print " EtaJES derivation"
        print "******************************************************************"
        print "******************************************************************"    
        print " EtaJES pass 1.1 :  Loop over ntuple and build responses vs true in each eta bins"
        print
        objs = self.objs
        objs.respBuilder.m_mode = ROOT.DJS.ResponseBuilder.EtaJES_RvsEtrue # set the running mode
        objs.respBuilder.processTree(self.tree)
        # processTree() has filled several vector in the central object objs :
        # R_vs_Etrue, Etrue_vs_Etrue, DEta_vs_Etrue, ...

        objs.saveEtaJESToFile( self.fOut_jes, [ "R_vs_Etrue", "R_vs_Ereco", "R_vs_mu", "Etrue_vs_Etrue", "Ereco_vs_Ereco","DEta_vs_Etrue" ,
                                                     "Ereco_vs_Etrue",  ] )
    def jesRawFits(self):
        print "******************************************************************"    
        print " EtaJES pass 1.2 : in each eta bin, fit responses and prepare <R> vs Etrue or <deltaEta> vs Etrue"
        print
        # make sure we have what we need (this will not 'overread' any histos, even if the previous steps was run):
        self.objs.readEtaJESFromFile(self.fOut_jes, [ "R_vs_Etrue", "R_vs_Ereco", "R_vs_mu", "Etrue_vs_Etrue", "Ereco_vs_Ereco","DEta_vs_Etrue" ,
                                                 "Ereco_vs_Etrue", ]  )

        # perform the fits (and save the relevant histos)
        self.fitEtaJESvsEtrue()
        self.fitJESvsMu()

    def jesNiResp(self):
        print
        print "******************************************************************"
        print "******************************************************************"            
        print " EtaJES pass 2.1 : Loop over ntuple and compute Resp vs E_numinv"
        print "    where E_numinv= Etrue * R_vs_Etrue_Func(Etrue)"
        print
        objs = self.objs
        
        objs.readEtaJESFromFile(self.fOut_jes, [ "R_vs_Etrue_Func",] )

        objs.respBuilder.m_mode = ROOT.DJS.ResponseBuilder.EtaJES_RvsEnuminv
        objs.respBuilder.processTree(self.tree)

        objs.saveEtaJESToFile( self.fOut_jes, [ "R_vs_Enuminv", "Enuminv_vs_Etrue",] )


    def jesNiFits(self):
        # make sure we have what we need (this will not 'overread' any histos, even if the previous steps was run):
        self.objs.readEtaJESFromFile(self.fOut_jes, [ "R_vs_Etrue", "Etrue_vs_Etrue", "DEta_vs_Etrue" , "Enuminv_vs_Etrue",
                                                      "Ereco_vs_Etrue", ]  )

        print "******************************************************************"            
        print " EtaJES pass 2.2 : Fit R vs Enuminv in each eta bin"
        print 
        self.fitEtaJESvsEnuminv()


    def jesClosureResp(self):
        print "******************************************************************"
        print "******************************************************************"    
        print " EtaJES pass 3.1 : EtaJES closure test  "
        print 
        objs = self.objs        

        objs.respBuilder.m_mode = ROOT.DJS.ResponseBuilder.EtaJES_Closure
        objs.readEtaJESFromFile(self.fOut_jes, [  "DEta_vs_Etrue_Func" , "R_vs_Enuminv_Func"
                                                     ]  )
        objs.respBuilder.processTree(self.tree)

        objs.saveEtaJESToFile( self.fOut_jes, [ "closure_R_vs_Etrue", "closure_DEta_vs_Etrue","closure_R_vs_pTtrue", "R_vs_pTtrue", "pTtrue_vs_pTtrue",] )

    def jesClosureFits(self):
        print "******************************************************************"
        print " EtaJES pass 3.2 : fit closure responses"
        print 
        self.objs.readEtaJESFromFile(self.fOut_jes, ["closure_R_vs_Etrue","Etrue_vs_Etrue", "closure_DEta_vs_Etrue", "closure_R_vs_pTtrue", "R_vs_pTtrue", "pTtrue_vs_pTtrue" ] )
        self.fitEtaJESClosure()



    def jesClosureRespAndjmsRawResp(self):
        print "******************************************************************"
        print "******************************************************************"    
        print " EtaJES pass 3.1 / JMS pass 1.1 : EtaJES closure test  and JMS raw responses"
        print 
        objs = self.objs        

        objs.respBuilder.m_mode = ROOT.DJS.ResponseBuilder.EtaJES_Closure_JMS_RvsEtrue
        objs.readEtaJESFromFile(self.fOut_jes, [  "DEta_vs_Etrue_Func" , "R_vs_Enuminv_Func",
                                                  "R_vs_Enuminv_Func", "R_vs_Etrue_Func", "DEta_vs_Etrue_Func"  
                                                     ]  )
        objs.respBuilder.processTree(self.tree)

        objs.saveEtaJESToFile( self.fOut_jes, [ "closure_R_vs_Etrue", "closure_DEta_vs_Etrue","closure_R_vs_pTtrue", "R_vs_pTtrue", "pTtrue_vs_pTtrue",] )

        # make sure we write JMS responses out
        self.fOut_jms.cd()
        objs.jmsBPS.write()
        objs.saveJMSObject(self.fOut_jms, objs.mR_in_PtEtaMtrue)
        objs.saveJMSObject(self.fOut_jms, objs.m_in_PtEtaMtrue)

        if objs.jmsNIopts.noNumInv:
            # in this case we also filled the 'numinv' responses (althought it's not numerical inversion in this case)
            # Save them too.
            objs.saveJMSObject(fOut_jms, objs.mR_in_PtEtaMnuminv_py)
            objs.saveJMSObject(fOut_jms, objs.m_in_PtEtaMnuminv)



    def jmsRawResp(self):
        print "******************************************************************"
        print "******************************************************************"    
        print " JMS pass 1.1 :  JMS raw responses"
        print
        objs = self.objs 
        # make sure we have the EtaJES constants loaded :
        objs.readEtaJESFromFile(self.fOut_jes, ["R_vs_Enuminv_Func", "R_vs_Etrue_Func", "DEta_vs_Etrue_Func"] )

        # set the appropriate running mode :
        objs.respBuilder.m_mode = ROOT.DJS.ResponseBuilder.JMS_RvsEtrue
        # run : assign each jet mass response to a bin of the bin space
        objs.respBuilder.processTree(self.tree)
        
        # make sure we write responses out
        self.fOut_jms.cd()
        objs.jmsBPS.write()
        objs.saveJMSObject(self.fOut_jms, objs.mR_in_PtEtaMtrue)
        objs.saveJMSObject(self.fOut_jms, objs.m_in_PtEtaMtrue)

        if objs.jmsNIopts.noNumInv:
            # in this case we also filled the 'numinv' responses (althought it's not numerical inversion in this case)
            # Save them too.
            objs.saveJMSObject(fOut_jms, objs.mR_in_PtEtaMnuminv_py)
            objs.saveJMSObject(fOut_jms, objs.m_in_PtEtaMnuminv)


    def jmsRawFits(self):
        print "******************************************************************"    
        print " JMS pass 1.2 : fit responses, construct <R> vs m or (pt,m) "
        print 
        objs = self.objs
        
        objs.mR_in_PtEtaMtrue_py.loadContent( self.fOut_jms )

        step = "trueBins" 

        self.fitJMS( objs.mR_in_PtEtaMtrue_py ,  responseEval=objs.jmsRespEvaluation)
        self.mergeVeryLowMassLowStatBins( objs.mR_in_PtEtaMtrue_py )
        self.reEvalJMSatLowM(inTrueBins=True, reEvalStrategy="linearMultipoints")

        # Build a few TGraphs to study performances
        self.buildJMSvsX( 1, step=step) # <mR> vs m
        self.buildJMSvsX( 1, step=step, value='mIQR') # <mIQR> vs m

        self.buildJMSvsX( 0, step=step) # <mR> vs pt
        self.buildJMSvsX( 0, step=step, value='mIQR') # <mIQR> vs m

        # build the JMS map vs (true) mass,pt per eta slice
        # (this creates and records BinnedTGraph2DErrors the mR_vs_PtMtrue_in_Eta object)
        self.buildJMSvsPtM( inTrueBins=True ) # <mR> vs (pt,m)

        # Smooth the JMS maps (create and record mRsmooth_vs_PtMtrue_in_Eta object)
        self.smoothJMSvsPtM( objs.mR_vs_PtMtrue_in_Eta, "mRsmooth_vs_PtMtrue_in_Eta"  ) # smooth <mR> vs (pt,m)
        
        #self.directJMSNumInv() # numerically inverse <mR> vs (pt,m) directly from the TH2.
                                # this part is optionnal
                                
        # Save the fits we produced, without overwriting the response histograms
        objs.saveJMSObject(self.fOut_jms, objs.mR_in_PtEtaMtrue, writeObjs=False) # re-save only the new info we added in this step


    def jmsNiResp(self):
        if self.objs.jmsNIopts.directNumInv :
            # direct numerical inversion : we skip this step
            return
        print "******************************************************************"
        print "******************************************************************"    
        print " JMS pass 2.1 : mass Resp in num inv bins"
        print
        ## Using the m response in true bins we run again over all events
        ## We fill m response per m_true*response(m_true) bins (== num inverted bins)
        ## In this case we use objs.mRsmooth_vs_PtMtrue_in_Eta as the response function.
        ## see ResponseBuilder::jmsRvsNuminv
        
        objs = self.objs 
        # make sure we have the EtaJES constant loaded :
        objs.readEtaJESFromFile(self.fOut_jes, ["R_vs_Enuminv_Func", "R_vs_Etrue_Func", "DEta_vs_Etrue_Func"] )
        # load the m response in true bins
        objs.readBinnedObjectsFromFile(self.fOut_jms, "mRsmooth_vs_PtMtrue_in_Eta")


        objs.respBuilder.m_mode = ROOT.DJS.ResponseBuilder.JMS_RvsNuminv
        objs.respBuilder.processTree(self.tree)

        objs.saveJMSObject(self.fOut_jms, objs.mR_in_PtEtaMnuminv)
        objs.saveJMSObject(self.fOut_jms, objs.m_in_PtEtaMnuminv)


    def jmsNiFits(self):
        print "******************************************************************"    
        print " JMS pass 2.2 : fit mass Resp, build <mR> vs m, pt, etc.."
        print
        objs = self.objs

        if objs.jmsNIopts.directNumInv :
            # no fits needed. Instead invoke the special object 
            # which perform the direct inversion 
            from DeriveJetScales.DirectNumericalInversion import DirectNumInverser
            ni = DirectNumInverser(objs, outTag="3Dnuminv")
            ni.doNumInv()
            return
        
        ## Here we fit the response in num inv bins 
        ## use same procedures as in raw response fits.
        # load the individual responses
        objs.mR_in_PtEtaMnuminv_py.loadHistos( self.fOut_jms )

        self.fitJMS( objs.mR_in_PtEtaMnuminv_py, responseEval=objs.jmsRespEvaluation)
        self.mergeVeryLowMassLowStatBins( objs.mR_in_PtEtaMnuminv_py )

        #if objs.useLogMoPtE and not objs.jmsNIopts.noNumInv: --> not sure we actually want to do it. 
        #    self.reEvalJMSatLowM(inTrueBins=False, lowErrExt=True)
        self.buildJMSvsX( 1, step="numinvBins" , )#recopyFitFunc=True)
        self.buildJMSvsX( 1, step="numinvBins", noExtrap=True )
        self.buildJMSvsX( 1, step="numinvBins", value='mIQR') # <mIQR> vs m

        self.buildJMSvsPtM( False )

        self.smoothJMSvsPtM( objs.mR_vs_PtMnuminv_in_Eta, "mRsmooth_vs_PtMnuminv_in_Eta"  )

        objs.saveJMSObject(self.fOut_jms, objs.mR_in_PtEtaMnuminv, writeObjs=False) # re-save only the new info we added in this step

        # Also build a TH3F containing the responses
        h3 = self.buildResponseAsTH3( objs.mRsmooth_vs_PtMnuminv_in_Eta)
        objs.mRsmooth_3Dnuminv = h3
        self.fOut_jms.cd()
        h3.Write("mRsmooth_3Dnuminv", ROOT.TObject.kOverwrite)


    def jmsClosureResp(self):
        # ******************************************
        # JMS pass 3
        print "******************************************************************"    
        print " JMS pass 3.1 : mass Resp closure tests"
        print
        ## Rebuild m response in true bins after applying the mass correction.
        ## 

        objs = self.objs
        # make sure we have the EtaJES constant loaded :
        objs.readEtaJESFromFile(self.fOut_jes, ["R_vs_Enuminv_Func", "R_vs_Etrue_Func", "DEta_vs_Etrue_Func"] )
        
        # enable jmsCorrection using TH3F,
        # Associate a TH3F to our JMSBinning helper if needed an make sure it is properly inited :
        if not objs.respBuilder.m_jmsRecoBin.m_correctionFactors3d:
            objs.mRsmooth_3Dnuminv = self.fOut_jms.Get("mRsmooth_3Dnuminv")
            objs.respBuilder.m_jmsRecoBin.m_correctionFactors3d = objs.mRsmooth_3Dnuminv
        objs.respBuilder.m_jmsRecoBin.initCorrections()

        # run
        objs.respBuilder.m_mode = ROOT.DJS.ResponseBuilder.JMS_Closure
        objs.respBuilder.processTree(self.tree)

        objs.jmsinclPtBPS.write()
        objs.saveJMSObject(self.fOut_jms, objs.closure_mR_in_PtEtaMtrue_py)
        objs.saveJMSObject(self.fOut_jms, objs.closure_mR_in_inclPtEtatrue)
        objs.saveJMSObject(self.fOut_jms, objs.mR_in_inclPtEtatrue)

    def jmsClosureFits(self):
        print " JMS pass 3.2 : mass Resp closure tests fits"
        print
        objs = self.objs

        objs.closure_mR_in_PtEtaMtrue_py.loadHistos( self.fOut_jms )
        self.fitJMS( objs.closure_mR_in_PtEtaMtrue_py,responseEval=objs.jmsRespEvaluation)

        self.buildJMSvsX( 1, step="closure" )# <mR> vs m
        self.buildJMSvsX( 1, step="closure", value='mIQR') # <mIQR> vs m

        self.buildJMSvsX( 0, step="closure") # <mR> vs pt
        self.buildJMSvsX( 0, step="closure", value='mIQR') # <mIQR> vs pt

        self.buildJMSvsPtM( (objs.closure_mR_in_PtEtaMtrue_py, 'closure_mR_vs_PtMtrue_in_Eta') )# closure <mR> vs pt,m
        self.smoothJMSvsPtM(objs.closure_mR_vs_PtMtrue_in_Eta, 'closure_mRsmooth_vs_PtMtrue_in_Eta')# smoothed closure <mR> vs pt,m

        self.buildJMSvsPtM( (objs.closure_mR_in_PtEtaMtrue_py, 'closure_mIQR_vs_PtMtrue_in_Eta'), useIQR=True)# closure mIQR vs pt,m
        self.smoothJMSvsPtM(objs.closure_mIQR_vs_PtMtrue_in_Eta, 'closure_mIQRsmooth_vs_PtMtrue_in_Eta')# smoothed closure mIQR vs pt,m

        objs.saveJMSObject(self.fOut_jms, objs.closure_mR_in_PtEtaMtrue_py, writeObjs=False)

        # build TH3F containing the closure responses and the closure mIQR
        h3 = self.buildResponseAsTH3( objs.closure_mRsmooth_vs_PtMtrue_in_Eta)
        objs.closure_mRsmooth_3D = h3
        h3_IQ = self.buildResponseAsTH3( objs.closure_mIQRsmooth_vs_PtMtrue_in_Eta)
        objs.closure_mIQRsmooth_3D = h3_IQ
        self.fOut_jms.cd()
        h3.Write("closure_mRsmooth_3D", ROOT.TObject.kOverwrite)
        h3_IQ.Write("closure_mIQRsmooth_3D", ROOT.TObject.kOverwrite)

        # also prepare plots in large mass bins (indepedently of what 
        #  exact parametrization for the bins is used).
        # This is useful to assess JMS perfs around masses of physical interest
        # and to compare several JMS derivation options

        # alias to workaround C++/python references issues.            
        objs.mR_in_inclPtEtatrue_py = objs.mR_in_inclPtEtatrue
        objs.closure_mR_in_inclPtEtatrue_py = objs.closure_mR_in_inclPtEtatrue
        objs.mR_in_inclPtEtatrue.loadContent( self.fOut_jms)
        objs.closure_mR_in_inclPtEtatrue.loadContent(self.fOut_jms)

        self.fitJMS(objs.mR_in_inclPtEtatrue_py, excludeLargeMoPtBins=False, saveFuncs=True, responseEval=objs.jmsRespEvaluation)
        self.fitJMS(objs.closure_mR_in_inclPtEtatrue_py, excludeLargeMoPtBins=False, saveFuncs=True, responseEval=objs.jmsRespEvaluation)
        objs.saveJMSObject(self.fOut_jms, objs.mR_in_inclPtEtatrue_py, writeObjs=False) # re-save only the new info we added in this step
        objs.saveJMSObject(self.fOut_jms, objs.closure_mR_in_inclPtEtatrue_py, writeObjs=False) # re-save only the new info we added in this step


        templName = '{value}_vs_{var}true_in_largeMassBins'
        self.buildJMSvsX( 0, step=(objs.mR_in_inclPtEtatrue_py,templName) ) # <mR> vs pt
        self.buildJMSvsX( 0, step=(objs.mR_in_inclPtEtatrue_py,templName), value='mIQR') # <mIQR> vs pt

        templName = 'closure_{value}_vs_{var}true_in_largeMassBins'
        self.buildJMSvsX( 0, step=(objs.closure_mR_in_inclPtEtatrue_py,templName) ) # <mR> vs pt
        self.buildJMSvsX( 0, step=(objs.closure_mR_in_inclPtEtatrue_py,templName), value='mIQR') # <mIQR> vs pt
        
        

    # ****************************************************************
    # EtaJES fit methods
    # ****************************************************************
    def fitEtaJESvsEtrue(self):
        self.fOut_jes.cd()
        objs = self.objs
        eRespFitter = objs.eRespFitter
        dEtaFitter = objs.dEtaFitter

        # prepare python list to store other intermediate histos
        objs.R_vs_Etrue_perEtaBin = []
        objs.DEta_vs_Etrue_perEtaBin = []

        for i in range(objs.etaBins.nBins() ):
            eRespFitter.prepareEtaBin(i)
            dEtaFitter.prepareEtaBin(i)
            eta= objs.etaBins.binCenter( i )
            coshEta = ROOT.TMath.CosH(eta)

            print " First pass : processing eta bin ", i , " center = ", eta

            # ********************
            # Prepare the <R> vs Etrue graph
            eCut = eRespFitter.m_minEtForMeasurement * coshEta
            graph_true = eRespFitter.constructRGraph(objs.R_vs_Etrue[i], objs.Etrue_vs_Etrue[i], objs.Etrue_vs_Etrue[i], eCut)
            # the call filled response histos per bin in eRespFitter.m_respPerEbins
            # save the intermediate response per Etrue bins
            self.jes_respDir.cd()
            for h in eRespFitter.m_respPerEbins:
                h.Write()
            objs.R_vs_Etrue_perEtaBin.append( list(eRespFitter.m_respPerEbins) ) 
            self.fOut_jes.cd()

            # also create the graph <R> vs Ereco
            graph_reco = eRespFitter.constructRGraph(objs.R_vs_Ereco[i], objs.Ereco_vs_Ereco[i], objs.Ereco_vs_Ereco[i], eCut)

            objs.push_back(objs.R_vs_Etrue_Graph, graph_true)
            objs.push_back(objs.R_vs_Ereco_Graph, graph_reco)

            # ********************
            # Fit the <R> vs Etrue graph to extract responses in TF1 form
            eCut = eRespFitter.m_minEtForExtrapolation * coshEta # not the same as above !
            emaxCut = eRespFitter.m_maxEtForFits * coshEta
            eRespFitter.fitRGraph(graph_true, eRespFitter.m_JES_Function, eCut, emaxCut, )

            # eRespFitter.m_R_vs_E_Func is now set to the function we want. Save it :
            objs.push_back(objs.R_vs_Etrue_Func, eRespFitter.m_R_vs_E_Func) 
            objs.push_back(objs.R_vs_Etrue_SLGraph, eRespFitter.m_R_vs_E_SLGraph)

            eRespFitter.fitRGraph(graph_reco, eRespFitter.m_JES_Function, eCut, emaxCut)
            objs.push_back(objs.R_vs_Ereco_Func, eRespFitter.m_R_vs_E_Func)
            objs.push_back(objs.R_vs_Ereco_SLGraph, eRespFitter.m_R_vs_E_SLGraph)


            # ********************
            # Delta Eta
            eCut = dEtaFitter.m_minEtForMeasurement * coshEta
            # Prepare the Deta vs pT_true graph (last 2 args are optimalRebin=True, includeBadFits=False
            detaGr = dEtaFitter.constructRGraph(objs.DEta_vs_Etrue[i], objs.Etrue_vs_Etrue[i], objs.Etrue_vs_Etrue[i], eCut,True, False)
            objs.push_back(objs.DEta_vs_Etrue_Graph, detaGr)


            # save the intermediate dEta per Etrue bins
            self.jes_respDir.cd()
            for h in dEtaFitter.m_respPerEbins: h.Write()
            objs.DEta_vs_Etrue_perEtaBin.append( list(dEtaFitter.m_respPerEbins) ) 
            self.fOut_jes.cd()


            if detaGr.GetN()==0:
                print "ERROR : empty Deta vs E graph. The progam will probably crash soon."
                print "ERROR :   --> Probably to low stat. Inspect objs.DEta_vs_Etrue_perEtaBin at bin", i, ". Consider to use coarser eta bins."

            # ********************
            # Fit the <Deta> vs Etrue graph to extract responses in TF1 form
            eCut = eRespFitter.m_minEtForExtrapolation * coshEta # not the same as above !
            dEtaFitter.fitDeltaEtagGraph(detaGr, "polBestChi2", eCut, dEtaFitter.m_maxEtForFits )

            objs.push_back(objs.DEta_vs_Etrue_Func, dEtaFitter.m_DEta_vs_E_Func)

        objs.saveEtaJESToFile( self.fOut_jes, [ "DEta_vs_Etrue_Func", "R_vs_Etrue_Graph", "R_vs_Ereco_Graph", "DEta_vs_Etrue_Graph", "R_vs_Etrue_Func","R_vs_Ereco_Func"])
        
    def fitJESvsMu(self):
        self.fOut_jes.cd()
        objs = self.objs
        for i in range(objs.etaBins.nBins() ):
            graph = ROOT.TGraphErrors()
            for nx in range(objs.R_vs_mu[i].GetNbinsX()):
               tempHist = objs.R_vs_mu[i].ProjectionY("R_vs_mu", nx+1, nx+1)
               mu = objs.R_vs_mu[i].GetXaxis().GetBinLowEdge(nx+1)
               graph.SetPoint(nx, mu, tempHist.GetMean())
               graph.SetPointError(nx, 0, tempHist.GetMeanError())
            objs.push_back(objs.R_vs_mu_Graph, graph)
        objs.saveEtaJESToFile(self.fOut_jes, ["R_vs_mu_Graph"])


    # 2nd pass fits :
    def fitEtaJESvsEnuminv(self):
        self.fOut_jes.cd()            
        objs = self.objs
        eRespFitter = objs.eRespFitter
        dEtaFitter = objs.dEtaFitter
        for i in range(objs.etaBins.nBins() ):
            eRespFitter.prepareEtaBin(i)
            dEtaFitter.prepareEtaBin(i)

            eta= objs.etaBins.binCenter( i )
            coshEta = ROOT.TMath.CosH(eta)

            print " Second pass : processing eta bin ", i , " center = ", eta

            # ********************
            # Prepare the <R> vs Etrue graph
            eCut = eRespFitter.m_minEtForMeasurement * coshEta
            # the call used in the 1st pass is :
            # graph = eRespFitter.constructRGraph(objs.R_vs_Etrue[i], objs.Etrue_vs_Etrue[i], objs.Etrue_vs_Etrue[i], eCut)
            #  but now we do NOT do :
            # graph = eRespFitter.constructRGraph(objs.R_vs_Enuminv[i], objs.Enuminv_vs_Etrue[i], objs.Etrue_vs_Etrue[i], eCut)
            #  instead difference with the call in 1st pass is the 2nd argument :
            graph = eRespFitter.constructRGraph(objs.R_vs_Etrue[i], objs.Enuminv_vs_Etrue[i], objs.Etrue_vs_Etrue[i], eCut)

            objs.push_back(objs.R_vs_Enuminv_Graph, graph)


            # ********************
            # Fit the <R> vs Enuminv graph to extract responses in TF1 form
            eCut = eRespFitter.m_minEtForExtrapolation * coshEta # not the same as above !
            emaxCut = eRespFitter.m_maxEtForFits * coshEta
            eRespFitter.fitRGraph(graph, eRespFitter.m_JES_Function, eCut, emaxCut, )

            # eRespFitter.m_R_vs_E_Func is now the final response function == inverse of calib constant
            # save it 
            objs.push_back(objs.R_vs_Enuminv_Func, eRespFitter.m_R_vs_E_Func) # this is the function !
            objs.push_back(objs.R_vs_Enuminv_SLGraph, eRespFitter.m_R_vs_E_SLGraph)

            #Groom function
            objs.push_back(objs.R_vs_Enuminv_GroomExt_Func, eRespFitter.m_GroomExt_Func)


        objs.saveEtaJESToFile( self.fOut_jes, [  "R_vs_Enuminv_Func", "R_vs_Enuminv_Graph",
                                                 #"Enuminv_vs_Etrue" ,  
                                                 "R_vs_Enuminv_SLGraph",
                                                 "R_vs_Enuminv_GroomExt_Func",
                                                ])


    # 3rd pass fits :
    def fitEtaJESClosure(self):
        self.fOut_jes.cd()        
        objs = self.objs
        eRespFitter = objs.eRespFitter
        dEtaFitter = objs.dEtaFitter
        # prepare python list to store other intermediate histos
        objs.closure_R_vs_Etrue_perEtaBin = []

        for i in range(objs.etaBins.nBins() ):
            eRespFitter.prepareEtaBin(i)
            dEtaFitter.prepareEtaBin(i)
            eta= objs.etaBins.binCenter( i )
            coshEta = ROOT.TMath.CosH(eta)

            print " Third pass : processing eta bin ", i , " center = ", eta
            
            # ********************
            # Prepare the <R> vs Etrue graph
            eCut = eRespFitter.m_minEtForMeasurement * coshEta
            #NOT graph = eRespFitter.constructRGraph(objs.R_vs_Enuminv[i], objs.Enuminv_vs_Etrue[i], objs.Etrue_vs_Etrue[i], eCut)
            graph = eRespFitter.constructRGraph(objs.closure_R_vs_Etrue[i], objs.Etrue_vs_Etrue[i], objs.Etrue_vs_Etrue[i], eCut)
            # save the intermediate response per Etrue bins
            self.jes_respDir.cd()
            for h in eRespFitter.m_respPerEbins:
                h.Write()
            objs.closure_R_vs_Etrue_perEtaBin.append( list(eRespFitter.m_respPerEbins) ) 
            self.fOut_jes.cd()

            objs.push_back(objs.closure_R_vs_Etrue_Graph, graph)

            # ********************
            # Delta Eta
            # Prepare the Deta vs pT_true graph (last 2 args are optimalRebin=True, includeBadFits=False
            eCut = dEtaFitter.m_minEtForMeasurement * coshEta

            detaGr = dEtaFitter.constructRGraph(objs.closure_DEta_vs_Etrue[i], objs.Etrue_vs_Etrue[i], objs.Etrue_vs_Etrue[i], eCut,True, False)
            objs.push_back(objs.closure_DEta_vs_Etrue_Graph, detaGr)


            # ********************
            # Prepare the <R> vs ptTrue graph
            eCut = eRespFitter.m_minEtForMeasurement #* coshEta
            #NOT graph = eRespFitter.constructRGraph(objs.R_vs_Enuminv[i], objs.Enuminv_vs_Etrue[i], objs.Etrue_vs_Etrue[i], eCut)
            graph = eRespFitter.constructRGraph(objs.closure_R_vs_pTtrue[i], objs.pTtrue_vs_pTtrue[i], objs.pTtrue_vs_pTtrue[i], eCut)
            objs.push_back(objs.closure_R_vs_pTtrue_Graph, graph)

            graph = eRespFitter.constructRGraph(objs.R_vs_pTtrue[i], objs.pTtrue_vs_pTtrue[i], objs.pTtrue_vs_pTtrue[i], eCut)
            objs.push_back(objs.R_vs_pTtrue_Graph, graph)


        objs.saveEtaJESToFile( self.fOut_jes, [  "closure_R_vs_Etrue", "closure_R_vs_Etrue_Graph", 
                                                 "closure_DEta_vs_Etrue_Graph", "closure_DEta_vs_Etrue",
                                                 "R_vs_pTtrue_Graph", "closure_R_vs_pTtrue_Graph",
                                                ])



    # ****************************************************************
    # JMS fit methods
    # ****************************************************************


    # define codes to investigate success/failure of hists
    fitStatus = dict( outOfRange=-1, lowStat=-2,   fitFailed=-3, ok=0, modeFallBack=1, meanFallBack=2)
    # JMS fit mass response in each bin 
    def fitJMS(self, mR_in_PtEtaM, excludeLargeMoPtBins=True, saveFuncs=True, responseEval="gaus", debugBin=None ):
        """Fit mass response in each bin & attach the result to each bin.
        mR_in_PtEtaM : the container (BinnedTH1) of the response histograms.
        
        This func also creates a BinnedTF1 object containing the resulting fit function (if fit suceeded) in each fit.
        This BinnedTF1 object is attached to self.objs."""
        
        objs = self.objs

        jmsFitter=objs.balanceFitter_jms
        mR_in_PtEtaM.initInfoContent(clear=True)
        fitStatus = self.fitStatus
        
        # prepare functions testing out of range.(depends on binSystem)
        ptBinCenter = mR_in_PtEtaM.m_phaseSpace.axis(0).binCenter
        mBinCenter = mR_in_PtEtaM.m_phaseSpace.axis(1).binCenter
        etaBinCenter = mR_in_PtEtaM.m_phaseSpace.axis(2).binCenter
        if objs.binSystem=="ELogMoEBins":
            exp = ROOT.TMath.Exp
            sqrt=ROOT.TMath.Sqrt
            cosh=ROOT.TMath.CosH
            def outOfRangeBin(bin):
                e = ptBinCenter(bin.i)
                m = exp(mBinCenter(bin.j))*e
                pt = sqrt(e*e-m*m)/cosh(etaBinCenter(bin.k))
                return pt<objs.pTmin
        elif obsj.binSystem=="PtMBins":
            outOfRangeBin = lambda bin :  0.8* mBinCenter(bin.j)/ ptBinCenter(bin.i) > objs.radius
        else: # other bin system to be implemented !!!
            outOfRangeBin = lambda bin :  False

            
        # Create the BinnedTF1 to store the results of the gaussian fits.
        if saveFuncs:
            mR_in_PtEtaM_func = getattr( objs, mR_in_PtEtaM.name()+"_func", None)
            if mR_in_PtEtaM_func is None:
                mR_in_PtEtaM_func = ROOT.BPS.BinnedTF1(mR_in_PtEtaM.name()+"_func", mR_in_PtEtaM.m_phaseSpace, None)

        fit0 = ROOT.TF1("noFunc","0")

        
        
        # create special funcs dedicated to mode/mean/median evaluations
        def calculateMedianIQR(h, binInfo):
            q1, q1_err, iqr, iqr_err = calcMedIQR(h)
            binInfo.med = q1
            binInfo.med_err = q1_err
            binInfo.IQR = iqr
            binInfo.IQR_err = iqr_err
            

        def median(h, binInfo):
            # assume calculateMedianIQR already called
            return binInfo.med, binInfo.med_err, fit0

        def fallBackToMean(h, binInfo):
            mean = h.GetMean()
            if h.GetEffectiveEntries()<8. or mean < 1e-3:
                binInfo.status=fitStatus['lowStat']
                binInfo.invalidFit = True
                return 1,1, fit0
            return  mean , h.GetMeanError() , fit0

        def mode(h,binInfo):
            mod, mod_err, modv,modv_err = calcSmoothMode(h, startBin=2)
            binInfo.mod = mod
            binInfo.mod_err = mod_err
            if (mod_err==0.) and (mod==0.):
                binInfo.status=fitStatus['meanFallBack']
                return fallBackToMean(h,binInfo)            
            return mod, mod_err, fit0

        fitmin, fitmax = ROOT.Double(), ROOT.Double()
        fitFunc = jmsFitter.FitNoRebin if objs.jmsNoRebin else jms.Fit
        def gausFit(h, binInfo):
            fit = fitFunc(h, 0.)
            fitR = jmsFitter.fitRes().Get();
            if not fitR:
                binInfo.invalidFit = True
                binInfo.status=fitStatus['fitFailed']
                return 1, 1, fit0
            if fitR.Status()==0 :
                mean= fit.GetParameter(1)
                mean_err= fit.GetParError(1)
                fit.GetRange(fitmin, fitmax)
                width= fit.GetParameter(2)
                if mean < 0 or mean >5 or width<0.03 or mean<fitmin or mean > fitmax:
                    # absurd values, fit failed, probably too bad response.
                    # we can't do anything : use the mode estimation instead :
                    binInfo.status=fitStatus['modeFallBack']
                    mean, mean_err, fit = mode(h,binInfo)
                else:
                    binInfo.status=0             
            else: # fit failed
                # use the mode estimation instead :
                binInfo.status = fitStatus['modeFallBack']
                mean, mean_err, fit = mode(h,binInfo)

            return mean, mean_err, fit

        def gausAndMode(h, binInfo):
            fit = fitFunc(h, 0.)
            fitR = jmsFitter.fitRes().Get();
            if not fitR:
                binInfo.invalidFit = True
                binInfo.status=fitStatus['fitFailed']
                return 1, 1, fit0

            if fitR.Status()==0 :
                mean= fit.GetParameter(1)
                mean_err= fit.GetParError(1)
                mod, mode_err, fit = mode(h,binInfo)
                dm = abs(2*(mean-mod)/(mean+mod))
                if dm>0.15:
                    binInfo.status=fitStatus['modeFallBack']
                    return mod, mode_err, fit
                # r = 0.5*(mod+mean)
                # r_err = max( abs(mean-mod), mean_err)
                return mean, mean_err, fit

            binInfo.status = fitStatus['modeFallBack']
            return mode(h,binInfo)
                
        # depending on the options we choose what function is used to get the response
        responseFunction = gausFit
        if responseEval == "mode" : responseFunction = mode
        elif responseEval == "mean" : responseFunction = fallBackToMean
        elif responseEval == "gausAndMode": responseFunction = gausAndMode
        numOcc = ROOT.DJS.numNonEmptyBins
        # Define an inner function which will be called on each bin to perform the fit
        # using the above functions.
        def fitJMSBin(bin, h, binInfo):
            calculateMedianIQR(h, binInfo)
            binInfo.invalidFit = False
            fit = fit0
            nocc = numOcc( h )
            binInfo.status= 0 # ok
            if outOfRangeBin(bin) or nocc < 5:
                mresp, mresp_err = 1 , 1
                binInfo.invalidFit = True
                binInfo.status=fitStatus['outOfRange'] if nocc>=5 else fitStatus['lowStat']
            #elif h.GetEffectiveEntries()<100 and h.GetEntries()<100:
            elif h.GetEffectiveEntries() / nocc < 1. or h.GetEffectiveEntries()< 20:
                mresp, mresp_err, fit = fallBackToMean(h, binInfo)
                mresp_err = mresp
            else:
                mresp, mresp_err, fit = responseFunction(h, binInfo)

            if saveFuncs: mR_in_PtEtaM_func.set(bin.index(), fit)
            if mresp<0.5:
                mresp=0.5
                mresp_err = 0.5
            binInfo.mresp = mresp
            # cap the mresp error at 5. Fits can give absurdly high value (>10e4) which
            # disturb the subsequent smoothing procedure.
            binInfo.mresp_err = mresp_err if mresp_err<5 else 5

        if debugBin is not None:
            fitJMSBin_ = fitJMSBin
            def fitJMSBin(bin, h, binInfo):
                fitJMSBin_(bin, h, binInfo)
                print binInfo.mresp , binInfo.mresp_err, binInfo.invalidFit
                
        # -------------------------------------
        # Finally,  do the fit in each bin by using the 'applyToBins()' extension of BinnedTH1
        mR_in_PtEtaM.applyToBins( fitJMSBin, range=debugBin) # if debugBin=None --> run on all bins
        # -------------------------------------

        if debugBin != None :
            return mR_in_PtEtaM.infoAt( *debugBin[0])
            setattr( objs, mR_in_PtEtaM_func.name(),  mR_in_PtEtaM_func)
        # massage and save the object containing fit func we created
        if saveFuncs :
            setattr( objs, mR_in_PtEtaM_func.name(),  mR_in_PtEtaM_func)
            mR_in_PtEtaM_func.setName(mR_in_PtEtaM_func.name()) # to reset the func name
            objs.saveJMSObject(self.fOut_jms, mR_in_PtEtaM_func)

        

    def mergeVeryLowMassLowStatBins(self, mR_in_PtEtaM):
        for ieta in range(self.objs.jmsBPS.axis(2).nBins()):
            self.mergeVeryLowMassLowStatBins_etaSlice(mR_in_PtEtaM,ieta)

    def mergeVeryLowMassLowStatBins_etaSlice(self, mR_in_PtEtaM, ieta):
        """This function deals with low mass bins with very low stats.
        Such bins have been flagged with invalidFit=True in the fitJMS() method.

        flagged bins contiguous along the pt axis are progressively
        merged together (i.e their histos are added) until histo contain enough stats.
        All bins clustered together are then assigned their common histo and common response evaluated
        from their histos.        
        """

        objs = self.objs
        info = mR_in_PtEtaM.infoContent
        # 1st step : list all invalidFit bins at low mass.
        # form 1 list per mass bin 
        from collections import defaultdict
        lowStatBins = defaultdict(list)
        for bin in objs.jmsBPS.fullRange():
            if bin.k != ieta: continue
            if bin.j > 5 : continue
            bi = info[bin.index()]
            if bi.invalidFit:
                lowStatBins[bin.j].append( bin.asTuple() )

        minStat = 10

        # define an object representing a cluster of contiguous bins
        class BinCluster(object):
            def __init__(self):
                self.bins = []            
                self.nentries = 0
                self.h = None

            def add(self, coord ):
                self.bins.append( coord )
                h = mR_in_PtEtaM.at( *coord)
                n = h.GetEffectiveEntries()
                self.nentries += n
                if self.h is None : self.h = h.Clone()
                else: self.h.Add(h)
                #print self.bins, " :  added ", coord , " n ", n , " --> ", self.nentries
                
            def merge(self,o):
                #print "merging ", self.bins, o.bins
                self.bins += o.bins
                self.nentries += o.nentries
                self.h.Add(o.h)

            def evaluateResp(self):
                mresp = self.h.GetMean()
                mresp_err = self.h.GetMeanError()
                bps = mR_in_PtEtaM.m_phaseSpace
                #print "Re-evaluated at ", self.bins
                #print " -------> ", mresp , mresp_err
                valid = self.nentries > minStat
                for coord in self.bins:
                    bi = mR_in_PtEtaM.infoAt( *coord)
                    bi.mresp = mresp
                    bi.mresp_err = mresp
                    bi.invalidFit = valid
                    mR_in_PtEtaM.setHisto(bps.bin(*coord), self.h) # causes a memory leak

        # define a reccursive function to merge coordinates of bins (l_in) into
        # a list of BinCluster each containing a stat > minStat
        def mergeBins( l_in , l_out ):
            if l_in == []: return
            if l_out==[] : l_out.append(BinCluster())
            cluster = l_out[-1]
            coord = l_in.pop(0)
            cluster.add( coord  )
            if cluster.nentries > minStat: # enough stats, create a new cluster
                l_out.append( BinCluster() )
            mergeBins(l_in, l_out)

        # create lists of BinCluster, 1 list per mass bin 
        clusters = []
        for mbin in sorted(lowStatBins.keys()):
            bins = lowStatBins[mbin] # the list of invalid coordinates a for mbin
            mergedBins = []
            mergeBins( bins, mergedBins) 
            clusters.append( mergedBins )

        # the last clusters in each m bin may still have stat< minStat.
        # we then merge them with the previous cluster  along the pt axis
        for clusterL in clusters:
            last = clusterL[-1] #
            if last.nentries > minStat: continue
            # try to merge it with others if possible
            if len(clusterL)>1:
                clusterL[-2].merge(last)
                clusterL.pop()
                
        # if unmerged/low stats last clusters remain, try to merge them accross mass bins
        lastClusters = [ clusterL[-1] for clusterL in reversed(clusters)]
        if len(lastClusters)>1:
            for i, last in enumerate(lastClusters[:-1]):
                if last.nentries>minStat: continue
                last.merge( lastClusters[i+1] )
                lastClusters[i+1] = last

            # last cluster :
            if lastClusters[-1].nentries<minStat:
                lastClusters[-2].merge(lastClusters[-1])
                lastClusters[-1] = lastClusters[-2]

        # finally re-set last clusters into the full list
        lastClusters.reverse()
        for i, last in enumerate(lastClusters):
            clusters[i][-1] = last

        # now all invalid bins are clustered.
        # use the merged histograms to assign new responses to each bins
        for clusterL in clusters:
            for clust in clusterL:
                clust.evaluateResp()




    def reEvalJMSatLowM(self,inTrueBins, reEvalStrategy="linear2points"):
        """Replace abberant low bin response by extrapolating from closest valid mass bins  """
        if inTrueBins:
            iname = "mR_in_PtEtaMtrue_py"
        else:
            iname = "mR_in_PtEtaMnuminv_py"
        objs = self.objs
        mR_in_PtEtaM = getattr(objs, iname)
        
        
        # below we define inner functions implementing various
        # extrapolation strategies
        # First define helpers/shortcuts :
        jmsBPS=objs.jmsBPS
        indexNextMass = lambda bin : jmsBPS.bin(bin.i, bin.j+1, bin.k)
        nMassBins = jmsBPS.axis(0).nBins()
        binsInfo = mR_in_PtEtaM.infoContent
        mBinCenter  = jmsBPS.axis(1).binCenter
        ptBinCenter = jmsBPS.axis(0).binCenter
        BinIterator = ROOT.BPS.BinnedPhaseSpace.BinIterator

        def extrapolateSimple(bin, histo, binInfo):
            # this func will act only on bins corresponding to lower mass bin.
            # that is bin in the form (ptbin, mbin) = (ptbin,0).
            # In this case it will re-evaluate the response in this bin by extrapolating
            # the responses from bins of higher masses : (ptbin,1) and (ptbin,2).
            
            if bin.j >0: return
            # else we're on the 1st M bin
            pbin = BinIterator( bin )
            # get Responses for higher masses
            previousR = []
            for i in [1,2]:                
                pbin.j = i
                previousR.append(binsInfo[pbin.index()].mresp )
            if previousR[0] < binInfo.mresp:
                # nothing to do.
                return
            if previousR[0] < previousR[1]:
                # we're going to extrapolate down : this is not what we want at low mass
                return
            # calculate linear extrapolation from higher mass bins: 
            masses = [mBinCenter(1), mBinCenter(2) ]
            extrapol = (previousR[0] - previousR[1])/(masses[0]-masses[1])*(mBinCenter(0)-masses[0]) + previousR[0]
            binInfo.mresp_orig = binInfo.mresp
            binInfo.mresp = extrapol


        theGr = ROOT.TGraphErrors(5)
        f = ROOT.TF1("linfit", "pol1",jmsBPS.axis(1).min(), jmsBPS.axis(1).max())
        g = ROOT.TGraphErrors(3)
        def extrapolateLine(bin, gr, binInfo):
            if bin.j >0: return
            pbin = BinIterator( bin )
            # get the 3 first valid points on the m axis
            i= 0
            pointsToSet=[]
            for c in range(nMassBins):
                pbin.j = c
                index = pbin.index()
                if binsInfo[index].invalidFit or binsInfo[index].mresp < binsInfo[indexNextMass(pbin)].mresp: #or binsInfo[index].mresp_err>0.5 or (c==0 and binsInfo[index].mresp<2) :
                    pointsToSet.append(c)
                    continue
                g.SetPoint(i, mBinCenter(c), binsInfo[index].mresp)
                g.SetPointError(i, 0, binsInfo[index].mresp_err)
                i+=1
                if i==3 : break
            if i<3:
                return
            g.Fit(f, "RQN")
            if f.GetParameter(1)>0:
                print "Wrong extrapolation at ",bin.asTuple(),
                return
            for c in pointsToSet:
                pbin.j = c
                #print bin.asTuple(), c ,' changing to ',f.Eval(mBinCenter(c))
                bi = binsInfo[ pbin.index() ]
                bi.mresp_orig = bi.mresp
                bi.mresp = f.Eval(mBinCenter(c))

        def extrapolateParabola(bin, gr, binInfo):
            if bin.j >0: return
            # we're on the 1st M bin
            pbin = BinIterator( bin )
            maxMass = max( ptBinCenter(pbin.i)/20. , 30)
            f = ROOT.TF1(iname+"_"+jmsBPS.binTag(pbin.index()), "pol2",0, maxMass)
            npoints = 0
            # place points in the graph
            pointsByR = []
            orig_resp = []
            for i in xrange(nMassBins):
                pbin.j = i
                m = mBinCenter(i)
                bi = binsInfo[pbin.index()]                
                theGr.SetPoint(i, m, bi.mresp)
                theGr.SetPointError(i, 0, bi.mresp_err)
                pointsByR += [ (bi.mresp, i) ]
                orig_resp += [bi.mresp]
                npoints +=1
            theGr.Set(npoints)
            theGr.Fit(f, "RQN") # R: use the TF1 range.
            pointsByR.sort()
            while f.GetParameter(2)<0:
                theGr.RemovePoint( 0 ) # ignore smallest response
                if maxMass-theGr.GetX()[0] < 40:
                    maxMass += 11
                    f.SetRange(0, maxMass)
                    # add a point
                theGr.Fit(f, "RQN")
                #pointsByR.pop(0)
                
            # replace by fitted values
            pbin.j=0
            for i in xrange(nMassBins):
                pbin.j = i
                m = mBinCenter(i)
                if m> maxMass: break
                binsInfo[pbin.index()].mresp_orig = orig_resp[i]
                binsInfo[pbin.index()].mresp = f.Eval( m )
                
            binInfo.parabola= f
            
        # apply extrapolation
        if reEvalStrategy == "linearMultipoints":
            mR_in_PtEtaM.applyToBins( extrapolateLine)
        elif reEvalStrategy=="linear2points":
            mR_in_PtEtaM.applyToBins( extrapolateSimple )


            
    def buildJMSvsX(self, dim, step="trueBins", noExtrap=False , recopyFitFunc=False,
                    value = "mR", # value to plot
                    binFilter = None,
                    ):
        """Build TGraphErrors where Y axis =(mass response (or other 'value')) and X axis=(the variable indexed by 'dim').
        The Y values will be calculated from one of the objs.mR_in_PtEtaMxxx container according to option 'step'.

        The TGraphErrors are collected within a BinnedTGraphErrors (spanning all other dimensions than 'dim') which is
        attached to self.objs as python attributes and saved in the current output jms file.
        """
        
        var = ["pt","m","eta"][dim]
        objs = self.objs
        if step=="trueBins":
            mR_in_PtEtaM = objs.mR_in_PtEtaMtrue_py
            templName = '{value}_vs_{var}true_in_PtEtatrue'
        elif step=="numinvBins":
            mR_in_PtEtaM = objs.mR_in_PtEtaMnuminv_py
            templName = '{value}_vs_{var}numinv_in_PtEtanuminv'
        elif step=="closure":
            mR_in_PtEtaM = objs.closure_mR_in_PtEtaMtrue_py
            templName = 'closure_{value}_vs_{var}true_in_PtEtatrue'
        else: # custom. we assume step is a tuple (mR_in_XX_object, templName)
            mR_in_PtEtaM, templName = step

        cname = templName.format( value=value, var = var)
        if noExtrap:
            cname = cname+"_noExtrap"


        # 1.) build TGraphError of mResp vs X in each (pt,eta)
        theBPS=mR_in_PtEtaM.m_phaseSpace
        axisBinCenter = theBPS.axis(dim).binCenter # define a shortcut 
        # this inner function will be called at each (pt,m,eta) bin 
        # It returns the fitted mR and its error to populate TGraphs
        def buildmRvsmreco(bin, histo, binInfo):
            var = axisBinCenter( bin.asTuple()[dim] )
            r = binInfo.mresp
            if noExtrap: # retrieve non-extrapolated response 
                try:
                    r = binInfo.mresp_orig
                except:
                    pass
            return var , r, 0, binInfo.mresp_err

        def buildmIQRvsmreco(bin, histo, binInfo):
            var = axisBinCenter( bin.asTuple()[dim] )
            r = binInfo.IQR 
            return var , r, 0, binInfo.IQR_err

        if value == "mR":
            valueFunction = buildmRvsmreco
        elif value == "mIQR" :
            valueFunction = buildmIQRvsmreco

        # Create a BinnedTGraphErrors container by applying the above function.
        #  for ex this will cast 'mR in (pt,m,eta) bins' into 'Tgraphs of mR vs m in (pt,eta) bins' 
        mR_vs_m_in_PtEta = mR_in_PtEtaM.lineToTGraph(dim , valueFunction, binFilter=binFilter )
        mR_vs_m_in_PtEta.initInfoContent()
        mR_vs_m_in_PtEta.updateHistoDesc(desc_x = theBPS.axis(dim).title() , desc_y=value)

        if recopyFitFunc:
            def addFittedTF1(bin, g, binInfo):
                binInfo.parabola = mR_in_PtEtaM.infoAt(bin.i,0,bin.j).parabola
            mR_vs_m_in_PtEta.applyToBins( addFittedTF1 )


        # save the result into the objs
        setattr(objs, cname, mR_vs_m_in_PtEta)
        mR_vs_m_in_PtEta.setName( cname)

        # mR_vs_m_in_PtEta corresponds to a new phase space (the one with dimension dim removed)
        # save it onto the object :
        setattr(objs, "sliceOn"+var+"BPS", mR_vs_m_in_PtEta.m_phaseSpace)
        
        # save result in output file
        mR_vs_m_in_PtEta.m_phaseSpace.write()
        objs.saveJMSObject(self.fOut_jms, mR_vs_m_in_PtEta)
        



    def buildJMSvsPtM(self, inTrueBins, noSave=False,  useIQR=False):
        """Build a TGraphError  mass response vs (pt,m) from objs.mR_in_PtEtaM*
        """
        objs = self.objs
        if isinstance(inTrueBins, tuple): # generic case, assume the caller passed the arguments directly
            mR_in_PtEtaM , cname = inTrueBins 
        elif inTrueBins:
            mR_in_PtEtaM = objs.mR_in_PtEtaMtrue_py
            m_in_PtEtaM = objs.m_in_PtEtaMtrue
            cname = "mR_vs_PtMtrue_in_Eta"
        else:
            mR_in_PtEtaM = objs.mR_in_PtEtaMnuminv_py
            m_in_PtEtaM = objs.m_in_PtEtaMnuminv
            cname = "mR_vs_PtMnuminv_in_Eta"

        # create short alias to a few functions
        jmsBPS=objs.jmsBPS
        mBinCenter = jmsBPS.axis(1).binCenter
        ptBinCenter = jmsBPS.axis(0).binCenter
        binIndex = jmsBPS.bin

        # a function to be used with BinnedHistos.planeToTGraph
        # returns the m response at a given bin
        def buildmRvsptm(bin, histo, binInfo):
            m = mBinCenter( bin.j) 
            pt = ptBinCenter( bin.i) 

            if binInfo.invalidFit: return None
            return pt,m , binInfo.mresp, 0, 0, binInfo.mresp_err

        # a function to be used with BinnedHistos.planeToTGraph
        # returns the IQR at a given bin
        def buildIQRvsptm(bin, histo, binInfo):
            m = mBinCenter( bin.j) 
            pt = ptBinCenter( bin.i) 

            if binInfo.invalidFit: return None
            return pt,m , binInfo.IQR, 0, 0, binInfo.IQR_err

        # chose wich function to use
        buildFunc = buildIQRvsptm if useIQR else buildmRvsptm

        # Construct a container of 2D graphs from the full content of the phasespace
        # by using planeToTGraph 
        mR_vs_PtM = mR_in_PtEtaM.planeToTGraph(0,1 , buildFunc )
        # associate info to each 2D graph
        mR_vs_PtM.initInfoContent()
        mR_vs_PtM.histoDesc().setProp(desc_x=objs.pt_desc, desc_y=objs.m_desc)


        # Below we hack the 2D graphs containing the response.
        # this is to avoid side effect in smoothing and/or to
        # avoid nasty freezing effects when applying calibration

        
        # A first option is a linear extrapolation : simply creates one extrapolated point
        # at m=0 for each pt bin and at pt=ptmin/2. for each mass bin
        ptMin = jmsBPS.axis(0).min()/2. # arbitrary !
        # define a function which add points at low mass and at low pt
        def extrapolateAt0(bin, histo, binInfo):
            N = histo.GetN()            
            ieta = bin.i
            _infoC = mR_in_PtEtaM.infoContent            
            infoAt = lambda i,j : _infoC[ binIndex(i,j,ieta) ] # returns the BinInfo object from mR_in_PtEtaM
            # loop over pt bins, extrapolate at low mass :
            for ipt in xrange(jmsBPS.axis(0).nBins()):
                r1 = infoAt(ipt,0).mresp
                r2 = infoAt(ipt,1).mresp
                m1 , m2 = mBinCenter(0), mBinCenter(1)
                slope= (r1-r2)/(m1-m2)
                r = r1-slope*m1
                if r<r1  : r=r1 # this can happen at very low stats/bad fits. Just freeze in this case.
                histo.SetPoint(N, ptBinCenter(ipt), 0., r)
                histo.SetPointError(N, 0, 0., infoAt(ipt,0).mresp_err )
                N +=1
            # loop over m bins, extrapolate at low pt :
            for im in xrange(jmsBPS.axis(1).nBins()):
                r1 = infoAt(0,im).mresp
                histo.SetPoint(N, ptMin, mBinCenter(im), r1 )
                histo.SetPointError(N, 0, 0,  infoAt(0,im).mresp_err )
                N+=1

        # A simpler option is to duplicate the points at lowest mass/pT
        # this way those points will be less affected by boundary effects the
        # kernel smoothing procedure.
        # This is useful in the case where the response is clearly decreasing/increasing
        # at the boundary of the phasespace.
        def duplicateAtBoundary(bin, histo, binInfo):
            N = histo.GetN()            
            ieta = bin.i
            _infoC = mR_in_PtEtaM.infoContent            
            infoAt = lambda i,j : _infoC[ binIndex(i,j,ieta) ] # returns the BinInfo object from mR_in_PtEtaM
            m0 = mBinCenter(0)
            pt0 = ptBinCenter(0)
            # loop over pt bins, add a point at 0 mass :
            for ipt in xrange(jmsBPS.axis(0).nBins()):
                bi = infoAt(ipt,0)
                histo.SetPoint(N, ptBinCenter(ipt), m0, bi.mresp)
                histo.SetPointError(N, 0, 0., bi.mresp_err )
                N +=1
            # loop over m bins, extrapolate at low pt :
            for im in xrange(jmsBPS.axis(1).nBins()):
                bi = infoAt(0,im)
                histo.SetPoint(N, pt0, mBinCenter(im), bi.mresp)
                histo.SetPointError(N, 0, 0., bi.mresp_err )
                N+=1

                
        # do not extrapolate  if using log(m/pt) parametrization
        if objs.binSystem == "PtMBins":                
            mR_vs_PtM.applyToBins( extrapolateAt0)
        if objs.binSystem == "ELogMoEBins":
            mR_vs_PtM.applyToBins( duplicateAtBoundary )

        # assign the mR_vs_PtM we created to objs
        setattr(objs, cname, mR_vs_PtM)            
        mR_vs_PtM.setName( cname)
        if noSave: # return immediately without saving result into object and file
            return mR_vs_PtM

        # save the result into the objs
        mR_vs_PtM.initInfoContent()

        setattr(objs, "etaBPS", mR_vs_PtM.m_phaseSpace)
        # save result in output file
        mR_vs_PtM.m_phaseSpace.write()
        objs.saveJMSObject(self.fOut_jms, mR_vs_PtM)
        return mR_vs_PtM



    def smoothJMSvsPtM(self, mR_vs_PtM, oname,  doSave=True):
        """ Perform smoothing on each TGraph2D from the input BinnedTGraphErrors 'mR_vs_PtM' and store the results
        in a BinnedTH2 named 'oname'.
        the resulting BinnedTH2 is also saved onto self.objs as 'oname' and in the output file.        
        """
        objs = self.objs

        
        # initialize the BinnedTH2 from the unsmooothed one
        # proceed this way (setattr then getattr)  to avoid C++/python ownership issues
        setattr(objs, oname, BinnedTH2(oname,mR_vs_PtM.m_phaseSpace, None, Title="Smoothed mass response", desc_x=objs.pt_desc, desc_y=objs.m_desc) )
        mRsmooth_vs_PtM = getattr( objs, oname) 

        smoother = objs.jms_tg2_smoother

        # prepare the TH2F model 
        ptmin,ptmax = objs.jmsBPS.axis(0).min() /2. , objs.jmsBPS.axis(0).max()
        mmin,mmax = objs.jmsBPS.axis(1).min() , objs.jmsBPS.axis(1).max()        
        model =ROOT.TH2F(mR_vs_PtM.name(), "Smoothed mass resp",smoother.m_npoints[0],ptmin,ptmax, smoother.m_npoints[1], mmin, mmax )

        # operations to smooth 1 TGrahErrors into a TH2F
        def doSmooth(bin, g, binInfo):
            i = bin.index()
            hs = model.Clone(str(i))
            # release ownership
            ROOT.SetOwnership(hs,False)
            hs.SetTitle("Smoothed mass resp "+str(i) )
            smoother.graphToHisto( g , hs)
            mRsmooth_vs_PtM.set( i, hs )

        # apply to each bin 
        mR_vs_PtM.applyToBins( doSmooth )

        setattr(objs, oname, mRsmooth_vs_PtM)
        mRsmooth_vs_PtM.setName( oname)
        mRsmooth_vs_PtM.initInfoContent()
        # save result in output file
        mRsmooth_vs_PtM.m_phaseSpace.write()
        if doSave: objs.saveJMSObject(self.fOut_jms, mRsmooth_vs_PtM)


    def buildResponseAsTH3(self, mRsmooth_vs_PtM , saveAs=""):
        """Build a TH3F from the BinnedTH2 'mRsmooth_vs_PtM'.
        This is done by stacking all the TH2 withing mRsmooth_vs_PtM along the 3rd axis of the TH3F.

        We apply special treatment on the Z (eta) axis to avoid interpolation issues due to large eta bins.
        See comments below.        
        """        
        objs = self.objs
        
        def binEdges(ax, addEndPoint=False):
            """returns a list of edges corresponding to TAxis ax.
            """
            N = ax.GetNbins()
            lastEdge = ax.GetBinUpEdge(N)
            return [ax.GetBinLowEdge(i) for i in range(1,N+1)] +[lastEdge]

        def extendEtaAxis(ax):
            """returns a list of edges corresponding to TAxis ax.
            A point is inserted in the middle of the last bin and 
            the last edge is modified so that it corresponds to the middle of the last bin
            in the returned list.
            """
            N = ax.GetNbins()
            lastEdge = ax.GetBinUpEdge(N)
            edges = [ax.GetBinLowEdge(i) for i in range(1,N+1)] 
        
            llEdge = edges[-1]
            newEdge=0.5*(llEdge+lastEdge)
            edges += [ newEdge,              # add a new edge
                       2*lastEdge-newEdge  ] # so that lastEdge is the middle of last bin

            #edges[0] =  2 *edges[0] -  edges[1] # so that edges[0] is the middle of 1st bin
            return edges

        h0 = mRsmooth_vs_PtM[0] # take the first histo as reference.

        # prepare the edge specifications of the TH3 :
        xbins = binEdges(h0.GetXaxis())
        ybins = binEdges(h0.GetYaxis())
        # handle eta specially : we extend and modify the last bin so interpolation in high
        # eta region behaves better. The last bin is splitted in 2 : we will fill the 2 new bins
        # with the same content (the content of the original last bin), see below.
        #   etabins = extendEtaAxis(objs.jmsBPS.axis(2).toTH1("tmp").GetXaxis() )
        # with fine eta bins the above is not necessary
        etabins = binEdges(objs.jmsBPS.axis(2).toTH1("tmp").GetXaxis() )
        
        # create the binning specification for a TH3F (h1 and h2 are helpers defined in BinnedPhaseSpace)
        from BinnedPhaseSpace.BinnedPhaseSpaceConfig import h1, h2
        hbins = h2(xbins, ybins) + h1(etabins)

        h3 = ROOT.TH3F("numInvResp3D", "numInvResponse", *hbins)

        # Fill the new TH3F
        netaBins = h3.GetNbinsZ()
        for ieta in range(1,netaBins+1)  :
            h2 = mRsmooth_vs_PtM[ieta-1] # get the TH2F
            # if special treatment of eta, do something for last bin :
            ## if ieta<netaBins:
            ##     h2 = mRsmooth_vs_PtM[ieta-1] # get the TH2F
            ## else:
            ##     h2 = mRsmooth_vs_PtM[ieta-2] # repeat the last TH2F (this is the new last eta bin created to avoid interpolation bias).    
            for ix in range(1,h3.GetNbinsX()+1) :
                for iy in range(1,h3.GetNbinsY()+1) :
                    b2 = h2.GetBin(ix,iy)
                    b3 = h3.GetBin(ix,iy,ieta)
                    h3.SetBinContent(b3, h2.GetBinContent(b2) )

        if saveAs!="":
            setattr(objs, saveAs , h3)
            self.fOut_jms.cd()
            h3.Write(saveAs, ROOT.TObject.kOverwrite)
            
        return h3

    # def directJMSNumInv(self,):
    #     """This creates a numInv mass responses directly from the responses in true bins (mRsmooth_vs_PtMtrue_in_Eta) by scaling the m-position of each bin by
    #     the content of each bin (i.e the mass response). 
    #     This option avoids to repeat the procedure implemented in JMS.niresp+JMSnifits (that is : loop over all events & fill responses in m_true*response(m_true) bins and re-fit all the bins).
    #     """
    #     objs = self.objs
        
    #     numinv_mR_vs_PtM = ROOT.BPS.BinnedTGraph2DErrors("numinv_mR_vs_PtMtrue_in_Eta", objs.mR_vs_PtMtrue_in_Eta.m_phaseSpace, None)
    #     for it in objs.mR_vs_PtMtrue_in_Eta.m_phaseSpace.fullRange():
    #         g = objs.mR_vs_PtMtrue_in_Eta[it.index()]
    #         g2 = ROOT.DJS.numInvYaxis( g ) 
    #         numinv_mR_vs_PtM.set(it.index(), g2)
    #     numinv_mR_vs_PtM.initInfoContent()
    #     numinv_mR_vs_PtM.histoDesc().setProp(desc_x=objs.pt_desc, desc_y=objs.m_desc)

    #     # save the result into the objs
    #     setattr(objs, "numinv_mR_vs_PtMtrue_in_Eta", numinv_mR_vs_PtM)
    #     numinv_mR_vs_PtM.setName("numinv_mR_vs_PtMtrue_in_Eta")
    #     numinv_mR_vs_PtM.initInfoContent()
    #     # save result in output file
    #     numinv_mR_vs_PtM.m_phaseSpace.write()
    #     objs.saveJMSObject(self.fOut_jms,numinv_mR_vs_PtM)

    #     objs.numinv_mR_vs_PtM_py = numinv_mR_vs_PtM

        




