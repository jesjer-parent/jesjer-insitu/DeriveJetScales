// this file is -*- C++ -*-
#ifndef DERIVEJETWSCALES_WEIGHTEDBINFINDER_H
#define DERIVEJETWSCALES_WEIGHTEDBINFINDER_H

#include "BinnedPhaseSpace/BinnedPhaseSpace.h"
#include "DeriveJetScales/JMSBinning.h"
#include <vector>

namespace DJS {

  ///////////////////////////////////////////////
  /// \class WeightedBinFinder
  ///
  /// Helper class to find a set of bins around a point in a BinnedPhaseSpace
  /// where each bin is associated to a geometrical weight depending on the distance to the point.
  ///
  /// More precisily, given a point P=(x,y,z) part of a phase space represented by a BinnedPhaseSpace,
  /// WeightedBinFinder::findBins(x,y,z) will return a vector<{bin_index, weight}> representing bins in the BinnedPhaseSpace.
  /// The returned bins are neighbours of the bin containing P and the weights are given by :
  /// weight = A x exp((x-bincenter_x)^2/m_sig1^2 + (y-bincenter_y)^2/m_sig2^2 + (z-bincenter_z)^2/m_sig3^2 )
  /// where A is a normalization factor such that sum(weights) == 1 (where sums runs over returned bins)
  ///
  /// For each dimension the gaussian widht m_sig and maximal number of neighbour to scan m_nn are configurable.
  ///
  class WeightedBinFinder {
  public:

    virtual ~WeightedBinFinder(){}

    struct WeightedBin {int index; double w;} ;

    WeightedBinFinder():m_bps(nullptr) {}
    WeightedBinFinder(BPS::BinnedPhaseSpace *p):m_bps(p) {}
    
    const std::vector<WeightedBin> & findBins(double x);
    const std::vector<WeightedBin> & findBins(double x, double y);
    const std::vector<WeightedBin> & findBins(double x, double y, double z);
    const std::vector<WeightedBin> & findBins(double x, double y, double z, double t);

    void setNN(int n1, int n2=0, int n3=0, int n4=0){m_nn1=n1;m_nn2=n2;m_nn3=n3;m_nn4=n4;}

    virtual void setSig1(double s1){m_sig1=s1*s1;}
    virtual void setSig2(double s1){m_sig2=s1*s1;}
    virtual void setSig3(double s1){m_sig3=s1*s1;}
    virtual void setSig4(double s1){m_sig4=s1*s1;}
    virtual void setSigmas(double s1,double s2=0,double s3=0,double s4=0){setSig1(s1);setSig2(s2);setSig3(s3);setSig4(s4);}

    
    /// internal function
    void fillBins(double x, int dim, int nn, double sig2, std::vector<WeightedBin> & v);

    
    
    int m_nn1;
    int m_nn2;
    int m_nn3;
    int m_nn4;

    double m_sig1;
    double m_sig2;
    double m_sig3;
    double m_sig4;
    BPS::BinnedPhaseSpace *m_bps;
    std::vector<WeightedBin> m_bins;
    
  };

  
  ///////////////////////////////////////////////
  /// \class JMSWBinning
  ///
  /// Combines a JMSBinning object to deal with coordinates used in a JMS derivation and WeightedBinFinder/
  /// Allows to set fractionnal gaussian width on 1st and 2nd coordinate (to extend WeightedBinFinder which uses only fixed width)
  ///
  class JMSWBinning : public WeightedBinFinder {
  public:

    virtual ~JMSWBinning(){}

    void initJet(double e, double m, double eta){
      m_binning->initJet(e,m,eta);
    }

    bool validBin(){return m_binning->validBin();}

    
    const std::vector<WeightedBinFinder::WeightedBin> & findBins();

    void setFracWidth1( double f){m_isfracWidth1=true; m_fWidth1=f;}
    void setFracWidth2( double f){m_isfracWidth2=true; m_fWidth2=f;}
    
    JMSBinning* m_binning;
    bool m_isfracWidth1 = false;
    bool m_isfracWidth2 = false;

    double m_fWidth1;
    double m_fWidth2;
  };


}

#endif
