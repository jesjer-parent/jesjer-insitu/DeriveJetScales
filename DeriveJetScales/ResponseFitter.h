// this file is -*- C++ -*-
#ifndef DERIVEJETWSCALES_RESPONSEFITTER_H
#define DERIVEJETWSCALES_RESPONSEFITTER_H

#include <vector>
#include "DeriveJetScales/Utilities.h"

class TF1;
class TH2;
class TH2F;
class TH1D;
class TProfile;
class TGraphErrors;
class JES_BalanceFitter;

namespace DJS {

  class ResponseFitter {
  public:
    
    std::vector<int> rebinXAxis(TH2F *inputHist, float minEffEntrie, TProfile *inputXAvg=nullptr, float minAvg=0);

    TGraphErrors *constructRGraph(TH2F *inputHist, TProfile *inputXAvg, TProfile *inputXAvgCut, double minCut, bool optimalRebin=true, bool includeBadFits=true);

    void fitRGraph(TGraphErrors *g, TString method, Double_t min, Double_t max) ;
    void fitDeltaEtagGraph(TGraphErrors *g, TString method, Double_t min, Double_t max) ;
    

    void prepareEtaBin(int ieta);

    // ********* Helpers ************************************
    /// Calculate the error-weighted average of inputProfile values taken on range [low, high]
    static void   getAvg(TProfile *inputProfile, int low, int high, double &avg, double &avg_err);

    /// Effective num entries when integrating all bins within lowbinX<bin_x< <highbinX
    static double getEffectiveEntries(TH1* h2, int lowbinX, int highbinX);



    TF1* m_R_vs_E_Func = 0;
    TF1* m_GroomExt_Func=0;
    TGraphErrors* m_R_vs_E_Graph = 0;
    TGraphErrors* m_R_vs_E_SLGraph = 0;

    TF1* m_DEta_vs_E_Func = 0;
    TGraphErrors* m_DEta_vs_E_SLGraph = 0;

    std::vector<TH1D*> m_respPerEbins;


    int m_currentEtaBin=0;
    AxisSpec * m_etaBins;

    // *********************************************
    // config parameters

    float m_minEtForMeasurement;
    float m_maxEtForFits;

    float m_minNEffP1;
    float m_minNEffP2;
    float m_minNEffP3;
    
    float m_NSigmaForFit;


    bool m_useBetterChi2;
    bool m_useGeoDetCorr;
    float m_minGeoDetCorr;
    float m_maxGeoDetCorr;
        

    std::vector<float> m_etaException;
    std::vector<float> m_JES_MaxOrderException;    

    float m_minNDataPointsForMinExtrapolation;
    bool  m_enableEMinExtrapolation;
    float m_minEtForExtrapolation;
    std::vector<bool> m_enableEMinExtrapolationException;
    bool m_enableEMaxExtrapolation;
    std::vector<bool> m_enableEMaxExtrapolationException;

    std::string m_JES_Function;
    float m_JES_MaxOrderP1;
    float m_JES_MaxOrderP2;
    float m_JES_MaxOrderP3;

    float m_maxAcceptableYDeviationFitVSMinGroom;

    float m_etaCorr_MaxOrderP1;
    float m_etaCorr_MaxOrderP2;
    float m_etaCorr_MaxOrderP3;


    JES_BalanceFitter *m_respFitter;

    std::string m_respHistTitle = "E_{reco}/E_{true}, %.1f < E_{true} < %.1f, etabin=%d";



    // Test : replace fit by mode calculation
    bool m_useMode = false;
    
  };

}

#endif
