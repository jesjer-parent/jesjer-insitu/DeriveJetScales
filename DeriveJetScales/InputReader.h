// this file is -*- C++ -*-
#ifndef DERIVEJETWSCALES_INPUTREADER_H
#define DERIVEJETWSCALES_INPUTREADER_H

#include <string>
#include <vector>
#include <iostream>

class TTree;
namespace BPS {

  class AxisSpec;
  class BinnedPhaseSpace;
  
}

namespace DJS {


  class JESCorrection;

  /////////////////////////////////////////////////////
  /// \class JetData
  /// 
  /// Simply the values needed for JES and JMS for 1 jet.
  /// The reason for this class is to make it easier to communicate
  /// the jet information amongst components of this package
  struct JetData {
    
    double e_true;
    double eta_true;
    double m_true;

    double e_reco;
    double eta_det;
    double eta_reco;
    double m_reco;

    int jes_ieta;
    bool jes_valid = false;
    bool jms_valid = false;

    double m_ta; // used only when testing mass combination
    double weight;
  };

  
  /////////////////////////////////////////////////////
  /// \class InputReader
  /// 
  /// Helper class which implements the loading of jet information (JetData) from
  /// the input TTree.
  /// Additionnaly it allows to apply the JES calibration so a JetData is ready to
  /// be used for the JMS derivation (see: readCalibJetData() )
  ///
  /// An advantage of having this separated class for the read operations is it will
  /// make easier to change the code to be able to read xAOD objects in the future.
  class InputReader
  {
  public:
    InputReader(){};
    virtual ~InputReader(){};

    virtual bool init(TTree * t) = 0;
    virtual bool initFileWeights(TTree *t);
    virtual bool initEventWeight(TTree *t);
    
    virtual int loadEntry(int i);
    virtual bool jetDataConsistency(){return true;}
    virtual size_t nJets()=0;

    /// read information for jet at index j in input branches and collect all of them in one JetData object
    virtual JetData readJetData(int j)=0;
    /// Same as readJetData() and also apply JES calibration on the JetData reco quantities.
    virtual JetData readCalibJetData(int j);


    typedef float evtweight_t;
    evtweight_t m_evntWeight;    
    std::vector<evtweight_t> m_fileWeights;

    typedef float mu_t;
    mu_t  m_mu;
    
    std::string m_evntWeight_bname = "new_EventWeight";
    std::string m_mu_bname = "mu";
    std::string m_e_true_bname = "E_true" ;
    std::string m_e_reco_bname = "E_corr1";
    std::string m_eta_reco_bname = "eta_corr1";
    std::string m_eta_det_bname = "";
    std::string m_eta_true_bname = "eta_true";
    std::string m_m_true_bname = "m_true";
    std::string m_m_reco_bname = "m_corr1"; 

    std::string m_m_ta_bname = ""; // used only when testing mass combination
    bool m_testMassComb=false;     // if true than m_reco is m_calo and m_ta is also loaded.
    bool m_preJESCalibrated = false; // Set to true if reading JES calibrated input : readCalibJetData() will then skip the JES calibration
    
    float m_energyUnit = 1; 
    
    // set to true if m_eta_det_bname != ""
    bool m_useEtaDet=false;

    // members expected to be set by the ResponseBuilder object
    TTree * m_tree =0 ;     
    BPS::AxisSpec * m_jesEtaBins=0;
    JESCorrection *m_jesCorr = 0;

    // 
  };

  
  /////////////////////////////////////////////////////
  /// \class InputReaderVector
  /// 
  /// InputReader specialized to read JetData from vector<float> branches in a TTree
  ///
  class InputReaderVector : public InputReader
  {
  public:
    InputReaderVector() : InputReader() {};
    virtual ~InputReaderVector(){};

    virtual bool init(TTree * t) ;

    virtual bool jetDataConsistency();
    virtual int loadEntry(int i);
    virtual size_t nJets();

    virtual JetData readJetData(int j);
    
    // **************************************
    /// kinematic variables to be read from ttrees
    typedef double brtype_t;
    //typedef float brtype_t;

    std::vector<brtype_t> *m_e_true=0;
    std::vector<brtype_t> *m_e_reco=0;
    std::vector<brtype_t> *m_eta_reco=0;
    std::vector<brtype_t> *m_eta_det=0;
    std::vector<brtype_t> *m_eta_true=0;
    std::vector<brtype_t> *m_m_true=0;
    std::vector<brtype_t> *m_m_reco=0;

    // necessary when dealing with xAOD. Variables in the "Aux." store needs different treatement than those in the "AuxDyn." store.
    //std::vector<brtype_t> m_e_reco_v; xAOD jet
    std::vector<brtype_t> m_eta_reco_v;
    std::vector<brtype_t> m_m_reco_v;
    bool m_xAODinput = false;

    
    std::vector<brtype_t> *m_m_ta=0; //used only when testing mass combination

  };

  class InputReaderVectorJetW : public InputReaderVector {
  public:
    typedef float jetweight_t;

    virtual bool initEventWeight(TTree *){m_evntWeight=1.;return true; }
    virtual bool init(TTree * t) ;
    virtual JetData readJetData(int j);

    std::vector<jetweight_t> *m_jet_weights=0;
    std::string m_jetWeight_bname = "new_JetWeight";
    
  };
  

  /////////////////////////////////////////////////////
  /// \class InputReaderVector
  /// 
  /// InputReader specialized to read JetData from simple float (or double) branches in a flat TTree (that is one entry per jet)
  ///
  class InputReaderFlatTree : public InputReader
  {
  public:
    InputReaderFlatTree() : InputReader() {};
    virtual ~InputReaderFlatTree(){};

    virtual bool init(TTree * t) ;

    //virtual int loadEntry(int i);
    virtual size_t nJets() {return 1;}

    virtual JetData readJetData(int j);
    //virtual JetData readCalibJetData(int j);


    typedef float brtype_t;
    //typedef float brtype_t;

    brtype_t m_e_true;
    brtype_t m_e_reco;
    brtype_t m_eta_reco;
    brtype_t m_eta_det;
    brtype_t m_eta_true;
    brtype_t m_m_true;
    brtype_t m_m_reco;

    brtype_t m_m_ta;
    
    // Optional branches specifiying the order of the jet in its event
    std::string m_jetind_bname;
    int m_jetind;
    
  };

}
#endif
