// this files is -*- C++ -*-
/*
  Copyright (C) 2002-2017 CERN for the benefit of the ATLAS collaboration

This file is imported from JetCalibTools.
It's used only for the 3D interpolation code

*/

class TH1;
class TAxis;

namespace IPH
{
  
  double Interpolate(const TH1* histo, const double x);
  
  double Interpolate(const TH1* histo, const double x, const double y);
  
  double Interpolate2D(const TH1* histo, const double x, const double y, const int xAxis, const int yAxis, const int otherDimBin);
  
  double Interpolate(const TH1* histo, const double x, const double y, const double z);

} 

