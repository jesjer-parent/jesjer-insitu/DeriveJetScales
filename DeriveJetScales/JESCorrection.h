// this file is -*- C++ -*-
#ifndef DERIVEJETWSCALES_JESCORRECTION_H
#define DERIVEJETWSCALES_JESCORRECTION_H

#include <vector>
class TF1;

namespace DJS {

  class JetData;

  /////////////////////////////////////////////////////
  /// \class JESCorrection
  /// 
  /// Helper class which implements the JES calibration
  ///
  /// 
  class JESCorrection
  {
  public:
    JESCorrection(){};
    virtual ~JESCorrection(){};

    double jesFactor(JetData &j);
    double jesFactor(double E, double eta_det, int ieta);

    double etaCorr(JetData &j);
    double etaCorr(int ieta_jes, double eCorrected, double eta_reco);


    std::vector<TF1*> m_jesFactorFunc;
    std::vector<TF1*> m_dEtaFunc;

    double m_minPtExtrapolated;

    double m_minPtETACorr;
    double m_maxEETACorr;
    
  };

}
#endif
