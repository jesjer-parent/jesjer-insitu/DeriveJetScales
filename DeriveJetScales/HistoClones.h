// this file is -*- C++ -*-
#ifndef DERIVEJETWSCALES_HISTOCLONES_H
#define DERIVEJETWSCALES_HISTOCLONES_H
///////////////////////////////////////////////
///
/// \class HistoClones
///
/// A compact container of identical histograms.
/// The intent of this class is to be able to store, save and load
/// sets of 10K to several 100K histograms in a compact and fast way.
/// 
/// On computers with limited memory/cpu the BinnedTH1 used in the default
/// implementation (which is essentially a vector<TH1f>) doesn't scale well
/// when reaching 100K entries. Hence the motivation for this specialized class.
///
/// Several manipulation convenience functions are written in python, see CppExtensions.py.
/// This functions are used in the JMS derivation scripts.
/// 
///////////////////////////////////////////////

#include "TObject.h"

#include "TArrayF.h"
#include "TArrayD.h"

#include "BinnedPhaseSpace/BinnedPhaseSpace.h"

class TH1F;

namespace DJS {

  class HistoClones : public TObject{
  public:

    HistoClones(const std::string& n="") : TObject() ,m_name(n){}
    ~HistoClones();

    /// init the container with nHistos and 1D histogram defined by a AxisSpec
    void init(int nHistos, BPS::AxisSpec &a);

    /// init the container with BinnedPhaseSpace (num histos == num bins in the phasespace) and 1D histogram defined by a AxisSpec
    void init(BPS::BinnedPhaseSpace * bps, BPS::AxisSpec &a);

    /// fill histo number i with an entry at position x and weight w
    void fillAt(int i, float x, float w);

    /// return true if x is within the range of the cloned histograms and set the corresponding bin index in binx
    bool histoBin(float x, int &binx);
    
    /// fill histo number i with an entry at histo bin binx and weight w
    void fillAtBin(int i, int binx, double w);

    /// Returns a "view" of histo number i as a TH1F.
    /// WARNING : this is rather a hack, not all TH1F methods may work as expected.
    TH1F * histoAtIndex(int i);

    
    std::string name()const {return m_name;}
    void setName(const std::string& n) {m_name=n;}


    void normalizeHistos();
    
    // *******
    // for debugging
    void inspect(bool showVoid=true);

    void releaseBPS(){m_phaseSpace=0;}

    
    TArrayF m_content;
    TArrayD m_sumw2;
    TArrayF m_sumwPerHisto;

    /// This defines the axis of the cloned histograms
    BPS::AxisSpec m_histoAxis;
    /// Optionnaly we can attach a BinnedPhaseSpace
    BPS::BinnedPhaseSpace * m_phaseSpace = nullptr;

    TH1F * m_hModel=nullptr; //!

    std::string m_name;
    int m_nHistos = 0 ;
    
    ClassDef(HistoClones, 1);

  };


    
  
  
}

#endif
