// this file is -*- C++ -*-
#ifndef DERIVEJETWSCALES_FINEBINRESPBUILDER_H
#define DERIVEJETWSCALES_FINEBINRESPBUILDER_H

#include "DeriveJetScales/ResponseBuilder.h"
#include "DeriveJetScales/WeightedBinFinder.h"

namespace DJS {

  
  
  /////////////////////////////////////////////////////
  /// \class FineBinRespBuilder
  /// Extend ResponseBuilder to deal with very fine bin JMS derivation.
  /// Essentially, the JMS derivation functions are redefined to fill some HistoClones containers
  /// rather than BinnedTH1 containers.
  class FineBinRespBuilder : public ResponseBuilder {
  public:


    // JMS steps --------------
    bool jmsRvsEtrue(int j);
    bool jmsRvsNuminv(int j);
    bool jmsClosure(int j);    

    bool jmsClosureMassComb(int i);

    /// Helper object which find the bins overlapping with a given point in phase space 
    JMSWBinning *m_jmsWBins;

    float m_closuremassCut=0;
  };
}

#endif
