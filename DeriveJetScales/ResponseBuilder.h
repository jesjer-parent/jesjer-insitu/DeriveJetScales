// this file is -*- C++ -*-
#ifndef DERIVEJETWSCALES_RESPONSEBUILDER_H
#define DERIVEJETWSCALES_RESPONSEBUILDER_H

#include "BinnedPhaseSpace/BinnedPhaseSpace.h"

#include "DeriveJetScales/Utilities.h"
#include "DeriveJetScales/JMSBinning.h"
#include "DeriveJetScales/JESCorrection.h"
#include "DeriveJetScales/InputReader.h"

class TTree;
class TH2F;
class TProfile;

namespace DJS {

  class JMSCombination;

  
  
  ///////////////////////////////////////
  /// \class ResponseBuilder 
  ///
  /// This class performs the E or mass response calculations by looping 
  /// over events in ntuples.
  /// It implements different modes (see ResponseBuilder::Mode enum) each in a different event processing function.
  /// Each mode correspond to one step of the EtaJES or JMS derivation
  ///
  /// This class is intended to be configured and run from python modules & scripts.
  class ResponseBuilder {
  public:

    enum Mode {
      
      EtaJES_RvsEtrue = 0,
      EtaJES_RvsEnuminv,
      EtaJES_Closure,

      EtaJES_Closure_JMS_RvsEtrue,

      JMS_RvsEtrue,
      JMS_RvsNuminv,
      JMS_Closure,

      JMS_ClosureMassComb,
    };
    
    virtual ~ResponseBuilder(){}
    virtual bool processTree(TTree * t);
    virtual bool processEvent();

    // EtaJes steps --------------
    virtual bool etajesRvsEtrue(int i);
    virtual bool etajesRvsEnuminv(int i);
    virtual bool etajesClosure(int i);


    // JMS steps --------------
    virtual bool jmsRvsEtrue(int i);
    virtual bool jmsRvsNuminv(int i);
    virtual bool jmsClosure(int i);

    virtual bool jmsClosureMassComb(int ){return true;}

    // for test/validation  -----------------
    //  left to users to implement it as they wish.
    virtual bool validation(int ) {return true;}



    // **************************************
    // Global config

    CalibrationObjects *m_objects = nullptr; /// centralizes all objects needed for calibration derivation
    Mode m_mode; /// corresponds to the calibration derivation step to be run

    size_t m_numJetsMax=5;
    
    int m_maxEvents=-1;
    int m_firstEvent=0;

    InputReader *m_inputReader=0;


    // **************************************
    // EtaJES config
    JESCorrection m_jesCorr;
    
    AxisSpec * m_etaBins;
    double m_minPtExtrapolated;
    double m_maxEETACorr;
    double m_minPtETACorr;
    // double getCorrFactor(double E, double eta_det, int ieta);
    // double getEtaCorr(int ieta_jet, double eCorrected, double eta_reco);


    // **************************************
    // JMS config
    BPS::BinnedPhaseSpace * m_jmsBPS;    /// Describes the bins in which mass response are calculated (ex : (pt,m,|eta|)


    /// This part governs what variables we use for JMS
    /// (pt,m) or (log(m/pt),E) ...
    /// These helpers are also in charge of retrieving JMS corrections factors according to the bins.
    JMSBinning* m_jmsTrueBin;
    JMSBinning* m_jmsRecoBin;

    JMSCombination* m_jmsCombination = nullptr; // used only when testing mass  combination

    

    bool m_noMassNumInv= false;



    // ******************
    // debug purposes
    int m_currentEntry=-1;
  };



}

#endif
