// this file is -*- C++ -*-
#ifndef DERIVEJETWSCALES_JMSBinning_H
#define DERIVEJETWSCALES_JMSBinning_H

#include "BinnedPhaseSpace/BinnedPhaseSpace.h"
#include "BinnedPhaseSpace/BinnedHistos.h"

class TH3F;

namespace DJS {

  /////////////////////////////////////////////////////
  /// \class JMSBinning
  /// Conversion between a jet (e,m,eta) into the parameters used to bin the derivation of JMS
  ///
  /// JMSBinning serves as a converter between a jet and 2 coordinates used to bin the derivation of JMS.
  /// JMSBinning is a virtual base class : derived class implement each a coordinate system (ex: PtMBinning)
  /// JMSBinning is initialized from a jet e, m , eta by calling JMSBinning::initJet(e, m, eta). Once done,  m_var1 and m_var2
  /// represent the coordinates in the binning system, m_ieta the index of the bin in eta and m_bin the index in the total 3D bin
  /// space (relevant to a BPS::BinnedPhaseSpace).
  ///
  /// JMSBinning can also be used to retrieve :
  ///   * the response/calibration factors
  ///   * the resolution 
  /// associated to the bin it represents.
  /// This works if m_correctionFactors3d and/or m_sigmas3d has been set to a valid TH3F pointer.
  /// 
  ///
  struct JMSBinning {

    JMSBinning(BPS::BinnedPhaseSpace *bps=0): m_bps(bps){}
    virtual ~JMSBinning(){}
    virtual bool initJet(double e, double m, double eta) = 0;

    inline void initBins(double eta){
      m_ieta = m_bps->axis(2)->findBin(std::fabs(eta));
      m_bin = m_bps->findBin(m_var1, m_var2, std::fabs(eta));
    }
    inline bool validEtaBin() {return m_bps->axis(2)->validBin(m_ieta);}
    inline bool validBin() {return (m_bin != -1);}
    
    /// Set-up the tool with responses stored in a TH3F 
    void initCorrections(TH3F* h3=nullptr);
    /// Set-up the tool with responses and resolutions stored in a TH3F 
    void initSignificance(TH3F* h3_1=nullptr, TH3F* h3_2=nullptr);

    /// returns the response for the current JMS bin, interpolated from the content of the TH3F histo m_correctionFactors3d
    virtual double jmsResponse();
    virtual double jmsResponse(double e, double m, double eta){ initJet(e,m,eta); return jmsResponse();}

    /// return the response for the current JMS bin from the content of the TH3F histo m_correctionFactors3d. Response is interpolated in the (e,m) plan but NOT in eta.
    virtual double jmsResponse_noEtaInt();
    virtual double jmsResponse_noEtaInt(double e, double m, double eta){ initJet(e,m,eta); return jmsResponse_noEtaInt();}

    /// return the response for the current JMS bin from the content of the TH3F histo m_correctionFactors3d. No interpolation : this is just the response in the bin containing (e,m,eta)
    virtual double jmsResponse_noInt();
    virtual double jmsResponse_noInt(double e, double m, double eta){ initJet(e,m,eta); return jmsResponse_noInt();}

    
    virtual double jmsCorrection() { return 1./jmsResponse();}
    virtual double jmsCorrection(double e, double m, double eta){ initJet(e,m,eta); return jmsCorrection();}
    virtual double jmsCorrection_noEtaInt(){return 1./jmsResponse_noEtaInt();}
    virtual double jmsCorrection_noEtaInt(double e, double m, double eta){ initJet(e,m,eta); return jmsCorrection_noEtaInt();}

    /// returns the resolution (as stored in m_sigmas3d) at current point 
    virtual double jmsSigma();
    /// returns the resolution (as stored in m_sigmas3d) at current point (e,m,eta)
    virtual double jmsSigma(double e, double m, double eta){ initJet(e,m,eta); return jmsSigma();}

    
    /// returns the significance ( (1-response)/resolution ) at current point 
    virtual double jmsSignificance();
    virtual double jmsSignificance(double e, double m, double eta){ initJet(e,m,eta); return jmsSignificance();}

    void setBinnedPhaseSpace(BPS::BinnedPhaseSpace * bps) {m_bps=bps;}

    /// utility : fill histo h2 with calibration factors at eta
    void fillRespTH2(TH2 & h2, float eta);

    
    double m_var1;
    double m_var2;
    double m_eta;
    int m_ieta;
    int m_bin;
    
    BPS::BinnedPhaseSpace *m_bps = nullptr;

    TH3F* m_correctionFactors3d = nullptr;
    TH3F* m_sigmas3d = nullptr;

    double m_xmin,m_xmax;
    double m_ymin,m_ymax;
    double m_zmin,m_zmax;
    
  };

  struct PtMBinning : public JMSBinning {
    PtMBinning(BPS::BinnedPhaseSpace *bps=0) : JMSBinning(bps){}
    virtual bool initJet(double e, double m, double eta) ;    
  };

  struct EMBinning : public JMSBinning {
    EMBinning(BPS::BinnedPhaseSpace *bps=0) : JMSBinning(bps){}
    virtual bool initJet(double e, double m, double eta) ;    
  };
  
  struct ELogMovEBinning : public JMSBinning {
    ELogMovEBinning(BPS::BinnedPhaseSpace *bps=0) : JMSBinning(bps){}
    virtual bool initJet(double e, double m, double eta);
  };

  struct MLogMovEBinning : public JMSBinning {
    MLogMovEBinning(BPS::BinnedPhaseSpace *bps=0) : JMSBinning(bps){}
    virtual bool initJet(double e, double m, double eta);
  };
  
  struct EtLogMovEtBinning : public JMSBinning {
    EtLogMovEtBinning(BPS::BinnedPhaseSpace *bps=0) : JMSBinning(bps){}
    virtual bool initJet(double e, double m, double eta);
  };


}

#endif
