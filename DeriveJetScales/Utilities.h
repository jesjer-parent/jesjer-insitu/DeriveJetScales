// this file is -*- C++ -*-
#ifndef DERIVEJETWSCALES_UTILITIES_H
#define DERIVEJETWSCALES_UTILITIES_H

#include "TNamed.h"
#include "BinnedPhaseSpace/BinnedHistos.h"
#include "TMath.h"

#include "DeriveJetScales/HistoClones.h"

class TH2F;
class TH1F;
class TProfile;
class TF1;
class TGraphErrors;
class TGraph2DErrors;

namespace DJS {

  typedef BPS::AxisSpec AxisSpec;


  /// Calculate the median, IQR of an histo. Also evaluates the errors on them
  // arr contains the return values : arr[0] = med, arr[1]=med_err, arr[2]=iqr, arr[3]=iqr_err
  void calcMedIQR(TH1& h, float arr[4]);
  
  /// Calculate the mode of h in a smooth way. See cxx comments for more details.
  void uniModalSmoothMax(TH1& h, float arr[4] , float outlierF=0.5, int startBin=1);


  /// return number of non empty bins (or having content <= threshold)
  int numNonEmptyBins(TH1& h, float threshold=0);
  
  TGraph2DErrors * numInvYaxis(TH2F& h);
  TGraph2DErrors * numInvYaxis(TGraph2DErrors& g);

  /// fill the empty bins of input TH2 'h'. Empty bins value are set to the average of closest non empty bin.
  ///   the method recursively calls itself until no empty bins remains.
  void fillEmptyBin(TH2& h, bool reverse = false);
  
  /// fill Histogram h_out from a slice of TH2 h2 along dimention 'preservedDim' at position 'sliceIndex' on the other dim
  ///  (for example if preservedDim==0 and sliceIndex=3, this is equivalent to h2.ProjectionX("xx",3,3))
  void sliceTH2(TH2& h2, TH1& h_out, int preservedDim, int sliceIndex);
  
  /// *******************************************
  /// simple structures to accumulate statistics and save them in a ROOT file
  ///  (this is currently DEPRECATED)
  struct Stats  {
    void add( double pt, double m, double w){sumw +=w; sumwm+=m*w; sumwpt+=pt*w ;}
    double massMean() const {return sumw == 0 ? 0 : sumwm/sumw;}
    double ptMean() const {return sumw == 0 ? 0 : sumwpt/sumw;}
    double sumw     = 0;
    double sumwm  = 0;
    double sumwpt = 0;
  };

  struct VecStats : public TNamed {
    VecStats():TNamed(){}
    VecStats(const std::string &n, int N=0) : TNamed(n.c_str(),"" ), m_v(N){}
    void resize(int i) {m_v.resize(i);}
    size_t size() const {return m_v.size();}
    Stats & operator[](int i){return m_v[i];}

    std::vector<Stats> m_v;
    ClassDef(VecStats, 1);
  };
  /// *******************************************


  
  /// Central object to collect all objects (TH2, TH1, TF1,...)
  /// needed to derive calibration.
  /// This central object helps to transfer the TObjects from
  /// a C++ component to another and from/to the python scripts.
  struct CalibrationObjects {

    // ***************************************
    // EtaJES objects 
    /// Each entry in the vectors correspond to 1 eta bin 

    // Histos build from ntuples
    std::vector<TH2F*> R_vs_Etrue;
    std::vector<TH2F*> R_vs_Ereco;
    std::vector<TH2F*> R_vs_pTtrue; // only to check Response closure against pt
    std::vector<TH2F*> R_vs_mu;
    std::vector<TProfile*> Etrue_vs_Etrue;
    std::vector<TProfile*> Ereco_vs_Ereco;
    std::vector<TProfile*> pTtrue_vs_pTtrue; //
    std::vector<TProfile*> Ereco_vs_Etrue;
    std::vector<TH2F*> DEta_vs_Etrue;


    std::vector<TH2F*> R_vs_Enuminv;
    std::vector<TProfile*> Enuminv_vs_Etrue;

    std::vector<TH2F*> closure_R_vs_Etrue;
    std::vector<TH2F*> closure_R_vs_pTtrue;
    std::vector<TH2F*> closure_DEta_vs_Etrue ;


    // Fit objects
    
    std::vector<TGraphErrors*> R_vs_Etrue_Graph ;
    std::vector<TGraphErrors*> R_vs_Ereco_Graph;
    std::vector<TGraphErrors*> R_vs_mu_Graph;
    std::vector<TF1*> R_vs_Etrue_Func;
    std::vector<TF1*> R_vs_Ereco_Func;
    std::vector<TF1*> R_vs_Etrue_GroomExt_Func;
    std::vector<TGraphErrors*> R_vs_Etrue_SLGraph ;
    std::vector<TGraphErrors*> R_vs_Ereco_SLGraph;
    std::vector<TGraphErrors*> R_vs_pTtrue_Graph ;

    
    std::vector<TGraphErrors*> R_vs_Enuminv_Graph ;
    std::vector<TF1*> R_vs_Enuminv_Func;
    std::vector<TF1*> R_vs_Enuminv_GroomExt_Func;
    std::vector<TGraphErrors*> R_vs_Enuminv_SLGraph ;

    std::vector<TGraphErrors*> closure_R_vs_Etrue_Graph ;
    std::vector<TGraphErrors*> closure_R_vs_pTtrue_Graph ;


    std::vector<TGraphErrors*> DEta_vs_Etrue_Graph ;
    std::vector<TF1*> DEta_vs_Etrue_Func;
    std::vector<TGraphErrors*> closure_DEta_vs_Etrue_Graph ;

    // just to workaround bugs in pyROOT : https://sft.its.cern.ch/jira/browse/ROOT-8809
    inline static void push_back(std::vector<TF1*> &v, TF1* f){v.push_back(f);}
    inline static void push_back(std::vector<TGraphErrors*> &v, TGraphErrors* f){v.push_back(f);}
    inline static void push_back(std::vector<TH2F*> &v, TH2F* f){v.push_back(f);}
    inline static void push_back(std::vector<TProfile*> &v, TProfile* f){v.push_back(f);}


    // ***************************************
    // JMS objects
    // container for raw mass responses
    BPS::BinnedTH1 mR_in_PtEtaMtrue;
    VecStats m_in_PtEtaMtrue;


    // container for numinv mass responses
    BPS::BinnedTH1 mR_in_PtEtaMnuminv;
    VecStats m_in_PtEtaMnuminv;

    // Response maps to be used in numerical inversion
    BPS::BinnedTH2 mRsmooth_vs_PtMtrue_in_Eta;
    // Response maps to be used in as final calib factor source
    BPS::BinnedTH2 mRsmooth_vs_PtMnuminv_in_Eta;

    // container for closure responses
    BPS::BinnedTH1 closure_mR_in_PtEtaMtrue;

    // these 2 containers serve as a final comparison of calibrations in bins inclusive in mass.
    BPS::BinnedTH1 closure_mR_in_inclPtEtatrue;
    BPS::BinnedTH1 mR_in_inclPtEtatrue;


    BPS::BinnedTGraph2DErrors numinv_mRsmooth_vs_PtMtrue_in_Eta;


    // ***************************************
    // Very fine bin containers 
    DJS::HistoClones mR_in_EMEtatrue;
    DJS::HistoClones mR_in_EMEtanuminv;
    DJS::HistoClones closure_mR_in_EMEtatrue;

    DJS::HistoClones mR_in_EMEtareco; // used for calibrated response to be used in IQR calculations

    
    TH2F* occ_in_EMEtatrue;
    

    // -----------------------------
    // Validation objects

    TH1F* v_pt;
    TH1F* v_m;
    TH1F* v_eta;
    TH1F* v_logmopt;
    TH2F* v_logmopt_vs_e;
    TH2F* v_m_vs_pt;

    TH2F* v_mR_vs_logmopt;
    TH2F* v_mR_vs_logmopt_reco;
    
  };

  //Easier for color-blind and greyscale output. Similar to Bird color pallette, but inverted so the light color is the low value
  void SetInvertedBird();
}

#endif
