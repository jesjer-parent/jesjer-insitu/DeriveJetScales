//  emacs, this file is -*- C++ -*-
#ifndef DERIVEJES_KERNELSMOOTHING_H
#define DERIVEJES_KERNELSMOOTHING_H
///////////////////////////////////////////////////////
///
/// This file defines utilities to perform kernel density smoothing of 
/// functions of arbitrary dimension, optionnaly taking errors into account.
///
/// Smoothing work by implementing this formula :
///   smoothedF(X_0) = \sum( K(X_0,X) F(X) ) / \sum( K(X_0,X) )
/// 
/// where K is the kernel and is a function of ||X_0 - X||. Typically is a gaussian of ||X_0-X||.
///
/// The smoothing calculations are performed by internally scaling all dimensions within [0,1].
/// That is, on each dimension i we perform x_i -> (x_i - x_i_min) /(x_i_max-x_i_min) 
/// 
/// The system supports "stretch factors" for each dimension to allow to treat distances differently
/// on each dimension.
/// The expected way to set these factors is from a Smoother object (see below), by calling Smoother<N>::setStretchFactor(i, f_i);
/// Then distances are calculated as D(X,Y) = \sqrt( \sum (  (x_i-y_i)^2 / f_i^2 ) )
/// So in the case of gaussian kernel, setting factors f_i is equivalent to set dimension dependent
/// widths  w_i = f_i x w (where w is the width of the gaussian fuction)
/// 
/// Several classes are defined to help with this task. They are all
/// templated with an int parameter which represent the dimension.
///   - Coord : represents Coordinate system in N dimension, together with usual vector
///             operations
///   - Point : groups a Coord with 2 double members. This represents the value and error taken by a 
///             function at a given point in space.
///   - PointContainer : a vector<Point>. Thus represents a full function.
///   - TObjectHelpers : specialized class to link objects templated on N to the corresponding non templated
///                   ROOT classes (when N=1, ROOT classes are TH1F and TGraphErrors. When N=2 ROOT classes are TH2F and TGraph2DErrors)
///   - Smoother : the class implementing the smoothing operations, making use of all the above.
///
///////////////////////////////////////////////////////

#include "TGraphErrors.h"
#include "TGraph2DErrors.h"
#include "TH1F.h"
#include "TMath.h"
#include "TF1.h"
#include "TH2F.h"
#include <vector>
#include <algorithm>
#include <iostream>


namespace KS {

  template<int N>
  struct TObjectHelpers { };


  ///////////////////////////////////////////////
  /// \class CoordND : coordinates in N dimension.
  ///
  /// Practical usage is to always use CoordND<N,N>.
  /// 
  /// The second parameter, S, is here for technical reasons and to allow to 
  ///  recursively define a N-dimension coordinate (thus using N values) based on 
  ///  the N-1 class (but still using N coordinates, so Coord<N-1,N>).
  template<int N, int S=N>
  struct CoordND : public CoordND<N-1,S> {
    typedef  CoordND<N-1, S> base_t;  
  
    CoordND() : base_t(){}
    CoordND(const double xx) {for(int i=0;i<N;i++) this->x[i]=xx;}
    CoordND(const double xx[]) : base_t(xx){this->x[N-1]=xx[N-1];}
    CoordND(const base_t& o) : base_t(o.x){}
    CoordND(const base_t& o, double xx) : base_t(o.x){this->x[N-1]=xx;}

    double dot(const CoordND& c) const {return base_t::dot(c)+this->x[N-1]*c.x[N-1]*CoordND::stretchFactor[N-1];}

    double norm2() const { return dot(*this);};
    double d2(const CoordND& c){return (*this-c).norm2();}
    double d(const CoordND& c){return TMath::Sqrt(d2(c));}

    CoordND minCoord(CoordND &o) const { return CoordND( base_t(*this).minCoord( o ) , o.x[N-1] < this->x[N-1] ? o.x[N-1] : this->x[N-1]) ; }
    CoordND maxCoord(CoordND &o) const { return CoordND( base_t(*this).maxCoord( o ) , o.x[N-1] > this->x[N-1] ? o.x[N-1] : this->x[N-1]) ; }

    CoordND& operator+=(const CoordND &o)  {base_t::operator+=(o);this->x[N-1] += o.x[N-1]; return *this;}
    CoordND& operator-=(const CoordND &o)  {base_t::operator-=(o);this->x[N-1] -= o.x[N-1]; return *this;}
    CoordND& operator*=(const CoordND &o)  {base_t::operator*=(o);this->x[N-1] *= o.x[N-1]; return *this;}
    CoordND& operator/=(const CoordND &o)  {base_t::operator/=(o);this->x[N-1] /= o.x[N-1]; return *this;}

    CoordND operator+(const CoordND &o) const  {CoordND c(*this); c+=o; return c;}
    CoordND operator-(const CoordND &o) const  {CoordND c(*this); c-=o; return c;}
    CoordND operator*(const CoordND &o) const  {CoordND c(*this); c*=o; return c;}
    CoordND operator/(const CoordND &o) const  {CoordND c(*this); c/=o; return c;}

    float operator[](int i) const {return this->x[i];}

    void setLog(bool log[]) { base_t::setLog(log); if(log[N-1]) this->x[N-1] = TMath::Log(this->x[N-1]) ;}
    void unsetLog(bool log[]) { base_t::unsetLog(log); if(log[N-1]) this->x[N-1] = TMath::Exp(this->x[N-1]) ;}

    CoordND logCoord(bool log[]){ CoordND c(*this); c.setLog(log); return c;}
    CoordND unlogCoord(bool log[]){ CoordND c(*this); c.unsetLog(log); return c;}

    
    static CoordND maximum() {return CoordND( base_t::maximum(), 1.0e40) ;}
    static CoordND minimum() {return CoordND( base_t::minimum(), -1.0e40) ;}

    void setStretchFactorPtr(const double *factors){CoordND::stretchFactor=factors;}

    static void initStretchFactor(){for(int i=0;i<N;i++)CoordND::s_defaultStretchFactor[i]=1.;}

    void addToStream(std::ostream& os) { base_t::addToStream(os); os << ", "<< this->x[N-1] ;}
    //friend std::ostream& operator<< <CoordND<N,S> > (std::ostream& os, CoordND & c); 
  };



  
  ///////////////////////////////////////////////
  /// \class CoordND<1,S> : coordinates in 1 dimension.
  ///
  /// The explicit specialization for N=1 is needed in order to bootstrap the 
  /// recursive definition in the generic case.
  template<int S>
  struct CoordND<1,S> {

    /// The coordinates
    double x[S];
    
    /// this defines how to stretch each dimension in the distance calculations.
    /// See discusion above.
    static double s_defaultStretchFactor[S]; // the default values. set to 1.
    const double *stretchFactor = CoordND::s_defaultStretchFactor; // the factors used by this CoordND. Be sure to always operate on coordinates pointing to the same factors.


    CoordND() {};
    CoordND(const double xx[])  {x[0]=xx[0];}
    CoordND(double xx)  {x[0]=xx;}

    double dot(const CoordND& c) const {return x[0]*c.x[0]*stretchFactor[0];}

    CoordND& operator+=(const CoordND &o)  {x[0] += o.x[0]; return *this;}
    CoordND& operator-=(const CoordND &o)  {x[0] -= o.x[0]; return *this;}
    CoordND& operator*=(const CoordND &o)  {x[0] *= o.x[0]; return *this;}
    CoordND& operator/=(const CoordND &o)  {x[0] /= o.x[0]; return *this;}

    double norm2() const { return dot(*this);};
    double d2(const CoordND& c){return (*this-c).norm2();}
    double d(const CoordND& c){return TMath::Sqrt(d2(c));}

    CoordND operator+(const CoordND &o) const  {CoordND c(*this) ;c+=o; return c;}
    CoordND operator-(const CoordND &o) const  {CoordND c(*this) ;c-=o; return c;}
    CoordND operator*(const CoordND &o) const  {CoordND c(*this) ;c*=o; return c;}
    CoordND operator/(const CoordND &o) const  {CoordND c(*this) ;c/=o; return c;}

    CoordND minCoord(CoordND &o) const { return CoordND(o.x[0] < x[0] ? o.x[0] : x[0]); }
    CoordND maxCoord(CoordND &o) const { return CoordND(o.x[0] > x[0] ? o.x[0] : x[0]); }
  
    float operator[](int ) const {return x[0];}

    void setLog(bool log[]) { setLog(log[0]);}
    void unsetLog(bool log[]) { unsetLog(log[0]);}

    void setLog(bool log) { if(log) x[0] = TMath::Log(x[0]) ;}
    void unsetLog(bool log) { if(log) x[0] = TMath::Exp(x[0]) ;}

    CoordND logCoord(bool log){ CoordND c(*this); c.setLog(log); return c;}
    CoordND unlogCoord(bool log){ CoordND c(*this); c.unsetLog(log); return c;}
    CoordND logCoord(bool log[]){ CoordND c(*this); c.setLog(log[0]); return c;}
    CoordND unlogCoord(bool log[]){ CoordND c(*this); c.unsetLog(log[0]); return c;}

    void setStretchFactorPtr(const double *factors){stretchFactor=factors;}

    static CoordND maximum() {return CoordND( 1.0e40) ;}
    static CoordND minimum() {return CoordND(-1.0e40) ;}

    static void initStretchFactor(){for(int i=0;i<S;i++)s_defaultStretchFactor[i]=1.;}

    void addToStream(std::ostream& os) { os << x[0] ;}
    //friend  std::ostream& operator<< <CoordND<1,S> > (std::ostream& os, CoordND<1,S> & c); 
    
  };


  template<int N, int S>
  std::ostream& operator<<(std::ostream& os, CoordND<N,S> & c){ os<< "("; c.addToStream(os); os << ")"; return os;}

  // template<int S>
  // std::ostream& operator<<(std::ostream& os, CoordND<1,S> & c){ c.addToStream(os); os << ")";return os;}

  
  typedef CoordND<1,1> Coord1D;

  template<int S>
  double CoordND<1,S>::s_defaultStretchFactor[S]; //

  void initStretchFactor();

  ////////////////////////////////////////////////////////
  /// \class Point represents the value of a N dim function at one point in space
  ///
  /// consists of a Coord<N>, a value and an error
  template<int N>
  struct Point {
    typedef CoordND<N,N> coord_t;

    static const int nDirectN = 2;
    typedef float val_t;
    Point(): coord(){};
    Point(coord_t  c, val_t v, val_t e=0): coord(c), value(v), error(e){}

    coord_t coord;
  
    val_t value=0., error=0.;
  
    double d2(const Point &p){ return coord.d2(p.coord); }
    double d(const Point &p){ return coord.d(p.coord); }

    void show()  {std::cout << coord << " v="<< value <<std::endl; }
  };

  template<int N>
  std::ostream& operator<<(std::ostream& os, Point<N> & p){ os<< "["<<p.coord <<", v="<<p.value << "err="<<p.error<<"]"; ; return os;}

  
  template<int N>
  struct ClosestToPoint {
    Point<N> p0;
    bool operator()(const Point<N>& p1, const Point<N>& p2) { return p0.d2(p1)<p0.d2(p2) ;}
  };


  ////////////////////////////////////////////////////////
  /// \class TObjectHelpers relates templated N dim classes to ROOT
  ///    - defines typedef relevant ROOT types in dim N 
  ///    - defines helpers function to convert from/to ROOT classes
  ///

  /// \class TObjectHelpers<1> specialized for 1 dimension
  template<>
  struct TObjectHelpers<1> { 
    typedef TGraphErrors graph_t;
    typedef TH1F histF_t;
    typedef TH1 hist_t;
    typedef  Point<1> point_t;
    typedef typename Point<1>::coord_t coord_t;
  
    static histF_t * emptyHistoFromGraph( graph_t& g, int npoints, CoordND<1> min, CoordND<1> max ){
      return  new TH1F(Form("%s_smooth",g.GetName()), g.GetTitle(), npoints, min[0], max[0] );
    }

    static histF_t * emptyHistoFromGraph( graph_t& g, int npoints[1], CoordND<1> min, CoordND<1> max ){
      return  emptyHistoFromGraph( g, npoints[0], min, max);
    }

    static histF_t * emptyHistoFromTH( hist_t& h, int npoints[1] ){
      TAxis * ax=h.GetXaxis();
      return  new histF_t(Form("%s_smooth",h.GetName()), h.GetTitle(), npoints[0], ax->GetXmin(), ax->GetXmax() );
    }

    static graph_t * emptyGraphFromGraph( graph_t& g, int npoints[1], coord_t min, coord_t max ){
      /// Build and return a new empty graph on a regular grid
      int N = npoints[0];
      graph_t * gn = new graph_t(N);
      gn->SetName(Form("%s_smooth", g.GetName()) );
      double step = (max[0]-min[0])/N;
      for(int i=0;i<N;i++) gn->SetPoint(i,min[0]+step*(i+0.5) , 0);
      return gn;
    }


    static std::vector<point_t > binCenters(hist_t& h){
      int npoints = h.GetXaxis()->GetNbins()-2;
      std::vector<point_t > vec(npoints);
      for(int i=0; i<npoints; i++){
        vec[i] = point_t( {h.GetBinCenter(i+1)} , i+1, 0) ;
      }
      return vec;
    }

    static point_t fromTGraph(graph_t &g, int i, bool logCoord[]){
      coord_t c {g.GetX()[i]};
      return point_t( c.logCoord(logCoord[0]) , g.GetY()[i], g.GetErrorY(i) );      
    }

    static point_t fromTH(hist_t &g, int i, bool logCoord[]){
      coord_t c {g.GetXaxis()->GetBinCenter(i)};
      return point_t( c.logCoord(logCoord[0]) , g.GetBinContent(i), g.GetBinError(i) );      
    }

    static size_t histoSize(hist_t &h){ return h.GetNbinsX();}
    static double histoErrorAt(hist_t* h, coord_t c){ return h->GetBinError(h->FindBin(c[0])); }

    static double* graphValues(graph_t &g){ return g.GetY();}
    static double* graphErrors(graph_t &g){ return g.GetEY();}
    static coord_t graphToCoord(graph_t &g, int i){return coord_t {g.GetX()[i]};}

  };


  /// \class TObjectHelpers<2> specialized for 2 dimensions
  template<>
  struct TObjectHelpers<2> { 
    typedef TGraph2DErrors graph_t;
    typedef TH2F histF_t;
    typedef TH2 hist_t;
    typedef  Point<2> point_t;
    typedef typename Point<2>::coord_t coord_t;
  
    static histF_t * emptyHistoFromGraph( graph_t& g, int npoints, coord_t min, coord_t max ){
      int n[2] = {npoints, npoints};
      return  emptyHistoFromGraph(g, n, min,max );
    }

    static histF_t * emptyHistoFromGraph( graph_t& g, int npoints[2], coord_t min, coord_t max ){
      return  new histF_t(Form("%s_smooth",g.GetName()), g.GetTitle(), npoints[0], min[0], max[0], npoints[1], min[1], max[1] );
    }

    static graph_t * emptyGraphFromGraph( graph_t& g, int npoints[2], coord_t min, coord_t max ){
      /// Build and return a new empty graph on a regular grid
      int N = npoints[0]*npoints[1];
      graph_t * gn = new graph_t(N);
      gn->SetName(Form("%s_smooth", g.GetName()));
      double steps[2]= { (max[0]-min[0])/npoints[0] , (max[1]-min[1])/npoints[1] };
      int p=0;
      for(int i=0;i<npoints[0];i++) 
        for(int j=0;j<npoints[1];j++) 
          {gn->SetPoint(p,min[0]+steps[0]*(i+0.5) , min[1]+steps[1]*(j+0.5) , 0 ); p++ ;}
      return gn;
    }

    static histF_t * emptyHistoFromTH( hist_t& h, int npoints[2] ){
      TAxis * ax=h.GetXaxis();
      TAxis * ay=h.GetYaxis();
      return  new histF_t(Form("%s_smooth",h.GetName()), h.GetTitle(), npoints[0], ax->GetXmin(), ax->GetXmax() , npoints[1], ay->GetXmin(), ay->GetXmax());
    }


    static std::vector<point_t > binCenters(hist_t& h){
      int npointsX = h.GetXaxis()->GetNbins();
      int npointsY = h.GetYaxis()->GetNbins();

      TAxis * ax = h.GetXaxis();
      TAxis * ay = h.GetYaxis();

      std::vector<point_t > vec(npointsX*npointsY);
      int c= 0;
      for(int i=0; i<npointsX; i++){
        for(int j=0; j<npointsY; j++){
          vec[c] = point_t( coord_t({ax->GetBinCenter(i+1),ay->GetBinCenter(j+1)}) , h.GetBin(i+1,j+1) , 0) ;
          c++;
        }
      }
      return vec;
    }
    static point_t fromTGraph(graph_t &g, int i, bool logCoord[]){
      coord_t c {g.GetX()[i], g.GetY()[i]};
      return point_t( c.logCoord(logCoord) , g.GetZ()[i], g.GetErrorZ(i) );      
    }

    static point_t fromTH(hist_t &g, int i, bool logCoord[]){
      int x,y,z; g.GetBinXYZ(i, x,y,z);
      coord_t c {g.GetXaxis()->GetBinCenter(x), g.GetYaxis()->GetBinCenter(y)};
      return point_t( c.logCoord(logCoord) , g.GetBinContent(i), g.GetBinError(i) );      
    }

    static size_t histoSize(hist_t &h){ return h.GetNbinsX()*h.GetNbinsY();}
    static double histoErrorAt(hist_t* h, coord_t & c){ return h->GetBinError(h->FindBin(c[0], c[1])); }


    static double* graphValues(graph_t &g){ return g.GetZ();}
    static double* graphErrors(graph_t &g){ return g.GetEZ();}
    static coord_t graphToCoord(graph_t &g, int i){return coord_t {g.GetX()[i], g.GetY()[i] };}
  };



  ////////////////////////////////////////////////////////
  /// \class PointContainer represents a full N dim function
  ///    - contains a vector<Point<N> >
  ///    - automatically normalizes dimensions into [0,1]
  ///    - imports from ROOT classes thanks to the above TObjectHelpers
  template<int N>
  struct PointContainer {
    typedef Point<N> point_t;
    typedef typename Point<N>::coord_t coord_t;

    std::vector<point_t> points;
    coord_t min, max;
    coord_t normFactor; // allows to translate physical coordinate into normalized coordinates in [0,1]

    const double *stretchFactor; // Pointer to coordinates stretch factors. The PointContainer guarantees all its points will use these factors
    
    point_t& operator[](int i)  {return points[i];}

    
    void setStretchFactorPtr(const double *f){
      stretchFactor = f;
      for(point_t &p: points) p.coord.setStretchFactorPtr(f) ;
      min.setStretchFactorPtr(f);
      max.setStretchFactorPtr(f);
    }
    
    void initFromTH(typename TObjectHelpers<N>::hist_t &h, bool logCoord[N]){
      size_t hsize = TObjectHelpers<N>::histoSize( h);
      size_t ncells = h.GetNcells();
      points.resize(hsize);
      min = coord_t::maximum();
      max = coord_t::minimum();
      int i=0;
      for(size_t c=1;c<ncells;c++){
        if( h.IsBinUnderflow(c) || h.IsBinOverflow(c) ) continue;
        point_t & p =points[i];
        p = TObjectHelpers<N>::fromTH(h,c, logCoord);
        min = min.minCoord(p.coord);
        max = max.maxCoord(p.coord);
        i++;
      }    

      // rescale in [0,1]
      normalize();
      setStretchFactorPtr(stretchFactor);
    }

    void initFromTGraph(typename TObjectHelpers<N>::graph_t &g, bool logCoord[N]){
      size_t gsize = g.GetN();
      points.resize(gsize);
      min = coord_t::maximum();
      max = coord_t::minimum();
      for(size_t i=0;i<gsize;i++){
        point_t & p =points[i];
        p = TObjectHelpers<N>::fromTGraph(g,i, logCoord);
        min = min.minCoord(p.coord);
        max = max.maxCoord(p.coord);
      }
      normalize();
      setStretchFactorPtr(stretchFactor);
    }


    std::vector<point_t> pointsAround(coord_t &c0, double d2max){
      std::vector<point_t> vecp;
      double d2min = 1e40;
      point_t pmin;
      for(point_t &p: points){
        double d2 = p.coord.d2(c0);      
        if(d2 <d2max ) {
          vecp.push_back( p );
        }
        if(d2<d2min){ d2min = d2; pmin = p;}
      }
      ClosestToPoint<N> cp0; cp0.p0.coord = c0;
      std::sort( vecp.begin(), vecp.end(), cp0 );
      if (vecp.empty()) vecp.push_back(pmin);
      return vecp;
    }

    coord_t normalizedCoord(const coord_t & c0){
      coord_t normc = (c0 - min);
      normc /= normFactor;
      normc.setStretchFactorPtr(stretchFactor);
      return normc;
    }

    coord_t unnormalizedCoord(const coord_t & c0){
      coord_t normc = c0*normFactor + min;
      return normc;
    }

    void normalize(){
      normFactor = max - min;
      // rescale in [0,1]
      for(point_t &p : points){
        p.coord =  normalizedCoord( p.coord)  ;
      }
    }

  };



  ////////////////////////////////////////////////////////
  /// \class Smoother performs smoothing calculations.
  /// 
  /// Smooth TGraph or TH to TH objects, internally using the above classes.
  ///  - use top-level function graphToHisto, histoFromGraph, histoFromHisto.
  ///  - configure by setting the members (see doc below)
  template<int N>
  struct Smoother {
  public:

    typedef Point<N> point_t;
    typedef typename Point<N>::coord_t coord_t;
    typedef typename TObjectHelpers<N>::graph_t graph_t;
    typedef typename TObjectHelpers<N>::hist_t hist_t;
    typedef typename TObjectHelpers<N>::histF_t histF_t;

    Smoother(){  m_points.setStretchFactorPtr(m_stretchFactor); setStretchFactor() ; }
    
    void setStretchFactor(const double *f=nullptr ){
      if(f==nullptr) for(int i=0;i<N;i++) m_stretchFactor[i]=1.;
      else for(int i=0;i<N;i++) m_stretchFactor[i]=1./(f[i]*f[i]);
    }

    void setStretchFactor(int i, double f) { m_stretchFactor[i]=1./(f*f) ;}
    
    void graphToHisto(graph_t& g, hist_t & h){
      m_points.initFromTGraph(g, m_logCoord);
      smoothToHisto( h );
    }


    histF_t * histoFromGraph(graph_t& g){
      m_points.initFromTGraph(g, m_logCoord);
      // create an empty histo fitting all the input points
      histF_t * h = TObjectHelpers<N>::emptyHistoFromGraph( g , m_npoints, m_points.min.unlogCoord(m_logCoord) , m_points.max.unlogCoord(m_logCoord));
      smoothToHisto( *h );
      return h;
    }

    graph_t * graphFromGraph(graph_t& g){
      m_points.initFromTGraph(g, m_logCoord);
      // create an empty histo fitting all the input points
      graph_t * gn = TObjectHelpers<N>::emptyGraphFromGraph( g , m_npoints, m_points.min.unlogCoord(m_logCoord) , m_points.max.unlogCoord(m_logCoord));
      smoothToGraph( *gn );
      return gn;
    }

    void histoToHisto(hist_t& h_in, hist_t & h_out){
      m_points.initFromTH(h_in, m_logCoord);
      smoothToHisto( h_out );
    }

    histF_t * histoFromHisto(hist_t& h_in){
      m_points.initFromTH(h_in, m_logCoord);
      // create an empty histo fitting all the input points
      histF_t * h_out = TObjectHelpers<N>::emptyHistoFromTH( h_in , m_npoints);
      smoothToHisto( *h_out );
      return h_out;
    }


    void smoothToHisto(hist_t& h){
      // get the points at which we want to smooth. binCenters() will set the 
      // point_t.value to the bin index in the histo h.
      std::vector<point_t> coords = TObjectHelpers<N>::binCenters(h);
      
      int i=0;
      // for each point, calculate the smoothed value
      for( point_t&  c : coords ) {
        //if(i>2000) break; 
        i++;
        float smoothV, smoothErr;
        if(m_useErrors)smoothErrAt( c.coord , smoothV, smoothErr);
        else smoothAt( c.coord , smoothV, smoothErr);

        /// set in the histo
        h.SetBinContent( int(c.value), smoothV);
        h.SetBinError( int(c.value), smoothErr);
      }
    }

    void smoothToGraph(graph_t& g){
      
      double * v = TObjectHelpers<N>::graphValues(g);
      double * v_err = TObjectHelpers<N>::graphErrors(g);
      // for each point, calculate the smoothed value
      for(int i=0;i<g.GetN();i++){
        coord_t c = TObjectHelpers<N>::graphToCoord(g,i);

        float smoothV, smoothErr;
        if(m_useErrors)smoothErrAt( c , smoothV, smoothErr);
        else smoothAt( c , smoothV, smoothErr);
        
        v[i] = smoothV;
        v_err[i] = smoothErr;
      }
    }

  
    void smoothErrAt( coord_t & c0, float & smoothV, float & smoothErr ){
      coord_t norm= m_points.normalizedCoord(c0.logCoord(m_logCoord) ); 
      std::vector<point_t> closePoints = m_points.pointsAround( norm , m_maxD2);
      double sum = 0;
      double sumw = 0;
      double sumErr2 = 0;
      if(m_useTwoPoints){
        // We evaluate a virtual new point 'p' situated at the projection of norm to the line p1p2.
        // We also evaluate the point 'mu' being the point at weighted average of p1 and p2 (thus also on the p1p2 line).
        // The value at 'mu' is the weighted average of values at p1 (v1) and p2 (v2).
        // The value at 'p' is defined as the value at 'mu' + (v1-v2)*(relative distance 'mu' to 'p' w.r.t p1 and p2).
        // Then we use this value as the contribution to smooth the point norm
        int maxPoints = (int) closePoints.size() < m_maxClosePoint?closePoints.size():m_maxClosePoint;
        for(int i=0; i<maxPoints; i++){
          point_t p1 = closePoints.at(i);
          if(p1.error==0.){
            coord_t cc = m_points.unnormalizedCoord(p1.coord);
            std::cout << "WARNING  zero error at (x0,y0)=" << cc << " // around" << c0 << std::endl; 
            continue;
          }
          for(int j=i+1; j<maxPoints; j++){
            point_t p2 = closePoints.at(j);
            if(p2.error==0.){
              coord_t cc = m_points.unnormalizedCoord(p2.coord);
              std::cout << "WARNING  zero error at (x0,y0)=" << cc << " // around" << c0 << std::endl; 
              continue;
            }
            double w1 = 1./(p1.error*p1.error);
            double w2 = 1./(p2.error*p2.error);
            coord_t p_w1(1./(p1.error*p1.error));
            coord_t p_w2(1./(p2.error*p2.error));
            coord_t mu_coord = (p_w1*p1.coord+p_w2*p2.coord)/(p_w1+p_w2); // point mu at the weighted average of p1 and p2
            double mu_d = 0.5*(p2.coord.d2(p1.coord)+p1.coord.d2(norm)-norm.d2(p2.coord))/p2.coord.d(p1.coord)-p1.coord.d(mu_coord); // signed distance between mu and p
            double mu_value = (w1*p1.value+w2*p2.value)/(w1+w2);
            double p_error = sqrt((1./w1+1./w2)*mu_d*mu_d/p2.coord.d2(p1.coord)+1./(w1+w2));
            double p_value = (p2.value-p1.value)*mu_d/p2.coord.d(p1.coord)+mu_value;
            if( (p_value>p1.value) && (p_value>p2.value) ){
              std::cout << c0 << "   "<< p1 << "  "<< p2 << " || mu_value = "<< mu_value << " rel dist="<< mu_d / p2.coord.d(p1.coord) << "  p_value="<< p_value<< std::endl;
            }
            double d = p1.coord.d(norm)>p2.coord.d(norm)?p1.coord.d(norm):p2.coord.d(norm);
            double k = m_kernel.Eval(d);
            double w = k*p_value/p_error; // weight by 1/relative_error
            sum += w*p_value;
            sumw += w;
            sumErr2 += w*p_error; // (will take the mean of the errors)
          }
        }
      }
      else{
        int c=0;
        for(point_t & p: closePoints){
          double d = norm.d(p.coord);
          double k = m_kernel.Eval(d);
          double error = p.error;
          if(error==0.){
            //if(p.value==0.) {error = TObjectHelpers<N>::histoErrorAt(m_defaultError,p.coord); if(error==0.){} } // ??}        
            coord_t cc = m_points.unnormalizedCoord(p.coord);
            std::cout << "WARNING  zero error at (x0,y0)=" << cc << " // around" << c0 << std::endl; 
            c++; continue;
          }
          //double w = k*p.value/error; // weight by 1/relative_error
          double w = k*TMath::Exp(-(error/p.value)); // weight by relative_error
          sum += w*p.value;
          sumw += w;
          sumErr2 += w*p.error ; // (will take the mean of the errors)
          c++; if(c>m_maxClosePoint) break;
        }
      }
      if(sumw==0) {
        smoothV=0;
        smoothErr=0;
      }else{
        smoothV = sum/sumw;
        //smoothErr = TMath::Sqrt(sumErr2)/sumw; --> no, otherwise we won't be able to claim the errors of 2 consecutive points are statiscally indep.
        smoothErr = sumErr2/sumw; // -->  more conservative.
      }
      //std::cout << "around "<< c0[0] << "  npoints= "<< closePoints.size() << "  ==> "<< smoothV << std::endl;
    }

    void smoothAt( coord_t & c0, float & smoothV, float & smoothErr ){
      coord_t norm= m_points.normalizedCoord(c0.logCoord(m_logCoord) ); 
      std::vector<point_t> closePoints = m_points.pointsAround( norm , m_maxD2);
      double sum = 0;
      double sumerr = 0;
      double sumw = 0;
      int c=0;
      for( point_t & p: closePoints){
        double d = norm.d(p.coord);
        double w = m_kernel.Eval( d );
        //std::cout << "    around ("<< p.coord  << ")  "<< d << " --> "<< p.value << "  x "<< w << std::endl;
        sum += w*p.value;
        sumerr+= w*p.error;
        sumw += w;
        c++; if(c>m_maxClosePoint) break;
      }
      if(sumw==0) {smoothV=0;smoothErr=0;}
      else {
        smoothV = sum/sumw;
        smoothErr=sumerr/sumw;
      }
      //std::cout << " ----------> "<< smoothV << "  , "<< smoothErr << std::endl;
    }


    /// The kernel smoothing function
    TF1 m_kernel;
    /// Will ignore points at greated distance than this (to avoid unnecessary calculations).
    float m_maxD2=0.3*0.3;
    /// Will consider only the closest (m_maxClosePoint) (to avoid unnecessary calculations).
    int m_maxClosePoint = 6*N; // underestimated !!

    /// Set m_logCoord[i] to True if point along dim i are logarithmically spaced.
    bool m_logCoord[N];

    /// When creating histos, use m_npoints[i] bins for dim i.
    int m_npoints[N];

    /// Take errors into account. Should be false for smoothing distributions. 
    ///  if true and if error at a point is 0, the point will be fully ignored.
    bool m_useErrors = false; 

    /// When smoothing, use 2 points at one time in order to avoid problems caused by small errors
    bool m_useTwoPoints = false;

    double m_stretchFactor[N];
    
    //    hist_t *m_defaultError; // not used by default

    /// Internal representation of points to be smoothed.
    PointContainer<N> m_points;

  };

  struct FastGausSmoothTH1 {
    FastGausSmoothTH1(float fw, int nn): m_fw(fw), m_nn(nn){}
    virtual ~FastGausSmoothTH1(){}
    void smooth(TH1 &h, int ignoredbins=0);
    virtual double distWeight(int i, int delta) = 0;
    virtual void setup(TH1 &h) ;
    
    static void smooth(TH1& h, float w, int nn, int ignoredbins=0);
    double m_fw;
    double m_width;
    int m_nn;
  };

  struct FastGausSmoothRegTH1 : public FastGausSmoothTH1 {
    FastGausSmoothRegTH1(float fw, int nn): FastGausSmoothTH1(fw,nn){}
    double distWeight(int i, int delta) ;
    void setup(TH1 &h) ;    
    std::vector<double> m_weights;
  };
  struct FastGausSmoothNonRegTH1 : public FastGausSmoothTH1 {
    FastGausSmoothNonRegTH1(float fw, int nn): FastGausSmoothTH1(fw,nn){}
    double distWeight(int i, int delta) ;
    void setup(TH1 &h) ;    
    TAxis *m_a;
  };
  
}
#endif
