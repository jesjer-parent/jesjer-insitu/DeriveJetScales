// this file is -*- C++ -*-
#ifndef DERIVEJETWSCALES_JMSCOMBINATION_H
#define DERIVEJETWSCALES_JMSCOMBINATION_H
#include "DeriveJetScales/InputReader.h"

namespace DJS {

  class JMSBinning;
  
  /////////////////////////////////////////////////////
  /// \class JMSCombination
  /// 
  /// Helper class which implements the JES calibration
  ///
  /// 
  class JMSCombination
  {
  public:
    JMSCombination(){};
    virtual ~JMSCombination(){};

    double combinedMass(double e, double mCalo, double mTA, double eta);
    double combinedMass(JetData &j) {return combinedMass(j.e_reco, j.m_reco, j.m_ta, j.eta_reco);}

    JMSBinning *m_mTACalib = nullptr;
    JMSBinning *m_mCaloCalib = nullptr;
    
  };

}
#endif
