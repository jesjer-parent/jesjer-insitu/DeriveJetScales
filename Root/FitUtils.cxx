#include "FitUtils.h"

#include "TDatime.h"
#include "TRandom.h"

namespace DJS {


  BestOrderFit::BestOrderFit(TGraphErrors *graph, Double_t first, Double_t last, Int_t minOrder, Int_t maxOrder, Double_t minChi2Limit, TString basis){

	//_inputGraph = graph;
	_inputGraph = (TGraphErrors*)graph->Clone();
	_xMin = first; _xMax = last;
	_bestOrderFormula = getBestOrderFormula(graph, genFormulaList(maxOrder, basis), minOrder, first, last, minChi2Limit);

  }

  BestOrderFit::~BestOrderFit(){}


  TF1 *BestOrderFit::getBestOrderTF1(){
    TRandom random;
    TString randomString;


    TDatime datime;
    //randomString.Append(Form("%d",random.Integer(30000000)));
    randomString.Append(Form("%d",datime.GetTime()));
     
    TF1 *bestTF1 = new TF1(Form("polBest%s",randomString.Data()), _bestOrderFormula->Data());
    //TGraphErrors *myCloneGraph = (TGraphErrors*)_inputGraph->Clone();
     
    if (_bestOrderFormula->Contains("[0]+[1]*(exp")){
      printf("TF1Name = %s", bestTF1->GetName());
      bestTF1->SetParameter(0, 1.);
      bestTF1->SetParameter(1, 1.3);
      bestTF1->SetParameter(2, 8.);
      bestTF1->SetParameter(3, 18.);
      bestTF1->SetParameter(4, 8.);
    }
     
    //_inputGraph->Fit(bestTF1, "RQMB","", _xMin, _xMax);
    _inputGraph->Fit(bestTF1, "RQMB","", _xMin, _xMax);
    // std::cout<< " bestofFit "<< _xMin << "  "<< _xMax << "  --- "<< _inputGraph <<std::endl;
    // _inputGraph->Print();
    // std::cout << "      -> "<< bestTF1->GetExpFormula() << "  _  "<< bestTF1->GetParameter(0)<< std::endl; 

    //delete myCloneGraph;
    //???The case "return bestTF1" delivers a wrong function???
    return	_inputGraph->GetFunction(Form("polBest%s",randomString.Data()));
  }

  TString *BestOrderFit::getBestOrderFormula(TGraphErrors *inputGraph, std::vector<TString> *formulaList, Int_t minOrder, Double_t xMin, Double_t xMax, Double_t minChi2Limit){


	Int_t bestOrder=minOrder;
	Double_t tmpChi2=0, minChi2=1000;

	for(Int_t iLoop=minOrder; iLoop<(Int_t)formulaList->size(); iLoop++){
      TF1 *tmpTF1 = new TF1(Form("polO%d",iLoop), formulaList->at(iLoop).Data());
      TGraphErrors *tmpGraph = (TGraphErrors*)inputGraph->Clone(Form("cloneGraph%d",iLoop));

      if (formulaList->at(iLoop).Contains("exp")){
        tmpTF1->SetParameter(0, 1.);
        tmpTF1->SetParameter(1, 1.3);
        tmpTF1->SetParameter(2, 8.);
        tmpTF1->SetParameter(3, 18.);
        tmpTF1->SetParameter(4, 8.);
      }

      //tmpTF1->SetParLimits(0, 0.98, 1.02);
      //tmpTF1->SetParLimits(1, 0.5, 5.);

      //tmpGraph->Fit(tmpTF1, "RQMEB","", xMin, xMax);
      //tmpGraph->Fit(tmpTF1, "RQMB","", xMin, xMax);
      tmpGraph->Fit(tmpTF1, "RQMB","", xMin, xMax);

      //tmpGraph->Fit(tmpTF1, "Q","", xMin, xMax);

      tmpChi2 = tmpTF1->GetChisquare()/(Double_t)tmpTF1->GetNDF();

      if (tmpChi2 < minChi2){
        //if (minChi2 > minChi2Limit)
        minChi2 = tmpChi2; bestOrder = iLoop;
               
      }

      delete tmpGraph;
      delete tmpTF1;
	}

	return new TString(formulaList->at(bestOrder).Data());



  }

  std::vector<TString> *BestOrderFit::genFormulaList(Int_t maxOrder, TString basis){
	std::vector<TString> *myFormulaList = new std::vector<TString>;

	for (Int_t iLoop=0; iLoop<=maxOrder; iLoop++){

      if(iLoop==0){
        myFormulaList->push_back("[0]");
      } else {
        TString tmpString = "[0]";

        for(Int_t jLoop=1; jLoop<=iLoop; jLoop++)
          tmpString.Append(Form("+[%d]*%s^%d",jLoop,basis.Data(),jLoop));

        myFormulaList->push_back(tmpString);
      }

	}


	return myFormulaList;
  }


  GroomExtrapolation::GroomExtrapolation(TGraphErrors *inputGraph, Double_t EMinForExtrapolation, Double_t EMaxForExtrapolation, bool useGeoDetCorr, Double_t eta, Double_t minGeoDetCorr, Double_t maxGeoDetCorr){

	//_eta = 0;

	_inputGraph = (TGraphErrors*)inputGraph->Clone(Form("%sClone", inputGraph->GetName()));
	Double_t xMin = _inputGraph->GetX()[0]; //MAX MOD
	Double_t xMax = _inputGraph->GetX()[_inputGraph->GetN()-1];

	BestOrderFit *myPolFit = new BestOrderFit(_inputGraph, xMin, xMax);

	TGraphErrors *graphForExt = gen3PointGraphForGroomFit(myPolFit->getBestOrderTF1(), xMin, xMax);
	TGraphErrors *graphForExt2 = gen5PointGraphForGroomFit(myPolFit->getBestOrderTF1(), _inputGraph, xMin, xMax);
	
	TF1 *groomForExt = fitGroom(graphForExt, EMinForExtrapolation, EMaxForExtrapolation);
	TF1 *groomForExt2 = fitGroom(graphForExt2, EMinForExtrapolation, EMaxForExtrapolation);
	
	_groomForExt = groomForExt;
	_groomForExt2 = groomForExt2;
		
	_extGraph = genExtGraph(_inputGraph, groomForExt, EMinForExtrapolation, EMaxForExtrapolation);
	_extGraph2 = genExtGraph(_inputGraph, groomForExt2, EMinForExtrapolation, EMaxForExtrapolation);

	_extGraphSL = gen5PointGraphForSecondLayer(graphForExt, groomForExt, EMinForExtrapolation, EMaxForExtrapolation, xMax);
	_extGraphSL2 = gen5PointGraphForSecondLayer(graphForExt2, groomForExt2, EMinForExtrapolation, EMaxForExtrapolation, xMax);

    //This condition is applied to avoid the divergences produced by the use of the full points on the groom extrapolation
    //that divergence depends on minGeoDetCorr and maxGeoDetCorr
    if (useGeoDetCorr and (minGeoDetCorr<TMath::Abs(eta) and TMath::Abs(eta)<maxGeoDetCorr)) {
	  TGraphErrors *graphForExt3 = genFullPointGraphForGroomFit(_inputGraph, xMin, xMax);		
	  TF1 *groomForExt3 = fitGroom(graphForExt3, EMinForExtrapolation, EMaxForExtrapolation);
	  _groomForExt3 = groomForExt3;
	  _extGraph3 = genExtGraph(_inputGraph, groomForExt3, EMinForExtrapolation, EMaxForExtrapolation);
	  _extGraphSL3 = gen5PointGraphForSecondLayer(graphForExt3, groomForExt3, EMinForExtrapolation, EMaxForExtrapolation, xMax); 	  	  			
	}		
	
  }

  GroomExtrapolation::~GroomExtrapolation(){}


  TGraphErrors *GroomExtrapolation::getExtGraph(){
	return _extGraph;
  }

  TGraphErrors *GroomExtrapolation::getExtGraph2(){
	return _extGraph2;
  }

  TGraphErrors *GroomExtrapolation::getExtGraph3(){
	return _extGraph3;
  }

  TGraphErrors *GroomExtrapolation::getExtGraphSL(){
	return _extGraphSL;
  }

  TGraphErrors *GroomExtrapolation::getExtGraphSL2(){
	return _extGraphSL2;
  }

  TGraphErrors *GroomExtrapolation::getExtGraphSL3(){
	return _extGraphSL3;
  }

  TF1 *GroomExtrapolation::getTF1GroomForExt(){
	return _groomForExt;
  }

  TF1 *GroomExtrapolation::getTF1GroomForExt2(){
	return _groomForExt2;
  }

  TF1 *GroomExtrapolation::getTF1GroomForExt3(){
	return _groomForExt3;
  }

  TF1 *GroomExtrapolation::fitGroom(TGraphErrors *inputGraph, Double_t EMinForExt, Double_t EMaxForExt){
	TString groomFormula="[0]*(1-[1]*pow(x/0.75,[2]-1))";
	TF1 *groom = new TF1(Form("groom%d", _eta), groomFormula, 3.0*cosh(_eta), EMaxForExt);
	groom->SetParameters(1.0,0.9,0.7);
	inputGraph->Fit(groom,"RQEMN");

	return groom;

  }

  TGraphErrors *GroomExtrapolation::gen3PointGraphForGroomFit(TF1 *inputTF1, Double_t eMin, Double_t eMax){
	Double_t logRange=log(eMax)-log(eMin);
	//Double_t E1=exp(log(eMin)+logRange/20);
	//Double_t E3=exp(log(eMax)-logRange/20);
	//Double_t E2=exp((log(E3)+log(E1))/2);
	Double_t E1=exp(log(eMin)+logRange/9);
	Double_t E3=exp(log(eMax)-logRange/10);
	Double_t E2=exp((log(E3)+log(E1))/2);

	Double_t R1 = inputTF1->Eval(E1);
	Double_t R2 = inputTF1->Eval(E2);
	Double_t R3 = inputTF1->Eval(E3);

	TGraphErrors *myGraph = new TGraphErrors(3);
	myGraph->SetPoint(0,E1, R1);
	myGraph->SetPoint(1,E2, R2);
	myGraph->SetPoint(2,E3, R3);

    if (R1>R2||R2>R3) printf("Problem!\n");

	return myGraph;
  }

  TGraphErrors *GroomExtrapolation::gen5PointGraphForGroomFit(TF1 *inputTF1, TGraphErrors *inputGraph, Double_t eMin, Double_t eMax){

	Double_t logRange=log(eMax)-log(eMin);

	Double_t E1=exp(log(eMin)+logRange/9);
	Double_t E3=exp(log(eMax)-logRange/10);
	Double_t E2=exp((log(E3)+log(E1))/2);
	
	Double_t R1 = inputTF1->Eval(E1);
	Double_t R2 = inputTF1->Eval(E2);
	Double_t R3 = inputTF1->Eval(E3);
	
	Double_t RMin = inputGraph->GetY()[0];
	Double_t RMax = inputGraph->GetY()[(inputGraph->GetN()-1)];
        
	TGraphErrors *myGraph = new TGraphErrors(5);
	
	myGraph->SetPoint(0, eMin, RMin) ;	
	myGraph->SetPoint(1, E1, R1);
	myGraph->SetPoint(2, E2, R2);
	myGraph->SetPoint(3, E3, R3);
	myGraph->SetPoint(4, eMax, RMax);	

	if (R1>R2||R2>R3) printf("Problem!\n");

	return myGraph;

  }

  TGraphErrors *GroomExtrapolation::genFullPointGraphForGroomFit(TGraphErrors *inputGraph, Double_t eMin, Double_t eMax){

	TGraphErrors *myGraph = new TGraphErrors(inputGraph->GetN());
	for(Int_t iLoop=0; iLoop<inputGraph->GetN(); iLoop++){
      myGraph->SetPoint((iLoop), inputGraph->GetX()[iLoop], inputGraph->GetY()[iLoop]);
      myGraph->SetPointError((iLoop), inputGraph->GetEX()[iLoop], inputGraph->GetEY()[iLoop]);
	}
	return myGraph;
	
  }
    
  TGraphErrors *GroomExtrapolation::gen5PointGraphForSecondLayer(TGraphErrors *input3PointGraph, TF1 *extTF1, Double_t eMin, Double_t eMax, Double_t lastDataPoint){
	
	TGraphErrors *myGraph = new TGraphErrors();

	myGraph->SetPoint(0, eMin ,extTF1->Eval(eMin));	
	myGraph->SetPointError(0,0.,0.001);
	//cout << "... point 0" << " --> Emin = " << extTF1->Eval(eMin) <<endl;	
		
	for(Int_t iLoop=0; iLoop<input3PointGraph->GetN(); iLoop++){
      //myGraph->SetPoint((iLoop), input3PointGraph->GetX()[iLoop], input3PointGraph->GetY()[iLoop]);
      myGraph->SetPoint((iLoop+1), input3PointGraph->GetX()[iLoop], input3PointGraph->GetY()[iLoop]);//MAX MOD
      myGraph->SetPointError((iLoop+1),0.,0.001);
      //cout << "... point " << (iLoop+1) << " --> E =" << input3PointGraph->GetY()[iLoop] <<endl;		
	}

	//Generate points beween eMax and the last data point to avoid oscillations (distance between points to large)
	Double_t minLog = log(lastDataPoint);
	Double_t maxLog = log(eMax);
	//Double_t distInLogScale = 0.3;
	Double_t distInLogScale = 0.3;//max mod
	Int_t NAdditionalPoints = (Int_t)((maxLog-minLog)/distInLogScale);   
    
	for (Int_t jLoop=1; jLoop<=NAdditionalPoints; jLoop++){
      myGraph->SetPoint(input3PointGraph->GetN()+jLoop, exp(minLog+distInLogScale*jLoop), extTF1->Eval(exp(minLog+distInLogScale*jLoop)));
      myGraph->SetPointError(input3PointGraph->GetN()+jLoop, 0., 0.001);
      //cout << "... point " << input3PointGraph->GetN()+jLoop << " --> E =" << extTF1->Eval(exp(minLog+distInLogScale*jLoop)) <<endl;
	}

	return myGraph;
  }


  TGraphErrors *GroomExtrapolation::genExtGraph(TGraphErrors *inputGraph, TF1 *extTF1, Double_t eMinForExt, Double_t eMaxForExt){
	TGraphErrors *myGraph = new TGraphErrors();

	myGraph->SetPoint(0, eMinForExt ,extTF1->Eval(eMinForExt));
	myGraph->SetPointError(0,0.,0.001);

	for(Int_t iLoop=0; iLoop<inputGraph->GetN(); iLoop++){
      myGraph->SetPoint((iLoop+1), inputGraph->GetX()[iLoop], inputGraph->GetY()[iLoop]);
      myGraph->SetPointError((iLoop+1), inputGraph->GetEX()[iLoop], inputGraph->GetEY()[iLoop]);
	}


	//myGraph->SetPoint(_inputGraph->GetN()+1, eMaxForExt, extTF1->Eval(eMaxForExt));
	//myGraph->SetPointError(_inputGraph->GetN()+1,0.,0.001);

	Double_t minLog = log( _inputGraph->GetX()[_inputGraph->GetN()-1]);
	Double_t maxLog = log(eMaxForExt);
	Double_t distInLogScale = 0.3;
	Int_t NAdditionalPoints = (Int_t)((maxLog-minLog)/distInLogScale);

	for (Int_t jLoop=1; jLoop<=NAdditionalPoints; jLoop++){
      myGraph->SetPoint(_inputGraph->GetN()+jLoop, exp(minLog+distInLogScale*jLoop), extTF1->Eval(exp(minLog+distInLogScale*jLoop)));
      myGraph->SetPointError(_inputGraph->GetN()+jLoop, 0., 0.001);
	}

	return myGraph;
  }


  LinExtrapolation::LinExtrapolation(TGraphErrors *inputGraph, Double_t EMinForExtrapolation, Double_t EMaxForExtrapolation, TString extFormula){
	_extGraphSL = new TGraphErrors();
	_extGraph = genLinExtGraph(inputGraph, EMinForExtrapolation, EMaxForExtrapolation, extFormula);

  }

  LinExtrapolation::~LinExtrapolation(){}

  TGraphErrors *LinExtrapolation::getExtGraph(){
	return _extGraph;
  }

  TGraphErrors *LinExtrapolation::getExtGraphSL(){
	return _extGraphSL;
  }

  TGraphErrors *LinExtrapolation::genLinExtGraph(TGraphErrors *inputGraph, Double_t Emin, Double_t Emax, TString extFormula) {

    // Edited for testing - Dec 9
    TGraphErrors *g = inputGraph?((TGraphErrors*)inputGraph->Clone("extGraph")):0;
 
    Double_t minLog; 
 
    if (!(g->GetX()==0 && g->GetN()==0)){
      minLog = g?(log(g->GetX()[g->GetN()-1])):0.00001;
    }
    else minLog = 1; 
 
    Double_t maxLog = log(Emax);
    Double_t distInLogScale = 0.3;
    Int_t NAdditionalPoints = (Int_t)((maxLog-minLog)/distInLogScale);

    TGraphErrors *gTmp = g;

    for(Int_t jLoop=1; jLoop<=NAdditionalPoints; jLoop++){
      //printf ("test: %d, NAddPoints = %d , lastPoint = %f, extraPoint = %f\n", jLoop, NAdditionalPoints, exp(minLog), exp(minLog+distInLogScale*jLoop));
      g = genLinExtGraphOnePoint(gTmp, Emin, exp(minLog+distInLogScale*jLoop), extFormula);
      delete gTmp;
      gTmp = g;
    }

    return gTmp;

  }

  TGraphErrors *LinExtrapolation::genLinExtGraphOnePoint(TGraphErrors *inputGraph, Double_t Emin, Double_t Emax, TString extFormula){
	TGraphErrors *g = (TGraphErrors*)inputGraph->Clone("extGraph1");

	Double_t xMax=g->GetX()[g->GetN()-1];
	Double_t xMid=g->GetX()[g->GetN()-3];

	TF1 pol("linExt", extFormula.Data(), xMid, xMax);
	g->Fit(&pol,"RQMEB");

	pol.SetRange(0,3500);

	Int_t posExtPoint = g->GetN();
	g->SetPoint(posExtPoint, Emax, pol.Eval(Emax));
	g->SetPointError(posExtPoint,0., 0.001);

	Int_t NPoints = _extGraphSL->GetN();
	_extGraphSL->SetPoint(NPoints, Emax, pol.Eval(Emax));
	_extGraphSL->SetPointError(NPoints, 0., 0.001);


	return g;
  }




}
