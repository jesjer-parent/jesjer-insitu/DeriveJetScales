// this file is -*- C++ -*-
#ifndef DERIVEJETWSCALES_FITUTILS_H
#define DERIVEJETWSCALES_FITUTILS_H


#include "TString.h"
#include "TGraphErrors.h"
#include "TF1.h"
#include "TMath.h"
#include <vector>

#include "TCanvas.h"
#include "TRandom.h"
//#include "anaBasic.h"

namespace DJS {

  class BestOrderFit{
  public:
	BestOrderFit(TGraphErrors *graph, Double_t first, Double_t last, Int_t minOrder=2, Int_t maxOrder=6, Double_t minChi2Limit=1.3, TString basis = "log(x)");
	~BestOrderFit();

	TF1 *getBestOrderTF1();

  private:
	TGraphErrors *_inputGraph;
	TString *_bestOrderFormula;
	Double_t _xMin, _xMax;

	TString *getBestOrderFormula(TGraphErrors *inputGraph, std::vector<TString> *formulaList, Int_t minOrder, Double_t xMin, Double_t xMax, Double_t minChi2Limit);
	std::vector<TString> *genFormulaList(Int_t maxOrder=6, TString basis = "log(x)");
  };

  class GroomExtrapolation{
  public:
	GroomExtrapolation(TGraphErrors *inputGraph, Double_t EMinForExtrapolation=600, Double_t EMaxForExtrapolation=2500, bool useGeoDetCorr=false, Double_t eta=0.0, Double_t minGeoDetCorr=3.2, Double_t maxGeoDetCorr=3.4);
	~GroomExtrapolation();

	TGraphErrors *getExtGraph();
	TGraphErrors *getExtGraphSL();
	TF1 *getTF1GroomForExt();
	
	TGraphErrors *getExtGraph2();
	TGraphErrors *getExtGraphSL2();
	TF1 *getTF1GroomForExt2();
	
	TGraphErrors *getExtGraph3();
	TGraphErrors *getExtGraphSL3();
	TF1 *getTF1GroomForExt3();	
		
  private:
	Double_t _eta;	
	TGraphErrors *_inputGraph;
	
	TGraphErrors *_extGraph;
	TGraphErrors *_extGraphSL;
	TF1 *_groomForExt;

	TGraphErrors *_extGraph2;
	TGraphErrors *_extGraphSL2;
	TF1 *_groomForExt2;
	
	TGraphErrors *_extGraph3;
	TGraphErrors *_extGraphSL3;
	TF1 *_groomForExt3;	
	
	TF1 *fitGroom(TGraphErrors *inputGraph, Double_t EMinForExt, Double_t EMaxForExt);
	
	TGraphErrors *gen3PointGraphForGroomFit(TF1 *inputTF1, Double_t eMin, Double_t eMax);
	TGraphErrors *gen5PointGraphForGroomFit(TF1 *inputTF1, TGraphErrors *inputGraph, Double_t eMin, Double_t eMax);
	TGraphErrors *genFullPointGraphForGroomFit(TGraphErrors *inputGraph, Double_t eMin, Double_t eMax);	
			
	TGraphErrors *gen5PointGraphForSecondLayer(TGraphErrors *input3PointGraph, TF1 *extTF1, Double_t eMin, Double_t eMax, Double_t lastDataPoint);
		
	TGraphErrors *genExtGraph(TGraphErrors *inputGraph, TF1 *extTF1, Double_t eMinForExt, Double_t eMaxForExt);

  };

  class LinExtrapolation{
  public:
	LinExtrapolation(TGraphErrors *inputGraph, Double_t EMinForExtrapolation, Double_t EMaxForExtrapolation=2500, TString extFormula="[0]+[1]*log(x)");
	~LinExtrapolation();

	TGraphErrors *getExtGraph();
	TGraphErrors *getExtGraphSL();

  private:
	Double_t _eta;
	TGraphErrors *_extGraph;
	TGraphErrors *_extGraphSL;

	TGraphErrors *genLinExtGraph(TGraphErrors *inputGraph, Double_t Emin, Double_t Emax, TString extFormula);
	TGraphErrors *genLinExtGraphOnePoint(TGraphErrors *inputGraph, Double_t Emin, Double_t Emax, TString extFormula);

  };


}
#endif
