#include "DeriveJetScales/ResponseFitter.h"

#include "TH2F.h"
#include "TProfile.h"
#include "TGraphErrors.h"
#include "JES_ResponseFitter/JES_BalanceFitter.h"
#include "FitUtils.h"

using std::cout;
using std::endl;

namespace DJS {

  int findEntryWithin(std::vector<float> etaVec, double etaMin, double etaMax){
    
    for(int iLoop=0; iLoop<(int)etaVec.size(); iLoop++){
      if (etaMin<etaVec.at(iLoop) && etaVec.at(iLoop)<etaMax) return iLoop;
    }
    
    return -1;
  }



  /// rebinXAxis returns a list of x-bins from inputHist to be combined.
  ///  The list is in a form of a vector<int> : lowbin0,highbin0, lowbin1,highbin1, lowbin2,highbin2, ... 
  /// thus the new bin i correspond to positions 2*i and 2*i+1 in the output vector
  ///  Bins are merged starting from bin with enough energy>minAvg, or whatever y-axis of inputXAvg means.
  ///  Contiguous bins are merged as long as they don't contain minEffEntrie
  std::vector<int> ResponseFitter::rebinXAxis(TH2F *inputHist, float minEffEntrie, TProfile *inputXAvg, float minAvg){
    

    int NBins = inputHist->GetNbinsX();
    int firstBin = 1;

    //List of low and high bins ([0]:lowBin [1]:highBin [2]:lowBin ...)
    std::vector<int> mergeList;
    
    if(inputXAvg != nullptr ) {
      //Detect first bin that contains e_true(BinCenter) > true energy threshold (MinEtForMeasurements) defined in config file
      for (int iLoop=1; iLoop<=NBins; iLoop++){
        double avg = inputXAvg->GetBinContent(iLoop) ;
        //cout << "xxxxxxxxx bin "<< iLoop << "   e="<< avg << " // "<< minAvg << " statcut="<< minEffEntrie << endl;
        if( avg > minAvg){ 
          firstBin=iLoop;
          break;
        }
      }
    }
    //Check for stats and merge bins if necessary
    for(int iLoop=firstBin; iLoop<=NBins; iLoop++){
      double neff = getEffectiveEntries(inputHist, firstBin, iLoop );
      cout << " __ " << firstBin << " , "<< iLoop << "    - "<< neff << endl;
      bool enoughStat = (neff> minEffEntrie) ; //( neff > _minNEffP1*exp(_minNEffP2*TMath::Abs(eta))+_    
      if(enoughStat) {
        mergeList.push_back(firstBin);
        mergeList.push_back(iLoop);
        firstBin=iLoop+1;
      }
    }
    
    return mergeList;
  }
  

  void ResponseFitter::prepareEtaBin(int ieta){
    m_currentEtaBin = ieta;
    for(auto * h: m_respPerEbins) if(h) delete h;
    m_respPerEbins.clear();
    m_R_vs_E_Func = 0;
    m_R_vs_E_SLGraph = 0;
  }

  
  TGraphErrors * ResponseFitter::constructRGraph(TH2F *inputHist, TProfile *inputXAvg, TProfile *inputXAvgCut, double inputXCut, bool optimalRebin, bool includeBadFits){
    /// Build and returns a TGraph MeanResponse vs <X> where 
    ///   - inputHist is a "Response vs X" TH2
    ///   - MeanResponse is fitted from Responses in inputHist
    ///   - <X> is the average X (or estimated X) evaluated from the y-axis of TProfile inputXAvg using the bins considered for the fit of MeanResponse
    ///   - the X bins considered for the response fits are obtain via rebinXAxis
    /// WARNING : this function has the side effect of filling m_respPerEbins !
    
    double avg, avgErr;
    m_R_vs_E_Graph = new TGraphErrors();
    m_R_vs_E_Graph->SetTitle( TString("Graph_")+inputHist->GetName());
    //cout << "uuu "<< inputHist << "  "<< inputXAvg << "  "<<m_minNEffP3<< endl;
    double eta = m_etaBins->binCenter(m_currentEtaBin);
    double statCut = m_minNEffP1*exp(m_minNEffP2*TMath::Abs(eta))+m_minNEffP3;

    cout << "  rebincuts : statCut= "<<statCut << "   inputXCut = "<< inputXCut <<  "    || "<< inputHist->GetEffectiveEntries()<< std::endl; 
    
    //Get list with new binning taking into account inputXCut (= minE cut typically) + enough stats
    std::vector<Int_t> mergeList = rebinXAxis(inputHist, statCut, inputXAvgCut, inputXCut);
    // mergeList contains the rebinned ranges : [lowbin0,highbin0, lowbin1,highbin1, lowbin2,highbin2, ... ]
    // thus bin i correspond to positions 2*i and 2*i+1
    
    cout << " xxx inputXCut ="<< inputXCut << "  bins "<< mergeList.size() << endl; //[0] << "  "<< mergeList[1] <<endl;
    //cout << "  uuuuuuu hist "<< inputHist << "  "<< inputHist->GetName() << "  "<< inputHist->GetMean() << "  fitsig="<< m_respFitter->m_Nsigma << endl;

    //Suggested change to the cut range by Oleg to fix dEta fit
    //Slide 13 in https://indico.cern.ch/event/691746/contributions/2900160/attachments/1603612/2543246/2018_02_14_JESJER_MCJESno4_Rscan_BRANDT.pdf
    m_respFitter->SetDefaultMinMax(-0.2, 2.0);
    
    for (Int_t iLoop=0; iLoop<(Int_t)mergeList.size(); iLoop=iLoop+2){
      Int_t lowBin=mergeList[iLoop] , highBin=mergeList[iLoop+1];
      
      getAvg(inputXAvg,lowBin,highBin,avg,avgErr);

      
      /// Project all responses between lowBin and highBin into 1 histo.
      TH1D *tmpHist = inputHist->ProjectionY( Form("%s_%d_%d",inputHist->GetName(), lowBin, highBin) , 
                                              lowBin, highBin);

      //cout << "... Will be use the Energy fitter "<< lowBin << "   " <<highBin << endl;
      if(m_useMode) {
        float mode[4];
        uniModalSmoothMax( *tmpHist, mode, 0.7) ;
        Int_t pointN = m_R_vs_E_Graph->GetN();
        m_R_vs_E_Graph->SetPoint(pointN,avg, mode[0]);
        m_R_vs_E_Graph->SetPointError(pointN,avgErr,mode[1] );

        m_respPerEbins.push_back( tmpHist );
        // in that case do not delete tmpHist
        
      } else {
        TFitResultPtr res;
        if( optimalRebin ) {
          // Rebinning and resizing histograms to better fits	
          double min, max;
          double scale;  
          tmpHist->Rebin(m_respFitter->OptimalRebin(tmpHist));
          //if(!includeBadFits) cout << " __uuuuuuuuuuuu "<< iLoop << "  "<< lowBin << ", "<< highBin << "  e="<< avg  << "   tmpHistMean="<< tmpHist->GetMean() << " rms=  " << tmpHist->GetRMS()<<   "orebin="<< m_respFitter->OptimalRebin(tmpHist)<< endl; DEBUG stateme

          if (avg<140.0)
            scale = 4; 	
          else if (avg>140.0 && avg<500.0)
            scale = 5;
          else scale = 6;
          min = tmpHist->GetMean() - tmpHist->GetRMS()*scale;
          max = tmpHist->GetMean() + tmpHist->GetRMS()*scale;  
          tmpHist->SetAxisRange(min,max);
        
          double m = tmpHist->GetMean(); double rms = tmpHist->GetRMS();

          //double mbug = tmpHist->GetBinCenter(tmpHist->GetMaximumBin());
          //double fitMax = mbug + m_NSigmaForFit * rms; // !!!! BUG in original anaBasic ?
          double fitMax = m + m_NSigmaForFit * rms; // !!!! BUG corrected
          double fitMin = m - m_NSigmaForFit * rms;

        
          //m_respFitter->FitNoRebinMax(tmpHist,fitMin, fitMax);
          m_respFitter->FitNoRebin(tmpHist,fitMin, fitMax);
          res = m_respFitter->fitRes();
          
          //if(!includeBadFits) cout << " ________ xxxx min="<< min << "  max="<< max <<" ::: "<< fitMin << " "<< fitMax << "    "<< m_respFitter->GetMean()  <<  "    nsig="<< m_NSigmaForFit <<  endl;

        } else { // no optimal rebin 
          double m = tmpHist->GetMean(); double rms = tmpHist->GetRMS();
          double fitMin = m - m_NSigmaForFit * rms;
          m_respFitter->Fit(tmpHist, fitMin);        
          res = m_respFitter->fitRes();
        }

        
        m_respFitter->GetHisto()->GetXaxis()->SetTitle(Form(m_respHistTitle.c_str(), inputXAvg->GetBinLowEdge(lowBin), inputXAvg->GetBinLowEdge(highBin+1), m_currentEtaBin));
        m_respPerEbins.push_back( (TH1D*) m_respFitter->GetHisto()->Clone(Form("%s_%d",inputHist->GetName(), iLoop/2)) );
      
        
        
        Int_t pointN = m_R_vs_E_Graph->GetN();

        double dEta = m_respFitter->GetMean();
        double dEta_err = m_respFitter->GetMeanError();
        bool isGoodFit = true;

        if(!includeBadFits){// this is actually used only for DEta fits.
          double mean = tmpHist->GetMean();
          double fitrmin, fitrmax; m_respFitter->GetFit()->GetRange(fitrmin, fitrmax);
          isGoodFit = isGoodFit && (fitrmin < mean) && (fitrmax>mean); // check if the range used to do the fit really includes the mean
          isGoodFit = isGoodFit && (fitrmin < dEta) && (fitrmax>dEta); // check if the range used to do the fit really includes the fitted mean
          isGoodFit = isGoodFit && (  (std::abs(dEta - mean) < 0.1) || (m_respFitter->GetMeanError() > 0.05) );
          //cout << "   pointN "<< pointN << "   "<<avg << "  fitmin="<<fitrmin << " _ "<< mean <<" _ "<< fitrmax << "  fitres="<< dEta << "   good="<< isGoodFit<< endl;

          if( !isGoodFit && (tmpHist->GetEffectiveEntries()>30 ) ){
            // if no good fit and enough stats, use the smooth mode calculation
            float mode[4];
            uniModalSmoothMax( *tmpHist, mode, 0.7) ;
            dEta = mode[0]; dEta_err = mode[1];
            isGoodFit = true;
          }
        }
        
        if( includeBadFits || isGoodFit) {
          m_R_vs_E_Graph->SetPoint(pointN,avg,dEta);
          m_R_vs_E_Graph->SetPointError(pointN,avgErr,dEta_err);
        }else {
          cout << " WARNING : bad fit in R vs E graph building. fitted mean ="<< m_respFitter->GetMean() << "  histo mean = "<< tmpHist->GetBinCenter(tmpHist->GetMaximumBin()) << endl;
        }
        delete tmpHist;
      } // not m_useMode
      

    } // loop over bins
    if( m_R_vs_E_Graph->GetN() < 2 ) {
      cout << " WARNING Empty Resp or Deta vs E graph !! Are you sure there is enough stats ? "<< std::endl;
      cout << " WARNING   input Hist is  "<< inputHist->GetName() << std::endl;
      cout << " WARNING   rebinned along E axis to "<< mergeList.size() /2<< " bins, using at least statCut="<< statCut << " entries per bin, from Emin="<< inputXCut << std::endl;
    }
    return m_R_vs_E_Graph;
  }



  //Three methods available for fitting:
  //1) polBestChi2 (Scans for the best Chi2ndof)
  //2) groom (Uses the groom function)
  //3) polBestChi2+groom (best Chi2ndof constrained by groom function)
  //Results stored in last entry of R_vs_ErecoFitVec  
  void ResponseFitter::fitRGraph(TGraphErrors *g, TString method, Double_t min, Double_t max) {
    TString myFormula;
    Int_t maxOrder=8; //FIXME
    
    Int_t exceptionPos = findEntryWithin(m_etaException,
                                         m_etaBins->binLowEdge(m_currentEtaBin),
                                         m_etaBins->binUpEdge(m_currentEtaBin) );
    cout << "... Fitting energy calibration curve"<< endl;

    double eta = m_etaBins->binCenter(m_currentEtaBin);

    int nPoints = g->GetN();
    if(nPoints==0){
      m_R_vs_E_Func = new TF1(Form("Const%d",m_currentEtaBin), "[0]", min, max);
      m_R_vs_E_Func->SetParameter(0, 0. );
      std::cout << " WARNING !! R vs E graph "<< g->GetTitle() << " empty "<< std::endl;
      return;      
    }else if(nPoints==1){      
      m_R_vs_E_Func = new TF1(Form("Const%d",m_currentEtaBin), "[0]", min, max);
      m_R_vs_E_Func->SetParameter(0, g->GetY()[0]);
      std::cout << " WARNING !! R vs E graph "<< g->GetTitle() << " contains only 1 point "<< std::endl;
      return;
    } else if(nPoints==2){
      m_R_vs_E_Func = new TF1(Form("Lin%d",m_currentEtaBin), "pol1", min, max);
      g->Fit(m_R_vs_E_Func,"Q");
      std::cout << " WARNING !! R vs E graph "<< g->GetTitle() << " contains only 2 points "<< std::endl;
      return;
    }


    if (method.Contains("groom")){
      cout << "... Fitting using the groom" << endl;		
      TString groomFormula="[0]*(1-[1]*pow(x/0.75,[2]-1))";
      TF1 *groom = new TF1(Form("groom%d",m_currentEtaBin), groomFormula, min, max);
      groom->SetParameters(1.0,0.9,0.7);
      g->Fit(groom,"RQEMN");
      m_R_vs_E_Func = groom;
    }



    if(exceptionPos==-1){
      cout << "... There are no eta exceptions"<< endl;	

      maxOrder = (Int_t)(m_JES_MaxOrderP1*exp(m_JES_MaxOrderP2*((double)nPoints))+m_JES_MaxOrderP3); //FIXME
    } else {
      cout << "... There are exceptions"<< endl;		
      maxOrder = (Int_t)m_JES_MaxOrderException.at(exceptionPos);
    }


    if (method=="polBestChi2"){
      cout << "... Fitting using the polBestChi2. min="<< min <<" max="<< max << " maxorder="<< maxOrder <<  endl;		
      //g->Print();
      BestOrderFit *myPolFit = new BestOrderFit(g, min, max, 0, maxOrder);
      m_R_vs_E_Func = myPolFit->getBestOrderTF1();      
      //      cout << m_R_vs_E_Func << "  par0="<<     m_R_vs_E_Func->GetParameter(0)<< endl;
      delete myPolFit;
    }


    
    if (method=="polBestChi2+groom"){	
      
      if (m_useBetterChi2 || m_useGeoDetCorr) {
        
        GroomExtrapolation *myGroomExt = new GroomExtrapolation(g, min, max, m_useGeoDetCorr, eta, m_minGeoDetCorr, m_maxGeoDetCorr);			
        
        //enable/disable groom extrapolation in low energy regions by setting min/min+0.01 for the fit
        //Function (Int_t)(-5.0*exp(-0.1*((double)nPoints))+7.) gives the maximum order as a function of the number
        //of data points - reduces order, needed to avoid oscillations and other phenomena.
        if(exceptionPos==-1){
          if(nPoints<m_minNDataPointsForMinExtrapolation) m_enableEMinExtrapolation=true;
        } else {
          m_enableEMinExtrapolation=m_enableEMinExtrapolationException.at(exceptionPos);
          m_enableEMaxExtrapolation=m_enableEMaxExtrapolationException.at(exceptionPos);
        }

        Double_t EminFit = m_enableEMinExtrapolation?min:(min+0.01);
        Double_t EmaxFit = max+1.;

        BestOrderFit *myPolFitGroom = new BestOrderFit(myGroomExt->getExtGraph(), EminFit, EmaxFit, 0, maxOrder);	
        BestOrderFit *mySecPolFitGroom = new BestOrderFit(myGroomExt->getExtGraph2(), EminFit, EmaxFit, 0, maxOrder); 

        //Double_t MinGroom = TMath::Abs(myGroomExt->getExtGraph()->GetY()[0]);
        //Double_t MinFit = myPolFitGroom->getBestOrderTF1()->Eval(myGroomExt->getExtGraph()->GetX()[0]);
        //Double_t YDeviationFitVSMinGroom = TMath::Abs(MinGroom-MinFit);
        //cout << "... The Y Deviation Fit vs MinGroom is: " << YDeviationFitVSMinGroom << endl;  

        double chi2 = myPolFitGroom->getBestOrderTF1()->GetChisquare();
        int Ndof = myPolFitGroom->getBestOrderTF1()->GetNDF();
        double Chi2 = chi2/Ndof;
        cout << "... Chi2 using 3 points on Groom is: " << Chi2 << endl; 

        double chi2_Sec = mySecPolFitGroom->getBestOrderTF1()->GetChisquare();
        int Ndof_Sec = mySecPolFitGroom->getBestOrderTF1()->GetNDF();
        double Chi2_Sec = chi2_Sec/Ndof_Sec;
        cout << "... Chi2 using 5 points on Groom is: " << Chi2_Sec << endl;                             

        if (m_useGeoDetCorr && (m_minGeoDetCorr<TMath::Abs(eta) && TMath::Abs(eta)<m_maxGeoDetCorr)) {

          cout << "... Will be considered the geometry detector correction" << endl;				

          BestOrderFit *myFullPolFitGroom = new BestOrderFit(myGroomExt->getExtGraph3(), EminFit, EmaxFit, 0, maxOrder);
          double chi2_Full = myFullPolFitGroom->getBestOrderTF1()->GetChisquare();
          int Ndof_Full = myFullPolFitGroom->getBestOrderTF1()->GetNDF();
          double Chi2_Full = chi2_Full/Ndof_Full;
          //cout << "... Chi2 using full re-fit on Groom is: " << Chi2_Full << endl;

          if ( (Chi2_Full<Chi2_Sec) && Chi2_Full<Chi2){
            //cout << "... Will be use full re-fit on Groom" << endl;                 				
            m_GroomExt_Func = myGroomExt->getTF1GroomForExt3();		         
            m_R_vs_E_Func = myFullPolFitGroom->getBestOrderTF1();
            m_R_vs_E_SLGraph = myGroomExt->getExtGraphSL3() ;

          } else if (Chi2_Sec<Chi2){
            //cout << "... Will be use 5 points on Groom" << endl;
            m_GroomExt_Func = myGroomExt->getTF1GroomForExt2();		         
            m_R_vs_E_Func = mySecPolFitGroom->getBestOrderTF1();
            m_R_vs_E_SLGraph = myGroomExt->getExtGraphSL2() ;

          } else { 
            //cout << "... Will be use 3 points on Groom" << endl;				
            m_GroomExt_Func = myGroomExt->getTF1GroomForExt();
            m_R_vs_E_Func = myPolFitGroom->getBestOrderTF1();
            m_R_vs_E_SLGraph = myGroomExt->getExtGraphSL() ;							          
          }				  

          // for now, I decided to avoid the YDeviation condition.   	  
        } else if (Chi2_Sec<Chi2){
          //cout << "... Will be use 5 points on Groom" << endl;
          m_GroomExt_Func = myGroomExt->getTF1GroomForExt2();		         
          m_R_vs_E_Func = mySecPolFitGroom->getBestOrderTF1();
          m_R_vs_E_SLGraph = myGroomExt->getExtGraphSL2() ;

        } else { 
          //cout << "... Will be use 3 points on Groom" << endl;				
          m_GroomExt_Func = myGroomExt->getTF1GroomForExt();
          m_R_vs_E_Func = myPolFitGroom->getBestOrderTF1();
          m_R_vs_E_SLGraph = myGroomExt->getExtGraphSL() ;							          
        }

        delete myGroomExt;
        delete myPolFitGroom;
        delete mySecPolFitGroom;

      } else {

        //current method... I didn't change anything
        //cout << "... Will be applied the current method " << endl;
        //		cout<<"points: "<<nPoints<<endl;	
        GroomExtrapolation *myGroomExt = new GroomExtrapolation(g, min, max);			
        m_GroomExt_Func = myGroomExt->getTF1GroomForExt();

        //enable/disable groom extrapolation in low energy regions by setting min/min+0.01 for the fit
        //Function (Int_t)(-5.0*exp(-0.1*((double)nPoints))+7.) gives the maximum order as a function of the number
        //of data points - reduces order, needed to avoid oscillations and other phenomena.
        if(exceptionPos==-1){
          if(nPoints<m_minNDataPointsForMinExtrapolation) m_enableEMinExtrapolation=true;
        } else {
          m_enableEMinExtrapolation=m_enableEMinExtrapolationException.at(exceptionPos);
          m_enableEMaxExtrapolation=m_enableEMaxExtrapolationException.at(exceptionPos);
        }

        BestOrderFit *myPolFitGroom = new BestOrderFit(myGroomExt->getExtGraph(), m_enableEMinExtrapolation?min:(min+0.01), max+1.,0 , maxOrder);
        if (TMath::Abs(myGroomExt->getExtGraph()->GetY()[0]-myPolFitGroom->getBestOrderTF1()->Eval(myGroomExt->getExtGraph()->GetX()[0])) < m_maxAcceptableYDeviationFitVSMinGroom){
          m_R_vs_E_Func = myPolFitGroom->getBestOrderTF1();
          m_R_vs_E_SLGraph = myGroomExt->getExtGraphSL() ;
        } else {
          if(exceptionPos==-1){
            m_enableEMinExtrapolation=true;
          } else {
            m_enableEMinExtrapolation=m_enableEMinExtrapolationException.at(exceptionPos);
          }
          m_enableEMinExtrapolation=false; //MAX MOD disable this sucker
          BestOrderFit *mySecPolFitGroom = new BestOrderFit(myGroomExt->getExtGraph(), m_enableEMinExtrapolation?min:(min+0.01), m_enableEMaxExtrapolation?(max+1.):g->GetX()[nPoints-1],0 , maxOrder);
          m_R_vs_E_Func = mySecPolFitGroom->getBestOrderTF1();
          m_R_vs_E_SLGraph = myGroomExt->getExtGraphSL() ;
        }				
      }

      double chi2 = m_R_vs_E_Func->GetChisquare();
      int Ndof = m_R_vs_E_Func->GetNDF();//R_vs_ErecoFitVec->back()->GetNDF();

      cout << "... E fit has as chi^2/Ndof = " << chi2/Ndof << endl;        

    } // end if method.Contains("polBestChi2+groom")) 
    

  }


  void ResponseFitter::fitDeltaEtagGraph(TGraphErrors *g, TString method, Double_t min, Double_t max) {
    TString myFormula;
    cout << "... Fitting delta eta correction curve" << endl;	
    
    LinExtrapolation myLinExt(g,min,max, "[0]");
    BestOrderFit myPolFit(myLinExt.getExtGraph(), min, max, 0 , (Int_t)(m_etaCorr_MaxOrderP1*exp(m_etaCorr_MaxOrderP2*((double)g->GetN()))+m_etaCorr_MaxOrderP3));

    // DEta_vs_EtrueFitVec->push_back(myPolFit.getBestOrderTF1());
    // DEta_vs_EtrueGraphSLVec->push_back(myLinExt.getExtGraphSL());
    m_DEta_vs_E_Func = myPolFit.getBestOrderTF1();
    m_DEta_vs_E_SLGraph = myLinExt.getExtGraphSL();
    
    // double chi2 = m_DEta_vs_EtrueFit->GetChisquare(); 
    // int Ndof = m_DEta_vs_EtrueFit->GetNDF(); 
    // cout << "... Eta fit has as chi^2/Ndof = " << chi2/Ndof << endl;
    // cout << "..." << endl;	 	
    
  
  }





  // Calculate the error-weighted average of the bins in [low,high] from a TProfile
  void ResponseFitter::getAvg(TProfile *inputProfile, int low, int high, double &avg, double &avg_err){
    if (inputProfile==NULL) { printf("Error in energyCalib::getAvg -> TProfile = NULL"); abort();};

    //Calc average
    double sum_wy=0, sum_w=0;
    double sum_w_pos=0;
    for (int i=low; i<=high;++i) {
      double y=inputProfile->GetBinContent(i), err=inputProfile->GetBinError(i);
      if (err!=0){
        double inv_err2 = 1./err /err;
        sum_w+=inv_err2; sum_wy+=y * inv_err2;
        if(err>0) sum_w_pos += inv_err2;
      }
    }
    avg = sum_w>0?sum_wy/sum_w:0;
    avg_err = sum_w_pos>0? 1.0/sqrt(sum_w_pos):0;
  }


  double ResponseFitter::getEffectiveEntries(TH1* h2, int lowbinX, int highbinX){
    TAxis * ax = h2->GetXaxis();
    ax->SetRange(lowbinX, highbinX);
    double ne = h2->GetEffectiveEntries();
    ax->SetRange();
    return ne;    
  }

}
