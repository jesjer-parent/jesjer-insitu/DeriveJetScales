#include "DeriveJetScales/FineBinRespBuilder.h"
#include "DeriveJetScales/JMSCombination.h"
#include <iostream>

#include <cmath>

namespace DJS {


  // JMS steps --------------
  bool FineBinRespBuilder::jmsRvsEtrue(int i){
    JetData j = m_inputReader->readCalibJetData(i);

    if(! j.jms_valid ) return true;

    if(m_noMassNumInv) m_jmsWBins->initJet(j.e_reco, j.m_reco, j.eta_det);
    else m_jmsWBins->initJet(j.e_true, j.m_true, j.eta_det);
    if(! m_jmsWBins->validBin() ) return true;    
    
    double mResp = j.m_reco / j.m_true;
    
    //std::cout << "Fill resp at ( "<< j.e_true<< "/"<<j.e_reco<<" , "<< ")  "<< m_jmsWBins->m_binning->m_bin << std::endl;
    int binx;

    DJS::HistoClones & mR_in_EMEta = m_noMassNumInv ? m_objects->mR_in_EMEtanuminv : m_objects->mR_in_EMEtatrue ;


    if( ! mR_in_EMEta.histoBin(mResp, binx) ) return true;
    //std::cout <<" ---------> resp bin =" << binx << "  "<< mResp<< std::endl;
    for(WeightedBinFinder::WeightedBin b : m_jmsWBins->findBins( ) ){
      double w = b.w * j.weight;
      mR_in_EMEta.fillAtBin(b.index, binx, w);                
    }        
    
    return true;
  }

  
  bool FineBinRespBuilder::jmsRvsNuminv(int i){
    JetData j = m_inputReader->readCalibJetData(i);

    if(!j.jms_valid) return true;

    double resp=m_jmsTrueBin->jmsResponse(j.e_true, j.m_true, j.eta_det);
    
    m_jmsWBins->initJet(j.e_reco, j.m_true*resp, j.eta_det);
    if(! m_jmsWBins->validBin() ) return true;    


    double mResp = j.m_reco / j.m_true;
    int binx;
    if( ! m_objects->mR_in_EMEtatrue.histoBin(mResp, binx) ) return true;
    for(WeightedBinFinder::WeightedBin b : m_jmsWBins->findBins() ){
      double w = b.w * j.weight;
      if( std::isnan(w) ) { std::cout << j.e_reco << "  "<< j.m_reco << "  ; "<< b.index <<" w="<< b.w << "  _ "<< j.weight<< std::endl; return true;}
      
      m_objects->mR_in_EMEtanuminv.fillAtBin(b.index, binx, w);                
    }        
    
    return true;
  }

  
  bool FineBinRespBuilder::jmsClosure(int i){
    JetData j = m_inputReader->readCalibJetData(i);

    if(! j.jms_valid ) return true;
    

    double pt_true = TMath::Sqrt(j.e_true*j.e_true - j.m_true*j.m_true)/TMath::CosH(j.eta_true);

    double jmsCorr = m_jmsRecoBin->jmsCorrection(j.e_reco, j.m_reco, j.eta_det);
    double mResp =j.m_reco/j.m_true;        
    int incbin = m_objects->mR_in_inclPtEtatrue.m_phaseSpace->findBin( pt_true, j.m_true, std::fabs(j.eta_det) );
    
    if( (incbin>-1) && (j.m_reco> m_closuremassCut) ) {
      m_objects->mR_in_inclPtEtatrue[incbin]->Fill(mResp, j.weight );
      m_objects->closure_mR_in_inclPtEtatrue[incbin]->Fill(mResp*jmsCorr, j.weight );
    }
    
    mResp *= jmsCorr;
    int binx;
    if( ! m_objects->mR_in_EMEtareco.histoBin(mResp, binx) ) return true;
    
    
    // Fill responses in true bins : closure plots
    m_jmsWBins->initJet(j.e_true, j.m_true, j.eta_det);
    if(! m_jmsWBins->validBin() ) return true;    
    
    //std::cout << "   at " <<j.e_reco << " ,"<<  j.m_reco<< ", "<<  j.eta_det << " ---> "<< jmsCorr << "   "<< incbin << " true: "<< pt_true << ", " << j.m_true <<" ," << binx<<std::endl;
    for(WeightedBinFinder::WeightedBin b : m_jmsWBins->findBins() ){
      double w = b.w * j.weight;
      //std::cout << "     "<<b.index << ",, " << w << std::endl;
      m_objects->closure_mR_in_EMEtatrue.fillAtBin(b.index, binx, w);
    }        

    
    // Fill responses in reco bins : used to compute resolution (then used in mass combination weights)
    m_jmsWBins->initJet(j.e_reco, j.m_reco*jmsCorr, j.eta_det);
    if(! m_jmsWBins->validBin() ) return true;    
    
    //std::cout << "   at " <<j.e_reco << " ,"<<  j.m_reco<< ", "<<  j.eta_det << " ---> "<< jmsCorr << "   "<< incbin << " true: "<< pt_true << ", " << j.m_true <<" ," << binx<<std::endl;
    for(WeightedBinFinder::WeightedBin b : m_jmsWBins->findBins() ){
      double w = b.w * j.weight;
      //std::cout << "     "<<b.index << ",, " << w << std::endl;
      m_objects->mR_in_EMEtareco.fillAtBin(b.index, binx, w);
    }        
    
    return true;
  }



  
  bool FineBinRespBuilder::jmsClosureMassComb(int i){
    JetData j = m_inputReader->readCalibJetData(i);

    if(! j.jms_valid ) return true;

    double pt_true = TMath::Sqrt(j.e_true*j.e_true - j.m_true*j.m_true)/TMath::CosH(j.eta_true);
    
    double mComb = m_jmsCombination->combinedMass(j);
    double mResp = mComb/j.m_true;        
    int incbin = m_objects->mR_in_inclPtEtatrue.m_phaseSpace->findBin( pt_true, j.m_true, std::fabs(j.eta_det) );

    if(incbin>-1) {
      m_objects->mR_in_inclPtEtatrue[incbin]->Fill(j.m_reco/j.m_true, j.weight );
      m_objects->closure_mR_in_inclPtEtatrue[incbin]->Fill(mResp, j.weight );
    }

    
    return true;
  }


}
