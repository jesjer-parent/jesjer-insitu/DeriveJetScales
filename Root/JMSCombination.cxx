#include "DeriveJetScales/JMSCombination.h"
#include "DeriveJetScales/JMSBinning.h"

namespace DJS {



  double JMSCombination::combinedMass(double e, double mCalo, double mTA, double eta){
    // case of 0 mass :
    if(mCalo<1) return mTA;
    if(mTA<1) return mCalo;
    
    m_mCaloCalib->initJet(e, mCalo, eta);
    double jmsCorr = m_mCaloCalib->jmsCorrection();
    double sigma   = m_mCaloCalib->jmsSigma();

    m_mTACalib->initJet(e, mTA, eta);
    double jmsCorr_mta = m_mTACalib->jmsCorrection();    
    double sigma_mta   = m_mTACalib->jmsSigma();

    double w_norm = (sigma*sigma + sigma_mta*sigma_mta);
    double w_calo = sigma_mta*sigma_mta/w_norm;
    double w_mta = (1-w_calo); // == sigma*sigma/w_norm;

    // std::cout << "calo corr "<< jmsCorr << "  _  sigma ="<<sigma << std::endl;
    // std::cout << "mta corr "<< jmsCorr_mta << "  _  sigma ="<<sigma_mta << std::endl;
    // std::cout << "  w_calo ="<< w_calo << "  , w_mta="<< w_mta<<std::endl;
    return (jmsCorr*mCalo*w_calo+jmsCorr_mta*mTA*w_mta);
    
  }



}
