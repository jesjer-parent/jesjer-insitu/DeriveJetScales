#include "DeriveJetScales/ResponseBuilder.h"
#include "DeriveJetScales/JMSCombination.h"

#include "TTree.h"
#include "TBranch.h"
#include "TH2F.h"
#include "TProfile.h"
#include "TF1.h"

#include "TMath.h"

#include <iostream>
//#include "valgrind/callgrind.h"

using std::cout;
using std::endl;

namespace DJS {



  
#define SETBR( bname, vec, bvec) tree->SetBranchStatus(bname.c_str(), 1); tree->SetBranchAddress(bname.c_str(), & vec, &bvec)

  bool ResponseBuilder::processTree(TTree * tree){

    // Setup the JES correction part. We copy the TF1 function read from
    // our collection of object into m_jesCorr.
    cout << "___ "<< tree->GetEntries() << "   "<< m_inputReader<< endl; 
    m_jesCorr.m_jesFactorFunc = m_objects->R_vs_Enuminv_Func;
    m_jesCorr.m_dEtaFunc      = m_objects->DEta_vs_Etrue_Func;

    // Setup the JMS correction and binning system : we share the phasespace object
    if(m_jmsTrueBin->m_bps==nullptr) m_jmsTrueBin->m_bps = m_jmsBPS;
    if(m_jmsRecoBin->m_bps==nullptr) m_jmsRecoBin->m_bps = m_jmsBPS;
    if(m_jmsCombination!=nullptr) {
      if(m_jmsCombination->m_mCaloCalib->m_bps==nullptr) m_jmsCombination->m_mCaloCalib->m_bps=m_jmsBPS;
      if(m_jmsCombination->m_mTACalib->m_bps==nullptr) m_jmsCombination->m_mTACalib->m_bps=m_jmsBPS;
    }
    
    // Setup the input reading part
    m_inputReader->m_jesEtaBins = m_etaBins;
    m_inputReader->m_jesCorr = &m_jesCorr;
    if( ! m_inputReader->init( tree ) ) {
      std::cout << " ERROR while initializing input reader."<< std::endl;
      return false;
    }
    
    // calculate the last event to be processed
    int N = tree->GetEntries();
    cout << " firstEvt = "<< m_firstEvent << "  Ntot="<< N << endl;
    int last = m_firstEvent+ (m_maxEvents>-1 ? m_maxEvents : N);
    if( last > N) last = N;


    //cout<< " Starting loop over "<< (last-m_firstEvent) << " events. from  "<< m_firstEvent  << "  to  "<< last << endl;

    //------------------------------------
    // The Loop over events :
    for (int i=m_firstEvent;i<last;++i) {
      //CALLGRIND_START_INSTRUMENTATION;
      m_inputReader->loadEntry(i);
      m_currentEntry = i;
      //cout << ii << " Event "<< i << "  size = "<< m_eta_true->size() << endl;
      bool res = processEvent( );
      //CALLGRIND_STOP_INSTRUMENTATION;
      
      if(!res) {cout<< "Error while processing event "<< i << endl; return res;}
      
      if(i%1000000 == 0) cout<< "      Events processed : "<< i << " / "<< N << endl; 
    }

    return true;
  }


  bool ResponseBuilder::processEvent(){ 

    if (!m_inputReader->jetDataConsistency() ) {
      std::cout << "ERROR  reco and true size differ !" << endl;
      return false;
    }
    
    for (unsigned int i=0; i<std::min(m_numJetsMax,m_inputReader->nJets());i++) {
      switch(m_mode){
      case EtaJES_RvsEtrue:    etajesRvsEtrue(i); break;
      case EtaJES_RvsEnuminv: etajesRvsEnuminv(i);break;
      case EtaJES_Closure:     etajesClosure(i); break;
      case EtaJES_Closure_JMS_RvsEtrue:{
        etajesClosure(i);
        jmsRvsEtrue(i);break;
      }
      case JMS_RvsEtrue: jmsRvsEtrue(i);break;
      case JMS_RvsNuminv: jmsRvsNuminv(i);break;
      case JMS_Closure: jmsClosure(i);break;
      case JMS_ClosureMassComb: jmsClosureMassComb(i);break;
      }
      validation(i);
    }

    return true;
  }
  
  bool ResponseBuilder::etajesRvsEtrue(int i){ 
    JetData j = m_inputReader->readJetData(i);

    if(!j.jes_valid) return true; // out of range

    int ieta = j.jes_ieta;
    double mu = m_inputReader->m_mu;
    m_objects->R_vs_Etrue[ieta]->Fill( j.e_true, j.e_reco/j.e_true, j.weight);
    m_objects->R_vs_Ereco[ieta]->Fill(j.e_reco, j.e_reco/j.e_true, j.weight);
    m_objects->R_vs_mu[ieta]->Fill(mu, j.e_reco/j.e_true, j.weight);
    m_objects->Etrue_vs_Etrue[ieta]->Fill(j.e_true,j.e_true, j.weight);
    m_objects->Ereco_vs_Ereco[ieta]->Fill(j.e_reco,j.e_reco, j.weight);
    m_objects->Ereco_vs_Etrue[ieta]->Fill(j.e_true,j.e_reco,j.weight);
    m_objects->DEta_vs_Etrue[ieta]->Fill( j.e_true, j.eta_reco - j.eta_true, j.weight);
    
    return true;

  }





  bool ResponseBuilder::etajesRvsEnuminv(int i){ 
    JetData j = m_inputReader->readJetData(i);

    if(!j.jes_valid) return true; // out of range

    int ieta = j.jes_ieta;

    double e_numinv = j.e_true* m_objects->R_vs_Etrue_Func[ieta]->Eval(j.e_true);
        
    m_objects->R_vs_Enuminv[ieta]->Fill( e_numinv, j.e_reco/j.e_true, j.weight);
    m_objects->Enuminv_vs_Etrue[ieta]->Fill( j.e_true, e_numinv, j.weight); 
    
    return true;
  }


  bool ResponseBuilder::etajesClosure(int i){ 
    JetData j = m_inputReader->readCalibJetData(i);

    if(!j.jes_valid) return true; // out of range

    int ieta = j.jes_ieta;

    double pt_true = TMath::Sqrt(j.e_true*j.e_true - j.m_true*j.m_true)/TMath::CosH(j.eta_true);

    
    // Fill closure Histos
    m_objects->closure_R_vs_Etrue[ieta]->Fill( j.e_true, j.e_reco/j.e_true, j.weight);
    m_objects->closure_DEta_vs_Etrue[ieta]->Fill( j.e_true, j.eta_reco - j.eta_true , j.weight);
    // also fill histos vs pt 
    m_objects->closure_R_vs_pTtrue[ieta]->Fill( pt_true, j.e_reco/j.e_true, j.weight);
    m_objects->R_vs_pTtrue[ieta]->Fill( pt_true, j.e_reco/j.e_true, j.weight);
    m_objects->pTtrue_vs_pTtrue[ieta]->Fill( pt_true, pt_true, j.weight);
    
    return true;
  }  


  bool ResponseBuilder::jmsRvsEtrue(int i){ 

    JetData j = m_inputReader->readCalibJetData(i);

    if(!j.jms_valid) return true; // out of range
    
    m_jmsTrueBin->initJet(j.e_true, j.m_true, j.eta_det);

    //    cout << " true bin  :  "<< m_jmsTrueBin->m_var1 << "   "<< m_jmsTrueBin->m_var2 << endl;
    if(! m_jmsTrueBin->validBin() ) return true;
    
    //std::cout << "xx "<< bin << "  "<< m_recoCorr << "  "<< e_recoCorr << "  | "<< m_j.weight<<std::endl;
    int bin = m_jmsTrueBin->m_bin;
    m_objects->mR_in_PtEtaMtrue[bin]->Fill( j.m_reco/j.m_true, j.weight );
    //m_objects->m_in_PtEtaMtrue[bin].add( pt_var_true, m_var_true, m_evntWeight ); not used anymore ??


    if(m_noMassNumInv){
      // then also fill mR_in_PtEtaMnuminv right now
      m_jmsRecoBin->initJet(j.e_reco, j.m_reco, j.eta_det);

      if(! m_jmsRecoBin->validBin() ) return true;

      int bin = m_jmsRecoBin->m_bin;
      m_objects->mR_in_PtEtaMnuminv[bin]->Fill( j.m_reco/j.m_true, j.weight );
    } 
   
    return true;
  }

  
  bool ResponseBuilder::jmsRvsNuminv(int i){ 

    JetData j = m_inputReader->readCalibJetData(i);

    if(!j.jms_valid) return true; // out of range

    m_jmsTrueBin->initJet(j.e_true, j.m_true, j.eta_det);
    if(! m_jmsTrueBin->validBin() ) return true;
    

    int ieta_jms = m_jmsBPS->axis(2)->findBin( std::fabs(j.eta_det) );
    if(!m_jmsBPS->axis(2)->validBin(ieta_jms)) return true; // out of range


    // numerical inversion of mass    
    double jmsF = 1;
    float v1 = m_jmsBPS->axis(0)->inRange( m_jmsTrueBin->m_var1 );
    float v2 = m_jmsBPS->axis(1)->inRange( m_jmsTrueBin->m_var2 );
    int bin = m_objects->mRsmooth_vs_PtMtrue_in_Eta[ieta_jms]->FindBin( v1, v2 );
    if( !m_objects->mRsmooth_vs_PtMtrue_in_Eta[ieta_jms]->IsBinUnderflow(bin) && 
        !m_objects->mRsmooth_vs_PtMtrue_in_Eta[ieta_jms]->IsBinOverflow(bin) ){
      jmsF = m_objects->mRsmooth_vs_PtMtrue_in_Eta[ieta_jms]->Interpolate(v1, v2);
    }

    m_jmsRecoBin->initJet( j.e_reco, j.m_true*jmsF, j.eta_det);

    if(! m_jmsRecoBin->validBin() ) return true;
    if(std::isnan(m_jmsRecoBin->m_var1)) m_jmsRecoBin->m_var1=0;// can happen when m_numinv is overestimated
    
    // Fill histo at numinv bin 
    int bin_reco = m_jmsRecoBin->m_bin;
    m_objects->mR_in_PtEtaMnuminv[bin_reco]->Fill( j.m_reco/j.m_true, j.weight );

    return true;
  }


  bool ResponseBuilder::jmsClosure(int i){ 

    JetData j = m_inputReader->readCalibJetData(i);

    if(!j.jms_valid) return true; // out of range

    // if we use pre-jms : add it now.
    //if(m_jmsPreCorr) m_recoCorr *= jmsCorrection(e_recoCorr, m_recoCorr, eta_Corr, true);
    
    m_jmsRecoBin->initJet(j.e_reco, j.m_reco, j.eta_det);
    
    double jmsCorr = m_jmsRecoBin->jmsCorrection();
    double mResp = j.m_reco*jmsCorr/j.m_true;


    // Fill  histos in (pt, mass window) -----
    double pt_true = TMath::Sqrt(j.e_true*j.e_true - j.m_true*j.m_true)/TMath::CosH(j.eta_true);
    int incbin = m_objects->closure_mR_in_inclPtEtatrue.m_phaseSpace->findBin( pt_true, j.m_true, std::fabs(j.eta_det) );
    
    if(incbin>-1) {
      m_objects->closure_mR_in_inclPtEtatrue[incbin]->Fill(mResp, j.weight );
      m_objects->mR_in_inclPtEtatrue[incbin]->Fill(j.m_reco/j.m_true, j.weight );
    }
    // ----------------

    m_jmsTrueBin->initJet(j.e_true, j.m_true, j.eta_det);
    if(! m_jmsTrueBin->validBin() ) return true;    

    // fill the calibrated jms;
    m_objects->closure_mR_in_PtEtaMtrue[m_jmsTrueBin->m_bin]->Fill( mResp, j.weight );
    return true;
  }

  
  
}
