#include "DeriveJetScales/JMSBinning.h"
#include "TMath.h"
#include "TH3F.h"

#include "DeriveJetScales/InterpolationHelpers.h"

namespace DJS {


  void initMinMax(TAxis *ax, double &xmin,double &xmax) {
    xmin = ax->GetBinLowEdge(1);
    xmax = ax->GetBinUpEdge(ax->GetNbins());
    xmin = std::nextafter(xmin, xmax); // make sure xmin is strictly within the bounds
    xmax = std::nextafter(xmax, xmin); // make sure xmax is strictly within the bounds
  }
  
  void JMSBinning::initCorrections(TH3F* h3){
    if(h3!=nullptr) m_correctionFactors3d = h3; // overwrite m_correctionFactors3d
    // else assume we already have a valid pointer
    // init our min and max according to the TH3F axis.
    initMinMax(m_correctionFactors3d->GetXaxis(), m_xmin, m_xmax);
    initMinMax(m_correctionFactors3d->GetYaxis(), m_ymin, m_ymax);
    initMinMax(m_correctionFactors3d->GetZaxis(), m_zmin, m_zmax);
  }

  void JMSBinning::initSignificance(TH3F* h3_1, TH3F* h3_2){
    if(h3_1!=nullptr) m_correctionFactors3d = h3_1;
    initMinMax(m_correctionFactors3d->GetXaxis(), m_xmin, m_xmax);
    initMinMax(m_correctionFactors3d->GetYaxis(), m_ymin, m_ymax);
    initMinMax(m_correctionFactors3d->GetZaxis(), m_zmin, m_zmax);
    if(h3_2!=nullptr) m_sigmas3d = h3_2;
    initMinMax(m_sigmas3d->GetXaxis(), m_xmin, m_xmax);
    initMinMax(m_sigmas3d->GetYaxis(), m_ymin, m_ymax);
    initMinMax(m_sigmas3d->GetZaxis(), m_zmin, m_zmax);
  }

  double JMSBinning::jmsResponse(){
    double jms =1;
    double v1 = m_var1 < m_xmax ? m_var1 : m_xmax; v1 = v1 < m_xmin  ? m_xmin : v1;
    double v2 = m_var2 < m_ymax ? m_var2 : m_ymax; v2 = v2 < m_ymin  ? m_ymin : v2;
    double v3 = std::fabs(m_eta);
    v3 = v3 < m_zmax ? v3 : m_zmax; v3 = v3 < m_zmin  ? m_zmin : v3;
    jms=IPH::Interpolate(m_correctionFactors3d, v1, v2, v3 );
    //std::cout << " interpolated at "<< v1 <<", "<< v2 <<", "<<v3 <<" --> "<< jms<< ""<< std::endl;
    return jms;
  }

  double JMSBinning::jmsSignificance(){
    double significance = 1.;
    double closuremR = 1.;
    double closuremIQR = 1.;
    double v1 = m_var1 < m_xmax ? m_var1 : m_xmax; v1 = v1 < m_xmin  ? m_xmin : v1;
    double v2 = m_var2 < m_ymax ? m_var2 : m_ymax; v2 = v2 < m_ymin  ? m_ymin : v2;
    double v3 = std::fabs(m_eta);
    v3 = v3 < m_zmax ? v3 : m_zmax; v3 = v3 < m_zmin  ? m_zmin : v3;
    closuremR =IPH::Interpolate(m_correctionFactors3d, v1, v2, v3 );
    closuremIQR =IPH::Interpolate(m_sigmas3d, v1, v2, v3 );
    significance = (1. - closuremR) / closuremIQR;
    return significance;
}
  
  double JMSBinning::jmsSigma(){
    double jms =1;
    double v1 = m_var1 < m_xmax ? m_var1 : m_xmax; v1 = v1 < m_xmin  ? m_xmin : v1;
    double v2 = m_var2 < m_ymax ? m_var2 : m_ymax; v2 = v2 < m_ymin  ? m_ymin : v2;
    double v3 = std::fabs(m_eta);
    v3 = v3 < m_zmax ? v3 : m_zmax; v3 = v3 < m_zmin  ? m_zmin : v3;
    jms=IPH::Interpolate(m_sigmas3d, v1, v2, v3 );
    //std::cout << " interpolated at "<< v1 <<", "<< v2 <<", "<<v3 <<" --> "<< jms<< ""<< std::endl;
    return jms;
  }

  
  double JMSBinning::jmsResponse_noEtaInt(){
    double jms =1;
    double v1 = m_var1 < m_xmax ? m_var1 : m_xmax; v1 = v1 < m_xmin  ? m_xmin : v1;
    double v2 = m_var2 < m_ymax ? m_var2 : m_ymax; v2 = v2 < m_ymin  ? m_ymin : v2;
    double v3 = std::fabs(m_eta);
    v3 = v3 < m_zmax ? v3 : m_zmax; v3 = v3 < m_zmin  ? m_zmin : v3;
    int beta = m_correctionFactors3d->GetZaxis()->FindBin(v3);
    jms=IPH::Interpolate2D(m_correctionFactors3d, v1, v2, 1,2, beta );
    return jms;
  }

  double JMSBinning::jmsResponse_noInt(){
    double v1 = m_var1 < m_xmax ? m_var1 : m_xmax; v1 = v1 < m_xmin  ? m_xmin : v1;
    double v2 = m_var2 < m_ymax ? m_var2 : m_ymax; v2 = v2 < m_ymin  ? m_ymin : v2;
    double v3 = std::fabs(m_eta);
    v3 = v3 < m_zmax ? v3 : m_zmax; v3 = v3 < m_zmin  ? m_zmin : v3;
    int b = m_correctionFactors3d->FindBin(v1,v2,v3);
    std::cout << " response no int at "<< v1<< ", "<< v2<< " , "<< v3 << " :  "<< b <<" : "<< std::endl;
    return m_correctionFactors3d->GetBinContent(b);
  }



  void JMSBinning::fillRespTH2(TH2 & h2, float eta){

    TAxis *eAxis = h2.GetXaxis();
    TAxis *mAxis = h2.GetYaxis();
    for(int i=1; i<=eAxis->GetNbins();i++){
      for(int j=1; j<=mAxis->GetNbins();j++){
        int b = h2.GetBin(i,j);
        float r = jmsResponse_noEtaInt(eAxis->GetBinCenter(i),mAxis->GetBinCenter(j), eta) ;
        h2.SetBinContent(b, r);
      }
    }    
  }



  
  
  bool PtMBinning::initJet(double e, double m, double eta) {
    m_eta = eta;
    m_var1  = TMath::Sqrt(e*e - m*m)/TMath::CosH(eta); // pt
    m_var2 = m;
    initBins(eta);
    return validBin();
  }

  bool EMBinning::initJet(double e, double m, double eta) {
    m_eta = eta;
    m_var1  = e;
    m_var2 = m;
    initBins(eta);
    return validBin();
  }
  
  bool ELogMovEBinning::initJet(double e, double m, double eta) {
    m_eta = eta;
    m_var1  = e; 
    m_var2 = TMath::Log(m/e);
    initBins(eta);
    return validBin();
  }

  bool MLogMovEBinning::initJet(double e, double m, double eta) {
    m_eta = eta;
    m_var1  = m; 
    m_var2 = TMath::Log(m/e);
    initBins(eta);
    return validBin();
  }
  
  bool EtLogMovEtBinning::initJet(double e, double m, double eta) {
    m_eta = eta;
    double ceta = TMath::CosH(eta);
    m_var1  = e/ceta; 
    m_var2 = TMath::Log(m/m_var1);
    initBins(eta);
    return validBin();
  }

    



}
