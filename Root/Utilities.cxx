#include "DeriveJetScales/Utilities.h"
#include "TColor.h"

ClassImp(DJS::VecStats)

namespace DJS {


  float invDensityEstimation( TH1& h, int bin, float hintegral, float heffEntries, float size_default ){
    /// return 1/f(bin) where f is the density proba represented by h.
    /// Take the lowest estimate of f, so 1/f is maximized and can be used conservatively below.

    float binCont = h.GetBinContent(bin);
    float fq= size_default;
    if(binCont!=0.){ 
      float binCont_eff = h.GetBinError(bin)*h.GetBinError(bin) / (binCont*binCont);
      // stat error. Use effective entries to be compatible with weighted entries.
      float binCont_eff_err = TMath::Sqrt( binCont_eff*(1 - binCont_eff /heffEntries ) );
      // the relative error on bin content is thus : binCont_eff_err/binCont_eff
      // we take the (nominal - error) to have the lowes estimates
      binCont = binCont* (1 - binCont_eff_err/binCont_eff) ;
      fq = 1./( binCont / hintegral / h.GetBinWidth(bin) );
    }
    return fq;

  }

  TGraph2DErrors * numInvYaxis(TH2F& h2){
    int nx = h2.GetNbinsX();
    int ny = h2.GetNbinsY();

    TGraph2DErrors * g = new TGraph2DErrors(nx*ny);

    int p=0;
    for(int i=1;i<nx+1;i++){
      double x = h2.GetXaxis()->GetBinCenter(i);
      for(int j=1;j<ny+1;j++){
        double y = h2.GetYaxis()->GetBinCenter(j);
        double r = h2.GetBinContent(i,j);
        y *= r;
        g->SetPoint(p, x,y,r);
        g->SetPointError(p, 0,0,h2.GetBinError(i,j));
        p++;
      }
    }

    g->SetDirectory(0);
    g->SetName(Form("numinv_%s", h2.GetName()) );
    return g;
  }

  TGraph2DErrors * numInvYaxis(TGraph2DErrors& g2){
    int npoints = g2.GetN();

    TGraph2DErrors * g = new TGraph2DErrors(npoints);

    for(int i=0;i<npoints;i++){
      double x = g2.GetX()[i];
      double y = g2.GetY()[i];
      double r = g2.GetZ()[i];
      y *= r;
      g->SetPoint(i, x,y,r);
      g->SetPointError(i, 0,0,g2.GetEZ()[i]);
    }

    g->SetDirectory(0);
    g->SetName(Form("numinv_%s", g2.GetName()) );
    return g;
  }

  
  void fillEmptyBin(TH2& h, bool reverse){

    int nbinX = h.GetNbinsX();
    int nbinY = h.GetNbinsY();
    int nempty=0;
    int nTot= h.GetNcells();
    for(int bi=0;bi<nTot;bi++) {
      int b = reverse ? nTot-1-bi: bi;
      if ( h.IsBinUnderflow(b) || h.IsBinOverflow(b) ) continue;
      if(h.GetBinContent(b)>0) continue;
      nempty++;
      int i0,j0,k0; h.GetBinXYZ(b,i0,j0,k0);
      int nn = 0;
      float m=0;
      for(int i=i0-1;i<i0+2;i++){
        for(int j=j0-1;j<j0+2;j++){
          if( (i<=0) or (i>nbinX)) continue;
          if( (j<=0) or (j>nbinY)) continue;
          float c = h.GetBinContent(i,j);
          if(c>0){ m += c; nn++; }          
        }
      }
      if(nn>0){
        h.SetBinContent(b,m/nn);
        nempty--;
      }
    }
    if(nempty == (nbinY*nbinX) ) {
      std::cout<< " DJS::fillEmptyBin( ) : WARNING input histos fully empty "<< std::endl;
      return;
    }
    if(nempty>0) fillEmptyBin(h, !reverse);
  }

  
  void calcMedIQR(TH1& h, float arr[4]){

    // error estimate :
    // Median variance is : 1/(4 n f(q1)^2 ) 
    // Thus IQR sqrt(variance) is :
    // sqrt(a(1-a)/N) *sqrt( 1/f(q0)^2 + 1/f(q2)^2) 
    // where f(q0) = density function at q0
    // Evaluation of f(q0) is difficult.
    //  the estimation  GetBinContent(q0)/bin_size can be very wrong at low stat.
    //  Thus we estimate an error on this estimation and choose the worst case
    //  so the errors on med and iqr are conservative.
    
    //if h.GetBinContent(1) !=0 : h.SetBinContent(1,0)
    float n = h.GetEntries();
    if (n==0. ) {
      arr[0] = 0;
      arr[1] = 1;
      arr[2] = 0;
      arr[3] = 1;
      return ;
    }
    double q_pos[3] = {0.16, 0.5, 0.84};
    double q[3] = {0,0,0};
    h.GetQuantiles(3,q,q_pos);
    
    // find 1st non void bin
    int i0=1;
    for(;i0<h.GetNbinsX();i0++){
      if(h.GetBinContent(i0)>0) break;
    }
    // find last non void bin
    int i1=h.GetNbinsX();
    for(;i1>0;i1--){
      if(h.GetBinContent(i1)>0) break;
    }
    double size_tot = ( h.GetBinCenter(i1) - h.GetBinCenter(i0)) ; 

    int first = h.GetXaxis()->GetFirst();
    int last = h.GetXaxis()->GetLast();
    h.GetXaxis()->SetRange(i0-1,i1+1);

    float i = h.Integral();
    float i_eff = h.GetEffectiveEntries();

    if (size_tot==0.) {
      arr[0] = q[1];
      arr[1] = q[1];
      arr[2] = (q[2]-q[0])*0.5;;
      arr[3] = (q[2]-q[0])*0.5;;
      h.GetXaxis()->SetRange(first,last);
      return ;
    }

    int b1 = h.FindBin(q[1]);
    float fq1 = invDensityEstimation( h, b1, i, i_eff,  size_tot );

    int b0 = h.FindBin(q[0]);
    float fq0 = invDensityEstimation( h, b0, i, i_eff,  size_tot );

    int b2 = h.FindBin(q[2]);
    float fq2 = invDensityEstimation( h, b2, i, i_eff,  size_tot );
    

    //std::cout<< "size_tot "<< size_tot<< " _  bc1="<< hc1 << " bc1err="<< binCont1<< "  bincont_eff"<< binCont_eff<< "   i="<< i << "  fq1="<< fq1 << "  i_eff"<< i_eff <<std::endl;
    
    arr[0] = q[1];
    arr[1] = TMath::Max(0.5*TMath::Sqrt(1./i_eff )*fq1, h.GetBinWidth(b1) ) ;
    arr[2] = (q[2]-q[0])*0.5;
    arr[3] = TMath::Max(0.5*TMath::Sqrt(0.16*(1-0.16)/i_eff *(fq0*fq0+fq2*fq2) ), 0.5*(h.GetBinWidth(b0)+h.GetBinWidth(b2)) ) ;

    h.GetXaxis()->SetRange(first,last);

  }


  void uniModalSmoothMax(TH1& h, float arr[4], float outlierF, int startBin ){
    /// Evaluates a "smoothed mode" of the histogram h.
    /// The method assumes that h is unimodal (it has only 1 maximum).
    ///  * find the bin having  the absolute maximum content (call the bin center mode_0, the max value content_0)
    ///  * find all bins i with value statiscally compatible with max0 (that is for wich content_0-content_0_err< content_i < content_0 ), call their center mode_i 
    ///  * fill arr as in  arr={ average(mode_i), average_err(mode_i), average(content_i) , average_err(content_i)
    /// This procedures is performed ignoring all "outliers" bins where a bin is an outlier if "content_err/content > outlierF"


    int nBinTotal = h.GetNbinsX();
    // At very low stat (less entries than bins) potentially all bins are outliers.
    // --> adapt the outlier fraction.
    float occ = h.GetEffectiveEntries()/nBinTotal;
    if( (occ < 1) && (h.GetEntries()<0.5*nBinTotal) ) outlierF /=occ;
    
    // find max which is not an outlier.

    int maxbin = -1;

    float max = 0;
    float max_err = 0;
    for(int i=startBin; i<= nBinTotal; i++){
      float c = h.GetBinContent( i );
      if(c>max){
        float err = h.GetBinError(i);
        if( (err/c) < outlierF ) {
          maxbin = i;
          max_err = err; max = c;
        }
      }
    }    

    if(max == 0.) return;
    if(max_err == 0.) return;

    int binWindow = numNonEmptyBins(h)*(max_err/max)*0.3;
    binWindow= binWindow<=0 ? 1:binWindow;

    int finalBin = -1;
    float finalMax=0;
    float finalMaxErr=0;
    float finalPos = 0;
    float finalPosErr = 0;
    // find all bins statistically compatible with maxbin
    // evaluate the mean in the window around the max :
    //    std::cout << &h<<" "<<maxbin<< "   = "<< max<<  "   err="<< max_err<<  "  binWindow="<< binWindow << " / "<< outlierF<< std::endl;
    for(int i=startBin; i<= nBinTotal; i++){
      float c = h.GetBinContent( i );
      if(c==0) continue;
      float err = h.GetBinError(i);
      if( (err/c) > outlierF ) continue; // ignore outliers
      if ( (max-c)< (max_err+err)) {
        // compatible with max
        int minj = i-binWindow <= startBin ? startBin : i-binWindow;
        int maxj = i+binWindow >= nBinTotal ? nBinTotal : i+binWindow;
        float pos = 0;
        float pos2 = 0;
        float sum=0;
        float sum2=0;
        float localMax =0;
        for(int j=minj;j<=maxj;j++){
          float lc=h.GetBinContent(j);
          float w = (lc==0) ? 1 : h.GetBinContent(j)/ h.GetBinError(j);
          if( (1/w) > outlierF ) continue;
          float pi = h.GetBinCenter(j);
          pos +=  pi*w;
          pos2 += pi*pi*w;
          sum+= w;
          sum2 +=w*w;
          localMax += h.GetBinContent(j);
          //localMax +=w;
        }
        localMax /= (maxj-minj);
        //        std::cout << "    test at "<< i << "  "<< localMax << "   " << (maxj-minj) << "   pos "<< pos << std::endl;
        if(localMax>finalMax){
          finalPos = pos / sum;
          //std::cout << "  ***** found max "<< i << "  nbin= "<< (maxj-minj)<< "   "<< localMax << "  " <<finalPos <<  "  "<< pos2/sum - finalPos*finalPos <<std::endl; 
          finalMax = localMax ; finalBin = i;
          finalPosErr = TMath::Sqrt(pos2/sum - finalPos*finalPos);
          finalMaxErr = TMath::Sqrt( sum2)/(maxj-minj);          
        }
        
      }

    }

    //std::cout << " ----> maxpos ="<< maxpos << "  sum="<< sum << "  nbin="<< nbin <<  "  "<< maxbin << std::endl;

    //std::cout << " ----> finalPos="<< finalPos << " at bin "<< finalBin << "  max="<< finalMaxErr<<std::endl; 
    arr[0] = finalPos;
    arr[1] = finalPosErr;

    arr[2] = finalMax;
    arr[3] = finalMaxErr;
    
    // float maxpos=0;
    // float sum=0;
    // float sum2=0;
    // float maxpos2=0;
    // int nbin=0;
    // // find all bins statistically compatible with maxbin
    // TAxis * ax = h.GetXaxis();
    // //std::cout << &h<<" "<<maxbin<< "   = "<< max<<  "   err="<< max_err<<  "  err_ratio="<< max/max_err << " / "<< outlierF<< std::endl;
    // for(int i=startBin; i<= h.GetNbinsX(); i++){
    //   float c = h.GetBinContent( i );
    //   if(c==0) continue;
    //   float err = h.GetBinError(i);
    //   if( (err/c) > outlierF ) continue; // ignore outliers
    //   if ( (max-c)< (max_err+err)) {
    //     float w = c/err;
    //     float pos = ax->GetBinCenter( i );
    //     maxpos  +=  pos* w;
    //     maxpos2 +=  pos *pos *w;
    //     sum+= w;
    //     sum2+= w*w;
    //     nbin++;
    //     //std::cout << i <<  " ----------> " << c << " : "<< maxpos/sum<< std::endl;
    //   }
    // }


    // //std::cout << " ----> maxpos ="<< maxpos << "  sum="<< sum << "  nbin="<< nbin <<  "  "<< maxbin << std::endl;
    // // if only 1 bin, take nn bins into account
    // if(nbin==1){
    //   //maxpos = ax->GetBinCenter(maxbin)*max; sum=max;sum2= max*max;maxpos2=ax->GetBinWidth(maxbin)*ax->GetBinWidth(maxbin);
    //   //int nnlow = maxbin-1;
    //   int lowbin=maxbin -1;
    //   while(h.GetBinContent(lowbin)==0.){lowbin--; if(lowbin<=0) break;}
    //   if(lowbin>0){
    //     float c = ax->GetBinCenter(lowbin); float w= h.GetBinContent(lowbin);
    //     if(w!=0) {
    //       w /= h.GetBinError(lowbin);     
    //       maxpos +=c*w ; sum+=w;sum2+=w*w; maxpos2+=c*c*w;
    //       nbin++;
    //     }
    //   }
      
    //   int highbin=maxbin +1;
    //   while(h.GetBinContent(highbin)==0.){highbin++; if(highbin>=h.GetNbinsX()) break;}
    //   if(highbin<=h.GetNbinsX()){
    //     float c = ax->GetBinCenter(highbin); float w= h.GetBinContent(highbin);
    //     if(w!=0) {
    //       w /= h.GetBinError(highbin);     
    //       maxpos +=c*w ; sum+=w;sum2+=w*w; maxpos2+=c*c*w;
    //       nbin++;
    //     }
    //   }
    // }
       
    //std::cout << " ----> maxpos ="<< maxpos << "  sum="<< sum << "  nbin="<< nbin <<  "  "<< maxbin << std::endl;
    
    // arr[0] = maxpos/sum;
    // arr[1] = TMath::Sqrt( maxpos2/sum - arr[0]*arr[0]);
    // if((nbin==1) || (arr[1]==0.)) arr[1] = ax->GetBinWidth(maxbin);

    // arr[2] = sum/nbin;
    // arr[3] = TMath::Sqrt( sum2)/nbin;
  }

  int numNonEmptyBins(TH1& h, float threshold){
    int nocc = 0;
    for(int i=1; i<= h.GetNbinsX(); i++) if(h.GetBinContent(i)>threshold) nocc++;
    return nocc;
  }

  void sliceTH2(TH2& h2, TH1& h_out, int preservedDim, int sliceIndex){
    TAxis *a = preservedDim==0 ? h2.GetXaxis(): h2.GetYaxis();
    int nbin = a->GetNbins();
    if(nbin != h_out.GetNbinsX() ){
      if(!a->IsVariableBinSize()) {h_out.SetBins(nbin, a->GetBinLowEdge(1), a->GetBinUpEdge(nbin));}
      else{ h_out.SetBins(nbin, a->GetXbins()->GetArray()) ; }      
    }
    int pos[2] = {};
    pos[ 1-preservedDim ] = sliceIndex;
    for(int i=1;i<=nbin;i++) {
      pos[preservedDim] = i;
      int b = h2.GetBin(pos[0],pos[1]);
      h_out.SetBinContent(i, h2.GetBinContent(b)) ;
    }
  }

  
  
  //Easier for color-blind and greyscale output. Similar to Bird color pallette, but inverted so the light color is the low value
  void SetInvertedBird()
  {
	  TColor::InitializeColors();
	  Double_t stops[9] = { 0.0000, 0.1250, 0.2500, 0.3750, 0.5000, 0.6250, 0.7500, 0.8750, 1.0000 };
	  Double_t red[9] = { 0.9764, 0.9956, 0.8186, 0.5301, 0.1802, 0.0232, 0.0780, 0.0592, 0.2082 };
	  Double_t green[9] = { 0.9832, 0.7862, 0.7328, 0.7492, 0.7178, 0.6419, 0.5041, 0.3599, 0.1664 };
	  Double_t blue[9] = { 0.0539, 0.1968, 0.3499, 0.4662, 0.6425, 0.7914, 0.8385, 0.8684, 0.5293 };
	  TColor::CreateGradientColorTable(9, stops, red, green, blue, 255, 1.0);
  }
}
