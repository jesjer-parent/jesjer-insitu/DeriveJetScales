#include <iostream>

#include "TMath.h"
#include "TF1.h"

#include "DeriveJetScales/JESCorrection.h"
#include "DeriveJetScales/InputReader.h"

using std::cout;
using std::endl;

namespace DJS {

  double JESCorrection::jesFactor(JetData &j){
    return jesFactor(j.e_reco, j.eta_det, j.jes_ieta);
  }

  double JESCorrection::jesFactor(double E, double eta_det, int ieta){
    
    TF1* corrFunc = m_jesFactorFunc[ieta];
    
    if (E/cosh(eta_det)<m_minPtExtrapolated) {
      double eta_bin = std::floor(eta_det*10)/10; //-->> floor: if eta_det = -2.365 then eta_bin = -2.4
      double Ecutoff = m_minPtExtrapolated*cosh(eta_bin);
      double Rcutoff = corrFunc->Eval(Ecutoff);
      double slope=0;
      double logEcutoff = TMath::Log(Ecutoff);
      for(int iLoop=0; iLoop<(int)(corrFunc->GetNpar()); iLoop++) {
        double factor = corrFunc->GetParameter(iLoop);
        slope += iLoop*factor*TMath::Power(logEcutoff ,Int_t(iLoop-1))/Ecutoff;
      }
      if(slope > Rcutoff/Ecutoff) {       
        cout << Form("JES ERROR : Slope of calibration curve at minimum ET is too steep for the JES factors. Must use a higher MinPtForETAJES value for eta bin %d  eta=%f",ieta, eta_det) << endl;;
        return -1;
      }
      
      double R = slope*(E-Ecutoff)+Rcutoff;
      return 1.0/R;    
    }
    
    return 1.0/corrFunc->Eval(E);
  }

  double JESCorrection::etaCorr(JetData &j){

    return etaCorr(j.jes_ieta, j.e_reco, j.eta_reco);
  }

  double JESCorrection::etaCorr(int ieta_jes, double eCorrected, double eta_reco){

    double e_evaluation = eCorrected;
    //double cosh_etadet = TMath::CosH( m_etaBins->binCenter( ieta_jes ) ); old version 
    double cosh_eta = TMath::CosH( eta_reco );
    if( eCorrected/ cosh_eta < m_minPtETACorr ) { e_evaluation = m_minPtETACorr*cosh_eta; }
    if( eCorrected> m_maxEETACorr) { e_evaluation = m_maxEETACorr; }
    return  m_dEtaFunc[ieta_jes]->Eval( e_evaluation );

  }

  
}
