// this file is -*- C++ -*-
#include "TF1.h"
#include "DeriveJetScales/Utilities.h"
#include "DeriveJetScales/ResponseBuilder.h"
#include "DeriveJetScales/ResponseFitter.h"
#include "DeriveJetScales/KernelSmoothing.h"
#include "DeriveJetScales/JMSBinning.h"

#include "DeriveJetScales/WeightedBinFinder.h"
#include "DeriveJetScales/FineBinRespBuilder.h"
#include "DeriveJetScales/HistoClones.h"
#include "DeriveJetScales/JESCorrection.h"
#include "DeriveJetScales/InputReader.h"
#include "DeriveJetScales/JMSCombination.h"

#ifdef __CINT__

#pragma link off all globals;
#pragma link off all classes;
#pragma link off all functions;
#pragma link C++ nestedclass;

#pragma link C++ class DJS::Stats+;
#pragma link C++ class DJS::VecStats+;
#pragma link C++ class BPS::BinnedObjects<DJS::Stats>+;
#pragma link C++ class DJS::CalibrationObjects+;

#pragma link C++ class DJS::JetData;

#pragma link C++ class DJS::JMSBinning;
#pragma link C++ class DJS::ELogMovEBinning;
#pragma link C++ class DJS::MLogMovEBinning;
#pragma link C++ class DJS::PtMBinning;
#pragma link C++ class DJS::EtLogMovEtBinning;


#pragma link C++ namespace DJS;
#pragma link C++ class DJS::JESCorrection;
#pragma link C++ class DJS::InputReader;
#pragma link C++ class DJS::InputReaderVector;
#pragma link C++ class DJS::InputReaderFlatTree;
#pragma link C++ class DJS::ResponseBuilder;

#pragma link C++ class DJS::JMSCombination;


#pragma link C++ class DJS::ResponseFitter;
#pragma linc C++ std::vector<TF1*>;


#pragma link C++ class KS::Point<1>;
#pragma link C++ class KS::Point2<2>;
#pragma link C++ class KS::Smoother<1>;
#pragma link C++ class KS::Smoother<2>;

#pragma link C++ class KS::FastGausSmoothTH1;
#pragma link C++ class KS::FastGausSmoothNonRegTH1;
#pragma link C++ class KS::FastGausSmoothRegTH1;

#pragma link C++ function KS::initStretchFactor;
#pragma link C++ function DJS::calcMedIQR;
#pragma link C++ function DJS::uniModalSmoothMax;
#pragma link C++ function DJS::numInvYaxis;


#pragma link C++ class DJS::WeightedBinFinder;
#pragma link C++ class DJS::WeightedBinFinder::WeightedBin;
#pragma link C++ class std::vector<DJS::WeightedBinFinder::WeightedBin>;

#pragma link C++ class DJS::JMSWBinning+;

#pragma link C++ class DJS::FineBinRespBuilder+;
#pragma link C++ class DJS::HistoClones+;


#endif

