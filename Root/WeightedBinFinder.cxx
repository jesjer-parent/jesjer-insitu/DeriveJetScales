#include "DeriveJetScales/WeightedBinFinder.h"
#include "TMath.h"
#include <iostream>
namespace DJS {

  inline
  double exp(double x) {
    if(x<-1023) return 0;
    x = 1.0 + x / 1024;
    x *= x; x *= x; x *= x; x *= x;
    x *= x; x *= x; x *= x; x *= x;
    x *= x; x *= x;
    return x;
  }

  void WeightedBinFinder::fillBins(double x, int dim, int nn, double sig2,  std::vector<WeightedBinFinder::WeightedBin> & v){

    const BPS::AxisSpec *a = m_bps->axis(dim);
    int i0 = a->findBin(x);
    v.clear();
    if(!a->validBin(i0)) return;
    
    int first = i0-nn < 0 ? 0 : i0-nn;
    int last = i0+nn >= a->nBins() ? a->nBins()-1 : i0+nn;
    double sumw = 0;
    int c=0;
    v.resize(last-first+1);
    //std::cout << " fill bins "<< dim << " around "<<x << " i0="<<i0<< std::endl;
    for(int i=first;i<=last;i++){
      double d2 = (x-a->binCenter(i)); 
      //double w = TMath::Exp( - 0.5*d2*d2/sig2 ) ;
      double w = exp( - 0.5*d2*d2/sig2 ) ;
      v[c] = {i, w};
      sumw += w;
      //std::cout << "  WBF---> i  "<< i << "  "<< a->binCenter(i) << "  d="<<d2 << " ___ "<< w << std::endl;
      c++;
    }
    // normalize
    if(sumw>0) for(int i=0;i<c;i++) {
        v[i].w/=sumw;
      }
    
  }

  const std::vector<WeightedBinFinder::WeightedBin> & WeightedBinFinder::findBins(double x){
    fillBins( x , 0, m_nn1, m_sig1, m_bins);
    return m_bins;
  }

  const std::vector<WeightedBinFinder::WeightedBin> & WeightedBinFinder::findBins(double x, double y){
    static std::vector<WeightedBinFinder::WeightedBin> vb1;
    static std::vector<WeightedBinFinder::WeightedBin> vb2;
    fillBins( x , 0, m_nn1, m_sig1, vb1); if(vb1.empty() ) return vb1;
    fillBins( y , 1, m_nn2, m_sig2, vb2); if(vb2.empty() ) return vb2;
    double sumw = 0;
    m_bins.resize(vb1.size() * vb2.size() );
    int c=0;
    for(WeightedBin b1 : vb1 ) for(WeightedBin b2 : vb2 ){
        double w = b1.w * b2.w;
        m_bins[c] = { m_bps->bin(b1.index,b2.index) , w } ;
        sumw += w;
        c++;
      } 
    if(sumw>0)for(int i=0;i<c;i++) m_bins[i].w/=sumw;
    return m_bins;    
  }
  
  const std::vector<WeightedBinFinder::WeightedBin> & WeightedBinFinder::findBins(double x, double y, double z){ 
    static std::vector<WeightedBinFinder::WeightedBin> vb1;
    static std::vector<WeightedBinFinder::WeightedBin> vb2;
    static std::vector<WeightedBinFinder::WeightedBin> vb3;
    fillBins( x , 0, m_nn1, m_sig1, vb1); if(vb1.empty() ) return vb1;
    fillBins( y , 1, m_nn2, m_sig2, vb2); if(vb2.empty() ) return vb2;
    fillBins( z , 2, m_nn3, m_sig3, vb3); if(vb3.empty() ) return vb3;
    double sumw = 0;
    m_bins.resize(vb1.size() * vb2.size()* vb3.size() );
    int c=0;
    for(WeightedBin b1 : vb1 ) for(WeightedBin b2 : vb2 ) for(WeightedBin b3 : vb3 ){
          //std::cout << " bin on dim3 "<< b3.index << " "<< z << std::endl;
          double w = b1.w * b2.w* b3.w;
          m_bins[c] = { m_bps->bin(b1.index,b2.index,b3.index) , w } ;
          sumw += w;
          c++;
        } 
    if(sumw>0)for(int i=0;i<c;i++) m_bins[i].w/=sumw;
    return m_bins;     
  }

  const std::vector<WeightedBinFinder::WeightedBin> & WeightedBinFinder::findBins(double x, double y, double z, double t){ 

    static std::vector<WeightedBinFinder::WeightedBin> vb1;
    static std::vector<WeightedBinFinder::WeightedBin> vb2;
    static std::vector<WeightedBinFinder::WeightedBin> vb3;
    static std::vector<WeightedBinFinder::WeightedBin> vb4;
    fillBins( x , 0, m_nn1, m_sig1, vb1); if(vb1.empty() ) return vb1;
    fillBins( y , 1, m_nn2, m_sig2, vb2); if(vb2.empty() ) return vb2;
    fillBins( z , 2, m_nn3, m_sig3, vb3); if(vb3.empty() ) return vb3;
    fillBins( t , 3, m_nn4, m_sig4, vb4); if(vb4.empty() ) return vb4;
    double sumw = 0;
    m_bins.resize(vb1.size() * vb2.size()* vb3.size()* vb4.size() );
    int c=0;
    for(WeightedBin b1 : vb1 ) for(WeightedBin b2 : vb2 ) for(WeightedBin b3 : vb3 ) for(WeightedBin b4 : vb4 ){
            double w = b1.w * b2.w* b3.w* b4.w;
            m_bins[c] = { m_bps->bin(b1.index,b2.index,b3.index,b4.index) , w } ;
            sumw += w;
            c++;
          } 
    if(sumw>0)for(int i=0;i<c;i++) m_bins[i].w/=sumw;
    return m_bins;     

  }


  
  const std::vector<WeightedBinFinder::WeightedBin> & JMSWBinning::findBins(){

    if(m_isfracWidth1) setSig1( m_fWidth1*m_binning->m_var1);
    if(m_isfracWidth2) setSig2( m_fWidth1*m_binning->m_var2);

    return WeightedBinFinder::findBins(m_binning->m_var1, m_binning->m_var2, m_binning->m_eta);
  }

}
