#include "DeriveJetScales/KernelSmoothing.h"

namespace KS {

  void initStretchFactor(){
    CoordND<1,2>::initStretchFactor();
    CoordND<1,1>::initStretchFactor();
  }

  namespace {
    inline double fast_exp(double x) {
      if(x<-1023) return 0;
      x = 1.0 + x / 1024;
      x *= x; x *= x; x *= x; x *= x;
      x *= x; x *= x; x *= x; x *= x;
      x *= x; x *= x;
      return x;
    }
    inline double fast_gaus(double x,double w){
      double r = x/w; return fast_exp(-r*r);
    }
  }

  void FastGausSmoothTH1::setup(TH1 &h) {
    TAxis *a = h.GetXaxis();
    m_width = (a->GetXmax() - a->GetXmin()) *m_fw;    
  }


  void FastGausSmoothTH1::smooth(TH1& h, float w, int nn, int ignoredbins){
    TAxis *a = h.GetXaxis();
    if(a->IsVariableBinSize()){
      FastGausSmoothNonRegTH1 o(w,nn);
      o.smooth(h,ignoredbins);
    }else{
      FastGausSmoothRegTH1 o(w,nn);
      o.smooth(h,ignoredbins);      
    }    
  }
  
  void FastGausSmoothTH1::smooth(TH1& h, int ignoredbins){

    setup(h);

    int nbins = h.GetNbinsX();
    std::vector<double> content_tmp(nbins);
    std::vector<double> content_tmpErr(nbins);
    double totSum=0;
    double totSum_new=0;
    // loop over bins, evaluate the content of smoothed bin
    for(int i=ignoredbins; i < nbins; i++){
      int hbin=i+1;
      int lowbin = hbin-m_nn; lowbin = lowbin <ignoredbins+1 ? ignoredbins+1: lowbin;
      int highbin = hbin+m_nn; highbin = highbin > nbins ? nbins : highbin;
      double sum=0;
      double sumw=0;
      double sumErr2=0;
      double sumErr2w=0;
      //std::cout << i << " "<< lowbin << ", "<< highbin << std::endl;
      for(int n=lowbin;n<highbin+1;n++){
        double c = h.GetBinContent(n);
        double err = h.GetBinError(n);

        double dist_w = distWeight(hbin, hbin-n);
        // static double errC = 2.;
        double err_w =fast_gaus(1,1);
        if(c!=0. )err_w = fast_gaus(err/c,1);
        double w = dist_w*err_w;
        //std::cout << i << "      "<< n << "  err="<< err<< " ,  w =  "<< err_w << "  "<< dist_w << std::endl;      
        sumw +=w;
        sum += c*w;
        sumErr2 += err*err*w;
        sumErr2w += w;        
      }
      if(sumw!=0) sum /= sumw;
      if(sumErr2w!=0) sumErr2 = TMath::Sqrt(sumErr2)/sumErr2w;
      content_tmp[i] = sum;
      content_tmpErr[i] = sumErr2;
      // locate the maximum rel error
      double c = h.GetBinContent(hbin);
      totSum +=c;
      totSum_new += sum;
      
    }

    // set content of histo.
    // The procedure doesn't keep the Integral of histo invariant.
    // So we rescale errors so that the relative errors stay comparable.
    double errorR = 1;
    if(totSum!=0) errorR=totSum_new/totSum;
    //std::cout << " eR "<< errorR<<std::endl; 
    for(int i=0; i < nbins; i++){
      int hbin=i+1;
      h.SetBinContent(hbin, content_tmp[i]);
      h.SetBinError(hbin, content_tmpErr[i]*errorR);
    }
  }

  double FastGausSmoothRegTH1::distWeight(int , int delta) {return m_weights[abs(delta)];}
  void FastGausSmoothRegTH1::setup(TH1 &h) {
    FastGausSmoothTH1::setup(h);
    m_weights.resize(m_nn+1);
    double binW=h.GetBinWidth(1);
    m_weights[0]=1.;
    for(int i=1;i<=m_nn;i++) m_weights[i] = fast_gaus(binW*i,m_width);    
  }

  double FastGausSmoothNonRegTH1::distWeight(int i, int delta) {
    double d=m_a->GetBinCenter(i+delta)-m_a->GetBinCenter(i);
    return fast_gaus(d,m_width);
  }
  void FastGausSmoothNonRegTH1::setup(TH1 &h) {
    FastGausSmoothTH1::setup(h);
    m_a = h.GetXaxis();
  }


  
// explicit instantiation
template class Smoother<1> ;
template class Smoother<2> ;


}
