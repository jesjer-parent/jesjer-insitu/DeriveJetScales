#include <iostream>

#define protected public // terrible hack to allow overwrite TH1::fEntries
#include "TH1F.h"
#define protected protected


#include "DeriveJetScales/HistoClones.h"

ClassImp(DJS::HistoClones)

namespace DJS {

  void HistoClones::init(int nHistos, BPS::AxisSpec &a){
    m_histoAxis = a;
    m_nHistos = nHistos;
    m_content.Set(0);m_content.Set(nHistos*a.nBins());
    m_sumw2.Set(0);m_sumw2.Set(nHistos*a.nBins());
    m_sumwPerHisto.Set(0);m_sumwPerHisto.Set(nHistos);
    for(int i=0;i<nHistos;i++) m_sumwPerHisto.fArray[i]=0.;
  }
  
  void HistoClones::init(BPS::BinnedPhaseSpace * bps, BPS::AxisSpec &a){
    m_phaseSpace = bps;
    m_histoAxis = a;
    int nHistos = bps->nBins();
    m_nHistos = nHistos;
    m_content.Set(0);m_content.Set(nHistos*a.nBins());
    m_sumw2.Set(0);m_sumw2.Set(nHistos*a.nBins());
    m_sumwPerHisto.Set(0);m_sumwPerHisto.Set(nHistos);
    for(int i=0;i<nHistos;i++) m_sumwPerHisto.fArray[i]=0.;
  }

  bool HistoClones::histoBin(float x, int &binx){
    binx = m_histoAxis.findBin(x);
    return ! ( (binx<0) || (binx>=m_histoAxis.nBins()) ) ;
  }

  void HistoClones::fillAtBin(int i, int b, double w){

    int pos = i*m_histoAxis.nBins()+b ;
    m_content.fArray[pos] += w;
    m_sumw2.fArray[pos] += w*w;    
    m_sumwPerHisto[i] += 1;

  }

  void HistoClones::fillAt(int i, float x, float w){    
    int b = m_histoAxis.findBin(x);
    //std::cout << this << " HistoClones fillAt "<< i << "   "<< x  << "  b : "<< b << std::endl; //.nBins() << " " << m_histoAxis.m_edges.size() << std::endl;    
    if( (b<0) or (b>=m_histoAxis.nBins()) ) return;
    int pos = i*m_histoAxis.nBins()+b ;
    m_content.fArray[pos] += w;
    m_sumw2.fArray[pos] += w*w;    
    m_sumwPerHisto[i] += 1;
  }


  TH1F * HistoClones::histoAtIndex(int i){
    if(m_hModel==nullptr){
      m_hModel = new TH1F(m_histoAxis.name().c_str() , m_histoAxis.title().c_str(), m_histoAxis.nBins(), m_histoAxis.min() ,m_histoAxis.max() );
      m_hModel->Sumw2();
      m_hModel->Adopt(m_histoAxis.min() , nullptr);
      m_hModel->GetSumw2()->Adopt(m_histoAxis.min() , nullptr);
      m_hModel->SetDirectory(0);
    }
    // hacky  part : brute force the array content :
    m_hModel->fArray = m_content.GetArray()+ i*m_histoAxis.nBins() -1 ; // -1 
    m_hModel->GetSumw2()->fArray = m_sumw2.GetArray()+ i*m_histoAxis.nBins() -1 ;
    m_hModel->GetSumw2()->fN = m_histoAxis.nBins();

    m_hModel->fEntries = m_sumwPerHisto[i];
    m_hModel->fTsumw=0.; // resets stats
    return m_hModel; // 
  }

  HistoClones::~HistoClones(){
    if(m_hModel){
      //std::cout << "delet HistoClones"<< std::endl;
      m_hModel->fArray = 0;
      m_hModel->GetSumw2()->fArray = 0;      
      m_hModel->Adopt(0 , nullptr);
      m_hModel->GetSumw2()->Adopt(0 , nullptr);
      delete m_hModel;
    }
  }

  void HistoClones::inspect(bool showVoid){
    int n_void = 0;
    for(int i=0;i<m_nHistos;i++) {

      if( m_sumwPerHisto.fArray[i] > 0.) {
        if(showVoid) std::cout << i << "   "<< m_sumwPerHisto.fArray[i] << std::endl;
      } else {
        if(!showVoid) std::cout << i << "   "<< m_sumwPerHisto.fArray[i] << std::endl;
        n_void++;
      }
    }

    std::cout <<  "\n Total void =   "<< n_void << " / " << m_nHistos << std::endl;
  }


  void HistoClones::normalizeHistos(){
    int nbins = m_histoAxis.nBins();
    for(int h=0;h<m_nHistos; h++){

      double tot=0;
      for(int i=0;i<nbins;i++){
        tot+=m_content[h*nbins+i];
      }
      if(tot!=0){
        double tot2=tot*tot;
        for(int i=0;i<nbins;i++){
          m_content[h*nbins+i] /= tot;
          m_sumw2[h*nbins+i] /= tot2;
        }
      }
    }    
  }
  
}
