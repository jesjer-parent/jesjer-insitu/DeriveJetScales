#include "TTree.h"
#include "TChain.h"

#include "BinnedPhaseSpace/BinnedPhaseSpace.h"

#include "DeriveJetScales/InputReader.h"
#include "DeriveJetScales/JESCorrection.h"


namespace DJS {


  bool InputReader::initFileWeights(TTree *t){
    TChain * c= dynamic_cast<TChain*>(t);
    if( c ){
      size_t nf = c->GetListOfFiles()->GetEntries();
      if(m_fileWeights.empty()) m_fileWeights.resize(nf, 1.);
      else if(nf != m_fileWeights.size() ) {std::cout<< " !!!!!!!!!  ERROR !!!!!! number of input file="<< nf << " differ from number of file wights "<<  m_fileWeights.size() << std::endl; return false;}
      for(size_t i=0;i<nf;i++) {
        std::cout << i << "file "<< c->GetListOfFiles()->At(i)->GetTitle() << "  w="<< m_fileWeights[i]<< std::endl;
      }
    } else {
      m_fileWeights.resize(1,1.);
    }
    return true;
  }

  bool InputReader::initEventWeight(TTree *t){

    if(!t->GetBranch(m_evntWeight_bname.c_str()) ) {
      std::cout << "WARNING !!! Event weight TBranch '"<<m_evntWeight_bname.c_str()<< "' branch doesn't exist!, you may need to reweight MC (CreateCalibrationTreeFromxAOD/extras/reweight_allJs.C)"<< std::endl;
      m_evntWeight=1.0;
    } else {
      t->SetBranchStatus(m_evntWeight_bname.c_str(), true);
      t->SetBranchAddress(m_evntWeight_bname.c_str(), &m_evntWeight);
    }
    
    return true;
  }
  
  int InputReader::loadEntry(int i){

    m_tree->LoadTree(i);
    int r = m_tree->GetEntry(i);
    m_evntWeight *= m_fileWeights[ m_tree->GetTreeNumber() ];
    return r;
  }


  JetData InputReader::readCalibJetData(int jetindex){

    JetData j = readJetData(jetindex);

    if(!j.jes_valid) return j;
    if(!m_preJESCalibrated){      
      // JES factor 
      double jesFactor = m_jesCorr->jesFactor(j);
      j.e_reco *= jesFactor;
      j.m_reco *= jesFactor;

      if(m_testMassComb) j.m_ta *=jesFactor;
      // apply Eta correction
      //std::cout<< "  read "<< j.eta_reco <<" - "<<m_jesCorr->etaCorr(j)<< " // "<< jesFactor<<  "  jes_ieta="<< j.jes_ieta<< std::endl;
      j.eta_reco = j.eta_reco - m_jesCorr->etaCorr(j);
    }
    
    if(j.m_true < 0.0 ) return j;
    if(j.m_reco <= 0 ) return j;

    j.jms_valid = true;
    return j;    
  }  







  // ************************************************************
  // ************************************************************
  // vector<float> tree reader

#define SETBR( bname, vec, bvec) m_tree->SetBranchStatus(bname.c_str(), 1); m_tree->SetBranchAddress(bname.c_str(), & vec, &bvec)

  bool InputReaderVector::init(TTree * t){
    m_tree = t;
    m_tree->SetBranchStatus("*",false);

    bool evtWOk = initEventWeight(t);
    if(!evtWOk){ return false;}
    
    TBranch * b_mu ;       SETBR( m_mu_bname, m_mu, b_mu);
    TBranch * b_e_true ;   SETBR( m_e_true_bname, m_e_true, b_e_true);
    TBranch * b_e_reco ;   SETBR( m_e_reco_bname   ,m_e_reco  , b_e_reco );
    TBranch * b_eta_reco ; SETBR( m_eta_reco_bname ,m_eta_reco, b_eta_reco );
    TBranch * b_eta_true ; SETBR( m_eta_true_bname ,m_eta_true, b_eta_true );
    TBranch * b_m_true ;   SETBR( m_m_true_bname   ,m_m_true  , b_m_true );
    TBranch * b_m_reco ;   SETBR( m_m_reco_bname   ,m_m_reco  , b_m_reco );
    TBranch * b_m_ta ;    if(!m_m_ta_bname.empty()){
      if(m_m_ta_bname == m_m_reco_bname) {std::cout<< "ERROR : m_ta and m_reco variables have same TTree branch name..."<< std::endl; return false;}      
      SETBR( m_m_ta_bname   ,m_m_ta  , b_m_ta );
      m_testMassComb=true;
    }

    TBranch * b_eta_det ;
    m_useEtaDet = ! m_eta_det_bname.empty();
    if(m_useEtaDet){
      SETBR( m_eta_det_bname ,m_eta_det, b_eta_det );
    } else {
      std::cout << " WARNING no eta detector specified ! will derive calib using eta reco\n" << std::endl;
        
    }
      
    if( (m_eta_reco == nullptr) && !m_xAODinput){
      std::cout << " ERROR :  could not set an address for eta variable. Check for SetBranchAddress error message above. If none, you may want to set the option m_xAODinput to True !" << std::endl;
    }
    
    if( m_xAODinput){
      m_tree->SetMakeClass(1);
      m_tree->SetBranchAddress(m_eta_reco_bname.c_str(), &m_eta_reco_v, &b_eta_reco);
      m_eta_reco = & m_eta_reco_v;
      // m_tree->SetBranchAddress(m_e_reco_bname.c_str(), &m_e_reco_v, &b_e_reco);
      // m_e_reco = & m_e_reco_v;
      m_tree->SetBranchAddress(m_m_reco_bname.c_str(), &m_m_reco_v, &b_m_reco);
      m_m_reco = & m_m_reco_v;      
      m_energyUnit = 1./1000.;
    }
    

    if(!m_useEtaDet) m_eta_det = m_eta_reco;
        

    TBranch * allBranches[] = {b_e_reco, b_e_true, b_eta_true, b_eta_reco, b_m_reco, b_m_true};
    for(TBranch *b : allBranches) {m_tree->SetBranchStatus(b->GetName(), true ); }
    if(m_testMassComb) m_tree->SetBranchStatus(b_m_ta->GetName(), true);
    //std::cout << "inited tree "<< m_tree << " "<< m_useEtaDet << " "<< m_xAODinput << std::endl;

    return initFileWeights(t);
  }

  int InputReaderVector::loadEntry(int i){

    m_tree->LoadTree(i);
    int r = m_tree->GetEntry(i);
    if(!m_useEtaDet) m_eta_det = m_eta_reco; // reset just in case ROOT did change the pointer (can this happen ?) 
    return r;       
  }  

  
  size_t InputReaderVector::nJets(){
    return m_e_true->size();    
  }

  
  JetData InputReaderVector::readJetData(int jetindex){
    JetData j;

    j.e_true = (*m_e_true)[jetindex] * m_energyUnit;
    j.m_true = (*m_m_true)[jetindex] * m_energyUnit;
    j.e_reco = (*m_e_reco)[jetindex] * m_energyUnit;
    j.m_reco = (*m_m_reco)[jetindex] * m_energyUnit;
    j.eta_reco = (*m_eta_reco)[jetindex];
    j.eta_true = (*m_eta_true)[jetindex];
    j.eta_det = (*m_eta_det)[jetindex];
    j.jes_ieta = m_jesEtaBins->findBin( j.eta_det );
    j.jes_valid = m_jesEtaBins->validBin(j.jes_ieta);
    if(m_testMassComb) j.m_ta = (*m_m_ta)[jetindex];
    j.weight  = m_evntWeight;
    return j;    
  }

  bool InputReaderVector::jetDataConsistency(){
    return (m_e_true->size() == m_e_reco->size() );
  }



  // ************************************************************
  // ************************************************************
  // Reader with per-jet weights
  
  bool InputReaderVectorJetW::init(TTree * t) {
    bool ok = InputReaderVector::init(t);
    if(!ok) return false;
    TBranch * b_jet_weights ;   SETBR( m_jetWeight_bname   ,m_jet_weights  , b_jet_weights );
    return true;
  }
  
  JetData InputReaderVectorJetW::readJetData(int jetindex){
    JetData j=InputReaderVector::readJetData(jetindex);
    j.weight = m_evntWeight * (*m_jet_weights)[jetindex];
    return j;
  }
  


  // ************************************************************
  // ************************************************************
  // Flat tree reader

#undef SETBR
#define SETBR( bname, vec) m_tree->SetBranchStatus(bname.c_str(), 1); m_tree->SetBranchAddress(bname.c_str(), & vec)

  bool InputReaderFlatTree::init(TTree* t){
    m_tree = t;
    m_tree->SetBranchStatus("*",false);

    //TBranch * b_evntWeight = nullptr;
    if(!m_tree->GetBranch(m_evntWeight_bname.c_str()) ) {
      std::cout << "WARNING !!! Event weight TBranch '"<<m_evntWeight_bname.c_str()<< "' branch doesn't exist!, need to reweight MC (CreateCalibrationTreeFromxAOD/extras/reweight_allJs.C)"<< std::endl;
      m_evntWeight=1.0;
    } else {
      m_tree->SetBranchStatus(m_evntWeight_bname.c_str(), true);
      m_tree->SetBranchAddress(m_evntWeight_bname.c_str(), &m_evntWeight);
    }

    SETBR( m_mu_bname, m_mu);
    SETBR( m_e_true_bname, m_e_true );
    SETBR( m_e_reco_bname   ,m_e_reco  );
    if(m_useEtaDet) {SETBR( m_eta_det_bname ,m_eta_reco);}
    else SETBR( m_eta_reco_bname ,m_eta_reco);
    SETBR( m_eta_true_bname ,m_eta_true);
    SETBR( m_m_true_bname   ,m_m_true  );
    SETBR( m_m_reco_bname   ,m_m_reco  );    
    //SETBR( m_npv_bname   ,m_npv  );
    SETBR( m_jetind_bname   ,m_jetind  );
    if(!m_m_ta_bname.empty()){
      if(m_m_ta_bname == m_m_reco_bname) {std::cout<< "ERROR : m_ta and m_reco variables have same TTree branch name..."<< std::endl; return false;}
      SETBR( m_m_ta_bname   ,m_m_ta  ) ;  m_testMassComb=true;
    }

    return initFileWeights(t);

  }

  
  JetData InputReaderFlatTree::readJetData(int ){
    JetData j;

    j.e_true = m_e_true ;
    j.m_true = m_m_true ;
    j.e_reco = m_e_reco ;
    j.m_reco = m_m_reco ;
    j.m_ta = m_m_ta ;
    j.eta_reco = m_eta_reco;
    j.eta_det = m_eta_reco;
    j.eta_true = m_eta_true;

    j.jes_ieta = m_jesEtaBins->findBin( j.eta_det );
    j.jes_valid = m_jesEtaBins->validBin(j.jes_ieta);
    j.weight  = m_evntWeight;
    
    
    if(j.m_true==0) {j.jms_valid=false; return j;}
    

    j.jms_valid = true;
    
    return j ;
  }
  

  
}

