
from BinnedPhaseSpace.BinnedPhaseSpaceConfig import AxisSpec , BinnedPhaseSpace, BinnedTH1, BinnedTH2, BinnedTGraph2DErrors, h1, h2
from DeriveJetScales.PyHelpers import NIOptions

# **************************
# retrieve C++ objects.
# we test if a ResponseBuilder class has already been defined. If not we default to the C++ implementation in ROOT.DJS
if 'ResponseBuilder' not in dir():
    ResponseBuilder = ROOT.DJS.FineBinRespBuilder
# we test if a CalibrationObjects class has already been defined. If not we default to the C++ implementation in ROOT.DJS
if 'CalibrationObjects' not in dir():
    CalibrationObjects = ROOT.DJS.CalibrationObjects
    
# the above allows to exchange the default C++ class by debugging or validation classes.
# ********************************
# By default take the jetType from the script arguments
defaultjetType = args.jetType
#defaultjetType = "AntiKt10LCTopoTrimmedPtFrac5SmallR20" # uncomment to force to a given type
# ********************************


def symetrize(l):
    newL =[-i for i in l[1:] ] #assumee l starts with 0.
    newL.reverse()
    return newL+l

def doubleBins(l):
    newL = []
    for i, v in enumerate(l[1:]):
        newL += [ l[i], 0.5*(l[i]+v) ]

    newL.append( l[-1] )
    return newL

def tripleBins(l):
    newL = []
    for i, v in enumerate(l[1:]):
        li = l[i]
        w = v - li
        newL += [ li, li+0.333*w , li+0.666*w]

    newL.append( l[-1] )
    return newL

        

    
def configure():
    # ######################################################
    # ######################################################
    # EtaJES / JMS common configuration 
    
    # Build a CalibrationObjects which will centralize every
    # collections of TH1/TH2/TGraphErrors/TF1 needed for JES/JMS derivation 
    objs = CalibrationObjects()
    # this is a C++ object (see Utilities.h) but we will augment it with pure python attributes

    objs.AtlasLabel = "Simulation Internal"
    objs.jetType = defaultjetType
    objs.jetTypePublic = ["#sqrt{s}= 14 TeV, Pythia Dijets","anti-k_{t} R = 1.0, LCTopo+JES","Trimmed (f_{cut} = 0.05, R_{sub} = 0.2)"]
    objs.radius  = 1.0

    inputReader = ROOT.DJS.InputReaderVector()
    objs.inputReader = inputReader

    massType = "mCalo"
    #massType = "mTA"
    #massType = "mComb"

    objs.massType = massType
    
    # set input branch name :
    inputReader.m_evntWeight_bname = "new_EventWeight"
    inputReader.m_e_true_bname = "E_true"
    inputReader.m_e_reco_bname = "E_corr1"
    inputReader.m_eta_true_bname = "eta_true"
    inputReader.m_eta_reco_bname = "eta_corr1"
    inputReader.m_eta_det_bname = "eta_det_corr1"
    inputReader.m_m_true_bname = "m_true"
    # set mass branch name according to type 
    if massType == "mCalo":
        inputReader.m_m_reco_bname = "m_corr1"    
    elif massType == "mTA":
        inputReader.m_m_reco_bname = "m_TA_corr1"
    elif massType == "mComb":
        inputReader.m_m_reco_bname = "m_corr1"
        inputReader.m_m_ta_bname = "m_TA_corr1"


    respBuilder = ResponseBuilder()
    objs.respBuilder = respBuilder
    respBuilder.m_inputReader = inputReader
    # respBuilder.m_numJetsMax = 2

    # ######################################################
    # ######################################################
    # EtaJES configuration 

    # eta bins
    etaBins = AxisSpec("EtaBins", "#eta", seq(-3. , 3, 0.1 ) )
    etaBins.m_isGeV = True
    objs.etaBins = etaBins
    
    # Histogram bins
    eBins = [60, 80, 100, 120, 150, 200, 240, 300, 400, 500, 600, 800, 1000, 1200, 1500, 2000, 2500, 3000, 3500, 4000, 5000]
    pBins = [20, 25, 30, 35, 40, 50, 60, 80, 100, 120, 150, 200, 240, 300, 400, 500, 600, 800, 1000, 1200, 1500, 2000, ]

    # response bins 
    #rBins = seq(0,2.4, 0.01) # original binning
    rBins = (240, 0, 2.4)     # Histos defined with this produce very slightly different fits although the binning is actually identical !

    objs.defineHistos( # this function is defined in PyHelpers
        # each line defines a histogram which will be cloned for each eta bin
        #  (h1 and h2 are utilities which only convert python lists into arguments usable by TH1 ctors)
        R_vs_Etrue =     ("R_vs_Etrue","")+ h2(eBins, *rBins ) ,
        R_vs_Ereco=      ("R_vs_Ereco","")+ h2(eBins, *rBins ) ,
        R_vs_pTtrue =     ("R_vs_pTtrue","")+ h2(pBins, *rBins ) ,
        R_vs_mu =        ("R_vs_mu","")+h2(30, 10., 40., *rBins),
        Etrue_vs_Etrue = ("Etrue_vs_Etrue","")+ h1(eBins ) , 
        Ereco_vs_Ereco = ("Ereco_vs_Ereco","")+ h1(eBins ) ,
        pTtrue_vs_pTtrue = ("pTtrue_vs_pTtrue","")+ h1(pBins ) , 
        Ereco_vs_Etrue = ("Ereco_vs_Etrue","")+ h1(eBins ) ,
        DEta_vs_Etrue =  ("DEta_vs_Etrue","")+ h2(eBins,1400,-0.2,0.2 ) , 
        R_vs_Enuminv  =  ("R_vs_Enuminv","")+ h2(eBins, *rBins ) ,
        Enuminv_vs_Etrue = ("Enuminv_vs_Etrue","")+ h1(eBins ) ,
        closure_R_vs_Etrue =     ("closure_R_vs_Etrue","")+ h2(eBins, *rBins ) ,
        closure_R_vs_pTtrue =     ("closure_R_vs_pTtrue","")+ h2(pBins, *rBins ) ,
        closure_DEta_vs_Etrue =     ("closure_DEta_vs_Etrue","")+ h2(eBins, 1400,-0.2,0.2  ) ,
        )


    # ********************************************
    # configure the response builder

    respBuilder.m_etaBins = etaBins
    respBuilder.m_objects = objs

    # EtaJES config
    respBuilder.m_jesCorr.m_minPtExtrapolated = 10 # ?? guess ??
    respBuilder.m_jesCorr.m_minPtETACorr = 8
    respBuilder.m_jesCorr.m_maxEETACorr = 2500

    # ********************************************
    # configure the fitters
    eRespFitter = ROOT.DJS.ResponseFitter()
    eRespFitter.m_minNEffP1 = -3.1
    eRespFitter.m_minNEffP2 = 1.05
    eRespFitter.m_minNEffP3 = 400
    eRespFitter.m_NSigmaForFit = 1.5 # this is for energy !!
    eRespFitter.m_minEtForMeasurement = 100

    eRespFitter.m_useBetterChi2 = False
    eRespFitter.m_useGeoDetCorr = True
    eRespFitter.m_minGeoDetCorr = 2.5
    eRespFitter.m_maxGeoDetCorr = 3.5

    # not set for AntiKt10 :
    #eRespFitter.m_etaException
    #eRespFitter.m_JES_MaxOrderException 


    #eRespFitter.m_JES_Function = "polBestChi2"
    eRespFitter.m_JES_Function = "polBestChi2+groom"
    eRespFitter.m_JES_MaxOrderP1 = -3.1
    eRespFitter.m_JES_MaxOrderP2 = -0.05
    eRespFitter.m_JES_MaxOrderP3 = 5.5


    eRespFitter.m_etaCorr_MaxOrderP1 = -3.0
    eRespFitter.m_etaCorr_MaxOrderP2 = -0.25
    eRespFitter.m_etaCorr_MaxOrderP3 = 3.95



    # GCW and LC not well descriped in low E region by groom function
    # EnableEMinExtrapolation: false gives better results in these cases
    eRespFitter.m_enableEMinExtrapolation = False    
    eRespFitter.m_enableEMaxExtrapolation = True # (default value :True)
    #eRespFitter.m_enableEMaxExtrapolationException -> not set for AntiKt10
    #eRespFitter.m_enableEMinExtrapolationException -> not set for AntiKt10
    
    eRespFitter.m_maxAcceptableYDeviationFitVSMinGroom = 0.3

    # In case the number of data points in the R vs. E graph < MinNDataPointsForMinExtrapolation
    # => EnableEMinExtrapolation=true forced ("JES_Function: polBestChi2+groom" only)
    eRespFitter.m_minNDataPointsForMinExtrapolation = 9

    eRespFitter.m_minEtForExtrapolation = 50
    eRespFitter.m_maxEtForFits = 5000


    balanceFitter = ROOT.JES_BalanceFitter(eRespFitter.m_NSigmaForFit)
    balanceFitter.SetGaus()
    balanceFitter.SetFitOpt("REQS")
    eRespFitter.m_respFitter = balanceFitter

    eRespFitter.m_etaBins = etaBins

    objs.eRespFitter = eRespFitter

    # ******************************
    # config a fitter for delta Eta
    dEtaFitter = ROOT.DJS.ResponseFitter(eRespFitter)
    # same config as eRespFitter except the following :
    dEtaFitter.m_NSigmaForFit = 1.4

    balanceFitter = ROOT.JES_BalanceFitter(dEtaFitter.m_NSigmaForFit)
    balanceFitter.SetGaus()
    balanceFitter.SetFitOpt("REQS")
    dEtaFitter.m_respFitter = balanceFitter
    dEtaFitter.m_etaBins = etaBins
    dEtaFitter.m_respHistTitle = "#Delta Eta, %.1f < E_{true} < %.1f, etabin=%d";
    objs.dEtaFitter = dEtaFitter




    # ######################################################
    # ######################################################
    # JMS configuration 

    # ******************************
    # optionnal JMS strategies. By default, all are set to False
    jmsNIopts = NIOptions(
        noNumInv=False,
        directNumInv=False,
        flattenCalib=False,
        adjustFlatCalib=False,
        forceSuperIncrResp=False
    ) 
    objs.jmsNIopts = jmsNIopts 
    objs.jmsPreCorr       = False
    objs.jmsRespEvaluation = "gaus" # default is "gaus", alternative is "mode"
    #objs.jmsRespEvaluation = "mode" # default is "gaus", alternative is "mode"
    objs.binSystem = "ELogMoEBins"
    #objs.binSystem = "EtLogMoEtBins"
    #objs.binSystem = "MLogMoEBins"
    #objs.binSystem = "PtMBins"
    # ******************************

    objs.jmsVeryFineBinning = True

    if jmsNIopts.noNumInv:
        # when NOT doing JMS numerical inversion, we fill the 'numinv' histos directly in the JMS.rawresp step
        # we thus don't need the niresp step
        objs.vetoedCalibSteps = ["JMS.niresp"]


    ## eBins = [20,  60, 150,  400, 1000,  3000,  5500]
    ## logmoeBins = [-6, -3.25,  -2.25, -1.5, -1.25,-1,-0.8,0]
    jmsEtaBins = [0,0.2,0.4,0.8, 1.0 ,1.1 , 1.2, 1.3, 1.4,1.5, 1.6 ,1.7 , 2., 2.25, 2.5, 2.75, 3 ]
    if massType == "mTA" :
        jmsEtaBins = [0,0.2,0.4,0.8, 1.0 , 1.1, 1.2, 1.3, 1.4,1.5, 1.6 ,1.7 , 2.]

    objs.tmpBPS = BinnedPhaseSpace("DDD",AxisSpec("Et", "Et", doubleBins(seq(200,1000,200)+seq(1200,4000,400)+[6000]), isGeV=True ),)

    objs.pTmin = 140 
    
    if objs.binSystem=="PtMBins":
        BinningClass = ROOT.DJS.PtMBinning
        objs.pt_desc = 'p_{T} [GeV]'
        objs.m_desc = 'm [GeV]'
        jmsBPS = BinnedPhaseSpace( "JMSbins",
                                   # bins from BinHadler/BinHandler.cxx
                                   AxisSpec("pt", "p_{T} true", doubleBins(seq(200,1000,100)+seq(1200,2000,200)+[2500,3000,4000,5000]), isGeV=True ),
                                   AxisSpec("m", "m true",doubleBins(seq(0,200,10)+[225,250,275,300]), isGeV=True),
                                   AxisSpec("abseta", "|eta|",jmsEtaBins, isGeV=False)
                               )
    elif objs.binSystem=="ELogMoEBins":
        BinningClass = ROOT.DJS.ELogMovEBinning
        objs.pt_desc = 'E [GeV]'
        objs.m_desc = 'log(m/E)'
        jmsBPS = BinnedPhaseSpace( "JMSbins",
                                   # bins from BinHadler/BinHandler.cxx
                                   AxisSpec("E", "E", tripleBins(seq(200,1000,200)+seq(1200,4000,400)+[6000]), isGeV=True ),
                                   AxisSpec("logmopt", "log(m/E)",tripleBins([-6,-5, -4.5, -4.25, -4, -3.75, -3.5, -3.25, -3,-2.75, -2.5, -2.25, -2,-1.75,-1.5, -1.25,0]), isGeV=False), # with tripleBins, reduce number of high log(m/E) bins
                                   AxisSpec("abseta", "|eta|",jmsEtaBins, isGeV=False)
        )


    if objs.binSystem=="EtLogMoEtBins":
        BinningClass = ROOT.DJS.EtLogMovEtBinning
        objs.pt_desc = 'E_{T} [GeV]'
        objs.m_desc = 'log(m/Et)'
        jmsBPS = BinnedPhaseSpace( "JMSbins",
                                   # bins from BinHadler/BinHandler.cxx
                                   AxisSpec("Et", "Et", tripleBins(seq(200,1000,200)+seq(1200,4000,400)+[6000]), isGeV=True ),
                                   #AxisSpec("logmoet", "log(m/Et)",tripleBins([-6,-5, -4.5, -4.25, -4, -3.75, -3.5, -3.25, -3,-2.75, -2.5, -2.25, -2,-1.75,-1.5, -1.25,-1,-0.8,0]), isGeV=False), # m<40GeV for all log(m/E)<-4/5
                                   AxisSpec("logmoet", "log(m/Et)",tripleBins([-6,-5, -4.5, -4.25, -4, -3.75, -3.5, -3.25, -3,-2.75, -2.5, -2.25, -2,-1.75,-1.5, -1.25,0]), isGeV=False), # with tripleBins, reduce number of high log(m/E) bins
                                   AxisSpec("abseta", "|eta|",jmsEtaBins, isGeV=False)
                                   )
    if objs.binSystem=="MLogMoEBins":
        BinningClass = ROOT.DJS.MLogMovEBinning
        objs.pt_desc = 'm [GeV]'
        objs.m_desc = 'log(m/E)'
        jmsBPS = BinnedPhaseSpace( "JMSbins",
                                   # bins from BinHadler/BinHandler.cxx
                                   AxisSpec("m", "m true",seq(0,200,10)+[225,250,275,300], isGeV=True),
                                   AxisSpec("logmopt", "log(m/E)",[-6,-5.,-4.5,  -4.25, -4, -3.75, -3.5, -3.25, -3,-2.75, -2.5, -2.25, -2,-1.75,-1.5, -1.25,-1,-0.8,0], isGeV=False), # m<40GeV for all log(m/E)<-4/5
                                   AxisSpec("abseta", "|eta|",jmsEtaBins, isGeV=False)
                                   )



    # setup the JMS paths according to the options
    objs.jmsWorkDir = "jms_"+objs.binSystem+('_noni/' if jmsNIopts.noNumInv else '_ni/')
    objs.jmsQualifier = "vfb"+ massType

    # remember what binning class we use.
    objs.BinningClass = BinningClass


    respBuilder.m_noMassNumInv = jmsNIopts.noNumInv

    # instantiate and setup the binning helpers : 
    respBuilder.m_jmsTrueBin = BinningClass()
    respBuilder.m_jmsRecoBin = BinningClass()
    if massType == "mComb":
        respBuilder.m_jmsCombination
        mCombiner = ROOT.DJS.JMSCombination()
        mCombiner.m_mCaloCalib = respBuilder.m_jmsRecoBin
        mCombiner.m_mTACalib = BinningClass(jmsBPS)
        respBuilder.m_jmsCombination = mCombiner
        objs.mCombiner = mCombiner
    #respBuilder.m_jmsRecoBin.setCorrectionFactors( objs.mRsmooth_vs_PtMnuminv_in_Eta )

    objs.jmsBPS = jmsBPS
    respBuilder.m_jmsBPS = jmsBPS
    respBuilder.m_jmsRecoBin.setBinnedPhaseSpace( jmsBPS) # for interactive use 
    respBuilder.m_jmsTrueBin.setBinnedPhaseSpace( jmsBPS) # for interactive use 


    # Set-up a helper class specialized for very fine overlapping binning (see WeightedBinFinder.h)
    respBuilder.m_jmsWBins = ROOT.DJS.JMSWBinning()
    # re-use the binning helper class
    respBuilder.m_jmsWBins.m_binning = respBuilder.m_jmsTrueBin
    respBuilder.m_jmsWBins.m_bps = jmsBPS
    # configure the widths of gaussian used to evaluate overlapping weights
    #  -> set them to 0 for the 2 first dimension since we'll be using fractional widths
    respBuilder.m_jmsWBins.setSigmas(0, 0., 0.9)
    respBuilder.m_jmsWBins.setFracWidth1(0.1)
    #  --> the gaussian width along dimension 1, say pT, will then be 0.1 x pT
    respBuilder.m_jmsWBins.setFracWidth2(0.1)
    # For each dim, set how many bins below and above the nominal bin are impacted
    respBuilder.m_jmsWBins.setNN(3,3,0)
    # ex: if setNN(n1,n2,n3) then a total of (2xn1+1)x(2xn2+1)x(2xn3+1) bins are impacted.

    objs.respBuilder = respBuilder
    
    
    # --------------------------
    # Configure the 'HistoClones' containers which store the response histograms for the full bin space.
    objs.mR_in_EMEtatrue_py = objs.mR_in_EMEtatrue # alias to a python vaiable to avoid C++/python issue
    objs.mR_in_EMEtatrue.setName("mR_in_EMEtatrue")    
    objs.mR_in_EMEtatrue.init(jmsBPS, ROOT.BPS.AxisSpec("mresp","mass resp", 200,0,5) )

    objs.closure_mR_in_EMEtatrue_py = objs.closure_mR_in_EMEtatrue # alias to a python vaiable to avoid C++/python issue
    objs.closure_mR_in_EMEtatrue.setName("closure_mR_in_EMEtatrue")    
    objs.closure_mR_in_EMEtatrue.init(jmsBPS, ROOT.BPS.AxisSpec("mresp","mass resp", 200,0,5) )

    objs.mR_in_EMEtanuminv_py = objs.mR_in_EMEtanuminv # alias to a python vaiable to avoid C++/python issue
    objs.mR_in_EMEtanuminv.setName("mR_in_EMEtanuminv")    
    objs.mR_in_EMEtanuminv.init(jmsBPS, ROOT.BPS.AxisSpec("mresp","mass resp", 200,0,5)  )

    # the responses in reco bins are used to compute IQR values which can then be usable in mass combination
    objs.mR_in_EMEtareco_py = objs.mR_in_EMEtareco # alias to a python vaiable to avoid C++/python issue
    objs.mR_in_EMEtareco.setName("mR_in_EMEtareco")    
    objs.mR_in_EMEtareco.init(jmsBPS, ROOT.BPS.AxisSpec("mresp","mass resp", 200,0,5)  )
    # --------------------------

    
    # --------------------------
    # JMS Response fitter.
    balanceFitter_jms = ROOT.JES_BalanceFitter(1.3) #(1.6)
    balanceFitter_jms.SetGaus()
    balanceFitter_jms.SetDefaultMinMax(0. , 4. )
    balanceFitter_jms.SetFitOpt("RQE0S")
    #balanceFitter_jms.SetFitOpt("RWLQ0S") # for JMS, use WL="Weighted Likelihood" because we're not rebinning
    balanceFitter_jms.StopWhenFitFails(True)
    objs.balanceFitter_jms =balanceFitter_jms
    # --------



    # --------
    # JMS smoothers
    from DeriveJetScales.KernelSmoothing import buildSmoother
    jms_th1_smoother = buildSmoother(gausW=0.07, log=[True], npoints=[38] )        
    objs.jms_th1_smoother = jms_th1_smoother

    # for mR vs pt,m smoothing
    if objs.binSystem in ["ELogMoEBins" , "EtLogMoEtBins" ]:
        jms_tg2_smoother = buildSmoother(gausW=0.05, log=[False,False], npoints=[60,60] ,
                                         useErrors=True, maxNpoints=30,
                                         stretchF=(1,1) )        
    else: # objs.binSystem=="PtMBins":
        jms_tg2_smoother = buildSmoother(gausW=0.07, log=[False,False], npoints=[60,60] ,
                                         useErrors=True, maxNpoints=30,
                                         stretchF=(1.3,0.6) )        
        
    objs.jms_tg2_smoother = jms_tg2_smoother
    # --------

    # --------
    # define a phase space based on mass windows of physics interest to be used in final comparisons
    jmsEtaBins = [0,0.2,0.6, 1.0, 1.1, 1.2, 1.3,  1.4, 2., 3  ]
    jmsinclPtBPS = BinnedPhaseSpace( "JMSinclPt",
                                     AxisSpec("pt", "p_{T} true", seq(200,1000,100)+seq(1200,3000,200), isGeV=True ),
                                     AxisSpec("m", "mass true", [40, 80, 100,140,200], isGeV=True ),
                                     AxisSpec("abseta", "|eta|",jmsEtaBins, isGeV=False),
                                     )
    # define corresponding histogram containers
    objs.mR_in_inclPtEtatrue         = BinnedTH1("mR_in_inclPtEtatrue", jmsinclPtBPS, h1(200,0.01,5), Title="Mass Response", var_x='m response')
    objs.closure_mR_in_inclPtEtatrue = BinnedTH1("closure_mR_in_inclPtEtatrue", jmsinclPtBPS, h1(200,0.01,5), Title="Mass Response", var_x='m response')
    objs.jmsinclPtBPS = jmsinclPtBPS
    # --------



    # --------
    # We won't build validation plots/graphs for every possible bins (very fine bins ==> to many bins)
    # so we prepare a helper object to keep only one bin out of X (depending on dimension)
    from DeriveJetScales.VeryFineBinsDerivation import FilteredBinnedPhaseSpace    
    nMbins = objs.jmsBPS.m_a2.nBins()
    nPbins = objs.jmsBPS.m_a1.nBins()
    objs.plotsBPS = FilteredBinnedPhaseSpace(objs.jmsBPS,nPbins/10, nMbins/10, 1 )


    return objs


# We change the default CalibrationDerivation class to point
# to the FineBinCalibDerivation class.
from DeriveJetScales.VeryFineBinsDerivation import FineBinCalibDerivation
CalibrationDerivationClass = FineBinCalibDerivation
