
from DeriveJetScales.PyHelpers import h1, h2
from BinnedPhaseSpace.BinnedPhaseSpaceConfig import AxisSpec , BinnedPhaseSpace, BinnedTH1, BinnedTH2, BinnedTGraph2DErrors, h1, h2

def symetrize(l):
    newL =[-i for i in l[1:] ] #assumee l starts with 0.
    newL.reverse()
    return newL+l

def doubleBins(l):

    newL = []
    for i, v in enumerate(l[1:]):
        newL += [ l[i], 0.5*(l[i]+v) ]

    newL.append( l[-1] )
    return newL
    
def configure():

    # ######################################################
    # ######################################################
    # EtaJES / JMS common configuration 
    
    # Build an CalibrationObjects which will centralize every
    # collections of TH1/TH2/TGraphErrors/TF1 needed for JES/JMS derivation 
    objs = ROOT.DJS.CalibrationObjects()
    # this is a C++ object (see Utilities.h) but we will augment it with pure python attributes

    objs.AtlasLabel = "Simulation Internal"
    objs.jetType = "AntiKt4EMTopo"
    objs.jetTypePublic = ["#sqrt{s}= 14 TeV, Pythia Dijets","anti-k_{t} R = 0.4, EMTopo+JES"]
    objs.radius  = 0.4

    eBins = [0.]+doubleBins([20, 25, 30, 35, 40, 50, 60, 80, 100, 120, 150, 200, 240, 300, 400, 500, 600,
                        800, 1000, 1200, 1500, 2000, 2500, 3000, 3500, 4000, 5500])+[6000]
    logmoeBins = doubleBins([-6,-4.5, -4, -3.75, -3.5, -3.25, -3,-2.75, -2.5, -2.25, -2,-1.75,-1.5, -1.25,-1,-0.8,0])
    etaBins = symetrize( [ 0.0, 0.1, 0.2, 0.3, 0.4, 0.5, 0.6, 0.7, 0.8, 0.9,
                1.0, 1.05, 1.1, 1.15, 1.2, 1.25, 1.3, 1.35, 1.4, 1.45, 1.5, 1.55, 1.6, # finer seg near crack
                1.7, 1.8, 1.9, 2.0, 2.1, 2.2, 2.3, 2.4, 2.5, 2.6, 2.7, 2.8,
                2.9, 2.95, 3.0, 3.05, 3.1, 3.15, 3.2, 3.25, 3.3, 3.35, 3.4, 3.45, 3.5, 3.55, 3.6, # finer seg near crack
                3.7, 3.8, 3.9, 4.0, 4.1, 4.2, 4.3, 4.4, 4.5] )

    ## eBins = [20,  60, 150,  400, 1000,  3000,  5500]
    ## logmoeBins = [-6, -3.25,  -2.25, -1.5, -1.25,-1,-0.8,0]
    etaBins = [-5,0,5]

    ## etaBins = symetrize( [ 0.0,  0.9,                1.6, # finer seg near crack
    ##                        4.5] )

    
    theBPS = BinnedPhaseSpace( "JMSbins",
                               # bins from BinHadler/BinHandler.cxx
                               AxisSpec("E", "E", eBins ,isGeV=False),
                               AxisSpec("logmopt", "log(m/p_{T})",logmoeBins, isGeV=False), # m<40GeV for all log(m/E)<-4/5
                               AxisSpec("abseta", "|eta|",etaBins, isGeV=False)
                               )


    objs.theBPS = theBPS
    
    respBuilder = ROOT.DJS.SmoothRespBuilder()
    respBuilder.m_objects = objs
    respBuilder.m_wbf.m_bps = theBPS
    #respBuilder.m_wbf.setSigmas(1, 0.2, 0.03)
    #respBuilder.m_wbf.setNN(1,1,1)
    respBuilder.m_wbf.setSigmas(1, 0.2, 3.)
    respBuilder.m_wbf.setNN(1,1,0)
    
    objs.respBuilder = respBuilder
    
    # set input branch name :
    respBuilder.m_evntWeight_bname = "EW"
    respBuilder.m_e_true_bname = "te" 
    respBuilder.m_e_reco_bname = "e"
    respBuilder.m_eta_reco_bname = "eta"
    respBuilder.m_eta_true_bname = "teta"
    respBuilder.m_m_true_bname = "tm"
    respBuilder.m_m_reco_bname = "m" 

    
    objs.mR_in_EMEtatrue.init(theBPS, ROOT.BPS.AxisSpec("mresp","mass resp", 200,0,5) )
    objs.eR_in_EMEtatrue.init(theBPS, ROOT.BPS.AxisSpec("eresp","E resp", 240,0,2.4) )
    objs.dEta_in_EMEtatrue.init(theBPS, ROOT.BPS.AxisSpec("dEta","Delta eta", 200,-0.2,0.2) )
    hspec = theBPS.m_a1.hspec()+theBPS.m_a2.hspec()
    objs.occ_in_EMEtatrue = ROOT.TH2F("occ" , "occ", *hspec)

    return objs


# for debuging....
def configureValid(objs):
    def noOwnerShip(o):
        ROOT.SetOwnership(o,False) 
        o.SetDirectory(0)
        return o
    objs.v_pt = noOwnerShip(ROOT.TH1F("pt", "pT", 300, 0, 5000))
    objs.v_m = noOwnerShip(ROOT.TH1F("m", "m", 300, 0, 300))
    objs.v_eta = noOwnerShip(ROOT.TH1F("eta", "eta", 300, -5, 5))
    objs.v_logmopt = noOwnerShip(ROOT.TH1F("logompt", "Log(m/pT)", 300, -6, 0))
    objs.v_mR_vs_logmopt = noOwnerShip(ROOT.TH2F("mR_vs_logompt", "mR vs Log(m/pT)", 300, -6, 0, 200,0,5) )
    objs.v_mR_vs_logmopt_reco = noOwnerShip(ROOT.TH2F("mR_vs_logompt_reco", "mR vs Log(m/pT)", 300, -6, 0, 200,0,5) )

    objs.v_logmopt_vs_e = noOwnerShip(ROOT.TH2F("logompt_pt", "Log(m/pT) vs E", 150, 200, 5000, 150, -6, 0))
    objs.v_m_vs_pt = noOwnerShip(ROOT.TH2F("m_pt", "m vs pT", 150,200, 3000, 150, 0, 300))
